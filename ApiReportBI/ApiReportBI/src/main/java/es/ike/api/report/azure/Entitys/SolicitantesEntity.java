package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class SolicitantesEntity extends TableServiceEntity{
	
	// --------------------------------------------------------------------
		public SolicitantesEntity(String id, String businessUnit) {
			this.partitionKey = id;
			this.rowKey = businessUnit;
		}

		// --------------------------------------------------------------------
		public SolicitantesEntity() {

		}

		// --------------------------------------------------------------------

		public UUID businessUnit;
		public Integer id;
		public String applicantEmail;
		public String applicantLastname;
		public String applicantName;
		public String applicantPhoneNumber;
		public Double caseId;
		public Integer customerId;
		public String identificationNumberApplicant;
		public String identificationTypeApplicant;
		public String upadtedAT;
		public String createdAt;
		
		public UUID getBusinessUnit() {
			return businessUnit;
		}

		public void setBusinessUnit(UUID businessUnit) {
			this.businessUnit = businessUnit;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getApplicantEmail() {
			return applicantEmail;
		}

		public void setApplicantEmail(String applicantEmail) {
			this.applicantEmail = applicantEmail;
		}

		public String getApplicantLastname() {
			return applicantLastname;
		}

		public void setApplicantLastname(String applicantLastname) {
			this.applicantLastname = applicantLastname;
		}

		public String getApplicantName() {
			return applicantName;
		}

		public void setApplicantName(String applicantName) {
			this.applicantName = applicantName;
		}

		public String getApplicantPhoneNumber() {
			return applicantPhoneNumber;
		}

		public void setApplicantPhoneNumber(String applicantPhoneNumber) {
			this.applicantPhoneNumber = applicantPhoneNumber;
		}

		public Double getCaseId() {
			return caseId;
		}

		public void setCaseId(Double caseId) {
			this.caseId = caseId;
		}

		public Integer getCustomerId() {
			return customerId;
		}

		public void setCustomerId(Integer customerId) {
			this.customerId = customerId;
		}

		public String getIdentificationNumberApplicant() {
			return identificationNumberApplicant;
		}

		public void setIdentificationNumberApplicant(String identificationNumberApplicant) {
			this.identificationNumberApplicant = identificationNumberApplicant;
		}

		public String getIdentificationTypeApplicant() {
			return identificationTypeApplicant;
		}

		public void setIdentificationTypeApplicant(String identificationTypeApplicant) {
			this.identificationTypeApplicant = identificationTypeApplicant;
		}

		public String getUpadtedAT() {
			return upadtedAT;
		}

		public void setUpadtedAT(String upadtedAT) {
			this.upadtedAT = upadtedAT;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}
		
		public List<String> getColumns(){
			List<String> list = new ArrayList<String>();
			list.add("id");
			list.add("businessUnit");
			list.add("applicantEmail");
			list.add("applicantLastname");
			list.add("applicantName");
			list.add("applicantPhoneNumber");
			list.add("caseId");
			list.add("customerId");
			list.add("identificationNumberApplicant");
			list.add("identificationTypeApplicant");
			list.add("createdAt");
			list.add("upadtedAT");
			return list;
		}

}
