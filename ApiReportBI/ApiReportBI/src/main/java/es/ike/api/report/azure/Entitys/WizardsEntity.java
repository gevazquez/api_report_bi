package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class WizardsEntity extends TableServiceEntity{
	//------------------------------------------------------------------------------------------------------------
		public WizardsEntity(String id, String businessUnit) {
			this.partitionKey=id;
			this.rowKey=businessUnit;
		}
		
	//------------------------------------------------------------------------------------------------------------

		public WizardsEntity() {
			
		}
		
		public Integer id;
		public UUID businessUnit;
		public String description;
		public String createdAt;
		public String updatedAT;
		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public UUID getBusinessUnit() {
			return businessUnit;
		}

		public void setBusinessUnit(UUID businessUnit) {
			this.businessUnit = businessUnit;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}

		public String getUpdatedAT() {
			return updatedAT;
		}

		public void setUpdatedAT(String updatedAT) {
			this.updatedAT = updatedAT;
		}
		
		public List<String> getColumns(){
			List<String> list = new ArrayList<>();
			list.add("id");
			list.add("businessUnit");
			list.add("description");
			//list.add("createdAt");
			//list.add("updatedAT");
			return list;
		}
		
		
}
