package es.ike.api.report.dto;

public class GetRefreshTokenDTO {
	private String user_name;
	private String refreshToken;
	
	public GetRefreshTokenDTO() {}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	@Override
	public String toString() {
		return "GetRefreshTokenDTO [user_name=" + user_name + ", refreshToken=" + refreshToken + "]";
	}
	
	

}
