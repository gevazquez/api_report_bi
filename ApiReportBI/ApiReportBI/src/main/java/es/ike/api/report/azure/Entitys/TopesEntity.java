package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class TopesEntity extends TableServiceEntity {
	//------------------------------------------------------------------------------------------------------------
	public TopesEntity(String id, String businessUuid) {
		this.partitionKey = id;
		this.rowKey = businessUuid;  
	}
//------------------------------------------------------------------------------------------------------------
	public TopesEntity() {		}
//------------------------------------------------------------------------------------------------------------

	public  UUID businessUnit;
    public Integer id;
    public String  behavior;
    public Integer coverageId;
    public String currency;
    public String periodicity;
    public Boolean restrictive;
    public String renovation;
    public String scope;
    public String unitCode;
    public String createdAt;
    public String updatedAt;
    
    
	
    
    
    
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBehavior() {
		return behavior;
	}
	public void setBehavior(String behavior) {
		this.behavior = behavior;
	}
	public Integer getCoverageId() {
		return coverageId;
	}
	public void setCoverageId(Integer coverageId) {
		this.coverageId = coverageId;
	}
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getPeriodicity() {
		return periodicity;
	}
	public void setPeriodicity(String periodicity) {
		this.periodicity = periodicity;
	}
	
	public Boolean getRestrictive() {
		return restrictive;
	}
	public void setRestrictive(Boolean restrictive) {
		this.restrictive = restrictive;
	}
	public String getRenovation() {
		return renovation;
	}
	public void setRenovation(String renovation) {
		this.renovation = renovation;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	
	public String getUnitCode() {
		return unitCode;
	}
	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("businessUnit");
		list.add("behavior");
		list.add("coverageId");
		list.add("currency");
		list.add("periodicity");
		list.add("restrictive");
		list.add("renovation");
		list.add("scope");
		list.add("unitCode");
		list.add("createdAt");
		list.add("updatedAt");
		return list;
	}
	
}
