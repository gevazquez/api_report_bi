package es.ike.api.report.tuten;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import es.ike.api.report.utilerias.JsonUtility;

public class ObtenerOauthTL {

	public static String OAuthAPI_Report(String country) throws Exception {
		String StrSalida = "";
		int intHttpCode = 0;
		URL url = null;
		HttpsURLConnection conn = null;
		JSONParser parser = null;
		JSONObject json = null;
		StringBuilder response = null;
		// Lectura del archivo de configuración con los datos de resources
		Thread currentThread = Thread.currentThread();
		ClassLoader contextClassLoader = currentThread.getContextClassLoader();
		URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+country);
		Properties prop = new Properties();
		if (resource == null) {
			throw new Exception("ObtenerOauthTL.OAuthAPI_Report= No se pudo leer el archivo de configuración=TutenLabsconfig/TutenLabsConfig.properties"+country+"!"); 		}
		try (InputStream is = resource.openStream()) {
			prop.load(is);
			url = new URL(prop.getProperty("urlOauthTuten"));
			conn = (HttpsURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);
			conn.setRequestMethod("POST");
			conn.setUseCaches(false);
			conn.setRequestProperty(prop.getProperty("StrKey"), prop.getProperty("Key"));
			conn.setRequestProperty(prop.getProperty("StrSecret"), prop.getProperty("Secret"));
			conn.setRequestProperty(prop.getProperty("StrSystemId"), prop.getProperty("SystemId"));
			intHttpCode = conn.getResponseCode();
			if (intHttpCode == java.net.HttpURLConnection.HTTP_OK) {
				// ---------------
				try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"))) {
					String Stroutput;
					response = new StringBuilder();
					while ((Stroutput = in.readLine()) != null) {
						response.append(Stroutput);
					}
					parser = new JSONParser();
					json = (JSONObject) parser.parse(response.toString());
					StrSalida = (JsonUtility.validateJsonData(json, "token")) ? json.get("token").toString() : "";
				} catch (Exception e) {
					System.out.println("ObtenerOauthTL.OAuthAPI_Report= Error al leer Respuesta del Token "+country);
					e.printStackTrace();
					// --------------
				}
			} else {
				String Stroutput="";
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
				response = new StringBuilder();
				while ((Stroutput = in.readLine()) != null) {
					response.append(Stroutput);
				}
				System.out.println("ObtenerOauthTL.OAuthAPI_Report= Error al obtener token "+country+"/"+Stroutput);
			}

			return StrSalida;
		} catch (Exception e) {
			System.out.println("ObtenerOauthTL.OAuthAPI_Report= Error en Conexion a URL "+country);
			e.printStackTrace();
			throw new Exception(e);
		}
	}
}
