package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ChecklistsEntity extends TableServiceEntity {

	// --------------------------------------------------------------------
	public ChecklistsEntity(String id, String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;
	}

	// --------------------------------------------------------------------
	public ChecklistsEntity() {

	}

	// --------------------------------------------------------------------

	public UUID businessUnit;
	public Integer id;
	public String name;
	public String upadtedAT;
	public String createdAt;



	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUpadtedAT() {
		return upadtedAT;
	}

	public void setUpadtedAT(String upadtedAT) {
		this.upadtedAT = upadtedAT;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public List<String> getColumns(){
		List<String> list = new ArrayList<String>();
		list.add("id");
		list.add("businessUnit");
		list.add("name");
		list.add("createdAt");
		list.add("upadtedAT");
		return list;
	}
}
