package es.ike.api.report.utilerias;

public class Paginacion {
	//---------------------------------------------------------------------------	
	public static DatosPaginacion calculaPaginacion(Integer count,Integer numDiv) {
		DatosPaginacion datosPaginacion = null;
		try {
			Integer  enteros=0;
			Integer  resto=0;
			if(count>0 && numDiv>0) {
				enteros = count / numDiv;
				resto = count % numDiv;
				}
			datosPaginacion=new DatosPaginacion(enteros, resto);
		}catch(Exception e) {
			System.out.println("Paginacion.calculaPaginacion = Error en operacion en Calculo de datos de Paginacion,count="+count.toString()+"/numDiv="+numDiv.toString());
		}
		return datosPaginacion;
	}
	//---------------------------------------------------------------------------	
}
