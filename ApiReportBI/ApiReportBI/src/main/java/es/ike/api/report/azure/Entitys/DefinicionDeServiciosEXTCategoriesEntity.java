package es.ike.api.report.azure.Entitys;

import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class DefinicionDeServiciosEXTCategoriesEntity extends TableServiceEntity {
	public DefinicionDeServiciosEXTCategoriesEntity(String id, String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;
	}

	// --------------------------------------------------------------------
	public DefinicionDeServiciosEXTCategoriesEntity() {

	}

	// ------------------------------------------------------------------------------------------------------------
	public Integer categories;
	public Integer idExt;
	public UUID businessUnitExt;

	public Integer getCategories() {
		return categories;
	}

	public void setCategories(Integer categories) {
		this.categories = categories;
	}

	public Integer getIdExt() {
		return idExt;
	}

	public void setIdExt(Integer idExt) {
		this.idExt = idExt;
	}

	public UUID getBusinessUnitExt() {
		return businessUnitExt;
	}

	public void setBusinessUnitExt(UUID businessUnitExt) {
		this.businessUnitExt = businessUnitExt;
	}

	
	
	

}
