package es.ike.api.report.azure.Entitys;

import java.util.Date;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ApiConfiguration extends TableServiceEntity  {
	//-------------------------------------------------------
	public ApiConfiguration(String PartitionKey, String RowKey) {
		this.partitionKey = PartitionKey;
		this.rowKey = RowKey;  
	}
	//------------------------------------------------------
	public ApiConfiguration() {		}
	//------------------------------------------------------
	public String entidad;
	public int aplicaFiltroFecha;
	public Date ultimaFechaActualizacion;
	public int aplicaActualizacionCompleta;
	public int conteoTotal;
	
	public String getEntidad() {		return entidad;	}
	public void setEntidad(String entidad) {		this.entidad = entidad;	}
	
	public int getAplicaFiltroFecha() {		return aplicaFiltroFecha;	}
	public void setAplicaFiltroFecha(int aplicaFiltroFecha) {		this.aplicaFiltroFecha = aplicaFiltroFecha;	}
	
	public Date getUltimaFechaActualizacion() {return ultimaFechaActualizacion;	}
	public void setUltimaFechaActualizacion(Date ultimaFechaActualizacion) {		this.ultimaFechaActualizacion = ultimaFechaActualizacion;	}

	public int getAplicaActualizacionCompleta() {		return aplicaActualizacionCompleta;	}
	public void setAplicaActualizacionCompleta(int aplicaActualizacionCompleta) { this.aplicaActualizacionCompleta = aplicaActualizacionCompleta;	}
	
	public int getConteoTotal() {		return conteoTotal;	}
	public void setConteoTotal(int conteoTotal) {		this.conteoTotal = conteoTotal;	}
	
	
	//------------------------------------------------------
}