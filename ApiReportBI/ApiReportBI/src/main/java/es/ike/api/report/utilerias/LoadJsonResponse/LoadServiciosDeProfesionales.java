package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.ServiciosDeProfesionalesEntity;
import es.ike.api.report.azure.Entitys.SiniestralidadEntity;
import es.ike.api.report.utilerias.JsonUtility;

public class LoadServiciosDeProfesionales {
	public static void ServiciosDeProfesionales(String jsonResponse,String country ) throws Exception {
		//Se carga jsonResponse en Arreglo Json
		Integer iError=0;
		Instant instant=Instant.now();
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("42","42","professional_services",country);
		Logger logger = Logger.getLogger("Logger apiReport LoadServiciosDeProfesionales.ServiciosDeProfesionales");
		//Crea tabla Countries si no existe en Azure
		CloudTableClient tableClient1 = null;
		CreateTableConfiguration tableConfiguration = null;
		tableClient1 = TableClientProvider.getTableClientReference(country);
		tableConfiguration.createTable(tableClient1, "ProfessionalServices",country);
		 //Se carga jsonResponse en Arreglo Json
        try {
	    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
            	JSONArray jsonarray= new JSONArray(jsonResponse);  
            	CloudTableClient tableClient = null;
                tableClient = TableClientProvider.getTableClientReference(country);
              //Se genera la variable tabla, con el nombre de la tabla en AZURE
                CloudTable table = tableClient.getTableReference("ProfessionalServices");
                for (int a = 0; a<jsonarray.length(); a++) {
            		JSONParser parser=null;
            		JSONObject json=null;
            		String StrId="";
        		    String StrBusinessUnit="";
        		    String StrServiceName;
        			String StrProfessionalId;
        			String StrServiceId;
        			String StrBaseServiceCost;
        			String StrBaseProCost;
        			String StrExpressProCost;
        			String StrWizardPercentage;
        			String StrUpdatedAt;
        			String StrCreatedAt;
            		
            		
            		parser = new JSONParser();
            		json = (JSONObject)parser.parse(jsonarray.get(a).toString());
            		StrId=JsonUtility.JsonValidaExisteLlave(json, "id");
                    StrBusinessUnit=JsonUtility.JsonValidaExisteLlave(json,"businessUnit");
                    StrServiceName=JsonUtility.JsonValidaExisteLlave(json,"serviceName");
                    StrProfessionalId=JsonUtility.JsonValidaExisteLlave(json,"professionalId");
                    StrServiceId=JsonUtility.JsonValidaExisteLlave(json,"serviceId");
                    StrBaseServiceCost=JsonUtility.JsonValidaExisteLlave(json,"baseServiceCost");
                    StrBaseProCost=JsonUtility.JsonValidaExisteLlave(json,"baseProCost");
                    StrExpressProCost=JsonUtility.JsonValidaExisteLlave(json,"expressProCost");
                    StrWizardPercentage=JsonUtility.JsonValidaExisteLlave(json,"wizardPercentage");
                    StrUpdatedAt=JsonUtility.JsonValidaExisteLlave(json,"updatedAt");
                    StrCreatedAt=JsonUtility.JsonValidaExisteLlave(json,"createdAt");
            		
                    ServiciosDeProfesionalesEntity serviciosDeProfesionalesEntity = new ServiciosDeProfesionalesEntity(StrId,StrBusinessUnit);
                    serviciosDeProfesionalesEntity.setEtag(serviciosDeProfesionalesEntity.getEtag());
                    serviciosDeProfesionalesEntity.setId(StrId != null ? Integer.parseInt(StrId):null);
                    serviciosDeProfesionalesEntity.setBusinessUnit(StrBusinessUnit != null ? UUID.fromString(StrBusinessUnit):null);
                    serviciosDeProfesionalesEntity.setServiceName(StrServiceName != null ? StrServiceName: "nul");
                    serviciosDeProfesionalesEntity.setProfessionalId(StrProfessionalId != null ? Integer.parseInt(StrProfessionalId):0);
                    serviciosDeProfesionalesEntity.setServiceId(StrServiceId != null ? Integer.parseInt(StrServiceId):0);
                    serviciosDeProfesionalesEntity.setBaseServiceCost(StrBaseServiceCost != null ? StrBaseServiceCost: "null");
                    serviciosDeProfesionalesEntity.setBaseProCost(StrBaseProCost != null ? StrBaseProCost: "null");
                    serviciosDeProfesionalesEntity.setExpressProCost(StrExpressProCost != null ? StrExpressProCost: "null");
                    serviciosDeProfesionalesEntity.setWizardPercentage(StrWizardPercentage != null ? StrWizardPercentage: "null");
                    serviciosDeProfesionalesEntity.setUpdatedAt(StrUpdatedAt != null ? StrUpdatedAt: "null");
                    serviciosDeProfesionalesEntity.setCreatedAt(StrCreatedAt != null ? StrCreatedAt: "null");
                    table.execute(TableOperation.insertOrReplace(serviciosDeProfesionalesEntity));
                }
            }else {
            	iError=1;
            	logger.log(Level.WARNING, "LoadServiciosDeProfesionales.ServiciosDeProfesionales= Respuesta No identificada "+country+"="+jsonResponse);
            }
	    }catch(Exception e) {
	    	iError=1;
	    	logger.log(Level.WARNING, "LoadServiciosDeProfesionales.ServiciosDeProfesionales= Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse);
	    	e.printStackTrace();
	    }
	  //Fin de proceso de Entidad
		try { 	
			if(iError==0) {
				logger.log(Level.INFO, instant.toString()+" Se cargan registros entidad ServiciosDeProfesionales con filtro ProcessFilterBu "+country+"!!!");
				
				}
			}
		catch(Exception e) {
			iError=1;
			
			logger.log(Level.WARNING, "LoadServiciosDeProfesionales.ServiciosDeProfesionales = Error al actualizar Fecha"+country);
			e.printStackTrace();
		}
		if(iError==0) {
			logger.log(Level.INFO, instant.toString()+" Proceso Carga de Entidad con filtro ServiciosDeProfesionales Finalizada "+country+"!!!!");
			}
	
	}

}
