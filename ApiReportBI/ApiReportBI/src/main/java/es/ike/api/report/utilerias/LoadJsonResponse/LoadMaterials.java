package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.MateriasEntity;
import es.ike.api.report.utilerias.JsonUtility;

public class LoadMaterials {
	public static void Materials(String jsonResponse, String country)  throws Exception{
		Integer iError = 0;
		Instant instant = Instant.now();
		// Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration = null;
		apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("4","4","materials",country);
		Logger logger = Logger.getLogger("Logger apiReport LoadMaterials.Matrials");
		// Se carga jsonResponse en Arreglo Json
		try {
			if (String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")) {
				JSONArray jsonarray = new JSONArray(jsonResponse);
				CloudTableClient tableClient = null;
				tableClient = TableClientProvider.getTableClientReference(country);
				// Se genera la variable tabla, con el nombre de la tabla en AZURE
				 CloudTable table = tableClient.getTableReference("Materials");
				 for (int a = 0; a<jsonarray.length(); a++) {
	            		JSONParser parser=null;
	            		JSONObject json=null;
	            		String StrId="";
	            		String StrBusinessUnit="";
	            		String StrMaterialType="";
	            		String StrChasisNumber="";
	            		String StrModel="";
	            		String StrEngineNumber="";
	            		String StrPatentPlate="";
	            		String StrYear="";
	            		String StrAddress="";
	            		String StrCreatedAt="";
	            		String StrUpdatedAt="";
	            		String StrBrand ="";
	            		String StrKindOfPet="";
	            		String StrPetBreed="";
	            		String StrPetName="";
	            		parser = new JSONParser();
	                    json = (JSONObject)parser.parse(jsonarray.get(a).toString());
	                    StrId=JsonUtility.JsonValidaExisteLlave(json, "id");
	                    StrBusinessUnit=JsonUtility.JsonValidaExisteLlave(json,"businessUnit");
	                    StrMaterialType=JsonUtility.JsonValidaExisteLlave(json,"materialType");
	                    StrChasisNumber=JsonUtility.JsonValidaExisteLlave(json,"chasisNumber");
	                    StrModel=JsonUtility.JsonValidaExisteLlave(json,"model");
	                    StrEngineNumber=JsonUtility.JsonValidaExisteLlave(json,"engineNumber");
	                    StrPatentPlate=JsonUtility.JsonValidaExisteLlave(json,"patentPlate");
	                    StrYear=JsonUtility.JsonValidaExisteLlave(json,"year");
	                    StrAddress=JsonUtility.JsonValidaExisteLlave(json,"address"); 
	                    StrCreatedAt=JsonUtility.JsonValidaExisteLlave(json,"createdAt");
	                    StrUpdatedAt=JsonUtility.JsonValidaExisteLlave(json,"updatedAt");
	                    StrBrand=JsonUtility.JsonValidaExisteLlave(json,"brand");
	                    StrKindOfPet=JsonUtility.JsonValidaExisteLlave(json,"kindOfPet");
	                    StrPetBreed=JsonUtility.JsonValidaExisteLlave(json,"petBreed");
	                    StrPetName=JsonUtility.JsonValidaExisteLlave(json,"petName");
	                    
	                    MateriasEntity materiasEntity = new MateriasEntity(StrId,StrBusinessUnit);
	                    materiasEntity.setEtag(materiasEntity.getEtag());
	                    materiasEntity.setId(StrId != null ? Integer.parseInt(StrId): null);
	                    materiasEntity.setBusinessUnit(StrBusinessUnit != null ? UUID.fromString(StrBusinessUnit): null);
	                    materiasEntity.setMaterialType(StrMaterialType != null ? StrMaterialType: "null");
	                    materiasEntity.setChasisNumber(StrChasisNumber);
	                    materiasEntity.setModel(StrModel);
	                    materiasEntity.setEngineNumber(StrEngineNumber);
	                    materiasEntity.setPatentPlate(StrPatentPlate);
	                    materiasEntity.setYear(StrYear);
	                    materiasEntity.setAddress(StrAddress);					                    
	                    materiasEntity.setCreatedAt(StrCreatedAt);
	                    materiasEntity.setUpdatedAt(StrUpdatedAt);
	                    materiasEntity.setBrand(StrBrand != null ? StrBrand: "null");
	                    materiasEntity.setKindOfPet(StrKindOfPet != null ? StrKindOfPet: "null");
	                    materiasEntity.setPetBreed(StrPetBreed != null ? StrPetBreed: "null");
	                    materiasEntity.setPetName(StrPetName != null ? StrPetName: "null");
	                    table.execute(TableOperation.insertOrReplace(materiasEntity));  
				 }
			} else {
				iError = 1;
				logger.log(Level.WARNING, "LoadMaterials.Materials= Respuesta No identificada " + country + "=" + jsonResponse);
			}
		} catch (Exception e) {
			iError = 1;
			logger.log(Level.WARNING, "LoadMaterials.Materials= Error al procesar arreglo e insertar en AZURE " + country + "=" + jsonResponse);
			e.printStackTrace();
		}
		// Fin de proceso de Entidad
		try {
			if (iError == 0) {
				// ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
				logger.log(Level.INFO, instant.toString() + " Se cargan registros entidad Materials con filtro ProcessFilterBu "+ country + "!!!");
			}
		} catch (Exception e) {
			iError = 1;
			logger.log(Level.WARNING, "LoadMaterials.Materials = Error al actualizar Fecha" + country);
			e.printStackTrace();
		}
		if (iError == 0) {
			logger.log(Level.INFO, instant.toString() + " Proceso Carga de Entidad con filtro Materials Finalizada " + country + "!!!!");
		}

	}

}
