package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ServiciosAsociadosAlReclamoEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public ServiciosAsociadosAlReclamoEntity (String id,String businessUnit) {
		this.partitionKey=id;
		this.rowKey=businessUnit;
		
	}
	// --------------------------------------------------------------------
	public ServiciosAsociadosAlReclamoEntity() {
	
	}
	// ------------------------------------------------------------------------------------------------------------
			public Integer claimId;
			public UUID businessUnit;
			public Integer serviceId;
			public String updatedAt;
			
			
			
			public Integer getClaimId() {
				return claimId;
			}
			public void setClaimId(Integer claimId) {
				this.claimId = claimId;
			}
			public UUID getBusinessUnit() {
				return businessUnit;
			}
			public void setBusinessUnit(UUID businessUnit) {
				this.businessUnit = businessUnit;
			}
			public Integer getServiceId() {
				return serviceId;
			}
			public void setServiceId(Integer serviceId) {
				this.serviceId = serviceId;
			}
			public String getUpdatedAt() {
				return updatedAt;
			}
			public void setUpdatedAt(String updatedAt) {
				this.updatedAt = updatedAt;
			}
			public List<String>  getColumns(){
				List<String> list = new ArrayList<String>();
				list.add("claimId");
				list.add("businessUnit");
				list.add("serviceId");
				list.add("updatedAt");;
				return list;
			}
				
			

}
