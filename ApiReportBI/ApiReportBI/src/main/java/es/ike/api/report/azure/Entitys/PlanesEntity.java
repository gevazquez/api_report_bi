package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class PlanesEntity extends TableServiceEntity {
	//------------------------------------------------------------------------------------------------------------
		public PlanesEntity(String id, String businessUnit) {
			this.partitionKey = id;
			this.rowKey = businessUnit;  
		}
	//------------------------------------------------------------------------------------------------------------
		public PlanesEntity() {		}
	//------------------------------------------------------------------------------------------------------------

		public Integer id;
		public UUID businessUnit;
		public String planName;
		public String planExternalId;
		public Boolean planActivate;
		public Boolean validity;
		public Boolean definiteMatter;
		public Boolean genericPlan;
		public String contract;
		public String planStartDate;
		public String planEndDate;
		public Boolean lastVersionPlan;
		public String createdAt;
		public String updatedAt;
		public String contractorIds;
		public String contractors;
		public String planExternalVersionId;
		
		
	
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public UUID getBusinessUnit() {
			return businessUnit;
		}
		public void setBusinessUnit(UUID businessUnit) {
			this.businessUnit = businessUnit;
		}
		public String getPlanName() {
			return planName;
		}
		public void setPlanName(String planName) {
			this.planName = planName;
		}
		public String getPlanExternalId() {
			return planExternalId;
		}
		public void setPlanExternalId(String planExternalId) {
			this.planExternalId = planExternalId;
		}
		public Boolean getPlanActivate() {
			return planActivate;
		}
		public void setPlanActivate(Boolean planActivate) {
			this.planActivate = planActivate;
		}
		public Boolean getValidity() {
			return validity;
		}
		public void setValidity(Boolean validity) {
			this.validity = validity;
		}
		public Boolean getDefiniteMatter() {
			return definiteMatter;
		}
		public void setDefiniteMatter(Boolean definiteMatter) {
			this.definiteMatter = definiteMatter;
		}
		public Boolean getGenericPlan() {
			return genericPlan;
		}
		public void setGenericPlan(Boolean genericPlan) {
			this.genericPlan = genericPlan;
		}
		public String getContract() {
			return contract;
		}
		public void setContract(String contract) {
			this.contract = contract;
		}
		public String getPlanStartDate() {
			return planStartDate;
		}
		public void setPlanStartDate(String planStartDate) {
			this.planStartDate = planStartDate;
		}
		public String getPlanEndDate() {
			return planEndDate;
		}
		public void setPlanEndDate(String planEndDate) {
			this.planEndDate = planEndDate;
		}
		public Boolean getLastVersionPlan() {
			return lastVersionPlan;
		}
		public void setLastVersionPlan(Boolean lastVersionPlan) {
			this.lastVersionPlan = lastVersionPlan;
		}
		public String getCreatedAt() {
			return createdAt;
		}
		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}
		public String getUpdatedAt() {
			return updatedAt;
		}
		public void setUpdatedAt(String updatedAt) {
			this.updatedAt = updatedAt;
		}
		
		public String getContractorIds() {
			return contractorIds;
		}
		public void setContractorIds(String contractorIds) {
			this.contractorIds = contractorIds;
		}
		public String getContractors() {
			return contractors;
		}
		public void setContractors(String contractors) {
			this.contractors = contractors;
		}
		public String getPlanExternalVersionId() {
			return planExternalVersionId;
		}
		public void setPlanExternalVersionId(String planExternalVersionId) {
			this.planExternalVersionId = planExternalVersionId;
		}
		public List<String> getColumns(){
			List<String> list = new ArrayList<>();
			list.add("id");
			list.add("businessUnit");
			list.add("planName");
			list.add("planExternalId");
			list.add("planActivate");
			list.add("validity");
			list.add("definiteMatter");
			list.add("genericPlan");
			list.add("contract");
			list.add("planStartDate");
			list.add("planEndDate");
			list.add("lastVersionPlan");
			list.add("createdAt");
			list.add("updatedAt");
			list.add("contractorIds");
			list.add("contractors");
			list.add("planExternalVersionId");
			return list;
		}
		
		
}
