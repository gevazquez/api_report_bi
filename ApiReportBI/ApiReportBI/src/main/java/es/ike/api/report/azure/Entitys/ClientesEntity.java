package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ClientesEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public ClientesEntity(String id, String businessUuid) {
		this.partitionKey = id;
		this.rowKey = businessUuid;
	}

	// ---------------------------------------------------------------------
	public ClientesEntity() {
	}

	// ------------------------------------------------------------------------------------------------------------
	public Integer id;
	public UUID businessUnit;
	public String identificationType;
	public String identificationNumber;
	public String name;
	public String lastName;
	public String email;
	public String mobilePhone;
	public String address;
	public String createdAt;
	public String updatedAt;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<String> getColumns() {
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("businessUnit");
		list.add("identificationType");
		list.add("identificationNumber");
		list.add("name");
		list.add("lastName");
		list.add("email");
		list.add("mobilePhone");
		list.add("address");
		list.add("createdAt");
		list.add("updatedAt");
		return list;
	}

}
