package es.ike.api.report.utilerias;

import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public class PaginationConfig {
	public static Integer GetNumber(String country) throws Exception {
		Integer numPaginacion=0;
		//llamada al archivo properties
		Thread currentThread = Thread.currentThread();
		ClassLoader contextClassLoader = currentThread.getContextClassLoader();
		URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+country);
		Properties prop = new Properties();
		if (resource == null) {	
			throw new Exception("PaginationConfig.GetNumber= No se pudo leer el archivo de configuración TutenLabsconfig/TutenLabsConfig.properties"+country);		}
		try (InputStream is = resource.openStream()) {
			prop.load(is);
		//Se obtiene el numero de paginacion del archivo properties
			numPaginacion=Integer.parseInt(prop.getProperty("intPaginacion"));
		}catch(Exception e) {
			System.out.println("PaginationConfig.GetNumber= Error al obtener intPaginacion del archivo TutenLabsconfig/TutenLabsConfig.properties"+country);
		}
		return numPaginacion;
	}
}
