package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ServicesOrderEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public ServicesOrderEntity(String id, String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;
	}

	// --------------------------------------------------------------------
	public ServicesOrderEntity() {

	}

	// ------------------------------------------------------------------------------------------------------------
	public Integer id;
	public UUID businessUnit;
    public Double discountAmount;
    public Double paymentAmount;
    public Double subtotal;
    public String updatedAt;
    public String createdAt;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}



	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
    
	public List<String> getColumns() {
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("businessUnit");
		list.add("discountAmount");
		list.add("subtotal");
		list.add("paymentAmount");
		list.add("createdAt");
		list.add("updatedAt");
	
		return list;
	}
}
