package es.ike.api.report.services;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;

public interface CognitoUserService {
	
	public String calculateSecretHash(String awsCongitoClientId, String awsCognitoClientSecret, String userName);
    public boolean validateToken(String toke);
    public AWSCognitoIdentityProvider createIdentityProvider();

}
