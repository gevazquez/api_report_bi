package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class PresupuestosDeServiciosEntity extends TableServiceEntity{

	public PresupuestosDeServiciosEntity (String id,String businessUuid) {
		this.partitionKey=id;
		this.rowKey=businessUuid;
	}
	public PresupuestosDeServiciosEntity() {
		
	}
	
	public Integer id;
	public String createdAt;
	public UUID businessUnit;
	public Integer bookingId;
	public Integer totalCost;
	public Integer totalPrice;
	public String updatedAt;
	public Integer serviceDuration;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public Integer getBookingId() {
		return bookingId;
	}
	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
	public Integer getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Integer totalCost) {
		this.totalCost = totalCost;
	}
	public Integer getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Integer totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Integer getServiceDuration() {
		return serviceDuration;
	}
	public void setServiceDuration(Integer serviceDuration) {
		this.serviceDuration = serviceDuration;
	}
	
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("createdAt");
		list.add("businessUnit");
		list.add("bookingId");
		list.add("totalCost");
		list.add("totalPrice");
		list.add("serviceDuration");
		list.add("updatedAt");
		return list;
	}
}
