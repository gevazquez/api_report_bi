package es.ike.api.report.utilerias;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTableClient;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;

public class ProcessFilterId {
	public static void FilterById(String country, String where, String nameEntity)
			throws Exception {
		// metodo que genera filtro tutten para extraer registros de la fecha indicada
		// en adelante
		String cadenaList = "";
		Integer countTotal = 0;
		
		String token = ObtenerOauthTL.OAuthAPI_Report(country);
		String jsonResponse = "";
	    Logger logger = Logger.getLogger("Logger apiReport ProcessFilterId.FilterById");
		
		if (!token.equalsIgnoreCase("")) {
			try {
				// Inicia proceso de Carga de registros con actualización

				
				
					// lista que trae todos los campos de una entidad
					List<String> lista = Utils.getColumns(nameEntity);
					for (int i = 0; i < lista.size(); i++) {
						cadenaList = cadenaList + " " + lista.get(i);
					}
					// -----------------------------------------------
					//validacion de conteo por id
					Count count = new Count();
					countTotal=count.FilterCountById(country, where, nameEntity);

					if(countTotal == 1 ) {
						String strJSON ="{"
								+"\n"+"\"query\":\"{"+
								nameEntity+"( "+where+" )" +"{ "+
								cadenaList+
										" }"+
				                	"}\""+
								 "}";
						
						jsonResponse = obtenerJsonTuten.getJson(token, strJSON, country);
						if (!jsonResponse.equalsIgnoreCase("")) {
							// Parsear para obtener Arreglo json de Entidad
							try {
								JSONParser parser = new JSONParser();
								JSONObject json = (JSONObject) parser.parse(jsonResponse);
								jsonResponse = (JsonUtility.validateJsonData(json, "data"))
										? json.get("data").toString()
										: "";
								parser = null;
								json = null;
								parser = new JSONParser();
								json = (JSONObject) parser.parse(jsonResponse);
								jsonResponse = (JsonUtility.validateJsonData(json, nameEntity))
										? json.get(nameEntity).toString()
										: "";
							} catch (Exception e) {

								logger.log(Level.SEVERE, "ProcessFilterId.FilterById = Error al parsear JsonResponse de Entidad "+nameEntity);
								e.printStackTrace();
							}
							UtilsLoadEntity utilsLoadEntity = new UtilsLoadEntity();
							utilsLoadEntity.LoadEntity(nameEntity, jsonResponse, country);
						}
			}else {
				logger.log(Level.SEVERE, "ProcessFilterId.FilterById = Error se tiene mas de un registro con el mismo id en proceso filtro FilterById entidad "+nameEntity);
			}

				

			} catch (Exception e) {
				logger.log(Level.SEVERE, "ProcessFilterId.FilterById = Error al cargar informacion con proceso filtro FilterById entidad "+nameEntity);
				e.printStackTrace();

			}

		} else {
			logger.log(Level.INFO, "ProcessFilterId.FilterById = Problema en obtener el Token " + country);
		}

	}

}

