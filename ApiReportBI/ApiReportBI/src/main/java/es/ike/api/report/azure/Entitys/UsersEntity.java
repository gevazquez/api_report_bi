package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class UsersEntity extends TableServiceEntity {
	//------------------------------------------------------------------------------------------------------------
	public UsersEntity(String id, String businessUuid) {
		this.partitionKey =id;
		this.rowKey=businessUuid;
	}
	//------------------------------------------------------------------------------------------------------------
	public UsersEntity() {		}
	//------------------------------------------------------------------------------------------------------------
	public Integer id;
	public UUID businessUnit;
	public String username;
	public String firstName;
	public String lastName;
	public String phone;
	public String createdAt;
	public String updatedAt;
	public String email;
	public String secondLastName;
	public String businessUnits;
	public String identificationNumber;
	public Integer identificationTypeId;
	public String identificationTypeName;
	public String address;
	public Boolean active;
	public Integer roleId;
	public String  roleName;
	
	
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSecondLastName() {
		return secondLastName;
	}
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}
	public String getBusinessUnits() {
		return businessUnits;
	}
	public void setBusinessUnits(String businessUnits) {
		this.businessUnits = businessUnits;
	}
	public String getIdentificationNumber() {
		return identificationNumber;
	}
	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}
	public Integer getIdentificationTypeId() {
		return identificationTypeId;
	}
	public void setIdentificationTypeId(Integer identificationTypeId) {
		this.identificationTypeId = identificationTypeId;
	}
	public String getIdentificationTypeName() {
		return identificationTypeName;
	}
	public void setIdentificationTypeName(String identificationTypeName) {
		this.identificationTypeName = identificationTypeName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("businessUnit");
		list.add("username");
		list.add("firstName");
		list.add("lastName");
		list.add("phone");
		list.add("createdAt");
		list.add("updatedAt");
		list.add("email");
		list.add("secondLastName");
		list.add("businessUnits");
		list.add("identificationNumber");
		list.add("identificationTypeId");
		list.add("identificationTypeName");
		list.add("address");
		list.add("active");
		list.add("roleId");
		list.add("roleName");
		return list;
	}
	
}
