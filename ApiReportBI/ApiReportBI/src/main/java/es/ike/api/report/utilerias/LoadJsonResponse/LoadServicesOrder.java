package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.ServicesOrderEntity;
import es.ike.api.report.utilerias.ActualizaFecha;
import es.ike.api.report.utilerias.JsonUtility;

public class LoadServicesOrder {
	public static void ServicesOrder(String jsonResponse,String country ) throws Exception {
		//Se carga jsonResponse en Arreglo Json
		Integer iError=0;
		Instant instant=Instant.now();
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("45","45","services_order",country);
		Logger logger = Logger.getLogger("Logger apiReport LoadServicesOrder.ServicesOrder");
		//Crea tabla Countries si no existe en Azure
				CloudTableClient tableClient1 = null;
				CreateTableConfiguration tableConfiguration = null;
				tableClient1 = TableClientProvider.getTableClientReference(country);
				tableConfiguration.createTable(tableClient1, "ServicesOrder",country);
	    try {
	    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
            	JSONArray jsonarray= new JSONArray(jsonResponse);  
            	CloudTableClient tableClient = null;
                tableClient = TableClientProvider.getTableClientReference(country);
                //Se genera la variable tabla, con el nombre de la tabla en AZURE
                CloudTable table = tableClient.getTableReference("ServicesOrder");
            	for (int a = 0; a<jsonarray.length(); a++) {
            		JSONParser parser=null;
            		JSONObject jsonServicesOrder=null;
            		String strId="";
            		String strBusinessUnit="";
            		String strDiscountAmount="";
            		String strPaymentAmount="";
            		String strSubtotal="";
            		String strCreatedAt="";
            		String strUpdatedAt="";
            	
            		
            		parser = new JSONParser();
            		jsonServicesOrder = (JSONObject)parser.parse(jsonarray.get(a).toString());
            		strId = JsonUtility.JsonValidaExisteLlave(jsonServicesOrder,"id" );
            		strBusinessUnit = JsonUtility.JsonValidaExisteLlave(jsonServicesOrder,"businessUnit" );
            		strDiscountAmount = JsonUtility.JsonValidaExisteLlave(jsonServicesOrder,"discountAmount" );
            		strPaymentAmount = JsonUtility.JsonValidaExisteLlave(jsonServicesOrder,"paymentAmount" );
            		strSubtotal = JsonUtility.JsonValidaExisteLlave(jsonServicesOrder,"subtotal" );
            	    strCreatedAt = JsonUtility.JsonValidaExisteLlave(jsonServicesOrder,"createdAt" );
            	    strUpdatedAt = JsonUtility.JsonValidaExisteLlave(jsonServicesOrder,"updatedAt" );
            		
            		//---------------------- Asignacion de valores 
            	    ServicesOrderEntity servicesOrderEntity = new ServicesOrderEntity(strId, strBusinessUnit); //PartitionKey & RowKey
            		servicesOrderEntity.setEtag(servicesOrderEntity.getEtag());
            		servicesOrderEntity.setId(strId != null ? Integer.parseInt(strId) : null);
            		servicesOrderEntity.setBusinessUnit(strBusinessUnit != null ? UUID.fromString(strBusinessUnit): null);
            	    servicesOrderEntity.setDiscountAmount(strDiscountAmount != null ? Double.parseDouble(strDiscountAmount):0);
            	    servicesOrderEntity.setPaymentAmount(strPaymentAmount != null ? Double.parseDouble(strPaymentAmount):0);
            	    servicesOrderEntity.setSubtotal(strSubtotal != null ? Double.parseDouble(strSubtotal):0);
            		servicesOrderEntity.setCreatedAt(strCreatedAt != null ? strCreatedAt: "null");
            		servicesOrderEntity.setUpdatedAt(strUpdatedAt != null ? strUpdatedAt: "null");
                    table.execute(TableOperation.insertOrReplace(servicesOrderEntity));

            		}
	    	} else {
				iError = 1;
				logger.log(Level.WARNING, "LoadServicesOrders.ServicesOrders= Respuesta No identificada " + country + "=" + jsonResponse);
			}
		} catch (Exception e) {
			iError = 1;
			logger.log(Level.WARNING, "LoadServicesOrders.ServicesOrders= Error al procesar arreglo e insertar en AZURE " + country + "=" + jsonResponse);
			e.printStackTrace();
		}
		// Fin de proceso de Entidad
		try {
			if (iError == 0) {
				// ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
				logger.log(Level.INFO, " Se cargan registros entidad ServicesOrders con filtro ProcessFilterBu "+ country + "!!!");
			}
		} catch (Exception e) {
			iError = 1;
			logger.log(Level.WARNING, "LoadServicesOrders.ServicesOrders = Error al actualizar Fecha" + country);
			e.printStackTrace();
		}
		if (iError == 0) {
			
					logger.log(Level.INFO," Proceso Carga de Entidad con filtro ServicesOrders Finalizada " + country + "!!!!");
		}

}

}
