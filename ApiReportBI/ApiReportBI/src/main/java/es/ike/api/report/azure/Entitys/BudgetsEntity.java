package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class BudgetsEntity extends TableServiceEntity {
	//------------------------------------------------------------------------------------------------------------
	public BudgetsEntity(String budgetId, String businessUuid) {
		this.partitionKey = budgetId;
		this.rowKey = businessUuid;  
	}
//------------------------------------------------------------------------------------------------------------
	public BudgetsEntity() {		}
//------------------------------------------------------------------------------------------------------------

	public  UUID businessUnit;
    public Integer budgetId;
    public String budgetMargin;
    public String comments;
    public String iva;
    public String serviceDuration;
    public Integer serviceId;
    public String totalAmount;
    public String totalAmountWithIva;
    public String totalCost;
    public String createdAt;
    public String updatedAt;
    
    
	
    
    

	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public Integer getBudgetId() {
		return budgetId;
	}
	public void setBudgetId(Integer budgetId) {
		this.budgetId = budgetId;
	}
	public String getBudgetMargin() {
		return budgetMargin;
	}
	public void setBudgetMargin(String budgetMargin) {
		this.budgetMargin = budgetMargin;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getIva() {
		return iva;
	}
	public void setIva(String iva) {
		this.iva = iva;
	}
	public String getServiceDuration() {
		return serviceDuration;
	}
	public void setServiceDuration(String serviceDuration) {
		this.serviceDuration = serviceDuration;
	}
	public Integer getServiceId() {
		return serviceId;
	}
	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTotalAmountWithIva() {
		return totalAmountWithIva;
	}
	public void setTotalAmountWithIva(String totalAmountWithIva) {
		this.totalAmountWithIva = totalAmountWithIva;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("budgetId");
		list.add("businessUnit");
		list.add("budgetMargin");
		list.add("comments");
		list.add("iva");
		list.add("serviceDuration");
		list.add("serviceId");
		list.add("totalAmount");
		list.add("totalAmountWithIva");
		list.add("totalCost");
		list.add("createdAt");
		list.add("updatedAt");
		return list;
	}
	
}
