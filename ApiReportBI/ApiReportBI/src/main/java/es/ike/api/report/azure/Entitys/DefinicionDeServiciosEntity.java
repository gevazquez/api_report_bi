package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class DefinicionDeServiciosEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public DefinicionDeServiciosEntity (String id,String businessUnit) {
		this.partitionKey=id;
		this.rowKey=businessUnit;
		
	}
	// --------------------------------------------------------------------
	public DefinicionDeServiciosEntity() {
	
	}
	// ------------------------------------------------------------------------------------------------------------
	public Integer id;
	public UUID businessUnit;
    public String name;
    public String price;
    public String cost;
    public String duration;
    public String createdAt;
    public String updatedAt;
    public Integer countCategories;
    
    public String description;
    public String wizardId;
    public String checklistId;
    public String isExpress;
    public String active;
    public String minExpressBookingTime;
    public String maxExpressStartTime;
    public String avgExpressArrivalTime;
    public String motorId;
    public String sendsPublicLink;
    
    public Boolean isAutomatic;
    public Boolean automaticConceptCost;
    public Integer topeMinSatisfactory;
    public Integer topeMaxSatisfactory;
    public Integer topeMinUnsatisfactory;
    public Integer topeMaxUnsatisfactory;
    public UUID questionnaire;
    
    
	
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Integer getCountCategories() {
		return countCategories;
	}
	public void setCountCategories(Integer countCategories) {
		this.countCategories = countCategories;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getWizardId() {
		return wizardId;
	}
	public void setWizardId(String wizardId) {
		this.wizardId = wizardId;
	}
	public String getChecklistId() {
		return checklistId;
	}
	public void setChecklistId(String checklistId) {
		this.checklistId = checklistId;
	}
	public String getIsExpress() {
		return isExpress;
	}
	public void setIsExpress(String isExpress) {
		this.isExpress = isExpress;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getMinExpressBookingTime() {
		return minExpressBookingTime;
	}
	public void setMinExpressBookingTime(String minExpressBookingTime) {
		this.minExpressBookingTime = minExpressBookingTime;
	}
	public String getMaxExpressStartTime() {
		return maxExpressStartTime;
	}
	public void setMaxExpressStartTime(String maxExpressStartTime) {
		this.maxExpressStartTime = maxExpressStartTime;
	}
	public String getAvgExpressArrivalTime() {
		return avgExpressArrivalTime;
	}
	public void setAvgExpressArrivalTime(String avgExpressArrivalTime) {
		this.avgExpressArrivalTime = avgExpressArrivalTime;
	}
	
    public String getMotorId() {
		return motorId;
	}
	public void setMotorId(String motorId) {
		this.motorId = motorId;
	}
	
	public String getSendsPublicLink() {
		return sendsPublicLink;
	}
	public void setSendsPublicLink(String sendsPublicLink) {
		this.sendsPublicLink = sendsPublicLink;
	}
	
	
	
	public Boolean getIsAutomatic() {
		return isAutomatic;
	}
	public void setIsAutomatic(Boolean isAutomatic) {
		this.isAutomatic = isAutomatic;
	}
	public Boolean getAutomaticConceptCost() {
		return automaticConceptCost;
	}
	public void setAutomaticConceptCost(Boolean automaticConceptCost) {
		this.automaticConceptCost = automaticConceptCost;
	}
	public Integer getTopeMinSatisfactory() {
		return topeMinSatisfactory;
	}
	public void setTopeMinSatisfactory(Integer topeMinSatisfactory) {
		this.topeMinSatisfactory = topeMinSatisfactory;
	}
	public Integer getTopeMaxSatisfactory() {
		return topeMaxSatisfactory;
	}
	public void setTopeMaxSatisfactory(Integer topeMaxSatisfactory) {
		this.topeMaxSatisfactory = topeMaxSatisfactory;
	}
	public Integer getTopeMinUnsatisfactory() {
		return topeMinUnsatisfactory;
	}
	public void setTopeMinUnsatisfactory(Integer topeMinUnsatisfactory) {
		this.topeMinUnsatisfactory = topeMinUnsatisfactory;
	}
	public Integer getTopeMaxUnsatisfactory() {
		return topeMaxUnsatisfactory;
	}
	public void setTopeMaxUnsatisfactory(Integer topeMaxUnsatisfactory) {
		this.topeMaxUnsatisfactory = topeMaxUnsatisfactory;
	}
	
	public UUID getQuestionnaire() {
		return questionnaire;
	}
	public void setQuestionnaire(UUID questionnaire) {
		this.questionnaire = questionnaire;
	}
	public List<String> getColumns(){
    	List<String> list = new ArrayList<>();
    	list.add("id");
    	list.add("businessUnit");
    	list.add("name");
    	list.add("price");
    	list.add("cost");
    	list.add("categories");
    	list.add("duration");
    	list.add("description");
    	list.add("wizardId");
    	list.add("checklistId");
    	list.add("motorId");
    	list.add("isExpress");
    	list.add("active");
    	list.add("minExpressBookingTime");
    	list.add("maxExpressStartTime");
    	list.add("avgExpressArrivalTime");
    	list.add("createdAt");
    	list.add("updatedAt");
    	list.add("motorId");
    	list.add("sendsPublicLink");
    	list.add("isAutomatic");
    	list.add("automaticConceptCost");
    	list.add("topeMinSatisfactory");
    	list.add("topeMaxSatisfactory");
    	list.add("topeMinUnsatisfactory");
    	list.add("topeMaxUnsatisfactory");
    	list.add("questionnaire");
    	return list;
    }
    	
}
