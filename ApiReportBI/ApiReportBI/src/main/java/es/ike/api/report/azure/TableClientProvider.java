package es.ike.api.report.azure;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.table.CloudTableClient;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.Properties;

public class TableClientProvider {
	//----------------------------------------------------------------------------	
	/**
     * Valida la ConnectionString y regresa el StorageTableClient
     * La ConnectionString debe estar en el formato de AZURE ConnectionString
     *
     * @return Regresa el nuevo objeto CloudTableClient
     */
	//--------------------------------------------------------------------------	
    private static Properties prop;
    //--------------------------------------------------------------------------	
    static {
        // Retrieve the connection string
        prop = new Properties();
        try {
            InputStream propertyStream = TableBasics.class.getClassLoader().getResourceAsStream("application.properties");
            if (propertyStream != null) {
                prop.load(propertyStream);
            } else {
                throw new RuntimeException();
            }
        } catch (RuntimeException | IOException e) {
            System.out.println("TableClientProvider.static= Fallo la carga de alrchivo properties =application.properties");
            throw  new RuntimeException();
        }
    }
    //--------------------------------------------------------------------------	
    public static CloudTableClient getTableClientReference(String country) throws RuntimeException, IOException, URISyntaxException, InvalidKeyException {
        CloudStorageAccount storageAccount = null;
        try {
        	if(country.equals("MX")) {
        		storageAccount = CloudStorageAccount.parse(prop.getProperty("StorageConnectionStringMX"));
        	}else if (country.equals("AR")) {
        		storageAccount = CloudStorageAccount.parse(prop.getProperty("StorageConnectionStringAR"));
        	}else if (country.equals("CO")) {
        		storageAccount = CloudStorageAccount.parse(prop.getProperty("StorageConnectionStringCO"));
        	}else if (country.equals("COH")) {
        		storageAccount = CloudStorageAccount.parse(prop.getProperty("StorageConnectionStringCOH"));
        	}
            
        } catch (IllegalArgumentException | URISyntaxException e) {
            System.out.println("TableClientProvider.getTableClientReference= ConnectionString especifica una incorrecta URI, revisar la ConnectionString en formatu AZURE ");
            throw e;
        } catch (InvalidKeyException e) {
            System.out.println("TableClientProvider.getTableClientReference= La ConnectionString especifica una Key Incorrecta, validar AccountName y AccountKey");
            throw e;
        }
        return storageAccount.createCloudTableClient();
    }
    //--------------------------------------------------------------------------
    public static boolean isAzureCosmosdbTable() {
        if (prop != null) {
            String connectionString = prop.getProperty("StorageConnectionString");
            return connectionString != null && connectionString.contains("table.cosmosdb");
        }
        return false;
    }
    //--------------------------------------------------------------------------
}
