package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

import io.swagger.v3.oas.models.security.SecurityScheme.In;

public class MateriasEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public MateriasEntity(String id, String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;
	}

	// --------------------------------------------------------------------
	public MateriasEntity() {

	}

	// ------------------------------------------------------------------------------------------------------------
	public Integer id;
	public UUID businessUnit;
	public String materialType;
	public String chasisNumber;
	public String model;
	public String engineNumber;
	public String patentPlate;
	public String year;
	public String address;
	public String createdAt;
	public String updatedAt;
	public String brand;
	public String kindOfPet;
	public String petBreed;
	public String petName;
	public Integer typeMaterialId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getMaterialType() {
		return materialType;
	}

	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}

	public String getChasisNumber() {
		return chasisNumber;
	}

	public void setChasisNumber(String chasisNumber) {
		this.chasisNumber = chasisNumber;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public String getPatentPlate() {
		return patentPlate;
	}

	public void setPatentPlate(String patentPlate) {
		this.patentPlate = patentPlate;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getKindOfPet() {
		return kindOfPet;
	}

	public void setKindOfPet(String kindOfPet) {
		this.kindOfPet = kindOfPet;
	}

	public String getPetBreed() {
		return petBreed;
	}

	public void setPetBreed(String petBreed) {
		this.petBreed = petBreed;
	}

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}
	

	public Integer getTypeMaterialId() {
		return typeMaterialId;
	}

	public void setTypeMaterialId(Integer typeMaterialId) {
		this.typeMaterialId = typeMaterialId;
	}

	public List<String> getColumns() {
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("businessUnit");
		list.add("materialType");
		list.add("chasisNumber");
		list.add("model");
		list.add("engineNumber");
		list.add("patentPlate");
		list.add("year");
		list.add("address");
		list.add("createdAt");
		list.add("updatedAt");
		list.add("brand");
		list.add("kindOfPet");
		list.add("petBreed");
		list.add("petName");
		list.add("typeMaterialId");
		return list;
	}

}
