package es.ike.api.report.utilerias;

import org.json.simple.JSONObject;

public class JsonUtility {
	// -----------------Validar si existe campo en n JSON--------------------------------
	public static boolean validateJsonData(JSONObject json, String str) {
		boolean bExiste=false;
		try {
			if (json.containsKey(str)) {
				bExiste=true;
			}else { 
				if(!str.equalsIgnoreCase("errors")) {
					System.out.println("JsonUtility.validateJsonData Error: El campo "+str+" no existe en el JSON="+json.toString());
					}
				}
			return bExiste;
		} catch (Exception e) {
			if(!str.equalsIgnoreCase("errors")) {
				System.out.println("JsonUtility.validateJsonData Error: El campo "+str+" no existe en el JSON="+json.toString());
				e.printStackTrace();
				}
			return bExiste;
		}
	}
	//---------------------------------------------------------------------------------	
	public static String JsonValidaExisteLlave(JSONObject json, String str) {
		String StrSalida=null;
		try {
			org.json.JSONObject jsonObject =new org.json.JSONObject(json.toJSONString());
			//Valida si existe la Llave str
			if(jsonObject.has(str)) { 
				//Valida si el valor de la Llave str no es nulo, obtiene el dato
				if (!jsonObject.isNull(str)) {
					StrSalida=jsonObject.get(str).toString();
					}
			}
			
		} catch (Exception e) {
			System.out.println("JsonUtility.JsonValidaExisteLlave Error al validar campo ("+str+") en el JSON= "+json.toJSONString());
			e.printStackTrace();
		}
		return StrSalida;
	}
	//---------------------------------------------------------------------------------
}