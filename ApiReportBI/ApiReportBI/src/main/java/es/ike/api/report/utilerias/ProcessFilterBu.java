package es.ike.api.report.utilerias;

import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTableClient;

import java.util.logging.Level;
import java.util.logging.Logger;
import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;


public class ProcessFilterBu {
	public static void StartFilterBu(String country, String bUnit, String startDate, String endDate, String nameEntity)
			throws Exception {
		// metodo que genera filtro tutten para extraer registros de la fecha indicada
		// en adelante

		String cadenaList = "";
		Integer numPagination = 0;
		Integer offset = 0;
		Integer countTotal = 0;
		Integer pages = 0;
		String token = ObtenerOauthTL.OAuthAPI_Report(country);
		String refreshToken ="";
		String jsonResponse = "";
	    Logger logger = Logger.getLogger("Logger apiReport ProcessFilterBu.StartFilterBu");
	
	  	
	  	
	  	
		if (!token.equalsIgnoreCase("")) {
			try {
				// Inicia proceso de Carga de registros con actualización

				// Se ejecuta el conteo de resgistros para realizar la paginación
				try {
					Count countT = new Count();
					countTotal = countT.FilterCountDate(country, startDate, endDate, bUnit, nameEntity);

				} catch (Exception e) {
					logger.log(Level.SEVERE, "ProcessFilterBu.StartFilterBue =Error al extraer numero de conteo de registros actualizados");
					e.printStackTrace();
				}
				// Se incia el proceso de paginacion
				try {
					PaginationConfig pc = new PaginationConfig();
					numPagination = pc.GetNumber(country);
					Paginacion paginacion = new Paginacion();
					DatosPaginacion datosPaginacion = null;
					datosPaginacion = paginacion.calculaPaginacion(countTotal, numPagination);
					if (datosPaginacion.getResto() == 0) {
						pages = datosPaginacion.getEnteros();
					} else {
						pages = datosPaginacion.getEnteros() + 1;
					}
					// lista que trae todos los campos de una entidad
					List<String> lista = Utils.getColumns(nameEntity);
					for (int i = 0; i < lista.size(); i++) {
						cadenaList = cadenaList + " " + lista.get(i);
					}
					// -----------------------------------------------
					for (int i = 0; i < pages; i++) {
						String strJSON ="{"
								+"\n"+"\"query\":\"{"+
								nameEntity+"(limit:"+numPagination+",offset:"+offset+", where: {updatedAt: {_gte: \\\""+startDate+"\\\"}, _and: {updatedAt: {_lte: \\\""+endDate+"\\\"}, _and: {businessUnit: {_eq: \\\""+bUnit+"\\\"}}}} )" +"{ "+
								cadenaList+
										" }"+
				                	"}\""+
								 "}";
						offset = offset + numPagination;
						if(offset < 15000) {
							jsonResponse = obtenerJsonTuten.getJson(token, strJSON, country);	
						}else {
							refreshToken = ObtenerOauthTL.OAuthAPI_Report(country);
							jsonResponse = obtenerJsonTuten.getJson(refreshToken, strJSON, country);
						}
						
						if (!jsonResponse.equalsIgnoreCase("")) {
							// Parsear para obtener Arreglo json de Entidad
							try {
								JSONParser parser = new JSONParser();
								JSONObject json = (JSONObject) parser.parse(jsonResponse);
								jsonResponse = (JsonUtility.validateJsonData(json, "data"))
										? json.get("data").toString()
										: "";
								parser = null;
								json = null;
								parser = new JSONParser();
								json = (JSONObject) parser.parse(jsonResponse);
								jsonResponse = (JsonUtility.validateJsonData(json, nameEntity))
										? json.get(nameEntity).toString()
										: "";
							} catch (Exception e) {
								logger.log(Level.SEVERE, "ProcessFilterBu.StartFilterBu = Error al parsear JsonResponse de Entidad ");
								e.printStackTrace();
							}
							UtilsLoadEntity utilsLoadEntity = new UtilsLoadEntity();
							utilsLoadEntity.LoadEntity(nameEntity, jsonResponse, country);
						}
					}

				} catch (Exception e) {
					logger.log(Level.SEVERE, "ProcessFilterBu.StartFilterBu =Error al realizar paginación");
					e.printStackTrace();
				}

			} catch (Exception e) {
				logger.log(Level.SEVERE, "ProcessFilterBu.StartFilterBu = Error al cargar informacion con proceso filtro StartDate");
				e.printStackTrace();

			}

		} else {
			logger.log(Level.SEVERE, "ProcessFilterBu.StartFilterBu= Problema en obtener el Token " + country);
		}

	}

}
