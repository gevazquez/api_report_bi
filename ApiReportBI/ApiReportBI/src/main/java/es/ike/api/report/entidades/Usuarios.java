package es.ike.api.report.entidades;

import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.azure.data.tables.models.TableServiceException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import ch.qos.logback.core.pattern.parser.Parser;
import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.UsersEntity;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;
import es.ike.api.report.utilerias.ActualizaFecha;
import es.ike.api.report.utilerias.DatosPaginacion;
import es.ike.api.report.utilerias.Fecha;
import es.ike.api.report.utilerias.JsonUtility;
import es.ike.api.report.utilerias.Paginacion;

public class Usuarios {
	
	public static void ProcesoEntidadUsuarios(String StrToken,String country) throws Exception,TableServiceException{
		Integer iError=0;
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		Instant instant=Instant.now();
		apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("2","2","users",country);
		System.out.println(instant.toString()+"Obtiene Configuacion "+country+" de Entidad "+apiConfiguration.getEntidad());
		//Crea tabla Users si no existe en Azure
		CloudTableClient tableClient1 = null;
		CreateTableConfiguration tableConfiguration = null;
		tableClient1 = TableClientProvider.getTableClientReference(country);
		tableConfiguration.createTable(tableClient1, "Users",country);
		//Obtenemos Token de OAuth de TL
		String token="";
		token=ObtenerOauthTL.OAuthAPI_Report(country);
		System.out.println(instant.toString()+" Usuarios.ProcesoEntidadUsuarios=Obtiene Token "+country+" "+token);
		//Inserta conteo total
		if(!token.equalsIgnoreCase("")) {
			String StrJSONContoTotal="{"
					+"\n"+"\"query\":\"{"+
					"users_aggregate {"+
					"aggregate {"+
					"count"+
						"}"+
					"}"+
	                	"}\""+
					 "}";
			String jsonResponseConteoTotal="";
			String StrConteoTotal="";
			Integer numConteoTotal = 0;
			jsonResponseConteoTotal=obtenerJsonTuten.getJson(token,StrJSONContoTotal,country);
			//Inicia Parseo de conteo Total
		    try {
		    	JSONParser parser = new JSONParser();
		    	JSONObject json=(JSONObject) parser.parse(jsonResponseConteoTotal);
		    	jsonResponseConteoTotal = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
		    	parser=null;
	            json=null;
	            parser = new JSONParser();
	            json = (JSONObject) parser.parse(jsonResponseConteoTotal);
	            jsonResponseConteoTotal=(JsonUtility.validateJsonData(json,"users_aggregate"))?json.get("users_aggregate").toString():"";
	            parser=null;
                json=null;
                parser = new JSONParser();
                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
                jsonResponseConteoTotal=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
                StrConteoTotal=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
                numConteoTotal = Integer.parseInt(StrConteoTotal);
                try {
                	apiConfiguration.setConteoTotal(numConteoTotal);
                }catch(Exception e) {
                	iError=1;
			    	System.out.println("Usuarios.ProcesoEntidadUsuarios = Error al cargar conteo total en Azure Entidad Usuarios "+country);
                }
		    }catch(Exception e) {
		    	iError=1;
		    	System.out.println("Usuarios.ProcesoEntidadUsuarios = Error al parsear JsonResponseConteoTotal de Entidad Usuarios "+country);}
			
		}else {System.out.println("Usuarios.ProcesoEntidadUsuarios= Problema en obtener el Token del conteo Total "+country);	}
		//Implementación de filtro where
		String StrWhere=Fecha.filtroFecha(apiConfiguration, "id");
		String StrWhereCount="";
		String StrWhereQuery="";
		if (!StrWhere.equalsIgnoreCase("")) {
	    	StrWhereCount="("+StrWhere+")"; }
	    if (!StrWhere.equalsIgnoreCase("")) {
	    	StrWhereQuery=","+StrWhere; }
		if(!token.equalsIgnoreCase("")) {
			String StrJSONCount="{"
					+"\n"+"\"query\":\"{"+
					"users_aggregate "+StrWhereCount+"{"+
					"aggregate {"+
					"count"+
						"}"+
					"}"+
	                	"}\""+
					 "}"; 
			//Implementacion de paginación
			try {
				String jsonResponseCount="";
				Integer paginas=0;
				String StrCount="";
				Integer numCount = 0;
				String jsonResponse="";
				Integer numPaginacion=0;
				Integer offset=0;
				jsonResponseCount=obtenerJsonTuten.getJson(token,StrJSONCount,country);
				if(!jsonResponseCount.equalsIgnoreCase("")) {
				//---------------------------------------------------------------------
				//Inicia Parseo de count
			    try {
			    	 JSONParser parser = new JSONParser();
				    	JSONObject json=(JSONObject) parser.parse(jsonResponseCount);
				    	jsonResponseCount = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
				    	parser=null;
			            json=null;
			            parser = new JSONParser();
			            json = (JSONObject) parser.parse(jsonResponseCount);
			            jsonResponseCount=(JsonUtility.validateJsonData(json, "users_aggregate"))?json.get("users_aggregate").toString():"";
			            parser=null;
		                json=null;
		                parser = new JSONParser();
		                json= (JSONObject) parser.parse(jsonResponseCount);
		                jsonResponseCount=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
		                json= (JSONObject) parser.parse(jsonResponseCount);
		                StrCount=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
		                numCount = Integer.parseInt(StrCount);
			     }catch(Exception e) {
			    	 iError=1;
			    	 System.out.println("Usuarios.ProcesoEntidadUsuarios = Error al parsear JsonResponseCount de Entidad Usuarios "+country);}
					//llamada al archivo properties
					Thread currentThread = Thread.currentThread();
					ClassLoader contextClassLoader = currentThread.getContextClassLoader();
					URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+country);
					Properties prop = new Properties();
					if (resource == null) {	
						iError=1;
						throw new Exception("Usuarios.ProcesoEntidadUsuarios= No se pudo leer el archivo de configuración TutenLabsconfig/TutenLabsConfig.properties"+country);		}
					try (InputStream is = resource.openStream()) {
						prop.load(is);
					//Se obtiene el numero de paginacion del archivo properties
						numPaginacion=Integer.parseInt(prop.getProperty("intPaginacion"));
					}catch(Exception e) {
						iError=1;
						System.out.println("Usuarios.ProcesoEntidadUsuarios= Error en obtencion intPaginacion del archivo TutenLabsconfig/TutenLabsConfig.properties"+country);
					}	
					//Se llama y ejecuta el metodo calculaPaginacion
					Paginacion paginacion = new Paginacion();
					DatosPaginacion datosPaginacion = null;
					datosPaginacion= paginacion.calculaPaginacion(numCount, numPaginacion);
					if(datosPaginacion.getResto() ==0) { 	paginas=datosPaginacion.getEnteros();;
					}else {	paginas=datosPaginacion.getEnteros()+1;			}
					for(int i =0;i<paginas;i++){
						String StrJSON="{"
								+"\n"+"\"query\":\"{"+
								apiConfiguration.getEntidad()+"(limit:"+numPaginacion+",offset:"+offset+" "+StrWhereQuery+ ")" +"{ "+
								" id"+
								" businessUnit"+
								" username"+
								" firstName"+
								" lastName"+
								" phone"+
								" createdAt"+
								" updatedAt"+
								" email"+
								" secondLastName"+
								" businessUnits"+
								" identificationNumber"+
								" identificationTypeId"+
								" identificationTypeName"+
								" address"+
								" active"+
								" roleId"+
								" roleName"+
										" }"+
				                	"}\""+
								 "}";
					offset=offset+numPaginacion;
					jsonResponse=obtenerJsonTuten.getJson(StrToken, StrJSON,country);    
					if(!jsonResponse.equalsIgnoreCase("")) {	
						//Parsear para obtener Arreglo json de Entidad
					    try {
							JSONParser parser = new JSONParser();
					        JSONObject json=(JSONObject) parser.parse(jsonResponse);
					        jsonResponse = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
					        parser=null;
				            json=null;
				            parser = new JSONParser();
				            json = (JSONObject) parser.parse(jsonResponse);
				            jsonResponse = (JsonUtility.validateJsonData(json,apiConfiguration.getEntidad()))?json.get(apiConfiguration.getEntidad()).toString():"";	
						}catch(Exception e) {
							iError=1;
							System.out.println("Usuarios.ProcesoEntidadUsuarios = Error al parsear JsonResponse de Entidad Usuarios "+country);
							e.printStackTrace();
						}
						//Se carga jsonResponse en Arreglo Json
						try {
							   if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
								   JSONArray jsonarray= new JSONArray(jsonResponse);  
					            	CloudTableClient tableClient = null;
					                tableClient = TableClientProvider.getTableClientReference(country);
					                CloudTable table = tableClient.getTableReference("Users");
					                for (int a = 0; a<jsonarray.length(); a++) {
					                	JSONParser parserEntity=null;
					            		JSONObject jsonUsers=null;
					            		String strId="";
					            		String strBusinessUnit="";
					            		String strUsername;
					            		String strFirstName="";
					            		String strLastName="";
					            		String strPhone="";
					            		String strCreatedAt="";
					            		String strUpdatedAt="";
					            		String strEmail="";
					            		String strSecondLastName="";
					            		String strBusinessUnits="";
					            		String strIdentificationNumber="";
					            		String strIdentificationTypeId="";
					            		String strIdentificationTypeName="";
					            		String strAddress="";
					            		String strActive="";
					            		String strRoleId="";
					            		String strRoleName="";
					            		parserEntity = new JSONParser();
					                    jsonUsers = (JSONObject)parserEntity.parse(jsonarray.get(a).toString());
					                    strId=JsonUtility.JsonValidaExisteLlave(jsonUsers,"id");
					                    strBusinessUnit=JsonUtility.JsonValidaExisteLlave(jsonUsers,"businessUnit");
					                    strUsername=JsonUtility.JsonValidaExisteLlave(jsonUsers,"username");
					                    strFirstName=JsonUtility.JsonValidaExisteLlave(jsonUsers,"firstName");
					                    strLastName=JsonUtility.JsonValidaExisteLlave(jsonUsers,"lastName");
					                    strPhone=JsonUtility.JsonValidaExisteLlave(jsonUsers,"phone");
					                    strCreatedAt=JsonUtility.JsonValidaExisteLlave(jsonUsers,"createdAt");
					                    strUpdatedAt=JsonUtility.JsonValidaExisteLlave(jsonUsers,"updatedAt");
					                    strEmail=JsonUtility.JsonValidaExisteLlave(jsonUsers,"email");
					                    strSecondLastName=JsonUtility.JsonValidaExisteLlave(jsonUsers,"secondLastName");
					            		strBusinessUnits=JsonUtility.JsonValidaExisteLlave(jsonUsers,"businessUnits");
					            		strIdentificationNumber=JsonUtility.JsonValidaExisteLlave(jsonUsers,"identificationNumber");
					            		strIdentificationTypeId=JsonUtility.JsonValidaExisteLlave(jsonUsers,"identificationTypeId");
					            		strIdentificationTypeName=JsonUtility.JsonValidaExisteLlave(jsonUsers,"identificationTypeName");
					            		strAddress=JsonUtility.JsonValidaExisteLlave(jsonUsers,"address");
					            		strActive=JsonUtility.JsonValidaExisteLlave(jsonUsers,"active");
					            		strRoleId=JsonUtility.JsonValidaExisteLlave(jsonUsers,"roleId");
					            		strRoleName=JsonUtility.JsonValidaExisteLlave(jsonUsers,"roleName");
					                    UsersEntity usersEntity = new UsersEntity(strId,strBusinessUnit);
					                    usersEntity.setEtag(usersEntity.getEtag());
					                    usersEntity.setId(strId != null ?  Integer.parseInt(strId):null);
					                    usersEntity.setBusinessUnit(strBusinessUnit != null ? UUID.fromString(strBusinessUnit): null);
					                    usersEntity.setUsername(strUsername != null ? strUsername:  "null");
					                    usersEntity.setFirstName(strFirstName != null ? strFirstName: "null");
					                    usersEntity.setLastName(strLastName != null ? strLastName: "null");
					                    usersEntity.setPhone(strPhone != null ? strPhone: "null");
					                    usersEntity.setCreatedAt(strCreatedAt);
					                    usersEntity.setUpdatedAt(strUpdatedAt);
					                    usersEntity.setEmail(strEmail != null ? strEmail: "null");
					                    usersEntity.setSecondLastName(strSecondLastName!= null ? strSecondLastName: "null");
					                    usersEntity.setBusinessUnits(strBusinessUnits != null ?  strBusinessUnits: "null");
					                    usersEntity.setIdentificationNumber(strIdentificationNumber != null ? strIdentificationNumber: "null");
					                    usersEntity.setIdentificationTypeId(strIdentificationTypeId != null ?  Integer.parseInt(strIdentificationTypeId):0);
					                    usersEntity.setIdentificationTypeName(strIdentificationTypeName != null ? strIdentificationTypeName: "null");
					                    usersEntity.setAddress(strAddress != null ? strAddress: "null");
					                    usersEntity.setActive(strActive != null ? Boolean.valueOf(strActive): null);
					                    usersEntity.setRoleId(strRoleId != null ? Integer.parseInt(strRoleId):0);
					                    usersEntity.setRoleName(strRoleName != null ? strRoleName: "null");
					                    table.execute(TableOperation.insertOrReplace(usersEntity));
					                }
							   }else {
								   iError=1;
								   System.out.println("Usuarios.ProcesoEntidadUsuarios= Respuesta No identificada "+country+"="+jsonResponse);} 
						   }catch (Exception e){
							   iError=1;
							   System.out.println("Usuarios.ProcesoEntidadUsuarios= Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse);
							   e.printStackTrace();
						   }
						}else {
							iError=1;
					    	System.out.println("Usuarios.ProcesoEntidadUsuarios= Error en JSON QUERY GRAPHQL "+country);
						}
					}
				}else {
					iError=1;
					System.out.println("Usuarios.ProcesoEntidadUsuarios = Error en JSON QUERY COUNT "+country);
				}
			}catch(Exception e) {
				iError=1;
				System.out.println("Usuarios.ProcesoEntidadUsuarios = Error ejecutar paginación "+country);
				e.printStackTrace();
				}
		 //Fin de proceso de Entidad
			try {
				if(iError==0) {
					ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
					System.out.println(instant.toString()+" Actualizacion de Fecha completada "+country+"!!!");
					}
			}
			catch(Exception e) {
				iError=1;
				System.out.println("Usuarios.ProcesoEntidadUsuarios = Error al actualizar Fecha "+country);
				e.printStackTrace();
			}
			if(iError==0) {
				System.out.println(instant.toString()+" Proceso de Entidad Usuarios Finalizada "+country+"!!!!");}
		}else {System.out.println("Usuarios.ProcesoEntidadUsuarios= Problema en obtener el Token "+country);}		
    }
}
