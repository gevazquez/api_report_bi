package es.ike.api.report.utilerias;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;

public class Count {
	// metodo que realiza conteo de registros al aplicar un filtro con un rango es
	// especifico
	public static Integer FilterCountDate(String country, String startDate, String endDate, String bUnit,
			String nameEntity) {

		String token = "";
		String strJsonCountTotal = "";
		String countTotal = "";
		Integer intCountTotal = 0;
		String jsonResponseConteoTotal = "";
		Logger logger = Logger.getLogger("Logger apiReport Count.FilterCountDate");
		try {

			token = ObtenerOauthTL.OAuthAPI_Report(country);
			
			
			strJsonCountTotal ="{"
					+"\n"+"\"query\":\"{"+
					""+nameEntity+"_aggregate(where: {updatedAt: {_gte: \\\""+startDate+"\\\"}, _and: {updatedAt: {_lte: \\\""+endDate+"\\\"}, _and: {businessUnit: {_eq: \\\""+bUnit+"\\\"}}}}) {"+
					"aggregate {"+
					"count"+
						"}"+
					"}"+
	                	"}\""+
					 "}";

			jsonResponseConteoTotal = obtenerJsonTuten.getJson(token, strJsonCountTotal, country);
			// Inicia Parseo de conteo Total
			try {
				JSONParser parser = new JSONParser();
				JSONObject json = (JSONObject) parser.parse(jsonResponseConteoTotal);
				jsonResponseConteoTotal = (JsonUtility.validateJsonData(json, "data")) ? json.get("data").toString()
						: "";
				parser = null;
				json = null;
				parser = new JSONParser();
				json = (JSONObject) parser.parse(jsonResponseConteoTotal);
				jsonResponseConteoTotal = (JsonUtility.validateJsonData(json, nameEntity + "_aggregate"))
						? json.get(nameEntity + "_aggregate").toString()
						: "";
				parser = null;
				json = null;
				parser = new JSONParser();
				json = (JSONObject) parser.parse(jsonResponseConteoTotal);
				jsonResponseConteoTotal = (JsonUtility.validateJsonData(json, "aggregate"))
						? json.get("aggregate").toString()
						: "";
				json = (JSONObject) parser.parse(jsonResponseConteoTotal);
				countTotal = (JsonUtility.validateJsonData(json, "count")) ? json.get("count").toString() : "";
				intCountTotal = Integer.parseInt(countTotal);
			} catch (Exception e) {
				//System.out.println("Count.FilterCountDate = Error al parsear JsonResponseConteoTotal " + country);
				logger.log(Level.WARNING, "Count.FilterCountDate = Error al parsear JsonResponseConteoTotal " + country);
				e.printStackTrace();
			}

		} catch (Exception e) {
			//System.out.println("Count.FilterCountDate =Error al generar strJsonCountTotal");
			logger.log(Level.WARNING, "Count.FilterCountDate =Error al generar strJsonCountTotal");
			e.printStackTrace();
		}
		return intCountTotal;
	}
	
	public static Integer FilterCountById(String country, String where,
			String nameEntity) {
		String token = "";
		String strJsonCountTotal = "";
		String countTotal = "";
		Integer intCountTotal = 0;
		String jsonResponseConteoTotal = "";
		Logger logger = Logger.getLogger("Logger apiReport Count.FilterCountDate");
		try {

			token = ObtenerOauthTL.OAuthAPI_Report(country);
			
			
			strJsonCountTotal ="{"
					+"\n"+"\"query\":\"{"+
					""+nameEntity+"_aggregate("+where+") {"+
					"aggregate {"+
					"count"+
						"}"+
					"}"+
	                	"}\""+
					 "}";
			jsonResponseConteoTotal = obtenerJsonTuten.getJson(token, strJsonCountTotal, country);
			// Inicia Parseo de conteo Total
			try {
				JSONParser parser = new JSONParser();
				JSONObject json = (JSONObject) parser.parse(jsonResponseConteoTotal);
				jsonResponseConteoTotal = (JsonUtility.validateJsonData(json, "data")) ? json.get("data").toString()
						: "";
				parser = null;
				json = null;
				parser = new JSONParser();
				json = (JSONObject) parser.parse(jsonResponseConteoTotal);
				jsonResponseConteoTotal = (JsonUtility.validateJsonData(json, nameEntity + "_aggregate"))
						? json.get(nameEntity + "_aggregate").toString()
						: "";
				parser = null;
				json = null;
				parser = new JSONParser();
				json = (JSONObject) parser.parse(jsonResponseConteoTotal);
				jsonResponseConteoTotal = (JsonUtility.validateJsonData(json, "aggregate"))
						? json.get("aggregate").toString()
						: "";
				json = (JSONObject) parser.parse(jsonResponseConteoTotal);
				countTotal = (JsonUtility.validateJsonData(json, "count")) ? json.get("count").toString() : "";
				intCountTotal = Integer.parseInt(countTotal);
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Count.FilterCountById = Error al parsear JsonResponseConteoTotal " + country);
				e.printStackTrace();
			}

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Count.FilterCountById =Error al generar strJsonCountTotal");
			e.printStackTrace();
		}
		return intCountTotal;
	}

}
