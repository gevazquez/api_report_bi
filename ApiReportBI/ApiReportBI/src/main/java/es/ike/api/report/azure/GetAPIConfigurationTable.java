package es.ike.api.report.azure;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.sql.Date;

import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;
import com.microsoft.azure.storage.table.TableServiceEntity;
import es.ike.api.report.azure.Entitys.ApiConfiguration;


public class GetAPIConfigurationTable {
	//------------------------------------------------------

	public static TableServiceEntity getAPIConfigurationTable(String PartitionKey, String RowKey, String EntityName ,String country) throws Exception {

		ApiConfiguration apiConfiguration=new ApiConfiguration();
		Date date = new Date(1583572325000L);
		try {
		CloudTableClient tableClient = null;
        tableClient = TableClientProvider.getTableClientReference(country);
        CloudTable table = tableClient.getTableReference("APIConfiguration");
        apiConfiguration = table.execute(TableOperation.retrieve(PartitionKey, RowKey, ApiConfiguration.class)).getResultAsType();
        
        if (apiConfiguration == null) {
        	
        	//aqui ingresar codigo para ingresar registro
        	
        	ApiConfiguration apiConfigurationRegistroNew = new ApiConfiguration("PartitionKey", "RowKey");
        	apiConfigurationRegistroNew.setPartitionKey(PartitionKey);
        	apiConfigurationRegistroNew.setRowKey(RowKey);
        	apiConfigurationRegistroNew.setAplicaActualizacionCompleta(0);
			apiConfigurationRegistroNew.setAplicaFiltroFecha(1);
			apiConfigurationRegistroNew.setEntidad(EntityName);
			apiConfigurationRegistroNew.setUltimaFechaActualizacion(date);
			table.execute(TableOperation.insert(apiConfigurationRegistroNew));
			System.out.println("GetAPIConfigurationTable.getAPIConfigurationTable= Se Inserto nuevo registro "+EntityName+" en la tabla APIConfiguration "+country);

        	apiConfiguration = table.execute(TableOperation.retrieve(PartitionKey, RowKey, ApiConfiguration.class)).getResultAsType();
        }
		}catch(Exception e) {
			System.out.println("GetAPIConfigurationTable.getAPIConfigurationTable= Error al obtener datos de Configuracion de Entidad");
			e.printStackTrace();
			}
		return apiConfiguration;
	}
	//------------------------------------------------------
	public static int SaveApiConfiguration(ApiConfiguration apiConfiguration, String country) throws InvalidKeyException, RuntimeException, IOException, URISyntaxException, StorageException {
		//iRespuesta=0 Es sin error, iRespuesta=1 Es con error
		int iRespuesta=0;
		//Se hace conexion y configuracion de AZURE
		try {
		CloudTableClient tableClient = null;
    	tableClient = TableClientProvider.getTableClientReference(country);
    	CloudTable table = tableClient.getTableReference("ApiConfiguration");
    	table.execute(TableOperation.insertOrReplace(apiConfiguration));
		}catch(Exception e) {
			System.out.println("GetAPIConfigurationTable.SaveApiConfiguration= Error al guardar en Azure tabla API Configuration "+country);
		}
		return iRespuesta;
	}
	
	
	//------------------------------------------------------
}
