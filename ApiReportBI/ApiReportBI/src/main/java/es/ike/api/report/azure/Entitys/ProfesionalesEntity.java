package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ProfesionalesEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public ProfesionalesEntity (String id,String businessUnit) {
		this.partitionKey=id;
		this.rowKey=businessUnit;
		
	}
	// --------------------------------------------------------------------
	public ProfesionalesEntity() {
	
	}
	// ------------------------------------------------------------------------------------------------------------{
	public Integer id;
	public UUID businessUnit;
    public String name;
    public String lastName;
    public String email;
    public String phoneNumber;
    public String createdAt;
    public String updatedAt;
    
    
    public String providerId;
    public String status;
    public String approvalAt;
    public String approved; 
    public String bank;
    public String branchId;
    public String identificationTypeId;
    public String identificationTypeNumber;
    public String numberAccount;
    public String typeAccount;
    public String address;
    public String hourApproved;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getProviderId() {
		return providerId;
	}
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getApprovalAt() {
		return approvalAt;
	}
	public void setApprovalAt(String approvalAt) {
		this.approvalAt = approvalAt;
	}
	public String getApproved() {
		return approved;
	}
	public void setApproved(String approved) {
		this.approved = approved;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public String getIdentificationTypeId() {
		return identificationTypeId;
	}
	public void setIdentificationTypeId(String identificationTypeId) {
		this.identificationTypeId = identificationTypeId;
	}
	public String getIdentificationTypeNumber() {
		return identificationTypeNumber;
	}
	public void setIdentificationTypeNumber(String identificationTypeNumber) {
		this.identificationTypeNumber = identificationTypeNumber;
	}
	public String getNumberAccount() {
		return numberAccount;
	}
	public void setNumberAccount(String numberAccount) {
		this.numberAccount = numberAccount;
	}
	public String getTypeAccount() {
		return typeAccount;
	}
	public void setTypeAccount(String typeAccount) {
		this.typeAccount = typeAccount;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getHourApproved() {
		return hourApproved;
	}
	public void setHourApproved(String hourApproved) {
		this.hourApproved = hourApproved;
	}
    
    public List<String> getColumns(){
    	List<String> list = new ArrayList<>();
    	list.add("id");
    	list.add("businessUnit");
    	list.add("name");
    	list.add("lastName");
    	list.add("email");
    	list.add("phoneNumber");
    	list.add("status");
    	list.add("bank");
    	list.add("typeAccount");
    	list.add("numberAccount");
    	list.add("identificationTypeId");
    	list.add("identificationTypeNumber");
    	list.add("address");
    	list.add("approved");
    	list.add("approvalAt");
    	list.add("branchId");
    	list.add("providerId");
    	list.add("hourApproved");
    	list.add("createdAt");
    	list.add("updatedAt");
    	return list;
    }

}
