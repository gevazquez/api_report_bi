package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class OfertasAProfesionalesEntity extends TableServiceEntity{

	public OfertasAProfesionalesEntity (String id,String businessUuid) {
		this.partitionKey=id;
		this.rowKey=businessUuid;
	}
	
	public OfertasAProfesionalesEntity() {
		
	}
	
	public Integer id;
	public Integer serviceId;
	public UUID  businessUnit;
	public Integer professionalId;
	public String name;
	public String lastName;
	public String acceptedAt;
	public String rejectedAt;
	public String validUntil;
	public String createdAt;
	public String updatedAt;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}

	public Integer getProfessionalId() {
		return professionalId;
	}

	public void setProfessionalId(Integer professionalId) {
		this.professionalId = professionalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAcceptedAt() {
		return acceptedAt;
	}

	public void setAcceptedAt(String acceptedAt) {
		this.acceptedAt = acceptedAt;
	}

	public String getRejectedAt() {
		return rejectedAt;
	}

	public void setRejectedAt(String rejectedAt) {
		this.rejectedAt = rejectedAt;
	}

	public String getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(String validUntil) {
		this.validUntil = validUntil;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("serviceId");
		list.add("businessUnit");
		list.add("professionalId");
		list.add("name");
		list.add("lastName");
		list.add("acceptedAt");
		list.add("rejectedAt");
		list.add("validUntil");
		list.add("createdAt");
		list.add("updatedAt");
		return list;
	}
}
