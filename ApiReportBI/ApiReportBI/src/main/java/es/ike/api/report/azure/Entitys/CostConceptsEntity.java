package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class CostConceptsEntity extends TableServiceEntity{
	
	// --------------------------------------------------------------------
	public CostConceptsEntity(String id,String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;
	}
	// --------------------------------------------------------------------
	public CostConceptsEntity() {
		
	}
	// --------------------------------------------------------------------
	
	public Boolean active;
    public Integer actualStatus;
    public Integer  baseCostProvider;
    public Integer baseSale;
    public Integer budgetCCAmount;
    public Integer budgetCCBaseCost;
    public Integer budgetCCExcessBaseCost;
    public Integer budgetCCRateType;
    public Integer budgetCCTop;
    public Integer budgetCCTotal;
    public String budgetMargin;
    public Integer budgetTotalCostProvider;
    public UUID businessUnit;
    public Boolean calculateIva;
    public String comments;
    public String costConceptBaseCost;
    public Integer costConceptCost;
    public Integer costConceptId;
    public String costConceptName;
    public Integer costConceptRateType;
    public Integer costConceptSale;
    public Integer costConceptTop;
    public Integer costConceptTotalCostProvider;
    public String createdAt;
    public String creationDate;
    public Integer excessBaseCost;
    public Integer excessBaseSale;
    public Integer idBudget;
    public Integer idBudgetCc;
    public Boolean integrationMap;
    public Boolean isCovered;
    public Boolean isValidated;
    public Double iva;
    public String ivaCost; 
	public String margin;
    public Integer materialsCost;
    public Boolean providerPayment;
    public Boolean question;
    public Integer runtimeMin;
    public Integer saleDiscount;
    public Integer serviceDuration;
    public String state;
    public String statusName;
    public Integer subtotal;
    public Integer subtotalCost;
    public Integer totalBudget;
    public Integer totalCost;
    public String unitCode;
    public String unitName;
    public String updatedAt;
    public Boolean validCoverage;
    public Integer workforce;
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Integer getActualStatus() {
		return actualStatus;
	}
	public void setActualStatus(Integer actualStatus) {
		this.actualStatus = actualStatus;
	}
	public Integer getBaseCostProvider() {
		return baseCostProvider;
	}
	public void setBaseCostProvider(Integer baseCostProvider) {
		this.baseCostProvider = baseCostProvider;
	}
	public Integer getBaseSale() {
		return baseSale;
	}
	public void setBaseSale(Integer baseSale) {
		this.baseSale = baseSale;
	}
	public Integer getBudgetCCAmount() {
		return budgetCCAmount;
	}
	public void setBudgetCCAmount(Integer budgetCCAmount) {
		this.budgetCCAmount = budgetCCAmount;
	}
	public Integer getBudgetCCBaseCost() {
		return budgetCCBaseCost;
	}
	public void setBudgetCCBaseCost(Integer budgetCCBaseCost) {
		this.budgetCCBaseCost = budgetCCBaseCost;
	}
	public Integer getBudgetCCExcessBaseCost() {
		return budgetCCExcessBaseCost;
	}
	public void setBudgetCCExcessBaseCost(Integer budgetCCExcessBaseCost) {
		this.budgetCCExcessBaseCost = budgetCCExcessBaseCost;
	}
	public Integer getBudgetCCRateType() {
		return budgetCCRateType;
	}
	public void setBudgetCCRateType(Integer budgetCCRateType) {
		this.budgetCCRateType = budgetCCRateType;
	}
	public Integer getBudgetCCTop() {
		return budgetCCTop;
	}
	public void setBudgetCCTop(Integer budgetCCTop) {
		this.budgetCCTop = budgetCCTop;
	}
	public Integer getBudgetCCTotal() {
		return budgetCCTotal;
	}
	public void setBudgetCCTotal(Integer budgetCCTotal) {
		this.budgetCCTotal = budgetCCTotal;
	}
	
	public String getBudgetMargin() {
		return budgetMargin;
	}
	public void setBudgetMargin(String budgetMargin) {
		this.budgetMargin = budgetMargin;
	}
	public Integer getBudgetTotalCostProvider() {
		return budgetTotalCostProvider;
	}
	public void setBudgetTotalCostProvider(Integer budgetTotalCostProvider) {
		this.budgetTotalCostProvider = budgetTotalCostProvider;
	}
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public Boolean getCalculateIva() {
		return calculateIva;
	}
	public void setCalculateIva(Boolean calculateIva) {
		this.calculateIva = calculateIva;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getCostConceptBaseCost() {
		return costConceptBaseCost;
	}
	public void setCostConceptBaseCost(String costConceptBaseCost) {
		this.costConceptBaseCost = costConceptBaseCost;
	}
	public Integer getCostConceptCost() {
		return costConceptCost;
	}
	public void setCostConceptCost(Integer costConceptCost) {
		this.costConceptCost = costConceptCost;
	}
	public Integer getCostConceptId() {
		return costConceptId;
	}
	public void setCostConceptId(Integer costConceptId) {
		this.costConceptId = costConceptId;
	}
	public String getCostConceptName() {
		return costConceptName;
	}
	public void setCostConceptName(String costConceptName) {
		this.costConceptName = costConceptName;
	}
	public Integer getCostConceptRateType() {
		return costConceptRateType;
	}
	public void setCostConceptRateType(Integer costConceptRateType) {
		this.costConceptRateType = costConceptRateType;
	}
	public Integer getCostConceptSale() {
		return costConceptSale;
	}
	public void setCostConceptSale(Integer costConceptSale) {
		this.costConceptSale = costConceptSale;
	}
	public Integer getCostConceptTop() {
		return costConceptTop;
	}
	public void setCostConceptTop(Integer costConceptTop) {
		this.costConceptTop = costConceptTop;
	}
	public Integer getCostConceptTotalCostProvider() {
		return costConceptTotalCostProvider;
	}
	public void setCostConceptTotalCostProvider(Integer costConceptTotalCostProvider) {
		this.costConceptTotalCostProvider = costConceptTotalCostProvider;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public Integer getExcessBaseCost() {
		return excessBaseCost;
	}
	public void setExcessBaseCost(Integer excessBaseCost) {
		this.excessBaseCost = excessBaseCost;
	}
	public Integer getExcessBaseSale() {
		return excessBaseSale;
	}
	public void setExcessBaseSale(Integer excessBaseSale) {
		this.excessBaseSale = excessBaseSale;
	}
	public Integer getIdBudget() {
		return idBudget;
	}
	public void setIdBudget(Integer idBudget) {
		this.idBudget = idBudget;
	}
	public Integer getIdBudgetCc() {
		return idBudgetCc;
	}
	public void setIdBudgetCc(Integer idBudgetCc) {
		this.idBudgetCc = idBudgetCc;
	}
	public Boolean getIntegrationMap() {
		return integrationMap;
	}
	public void setIntegrationMap(Boolean integrationMap) {
		this.integrationMap = integrationMap;
	}
	public Boolean getIsCovered() {
		return isCovered;
	}
	public void setIsCovered(Boolean isCovered) {
		this.isCovered = isCovered;
	}
	public Boolean getIsValidated() {
		return isValidated;
	}
	public void setIsValidated(Boolean isValidated) {
		this.isValidated = isValidated;
	}
	public Double getIva() {
		return iva;
	}
	public void setIva(Double iva) {
		this.iva = iva;
	}
	
	public String getIvaCost() {
		return ivaCost;
	}
	public void setIvaCost(String ivaCost) {
		this.ivaCost = ivaCost;
	}
	
	
	public String getMargin() {
		return margin;
	}
	public void setMargin(String margin) {
		this.margin = margin;
	}
	public Integer getMaterialsCost() {
		return materialsCost;
	}
	public void setMaterialsCost(Integer materialsCost) {
		this.materialsCost = materialsCost;
	}
	public Boolean getProviderPayment() {
		return providerPayment;
	}
	public void setProviderPayment(Boolean providerPayment) {
		this.providerPayment = providerPayment;
	}
	public Boolean getQuestion() {
		return question;
	}
	public void setQuestion(Boolean question) {
		this.question = question;
	}
	public Integer getRuntimeMin() {
		return runtimeMin;
	}
	public void setRuntimeMin(Integer runtimeMin) {
		this.runtimeMin = runtimeMin;
	}
	public Integer getSaleDiscount() {
		return saleDiscount;
	}
	public void setSaleDiscount(Integer saleDiscount) {
		this.saleDiscount = saleDiscount;
	}
	public Integer getServiceDuration() {
		return serviceDuration;
	}
	public void setServiceDuration(Integer serviceDuration) {
		this.serviceDuration = serviceDuration;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	
	public Integer getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(Integer subtotal) {
		this.subtotal = subtotal;
	}
	public Integer getSubtotalCost() {
		return subtotalCost;
	}
	public void setSubtotalCost(Integer subtotalCost) {
		this.subtotalCost = subtotalCost;
	}
	public Integer getTotalBudget() {
		return totalBudget;
	}
	public void setTotalBudget(Integer totalBudget) {
		this.totalBudget = totalBudget;
	}
	public Integer getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Integer totalCost) {
		this.totalCost = totalCost;
	}
	public String getUnitCode() {
		return unitCode;
	}
	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Boolean getValidCoverage() {
		return validCoverage;
	}
	public void setValidCoverage(Boolean validCoverage) {
		this.validCoverage = validCoverage;
	}
	public Integer getWorkforce() {
		return workforce;
	}
	public void setWorkforce(Integer workforce) {
		this.workforce = workforce;
	}
    
    public List<String> getColumns(){
    	List<String> list = new ArrayList<String>();
    	list.add("idBudget");
    	list.add("idBudgetCc");
    	list.add("costConceptId");
    	list.add("active");
    	list.add("actualStatus");
    	list.add("baseCostProvider");
    	list.add("baseSale");
    	list.add("budgetCCAmount");
    	list.add("budgetCCBaseCost");
    	list.add("budgetCCExcessBaseCost");
    	list.add("budgetCCRateType");
    	list.add("budgetCCTop");
    	list.add("budgetCCTotal");
    	list.add("budgetMargin");
    	list.add("budgetTotalCostProvider");
    	list.add("businessUnit");
    	list.add("calculateIva");
    	list.add("comments");
    	list.add("costConceptBaseCost");
    	list.add("costConceptCost");
    	list.add(costConceptName);
    	list.add("costConceptRateType");
    	list.add("costConceptSale");
    	list.add("costConceptTop");
    	list.add("costConceptTotalCostProvider");
    	list.add("createdAt");
    	list.add("updatedAt");
    	list.add("creationDate");
    	list.add("excessBaseCost");
    	list.add("excessBaseSale");
    	list.add("integrationMap");
    	list.add("isCovered");
    	list.add("isValidated");
    	list.add("iva");
    	list.add("margin");
    	list.add("ivaCost");
    	list.add("materialsCost");
    	list.add("providerPayment");
    	list.add("question");
    	list.add("runtimeMin");
    	list.add("saleDiscount");
    	list.add("serviceDuration");
    	list.add("state");
    	list.add("statusName");
    	list.add("subtotal");
    	list.add("subtotalCost");
    	list.add("totalBudget");
    	list.add("totalCost");
    	list.add("unitCode");
    	list.add("unitName");
    	list.add("validCoverage");
    	list.add("workforce");

    	
    	
    	return list;
    }
	
}
