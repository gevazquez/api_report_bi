package es.ike.api.report.utilerias;

import es.ike.api.report.utilerias.LoadJsonResponse.LoadCases;
import es.ike.api.report.utilerias.LoadJsonResponse.LoadCustomers;
import es.ike.api.report.utilerias.LoadJsonResponse.LoadHistorialDeServicios;
import es.ike.api.report.utilerias.LoadJsonResponse.LoadMaterials;
import es.ike.api.report.utilerias.LoadJsonResponse.LoadPolls;
import es.ike.api.report.utilerias.LoadJsonResponse.LoadProviderPayments;
import es.ike.api.report.utilerias.LoadJsonResponse.LoadServiceBudgets;
import es.ike.api.report.utilerias.LoadJsonResponse.LoadServices;
import es.ike.api.report.utilerias.LoadJsonResponse.LoadServiciosDeProfesionales;
import es.ike.api.report.utilerias.LoadJsonResponse.LoadSiniestralidad;
import es.ike.api.report.utilerias.LoadJsonResponse.LoadSubscriptions;
import es.ike.api.report.utilerias.LoadJsonResponse.LoadSubscriptionsConsumed;
import es.ike.api.report.utilerias.LoadJsonResponse.LoadServicesOrder;

public class UtilsLoadEntity {
	public static String LoadEntity(String nameEntity, String jsonResponse, String country) throws Exception {
		switch (nameEntity) {
		case "cases":
			LoadCases loadCases = new LoadCases();
			loadCases.Cases(jsonResponse, country);
			break;
		case "customers":
			LoadCustomers loadCustomers = new LoadCustomers();
			loadCustomers.Customers(jsonResponse, country);
			break;
		case "materials":
			LoadMaterials loadMaterials = new LoadMaterials();
			loadMaterials.Materials(jsonResponse, country);
			break;
		case "polls":
			LoadPolls loadPolls = new LoadPolls();
			loadPolls.Polls(jsonResponse, country);
			break;
		case "provider_payments":
			LoadProviderPayments loadProviderPayments = new LoadProviderPayments();
			loadProviderPayments.ProviderPayments(jsonResponse, country);
			break;
		case "service_budgets":
			LoadServiceBudgets loadServiceBudgets = new LoadServiceBudgets();
			loadServiceBudgets.ServiceBudgets(jsonResponse, country);
			break;
		case "services":
			LoadServices loadServices = new LoadServices();
			loadServices.Services(jsonResponse, country);
			break;
		case "subscriptions":
			LoadSubscriptions loadSubscriptions = new LoadSubscriptions();
			loadSubscriptions.Subscriptions(jsonResponse, country);
			break;
		case "subscriptions_consumed":
			LoadSubscriptionsConsumed loadSubscriptionsConsumed = new LoadSubscriptionsConsumed();
			loadSubscriptionsConsumed.SubscriptionsConsumed(jsonResponse, country);
			break;
		case "coverage_balances":
			LoadSiniestralidad loadSiniestralidad = new LoadSiniestralidad();
			loadSiniestralidad.Siniestralidad(jsonResponse, country);
			break;
		case "service_status_change_history_logs":
			LoadHistorialDeServicios loadHistorialDeServicios = new LoadHistorialDeServicios();
			loadHistorialDeServicios.HistorialDeServicios(jsonResponse, country);
			break;
		case "professional_services":
			LoadServiciosDeProfesionales loadServiciosDeProfesionales = new LoadServiciosDeProfesionales();
			loadServiciosDeProfesionales.ServiciosDeProfesionales(jsonResponse, country);
			break;	
		case "services_order":
			LoadServicesOrder loadServicesOrder = new LoadServicesOrder();
			loadServicesOrder.ServicesOrder(jsonResponse, country);
			break;
		}
		return "Se utiliza entidad " + nameEntity + "para carga con filtro";
	}

}
