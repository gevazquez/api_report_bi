package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class BudgetSkusEntity extends TableServiceEntity{
	public BudgetSkusEntity  (String id,String businessUnit) {
		this.partitionKey=id;
		this.rowKey=businessUnit;
		
	}
	// --------------------------------------------------------------------
	
	public BudgetSkusEntity() {
		
	}
	
	// --------------------------------------------------------------------
	
	public Integer id;
	public Integer skuComuneId;
	public Integer budgetId;
	public Integer serviceId;
	public Integer quantity;
	public String cost;
	public String subtotalCost;
	public String sale;
	public String subtotal;
	public String discount;
	public UUID businessUnit;
	public String createdAt; 
	public String updatedAt;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSkuComuneId() {
		return skuComuneId;
	}

	public void setSkuComuneId(Integer skuComuneId) {
		this.skuComuneId = skuComuneId;
	}

	public Integer getBudgetId() {
		return budgetId;
	}

	public void setBudgetId(Integer budgetId) {
		this.budgetId = budgetId;
	}

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getSubtotalCost() {
		return subtotalCost;
	}

	public void setSubtotalCost(String subtotalCost) {
		this.subtotalCost = subtotalCost;
	}

	public String getSale() {
		return sale;
	}

	public void setSale(String sale) {
		this.sale = sale;
	}

	public String getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public List<String>  getColumns(){
		List<String> list = new ArrayList<String>();
		list.add("id");
		list.add("skuComuneId");
		list.add("budgetId");
		list.add("serviceId");
		list.add("quantity");
		list.add("cost");
		list.add("subtotalCost");
		list.add("sale");
		list.add("subtotal");
		list.add("discount");
		list.add("businessUnit");
		list.add("createdAt");
		list.add("updatedAt");
		return list;
	}

}
