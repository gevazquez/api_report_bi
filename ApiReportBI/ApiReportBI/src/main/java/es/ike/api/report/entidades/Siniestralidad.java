package es.ike.api.report.entidades;

import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.azure.data.tables.models.TableServiceException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.SiniestralidadEntity;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;
import es.ike.api.report.utilerias.ActualizaFecha;
import es.ike.api.report.utilerias.DatosPaginacion;
import es.ike.api.report.utilerias.Fecha;
import es.ike.api.report.utilerias.JsonUtility;
import es.ike.api.report.utilerias.Paginacion;

public class Siniestralidad {

	public static void ProcesoEntidadSiniestralidad(String StrToken,String country) throws Exception,TableServiceException{
		Integer iError=0;
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		Instant instant=Instant.now();
		apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("35","35","coverage_balances",country);
		System.out.println(instant.toString()+" Obtiene Configuacion "+country+" de Entidad "+apiConfiguration.getEntidad());
		//Crea tabla Countries si no existe en Azure
		CloudTableClient tableClient1 = null;
		CreateTableConfiguration tableConfiguration = null;
		tableClient1 = TableClientProvider.getTableClientReference(country);
		tableConfiguration.createTable(tableClient1, "CoverageBalances",country);
		//Obtenemos Token de OAuth de TL
		String token="";
		token=ObtenerOauthTL.OAuthAPI_Report(country);
		System.out.println(instant.toString()+" Siniestralidad.ProcesoEntidadSiniestralidad=Obtiene Token "+country+" "+token);
		//Inserta conteo total
			if(!token.equalsIgnoreCase("")) {
				String StrJSONContoTotal="{"
						+"\n"+"\"query\":\"{"+
						"coverage_balances_aggregate {"+
						"aggregate {"+
						"count"+
							"}"+
						"}"+
		                	"}\""+
						 "}";
				String jsonResponseConteoTotal="";
				String StrConteoTotal="";
				Integer numConteoTotal = 0;
				jsonResponseConteoTotal=obtenerJsonTuten.getJson(token,StrJSONContoTotal,country);
				//Inicia Parseo de conteo Total
			    try {
			    	JSONParser parser = new JSONParser();
			    	JSONObject json=(JSONObject) parser.parse(jsonResponseConteoTotal);
			    	jsonResponseConteoTotal = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
			    	parser=null;
		            json=null;
		            parser = new JSONParser();
		            json = (JSONObject) parser.parse(jsonResponseConteoTotal);
		            jsonResponseConteoTotal=(JsonUtility.validateJsonData(json,"coverage_balances_aggregate"))?json.get("coverage_balances_aggregate").toString():"";
		            parser=null;
	                json=null;
	                parser = new JSONParser();
	                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
	                jsonResponseConteoTotal=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
	                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
	                StrConteoTotal=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
	                numConteoTotal = Integer.parseInt(StrConteoTotal);
	                try {
	                	apiConfiguration.setConteoTotal(numConteoTotal);
	                }catch(Exception e) {
	                	iError=1;
				    	System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad = Error al cargar conteo total en Azure Entidad Siniestralidad "+country);
	                }
			    }catch(Exception e) {
			    	iError=1;
			    	System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad = Error al parsear JsonResponseConteoTotal de Entidad Siniestralidad "+country);}
				
			}else {System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad= Problema en obtener el Token del conteo Total "+country);	}
			//Implementación de filtro where
			String StrWhere=Fecha.filtroFecha(apiConfiguration, "id");
			String StrWhereCount="";
			String StrWhereQuery="";
			if (!StrWhere.equalsIgnoreCase("")) {
			   	StrWhereCount="("+StrWhere+")"; }
			if (!StrWhere.equalsIgnoreCase("")) {
			   	StrWhereQuery=","+StrWhere; }
			if(!token.equalsIgnoreCase("")) {
				String StrJSONCount="{"
						+"\n"+"\"query\":\"{"+
						"coverage_balances_aggregate "+StrWhereCount+" {"+
							"aggregate {"+
								" count"+
								"}"+
							"}"+
						"}\""+
						"}";
				//Implementacion de paginación
				try {
					String jsonResponseCount="";
					Integer paginas=0;
					String StrCount="";
					Integer numCount = 0;
					String jsonResponse="";
					Integer numPaginacion=0;
					Integer offset=0;
					jsonResponseCount=obtenerJsonTuten.getJson(token,StrJSONCount,country);
					if(!jsonResponseCount.equalsIgnoreCase("")) {
						//---------------------------------------------------------------------
						//Inicia Parseo de count
					    try {
					    	JSONParser parser = new JSONParser();
					    	JSONObject json=(JSONObject) parser.parse(jsonResponseCount);
					    	jsonResponseCount = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
					    	parser=null;
				            json=null;
				            parser = new JSONParser();
				            json = (JSONObject) parser.parse(jsonResponseCount);
				            jsonResponseCount=(JsonUtility.validateJsonData(json,"coverage_balances_aggregate"))?json.get("coverage_balances_aggregate").toString():"";
				            parser=null;
			                json=null;
			                parser = new JSONParser();
			                json= (JSONObject) parser.parse(jsonResponseCount);
			                jsonResponseCount=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
			                json= (JSONObject) parser.parse(jsonResponseCount);
			                StrCount=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
			                numCount = Integer.parseInt(StrCount);
					    }catch(Exception e) {
					    	iError=1;
					    	System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad = Error al parsear JsonResponseCount de Entidad Siniestralidad "+country);}
					  //llamada al archivo properties
						Thread currentThread = Thread.currentThread();
						ClassLoader contextClassLoader = currentThread.getContextClassLoader();
						URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+country);
						Properties prop = new Properties();
						if (resource == null) {	
							iError=1;
							throw new Exception("Siniestralidad.ProcesoEntidadSiniestralidad= No se pudo leer el archivo de configuración TutenLabsconfig/TutenLabsConfig.properties"+country);		}
						try (InputStream is = resource.openStream()) {
							prop.load(is);
						//Se obtiene el numero de paginacion del archivo properties
							numPaginacion=Integer.parseInt(prop.getProperty("intPaginacion"));
						}catch(Exception e) {
							iError=1;
							System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad= Error al obtener intPaginacion del archivo TutenLabsconfig/TutenLabsConfig.properties"+country);
						}
						//Se llama y ejecuta el metodo calculaPaginacion
						Paginacion paginacion = new Paginacion();
						DatosPaginacion datosPaginacion = null;
						datosPaginacion= paginacion.calculaPaginacion(numCount, numPaginacion);
						if(datosPaginacion.getResto() ==0) { 	paginas=datosPaginacion.getEnteros();
						}else {	paginas=datosPaginacion.getEnteros()+1;			}
						for(int i =0;i<paginas;i++){
							String StrJSON="{"
									+"\n"+"\"query\":\"{"+
									apiConfiguration.getEntidad()+"(limit:"+numPaginacion+",offset:"+offset+" "+StrWhereQuery+")" +"{ "+
									" id"+
									" businessUnit"+
									" available"+
									" behavior"+
									" consumed"+
									" coverageId"+
									" coverageName"+
									" coverageStatus"+
									" limitAmount"+
									" maximumCoverageId"+
									" periodicity"+
									" planId"+
									" planSubscriptionId"+
									" restrictive"+
									" subscriptionId"+
									" unitCode"+
									" createdAt"+
									" updatedAt"+
											" }"+
					                	"}\""+
									 "}";
							
							offset=offset+numPaginacion;
							jsonResponse=obtenerJsonTuten.getJson(StrToken, StrJSON,country);  
							if(!jsonResponse.equalsIgnoreCase("")) {
								//Parsear para obtener Arreglo json de Entidad
							    try {
							    	JSONParser parser = new JSONParser();
							        JSONObject json=(JSONObject) parser.parse(jsonResponse);
							        jsonResponse = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
							        parser=null;
						            json=null;
						            parser = new JSONParser();
						            json = (JSONObject) parser.parse(jsonResponse);
						            jsonResponse = (JsonUtility.validateJsonData(json,apiConfiguration.getEntidad()))?json.get(apiConfiguration.getEntidad()).toString():"";
							    }catch(Exception e){
							    	iError=1;
							    	System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad = Error al parsear JsonResponse de Entidad Siniestralidad "+country);
							    	e.printStackTrace();
							    	}
								//Se carga jsonResponse en Arreglo Json
							    try {
							    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
						            	JSONArray jsonarray= new JSONArray(jsonResponse);  
						            	CloudTableClient tableClient = null;
						                tableClient = TableClientProvider.getTableClientReference(country);
						                //Se genera la variable tabla, con el nombre de la tabla en AZURE
						                CloudTable table = tableClient.getTableReference("CoverageBalances");
						            	for (int a = 0; a<jsonarray.length(); a++) {
						            		JSONParser parser=null;
						            		JSONObject jsonAP=null;
						            		String strId="";
						            		String strBusinessUnit="";
						            		String strAvailable="";
											String strBehavior="";
											String strConsumed="";
											String strCoverageId="";
											String strCoverageName="";
											String strCoverageStatus="";
											String strLimitAmount="";
											String strMaximumCoverageId="";
											String strPeriodicity="";
											String strPlanId="";
											String strPlanSubscriptionId="";
											String strRestrictive="";
											String strSubscriptionId="";
											String strUnitCode="";
						            		String strCreatedAt="";
						            		String strUpdatedAt="";
						            		
						            		
						            		parser = new JSONParser();
						            		jsonAP = (JSONObject)parser.parse(jsonarray.get(a).toString());
						            		strId = JsonUtility.JsonValidaExisteLlave(jsonAP,"id" );
						            		strBusinessUnit = JsonUtility.JsonValidaExisteLlave(jsonAP,"businessUnit" );
						            		strAvailable = JsonUtility.JsonValidaExisteLlave(jsonAP,"available" );
						            		strBehavior = JsonUtility.JsonValidaExisteLlave(jsonAP,"behavior" );
						            		strConsumed= JsonUtility.JsonValidaExisteLlave(jsonAP,"consumed" );
						            		strCoverageId = JsonUtility.JsonValidaExisteLlave(jsonAP,"coverageId" );
						            		strCoverageName = JsonUtility.JsonValidaExisteLlave(jsonAP,"coverageName" );
						            		strCoverageStatus = JsonUtility.JsonValidaExisteLlave(jsonAP,"coverageStatus" );
						            		strLimitAmount= JsonUtility.JsonValidaExisteLlave(jsonAP,"limitAmount" );
						            		strMaximumCoverageId = JsonUtility.JsonValidaExisteLlave(jsonAP,"maximumCoverageId" );
						            		strPeriodicity = JsonUtility.JsonValidaExisteLlave(jsonAP,"periodicity" );
						            		strPlanId = JsonUtility.JsonValidaExisteLlave(jsonAP,"planId" );
						            		strPlanSubscriptionId = JsonUtility.JsonValidaExisteLlave(jsonAP,"planSubscriptionId" );
						            		strRestrictive = JsonUtility.JsonValidaExisteLlave(jsonAP,"restrictive" );
						            		strSubscriptionId = JsonUtility.JsonValidaExisteLlave(jsonAP,"subscriptionId" );
						            		strUnitCode = JsonUtility.JsonValidaExisteLlave(jsonAP,"unitCode" );						            	
						            	    strCreatedAt = JsonUtility.JsonValidaExisteLlave(jsonAP,"createdAt" );
						            	    strUpdatedAt = JsonUtility.JsonValidaExisteLlave(jsonAP,"updatedAt" );
						            		
						            		SiniestralidadEntity siniestralidadEntity = new SiniestralidadEntity(strId, strBusinessUnit); //PartitionKey & RowKey
						            		siniestralidadEntity.setEtag(siniestralidadEntity.getEtag());
						            		siniestralidadEntity.setId(strId != null ? Integer.parseInt(strId): 0);
						            		siniestralidadEntity.setBusinessUnit(strBusinessUnit != null ?  UUID.fromString(strBusinessUnit): null);
						            		siniestralidadEntity.setAvailable(strAvailable != null ? Integer.parseInt(strAvailable): 0);
						            		siniestralidadEntity.setBehavior(strBehavior != null ? strBehavior: "null");
						            		siniestralidadEntity.setConsumed(strConsumed != null ? Integer.parseInt(strConsumed): 0);
						            		siniestralidadEntity.setCoverageId(strCoverageId != null ? Integer.parseInt(strCoverageId): 0);
						            		siniestralidadEntity.setCoverageName(strCoverageName != null ? strCoverageName: "null");
						            		siniestralidadEntity.setCoverageStatus(strCoverageStatus != null ? strCoverageStatus: "null");
						            		siniestralidadEntity.setLimitAmount(strLimitAmount != null ? Integer.parseInt(strLimitAmount): 0);
						            		siniestralidadEntity.setMaximumCoverageId(strMaximumCoverageId != null ? Integer.parseInt(strMaximumCoverageId): 0);
						            		siniestralidadEntity.setPeriodicity(strPeriodicity != null ? strPeriodicity: "null");
						            		siniestralidadEntity.setPlanId(strPlanId != null ? Integer.parseInt(strPlanId): 0);
						            		siniestralidadEntity.setPlanSubscriptionId(strPlanSubscriptionId != null ? Integer.parseInt(strPlanSubscriptionId): 0);
						            		siniestralidadEntity.setRestrictive(strRestrictive != null ? Boolean.valueOf(strRestrictive): false);
						            		siniestralidadEntity.setSubscriptionId(strSubscriptionId != null ? Integer.parseInt(strSubscriptionId): 0);
						            		siniestralidadEntity.setUnitCode(strUnitCode != null ? strUnitCode: "null");
						            		siniestralidadEntity.setCreatedAt(strCreatedAt != null ? strCreatedAt: "null");
						            		siniestralidadEntity.setUpdatedAt(strUpdatedAt != null ? strUpdatedAt: "null");
						                    table.execute(TableOperation.insertOrReplace(siniestralidadEntity));
						            		}
						            }else {
						            	iError=1;
						            	System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad = Respuesta No identificada "+country+"="+jsonResponse);      }
							    }catch(Exception e) {
							    	iError=1;
							    	System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad = Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse);
							    	e.printStackTrace();
							    }	
							}else {
								iError=1;
						    	System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad= Error en JSON QUERY GRAPHQL "+country);
							}
						}
					}else {
						iError=1;
						System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad = Error en JSON QUERY COUNT "+country);
					}
				}catch(Exception e){
					iError=1;
					System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad = Error ejecutar paginación "+country);
					e.printStackTrace();
				}
				//Fin de proceso de Entidad
				try { 	
					if(iError==0) {
						ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
						System.out.println(instant.toString()+" Actualizacion de Fecha completada "+country+"!!!");
						}
					}
				catch(Exception e) {
					iError=1;
					System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad = Error al actualizar Fecha "+country);
					e.printStackTrace();
				}
				if(iError==0) {
					System.out.println(instant.toString()+" Proceso de Entidad Siniestralidad Finalizada "+country+"!!!!");}
			}else {System.out.println("Siniestralidad.ProcesoEntidadSiniestralidad = Problema en obtener el Token "+country);	}
		
	}
}
