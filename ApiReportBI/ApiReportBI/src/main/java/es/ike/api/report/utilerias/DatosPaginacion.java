package es.ike.api.report.utilerias;

public class DatosPaginacion {
	//---------------------------------------------------------------------------	
		public DatosPaginacion(Integer enteros, Integer resto) {
			this.enteros = enteros;
			this.resto = resto;
			}
	//---------------------------------------------------------------------------
		public DatosPaginacion() {	}
	//---------------------------------------------------------------------------
	public Integer enteros;
	public Integer resto;
	
	public Integer getEnteros() {		return enteros;	}
	public void setEnteros(Integer enteros) {		this.enteros = enteros;	}
	
	public Integer getResto() {		return resto;	}
	public void setResto(Integer resto) {		this.resto = resto;	}
	
}
