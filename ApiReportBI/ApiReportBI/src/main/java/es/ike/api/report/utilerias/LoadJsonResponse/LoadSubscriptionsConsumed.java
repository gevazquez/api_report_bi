package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.ConsumosDeSuscripcionesEntity;
import es.ike.api.report.utilerias.JsonUtility;

public class LoadSubscriptionsConsumed {
	public static void SubscriptionsConsumed(String jsonResponse, String country) throws Exception {
		// Se carga jsonResponse en Arreglo Json
				Integer iError = 0;
				Instant instant = Instant.now();
				// Obtiene configuracion de Entidad
				ApiConfiguration apiConfiguration = null;
				apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("15","15","subscriptions_consumed",country);
				Logger logger = Logger.getLogger("Logger apiReport LoadSubscriptionsConsumed.SubscriptionsConsumed");
				// Se carga jsonResponse en Arreglo Json
				try {
					if (String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")) {
						JSONArray jsonarray = new JSONArray(jsonResponse);
						CloudTableClient tableClient = null;
						tableClient = TableClientProvider.getTableClientReference(country);
						// Se genera la variable tabla, con el nombre de la tabla en AZURE
						CloudTable table = tableClient.getTableReference("SubscriptionsConsumed");
						for (int a = 0; a<jsonarray.length(); a++) {
		            		JSONParser parser=null;
		            		JSONObject json=null;
		            		String StrId="";
		        		    String StrBusinessUnit="";
		        			String StrServiceId="";
		        			String StrCreatedAt="";
		        			String strUpdatedAt="";
		        			parser = new JSONParser();
		                    json = (JSONObject)parser.parse(jsonarray.get(a).toString());
		                    StrId=JsonUtility.JsonValidaExisteLlave(json, "id");
		                    StrBusinessUnit=JsonUtility.JsonValidaExisteLlave(json, "businessUnit");
		                    StrServiceId=JsonUtility.JsonValidaExisteLlave(json, "serviceId");
		                    StrCreatedAt=JsonUtility.JsonValidaExisteLlave(json, "createdAt");
		                    strUpdatedAt=JsonUtility.JsonValidaExisteLlave(json, "updatedAt");
		                    ConsumosDeSuscripcionesEntity consumosDeSuscripcionesEntity = new ConsumosDeSuscripcionesEntity(StrId +"_"+ StrBusinessUnit,StrServiceId );
		                    consumosDeSuscripcionesEntity.setEtag(consumosDeSuscripcionesEntity.getEtag());
		                    consumosDeSuscripcionesEntity.setId(StrId != null ? Integer.parseInt(StrId): null);
		                    consumosDeSuscripcionesEntity.setBusinessUnit(StrBusinessUnit != null ? UUID.fromString(StrBusinessUnit): null);
		                    consumosDeSuscripcionesEntity.setServiceid(StrServiceId != null ? Integer.parseInt(StrServiceId): null);
		                    consumosDeSuscripcionesEntity.setCreatedAt(StrCreatedAt);
		                    consumosDeSuscripcionesEntity.setUpdatedAt(strUpdatedAt);
		                    table.execute(TableOperation.insertOrReplace(consumosDeSuscripcionesEntity));	
						}
					} else {
						iError = 1;
						logger.log(Level.WARNING, "LoadSubscriptionsConsumed.SubscriptionsConsumed= Respuesta No identificada " + country + "=" + jsonResponse);
					}
				} catch (Exception e) {
					iError = 1;
					logger.log(Level.WARNING, "LoadSubscriptionsConsumed.SubscriptionsConsumed= Error al procesar arreglo e insertar en AZURE " + country + "=" + jsonResponse);
					e.printStackTrace();
				}
				// Fin de proceso de Entidad
				try {
					if (iError == 0) {
						// ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
						logger.log(Level.INFO, " Se cargan registros entidad SubscriptionsConsumed con filtro ProcessFilterBu "+ country + "!!!");
					}
				} catch (Exception e) {
					iError = 1;
					logger.log(Level.WARNING, "LoadSubscriptionsConsumed.SubscriptionsConsumed = Error al actualizar Fecha" + country);
					e.printStackTrace();
				}
				if (iError == 0) {
					
							logger.log(Level.INFO," Proceso Carga de Entidad con filtro SubscriptionsConsumed Finalizada " + country + "!!!!");
				}

		}

		}