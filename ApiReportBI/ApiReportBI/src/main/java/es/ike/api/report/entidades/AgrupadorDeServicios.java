package es.ike.api.report.entidades;

import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.azure.data.tables.models.TableServiceException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.AgrupadorDeServiciosEntity;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;
import es.ike.api.report.utilerias.ActualizaFecha;
import es.ike.api.report.utilerias.DatosPaginacion;
import es.ike.api.report.utilerias.Fecha;
import es.ike.api.report.utilerias.JsonUtility;
import es.ike.api.report.utilerias.Paginacion;

public class AgrupadorDeServicios {

	public static void ProcesoEntidadAgrupadorDeServicios(String StrToken, String country)throws Exception,TableServiceException{
		Integer iError=0;
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		Instant instant=Instant.now();
	    apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("43","43","generic_coverages",country);
	    System.out.println(instant.toString()+" Obtiene Configuacion "+country+" de Entidad "+apiConfiguration.getEntidad());
	    //Crea tabla Cases si no existe en Azure
		CloudTableClient tableClient1 = null;
		CreateTableConfiguration tableConfiguration = null;
		tableClient1 = TableClientProvider.getTableClientReference(country);
		tableConfiguration.createTable(tableClient1, "GenericCoverages",country);
		//Obtenemos Token de OAuth de TL
		String token="";
		token=ObtenerOauthTL.OAuthAPI_Report(country);
		System.out.println(instant.toString()+" AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios=Obtiene Token "+country+" "+token);
		//Inserta conteo total
		if(!token.equalsIgnoreCase("")) {
			String StrJSONContoTotal="{"
					+"\n"+"\"query\":\"{"+
					"generic_coverages_aggregate {"+
					"aggregate {"+
					"count"+
						"}"+
					"}"+
	                	"}\""+
					 "}";
			String jsonResponseConteoTotal="";
			String StrConteoTotal="";
			Integer numConteoTotal = 0;
			jsonResponseConteoTotal=obtenerJsonTuten.getJson(token,StrJSONContoTotal,country);
			//Inicia Parseo de conteo Total
		    try {
		    	JSONParser parser = new JSONParser();
		    	JSONObject json=(JSONObject) parser.parse(jsonResponseConteoTotal);
		    	jsonResponseConteoTotal = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
		    	parser=null;
	            json=null;
	            parser = new JSONParser();
	            json = (JSONObject) parser.parse(jsonResponseConteoTotal);
	            jsonResponseConteoTotal=(JsonUtility.validateJsonData(json,"generic_coverages_aggregate"))?json.get("generic_coverages_aggregate").toString():"";
	            parser=null;
                json=null;
                parser = new JSONParser();
                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
                jsonResponseConteoTotal=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
                StrConteoTotal=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
                numConteoTotal = Integer.parseInt(StrConteoTotal);
                try {
                	apiConfiguration.setConteoTotal(numConteoTotal);
                }catch(Exception e) {
                	iError=1;
			    	System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios = Error al cargar conteo total en Azure Entidad AgrupadorDeServicios "+country);
                }
		    }catch(Exception e) {
		    	iError=1;
		    	System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios = Error al parsear JsonResponseConteoTotal de Entidad AgrupadorDeServicios "+country);}
			
		}else {System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios = Problema en obtener el Token del conteo Total "+country);	}
		//Implementación de filtro where
		String StrWhere=Fecha.filtroFecha(apiConfiguration,"id");
		String StrWhereCount="";
		String StrWhereQuery="";
		if (!StrWhere.equalsIgnoreCase("")) {
		   	StrWhereCount="("+StrWhere+")"; }
		if (!StrWhere.equalsIgnoreCase("")) {
		   	StrWhereQuery=","+StrWhere; }
		if(!token.equalsIgnoreCase("")) {
			String StrJSONCount="{"
					+"\n"+"\"query\":\"{"+
					"generic_coverages_aggregate"+StrWhereCount+"{"+
					"aggregate {"+
					"count"+
						"}"+
					"}"+
	                	"}\""+
					 "}";
			//Implementacion de paginación
			try {
				String jsonResponseCount="";
				Integer paginas=0;
				String StrCount="";
				Integer numCount = 0;
				String jsonResponse="";
				Integer numPaginacion=0;
				Integer offset=0;
				jsonResponseCount=obtenerJsonTuten.getJson(token,StrJSONCount,country);
				if(!jsonResponseCount.equalsIgnoreCase("")) { 
			   //---------------------------------------------------------------------
			  //Inicia Parseo de count
			    try {
			    	JSONParser parser = new JSONParser();
			    	JSONObject json=(JSONObject) parser.parse(jsonResponseCount);
			    	jsonResponseCount = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
			    	parser=null;
		            json=null;
		            parser = new JSONParser();
		            json = (JSONObject) parser.parse(jsonResponseCount);
		            
		            jsonResponseCount=(JsonUtility.validateJsonData(json, "generic_coverages_aggregate"))?json.get("generic_coverages_aggregate").toString():"";
		            parser=null;
	                json=null;
	                parser = new JSONParser();
	                json= (JSONObject) parser.parse(jsonResponseCount);
	                jsonResponseCount=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
	                json= (JSONObject) parser.parse(jsonResponseCount);
	                StrCount=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
	                numCount = Integer.parseInt(StrCount);
			    }catch(Exception e) {
			    	iError=1;
			    	System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios = Error al parsear JsonResponseCount de Entidad AgrupadorDeServicios "+country);
			    }
			  //llamada al archivo properties
				Thread currentThread = Thread.currentThread();
				ClassLoader contextClassLoader = currentThread.getContextClassLoader();
				URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+country);
				Properties prop = new Properties();
				if (resource == null) {
					iError=1;
					throw new Exception("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios= No se pudo leer el archivo de configuración TutenLabsconfig/TutenLabsConfig.properties"+country);		}
				try (InputStream is = resource.openStream()) {
					prop.load(is);
				//Se obtiene el numero de paginacion del archivo properties
					numPaginacion=Integer.parseInt(prop.getProperty("intPaginacion"));
				}catch(Exception e) {
					iError=1;
					System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios= Error en obtencion intPaginacion del archivo TutenLabsconfig/TutenLabsConfig.properties"+country);
				}
				//Se llama y ejecuta el metodo calculaPaginacion
				Paginacion paginacion = new Paginacion();
				DatosPaginacion datosPaginacion = null;
				datosPaginacion= paginacion.calculaPaginacion(numCount, numPaginacion);
				if(datosPaginacion.getResto() ==0) { 	paginas=datosPaginacion.getEnteros();;
				}else {	paginas=datosPaginacion.getEnteros()+1;			}
				for(int i =0;i<paginas;i++){
					String StrJSON="{"
							+"\n"+"\"query\":\"{"+
							apiConfiguration.getEntidad()+"(limit:"+numPaginacion+",offset:"+offset+" "+StrWhereQuery+ ")" +"{ "+
							" id"+
							" businessUnit"+
							" serviceAggregatorName"+
							" serviceCategoryId"+
							" materialType"+
							" serviceId"+
							" updatedAt"+
							" createdAt"+
									" }"+
			                	"}\""+
							 "}";
					offset=offset+numPaginacion;
					jsonResponse=obtenerJsonTuten.getJson(StrToken, StrJSON,country); 				    
					if(!jsonResponse.equalsIgnoreCase("")) {
					//Parsear para obtener Arreglo json de Entidad/ 
				    try {
				        JSONParser parser = new JSONParser();
				        JSONObject json=(JSONObject) parser.parse(jsonResponse);
				        jsonResponse = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
				        parser=null;
			            json=null;
			            parser = new JSONParser();
			            json = (JSONObject) parser.parse(jsonResponse);
			            jsonResponse = (JsonUtility.validateJsonData(json,apiConfiguration.getEntidad()))?json.get(apiConfiguration.getEntidad()).toString():"";
					}catch(Exception e) {
						iError=1;
						System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios = Error al parsear JsonResponse de Entidad AgrupadorDeServicios "+country);
						e.printStackTrace();
					}
			        //Se carga jsonResponse en Arreglo Json
			        try {
				    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
			            	JSONArray jsonarray= new JSONArray(jsonResponse);  
			            	CloudTableClient tableClient = null;
			                tableClient = TableClientProvider.getTableClientReference(country);
			              //Se genera la variable tabla, con el nombre de la tabla en AZURE
			                CloudTable table = tableClient.getTableReference("GenericCoverages");
			                for (int a = 0; a<jsonarray.length(); a++) {
			            		JSONParser parser=null;
			            		JSONObject json=null;
			            		String StrId="";
			        		    String StrBusinessUnit="";
			        		    String StrServiceAggregatorName="";
			        		    String StrServiceCategoryId="";
			        		    String StrMaterialType="";
			        		    String StrServiceId="";
			        			String StrUpdatedAt="";
			        			String StrCreatedAt="";
			        			
			        			parser = new JSONParser();
			                    json = (JSONObject)parser.parse(jsonarray.get(a).toString());
			                    StrId=JsonUtility.JsonValidaExisteLlave(json, "id");
			                    StrBusinessUnit=JsonUtility.JsonValidaExisteLlave(json,"businessUnit");
			                    StrServiceAggregatorName=JsonUtility.JsonValidaExisteLlave(json,"serviceAggregatorName");
			                    StrServiceCategoryId=JsonUtility.JsonValidaExisteLlave(json,"serviceCategoryId");
			                    StrMaterialType=JsonUtility.JsonValidaExisteLlave(json,"materialType");
			                    StrServiceId=JsonUtility.JsonValidaExisteLlave(json,"serviceId");
			                    StrUpdatedAt=JsonUtility.JsonValidaExisteLlave(json,"updatedAt");
			                    StrCreatedAt=JsonUtility.JsonValidaExisteLlave(json,"createdAt");

			                    AgrupadorDeServiciosEntity agrupadorDeServiciosEntity = new AgrupadorDeServiciosEntity(StrId+"_"+StrServiceId,StrBusinessUnit);
			                    agrupadorDeServiciosEntity.setEtag(agrupadorDeServiciosEntity.getEtag());
			                    agrupadorDeServiciosEntity.setId(StrId != null ? Integer.parseInt(StrId):null);
			                    agrupadorDeServiciosEntity.setBusinessUnit(StrBusinessUnit != null ? UUID.fromString(StrBusinessUnit):null);
			                    agrupadorDeServiciosEntity.setServiceAggregatorName(StrServiceAggregatorName != null ? StrServiceAggregatorName: "null");
			                    agrupadorDeServiciosEntity.setServiceCategoryId(StrServiceCategoryId != null ? Integer.parseInt(StrServiceCategoryId):0);
			                    agrupadorDeServiciosEntity.setMaterialType(StrMaterialType != null ? StrMaterialType: "null");
			                    agrupadorDeServiciosEntity.setServiceId(StrServiceId != null ? Integer.parseInt(StrServiceId):0);
			                    agrupadorDeServiciosEntity.setUpdatedAt(StrUpdatedAt != null ? StrUpdatedAt: "null");
			                    agrupadorDeServiciosEntity.setCreatedAt(StrCreatedAt != null ? StrCreatedAt: "null");
			                    table.execute(TableOperation.insertOrReplace(agrupadorDeServiciosEntity));			                     
			                }
				    	} else {  	
				    		iError=1;
				    		System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios= Respuesta No identificada "+country+"="+jsonResponse);      }
				    }catch(Exception e) {
				    	iError=1;
				    	System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios= Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse); 
				    	e.printStackTrace();
				    	}
					}else {
						iError=1;
				    	System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios= Error en JSON QUERY GRAPHQL "+country);
					}
				}
			  }else {
				  iError=1;
					System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios = Error en JSON QUERY COUNT "+country);
			  }
			}catch(Exception e) {
				System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios = Error ejecutar paginación "+country);
			}
		    //Fin de proceso de Entidad
			try { 	
				if(iError==0) {
					ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
					System.out.println(instant.toString()+" Actualizacion de Fecha completada "+country+"!!!");
					}
				}
			catch(Exception e) {
				iError=1;
				System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios = Error al actualizar Fecha "+country);
				e.printStackTrace();
			}
			if(iError==0) {
				System.out.println(instant.toString()+" Proceso de Entidad AgrupadorDeServicios Finalizada "+country+"!!!!");}       
		}else {
			System.out.println("AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios= Problema en obtener el Token "+country);
			  }
		}
	}
