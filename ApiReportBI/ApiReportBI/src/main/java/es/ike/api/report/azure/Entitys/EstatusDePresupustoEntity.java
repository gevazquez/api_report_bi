package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class EstatusDePresupustoEntity extends TableServiceEntity{

	// --------------------------------------------------------------------
		public EstatusDePresupustoEntity (String id,String businessUuid) {
			this.partitionKey=id;
			this.rowKey=businessUuid;
			
		}
		// --------------------------------------------------------------------
		public  EstatusDePresupustoEntity() {
			
		}
		
		public Integer budgetId;
		public String status;
		public UUID businessUnit;
		public String createdAt;
		public String updatedAt;
		public Integer getBudgetId() {
			return budgetId;
		}
		public void setBudgetId(Integer budgetId) {
			this.budgetId = budgetId;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public UUID getBusinessUnit() {
			return businessUnit;
		}
		public void setBusinessUnit(UUID businessUnit) {
			this.businessUnit = businessUnit;
		}
		public String getCreatedAt() {
			return createdAt;
		}
		public void setCreatedAt(String createdAt) {
			this.createdAt = createdAt;
		}
		public String getUpdatedAt() {
			return updatedAt;
		}
		public void setUpdatedAt(String updatedAt) {
			this.updatedAt = updatedAt;
		}
		
		public List<String> getColumns(){
			List<String> list = new ArrayList<>();
			list.add("budgetId");
			list.add("serviceId");
			list.add("status");
			list.add("businessUnit");
			list.add("createdAt");
			list.add("updatedAt	");
			return list;
		}
		
		
}
