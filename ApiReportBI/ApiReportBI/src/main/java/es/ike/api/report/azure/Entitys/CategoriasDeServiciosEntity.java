package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class CategoriasDeServiciosEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public CategoriasDeServiciosEntity(String id, String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;

	}

	// --------------------------------------------------------------------
	public CategoriasDeServiciosEntity() {

	}

	// ------------------------------------------------------------------------------------------------------------
	public Integer id;
	public UUID businessUnit;
	public String name;
	public String updatedAt;
	public String createdAt;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public List<String> getColumns() {
		List<String> list = new ArrayList<String>();
		list.add("id");
		list.add("businessUnit");
		list.add("name");
		list.add("updatedAt");
		list.add("createdAt");
		return list;
	}

}
