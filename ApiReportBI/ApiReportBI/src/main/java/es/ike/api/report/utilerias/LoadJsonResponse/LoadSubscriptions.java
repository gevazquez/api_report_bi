package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.SuscripcionesEntity;
import es.ike.api.report.utilerias.JsonUtility;

public class LoadSubscriptions {
	public static void Subscriptions(String jsonResponse, String country) throws Exception {
		// Se carga jsonResponse en Arreglo Json
				Integer iError = 0;
				Instant instant = Instant.now();
				// Obtiene configuracion de Entidad
				ApiConfiguration apiConfiguration = null;
				apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("14","14","subscriptions",country);
				Logger logger = Logger.getLogger("Logger apiReport LoadSubscriptions.Subscriptions");
				//Crea tabla Countries si no existe en Azure
				CloudTableClient tableClient1 = null;
				CreateTableConfiguration tableConfiguration = null;
				tableClient1 = TableClientProvider.getTableClientReference(country);
				tableConfiguration.createTable(tableClient1, "Subscriptions",country);
				// Se carga jsonResponse en Arreglo Json
				try {
					if (String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")) {
						JSONArray jsonarray = new JSONArray(jsonResponse);
						CloudTableClient tableClient = null;
						tableClient = TableClientProvider.getTableClientReference(country);
						// Se genera la variable tabla, con el nombre de la tabla en AZURE
						CloudTable table = tableClient.getTableReference("Subscriptions");
						for (int a = 0; a<jsonarray.length(); a++) {
		            		JSONParser parser=null;
		            		JSONObject jsonSubscriptions=null;
		            		String StrId="";
		            		String StrUuidBusinessUnit="";
		            		String StrPlanName="";
		            		String StrCategory="";
		            		String StrPlanRcu="";
		            		String StrSubscriptionRcu="";
		            		String StrStart="";
		            		String StrEnd="";
		            		String StrCreatedAt="";
		            		String StrUpdatedAt="";
		            		
		            		String StrValidity="";
		            		String StrStatus="";
		            		String StrbeneficiaryIds="";
		            		String StrMaterialIds="";
		            		String StrtitularId="";
		            		String StrPlanSubscriptionStatus="";
		            		String StrPurchaseChannel="";
		            		String StrPlanSubscriptionCoverageStatus="";
		            		String StrContractorId="";
		            		
		            		parser = new JSONParser();
		            		jsonSubscriptions = (JSONObject)parser.parse(jsonarray.get(a).toString());
		            		StrId = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"id" );
		            		StrUuidBusinessUnit = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"businessUnit" );
		            		StrPlanName = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"planName" );
		            		StrCategory = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"category" );
		            		StrPlanRcu = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"planRcu" );
		            		StrSubscriptionRcu = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"subscriptionRcu" );
		            		StrStart = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"start" );
		            		StrEnd = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"end" );
		            		StrCreatedAt = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"createdAt" );
		            		StrUpdatedAt = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"updatedAt" );
		            		
		            		StrValidity = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"validity");
		            		StrStatus = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"status");
		            		StrbeneficiaryIds = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"beneficiaryIds");
		            		StrMaterialIds = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"materialIds");
		            		StrtitularId = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"titularId");
		            		
		            		StrPlanSubscriptionStatus = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"planSubscriptionStatus");
		            		StrPurchaseChannel = JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"purchaseChannel");
		            		StrPlanSubscriptionCoverageStatus= JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"planSubscriptionCoverageStatus");
		            		StrContractorId= JsonUtility.JsonValidaExisteLlave(jsonSubscriptions,"contractorId");
		            		
		            		SuscripcionesEntity suscripcionesEntity = new SuscripcionesEntity(StrId, StrUuidBusinessUnit); //PartitionKey & RowKey
		                    suscripcionesEntity.setEtag(suscripcionesEntity.getEtag());	
		                    suscripcionesEntity.setId(StrId != null ? Integer.parseInt(StrId) : null);
		                    suscripcionesEntity.setBusinessUnit(StrUuidBusinessUnit !=null ? UUID.fromString(StrUuidBusinessUnit):null);
		                    suscripcionesEntity.setPlanName(StrPlanName);
		                    suscripcionesEntity.setCategory(StrCategory);
		                    suscripcionesEntity.setPlanRcu(StrPlanRcu);
		                    suscripcionesEntity.setSubscriptionRcu(StrSubscriptionRcu);
		                    suscripcionesEntity.setStart(StrStart);
		                    suscripcionesEntity.setEnd(StrEnd);
		                    suscripcionesEntity.setCreatedAt(StrCreatedAt  !=null ? StrCreatedAt:null);
		                    suscripcionesEntity.setUpdatedAt(StrUpdatedAt !=null ? StrUpdatedAt:null);
		                    
		                    suscripcionesEntity.setValidity(StrValidity != null ? Boolean.valueOf(StrValidity) : null);
		                    suscripcionesEntity.setStatus(StrStatus != null ? Boolean.valueOf(StrStatus) : null);
		                    suscripcionesEntity.setBeneficiaryIds(StrbeneficiaryIds);
		                    suscripcionesEntity.setMaterialIds(StrMaterialIds);
		                    suscripcionesEntity.setTitularId(StrtitularId != null ? Integer.parseInt(StrtitularId):null);
		                    
		                    suscripcionesEntity.setPlanSubscriptionStatus(StrPlanSubscriptionStatus  != null ? Boolean.valueOf(StrPlanSubscriptionStatus): false);
		                    suscripcionesEntity.setPurchaseChannel(StrPurchaseChannel != null ? StrPurchaseChannel: "null");
		                    suscripcionesEntity.setPlanSubscriptionCoverageStatus(StrPlanSubscriptionCoverageStatus != null ? StrPlanSubscriptionCoverageStatus: "null");
		                    suscripcionesEntity.setContractorId(StrContractorId != null ? Integer.parseInt(StrContractorId): 0);
		                    table.execute(TableOperation.insertOrReplace(suscripcionesEntity));
						}
					} else {
						iError = 1;
						logger.log(Level.WARNING, "LoadSubscriptions.Subscriptions= Respuesta No identificada " + country + "=" + jsonResponse);
					}
				} catch (Exception e) {
					iError = 1;
					logger.log(Level.WARNING, "LoadSubscriptions.Subscriptions= Error al procesar arreglo e insertar en AZURE " + country + "=" + jsonResponse);
					e.printStackTrace();
				}
				// Fin de proceso de Entidad
				try {
					if (iError == 0) {
						// ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
						logger.log(Level.INFO, " Se cargan registros entidad Subscriptions con filtro ProcessFilterBu "+ country + "!!!");
					}
				} catch (Exception e) {
					iError = 1;
					logger.log(Level.WARNING, "LoadSubscriptions.Subscriptions = Error al actualizar Fecha" + country);
					e.printStackTrace();
				}
				if (iError == 0) {
					
							logger.log(Level.INFO," Proceso Carga de Entidad con filtro Subscriptions Finalizada " + country + "!!!!");
				}

		}

		}
