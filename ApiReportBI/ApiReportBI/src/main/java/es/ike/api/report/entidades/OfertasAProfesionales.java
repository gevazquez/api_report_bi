package es.ike.api.report.entidades;

import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.azure.data.tables.models.TableServiceException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.OfertasAProfesionalesEntity;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;
import es.ike.api.report.utilerias.ActualizaFecha;
import es.ike.api.report.utilerias.DatosPaginacion;
import es.ike.api.report.utilerias.Fecha;
import es.ike.api.report.utilerias.JsonUtility;
import es.ike.api.report.utilerias.Paginacion;

public class OfertasAProfesionales {
	public static void ProcesoEntidadOfertasAProfesionales (String StrToken , String country) throws Exception,TableServiceException{
		Integer iError=0;
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		Instant instant=Instant.now();
	    apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("20","20","professional_offers",country);
	    System.out.println(instant.toString()+" Obtiene Configuacion "+country+" de Entidad "+apiConfiguration.getEntidad());
	  //Crea tabla CustomersUS si no existe en Azure
	  	CloudTableClient tableClient1 = null;
	  	CreateTableConfiguration tableConfiguration = null;
	  	tableClient1 = TableClientProvider.getTableClientReference(country);
	  	tableConfiguration.createTable(tableClient1, "ProfessionalOffers",country);
		//Obtenemos Token de OAuth de TL
		String token="";
		token=ObtenerOauthTL.OAuthAPI_Report(country);
		System.out.println(instant.toString()+" OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales =Obtiene Token "+country+" "+token);
		//Inserta conteo total
				if(!token.equalsIgnoreCase("")) {
					String StrJSONContoTotal="{"
							+"\n"+"\"query\":\"{"+
							"professional_offers_aggregate {"+
							"aggregate {"+
							"count"+
								"}"+
							"}"+
			                	"}\""+
							 "}";
					String jsonResponseConteoTotal="";
					String StrConteoTotal="";
					Integer numConteoTotal = 0;
					jsonResponseConteoTotal=obtenerJsonTuten.getJson(token,StrJSONContoTotal,country);
					//Inicia Parseo de conteo Total
				    try {
				    	JSONParser parser = new JSONParser();
				    	JSONObject json=(JSONObject) parser.parse(jsonResponseConteoTotal);
				    	jsonResponseConteoTotal = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
				    	parser=null;
			            json=null;
			            parser = new JSONParser();
			            json = (JSONObject) parser.parse(jsonResponseConteoTotal);
			            jsonResponseConteoTotal=(JsonUtility.validateJsonData(json,"professional_offers_aggregate"))?json.get("professional_offers_aggregate").toString():"";
			            parser=null;
		                json=null;
		                parser = new JSONParser();
		                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
		                jsonResponseConteoTotal=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
		                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
		                StrConteoTotal=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
		                numConteoTotal = Integer.parseInt(StrConteoTotal);
		                try {
		                	apiConfiguration.setConteoTotal(numConteoTotal);
		                }catch(Exception e) {
		                	iError=1;
					    	System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Error al cargar conteo total en Azure Entidad OfertasAProfesionales "+country);
		                }
				    }catch(Exception e) {
				    	iError=1;
				    	System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Error al parsear JsonResponseConteoTotal de Entidad OfertasAProfesionales "+country);}
					
				}else {System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Problema en obtener el Token del conteo Total "+country);	}
		//Implementación de filtro where
		String StrWhere=Fecha.filtroFecha(apiConfiguration, "id");
		String StrWhereCount="";
		String StrWhereQuery="";
	    if (!StrWhere.equalsIgnoreCase("")) {
	    	StrWhereCount="("+StrWhere+")"; }
	    if (!StrWhere.equalsIgnoreCase("")) {
	    	StrWhereQuery=","+StrWhere; }
		if(!token.equalsIgnoreCase("")) {
			String StrJSONCount="{"
									+"\n"+"\"query\":\"{"+
													"professional_offers_aggregate "+StrWhereCount+" {"+
														"aggregate {"+
															" count"+
															"}"+
														"}"+
													"}\""+
								"}";
			//Implementacion de paginación
			try {
				String jsonResponseCount="";
				Integer paginas=0;
				String StrCount="";
				Integer numCount = 0;
				String jsonResponse="";
				Integer numPaginacion=0;
				Integer offset=0;
				jsonResponseCount=obtenerJsonTuten.getJson(token,StrJSONCount,country);
				if(!jsonResponseCount.equalsIgnoreCase("")) { 
				//---------------------------------------------------------------------
				//Inicia Parseo de count
				try {
			    	 JSONParser parser = new JSONParser();
				    	JSONObject json=(JSONObject) parser.parse(jsonResponseCount);
				    	jsonResponseCount = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
				    	parser=null;
			            json=null;
			            parser = new JSONParser();
			            json = (JSONObject) parser.parse(jsonResponseCount);
			            jsonResponseCount=(JsonUtility.validateJsonData(json, "professional_offers_aggregate"))?json.get("professional_offers_aggregate").toString():"";
			            parser=null;
		                json=null;
		                parser = new JSONParser();
		                json= (JSONObject) parser.parse(jsonResponseCount);
		                jsonResponseCount=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
		                json= (JSONObject) parser.parse(jsonResponseCount);
		                StrCount=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
		                numCount = Integer.parseInt(StrCount);
			     }catch(Exception e) {
			    	 iError=1;
			    	 System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Error al parsear JsonResponseCount de Entidad OfertasAProfesionales "+country);}
					//llamada al archivo properties
					Thread currentThread = Thread.currentThread();
					ClassLoader contextClassLoader = currentThread.getContextClassLoader();
					URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+country);
					Properties prop = new Properties();
					if (resource == null) {
						iError=1;
						throw new Exception("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = No se pudo leer el archivo de configuración TutenLabsconfig/TutenLabsConfig.properties"+country);		}
					try (InputStream is = resource.openStream()) {
						prop.load(is);
					//Se obtiene el numero de paginacion del archivo properties
						numPaginacion=Integer.parseInt(prop.getProperty("intPaginacion"));
					}catch(Exception e) {
						iError=1;
						System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Error en obtencion intPaginacion del archivo TutenLabsconfig/TutenLabsConfig.properties"+country);
					}	
					//Se llama y ejecuta el metodo calculaPaginacion
					Paginacion paginacion = new Paginacion();
					DatosPaginacion datosPaginacion = null;
					datosPaginacion= paginacion.calculaPaginacion(numCount, numPaginacion);
					if(datosPaginacion.getResto() ==0) { 	paginas=datosPaginacion.getEnteros();;
					}else {	paginas=datosPaginacion.getEnteros()+1;			}
					for(int i =0;i<paginas;i++){
						String StrJSON="{"
								+"\n"+"\"query\":\"{"+
								apiConfiguration.getEntidad()+"(limit:"+numPaginacion+",offset:"+offset+" "+StrWhereQuery+")" +"{ "+
								" id"+
								" serviceId"+
								" businessUnit"+
								" professionalId"+
								" name"+
								" lastName"+
								" acceptedAt"+
								" rejectedAt"+
								" validUntil"+
								" createdAt"+
								" updatedAt"+
										" }"+
				                	"}\""+
								 "}";
						offset=offset+numPaginacion;
						jsonResponse=obtenerJsonTuten.getJson(StrToken, StrJSON,country);
						if(!jsonResponse.equalsIgnoreCase("")) {
					  //Parsear para obtener Arreglo json de Entidad/
					    try {
							JSONParser parser = new JSONParser();
					        JSONObject json=(JSONObject) parser.parse(jsonResponse);
					        jsonResponse = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
					        parser=null;
				            json=null;
				            parser = new JSONParser();
				            json = (JSONObject) parser.parse(jsonResponse);
				            jsonResponse = (JsonUtility.validateJsonData(json,apiConfiguration.getEntidad()))?json.get(apiConfiguration.getEntidad()).toString():"";	
						}catch(Exception e) {
							iError=1;
							System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Error al parsear JsonResponse de Entidad OfertasAProfesionales "+country);
							e.printStackTrace();
						}	
						//Se carga jsonResponse en Arreglo Json
						   try {
							   if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
								   JSONArray jsonarray= new JSONArray(jsonResponse);  
					            	CloudTableClient tableClient = null;
					            	tableClient = TableClientProvider.getTableClientReference(country);
					            	CloudTable table = tableClient.getTableReference("ProfessionalOffers");
					            	for (int a = 0; a<jsonarray.length(); a++) {
					                	JSONParser parser=null;
					            		JSONObject json=null;
					            		String strId="";
					            		String strServiceId="";
					            		String strBusinessUnit="";
					            		String strProfessionalId="";
					            		String strName="";
					            		String strLastName="";
					            		String strAcceptedAt="";
					            		String strRejectedAt="";
					            		String strValidUntil="";
					            		String strCreatedAt="";
					            		String strUpdatedAt="";
					            		
					            		parser = new JSONParser();
					                    json = (JSONObject)parser.parse(jsonarray.get(a).toString());
					                    strId=JsonUtility.JsonValidaExisteLlave(json,"id");
					                    strServiceId=JsonUtility.JsonValidaExisteLlave(json,"serviceId");
					                    strBusinessUnit=JsonUtility.JsonValidaExisteLlave(json,"businessUnit");
					                    strProfessionalId=JsonUtility.JsonValidaExisteLlave(json,"professionalId");
					                    strName=JsonUtility.JsonValidaExisteLlave(json, "name");
					                    strLastName=JsonUtility.JsonValidaExisteLlave(json, "lastName");
					                    strAcceptedAt=JsonUtility.JsonValidaExisteLlave(json, "acceptedAt");
					                    strRejectedAt=JsonUtility.JsonValidaExisteLlave(json, "rejectedAt");
					                    strValidUntil=JsonUtility.JsonValidaExisteLlave(json, "validUntil");
					                    strCreatedAt=JsonUtility.JsonValidaExisteLlave(json, "createdAt");
					                    strUpdatedAt=JsonUtility.JsonValidaExisteLlave(json, "updatedAt");
					                    
					                    OfertasAProfesionalesEntity ofertasAProfesionalesEntity = new OfertasAProfesionalesEntity(strId,strBusinessUnit);
					                    ofertasAProfesionalesEntity.setEtag(ofertasAProfesionalesEntity.getEtag());
					                    ofertasAProfesionalesEntity.setId(strId != null ? Integer.parseInt(strId): null);
					                    ofertasAProfesionalesEntity.setServiceId(strServiceId != null ? Integer.parseInt(strServiceId): null);
					                    ofertasAProfesionalesEntity.setBusinessUnit(strBusinessUnit != null ? UUID.fromString(strBusinessUnit): null);
					                    ofertasAProfesionalesEntity.setProfessionalId(strProfessionalId != null ? Integer.parseInt(strProfessionalId): null);
					                    ofertasAProfesionalesEntity.setName(strName);
					                    ofertasAProfesionalesEntity.setLastName(strLastName);
					                    ofertasAProfesionalesEntity.setAcceptedAt(strAcceptedAt   != null ? strAcceptedAt : "null");
					                    ofertasAProfesionalesEntity.setRejectedAt(strRejectedAt != null ? strRejectedAt : "null");
					                    ofertasAProfesionalesEntity.setValidUntil(strValidUntil);
					                    ofertasAProfesionalesEntity.setCreatedAt(strCreatedAt);
					                    ofertasAProfesionalesEntity.setUpdatedAt(strUpdatedAt);
					                    table.execute(TableOperation.insertOrReplace(ofertasAProfesionalesEntity));
					            	}
							   }else {
								   iError=1;
								   System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Respuesta No identificada "+country+"="+jsonResponse);}
						   }catch (Exception e){
							   iError=1;
							   System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse);
							   e.printStackTrace();
						   }						   	   
						}else {
							iError=1;
					    	System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Error en JSON QUERY GRAPHQL "+country);
						}
					}
				}else {
					iError=1;
					System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Error en JSON QUERY COUNT "+country);
				}
			}catch (Exception e) {
				iError=1;
				System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Error ejecutar paginación "+country);
				e.printStackTrace();
				}
					 //Fin de proceso de Entidad
			try { 	
				if(iError==0) {
					ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
					System.out.println(instant.toString()+" Actualizacion de Fecha completada "+country+"!!!");
					}
				}
			catch(Exception e) {
				iError=1;
				System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Error al actualizar Fecha "+country);
				e.printStackTrace();
			}
			if(iError==0) {
				System.out.println(instant.toString()+" Proceso de Entidad OfertasAProfesionales Finalizada "+country+"!!!!");}
				}else {	System.out.println("OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales = Problema en obtener el Token "+country);}
	}
}