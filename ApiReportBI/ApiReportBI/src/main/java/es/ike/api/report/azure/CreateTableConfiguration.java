package es.ike.api.report.azure;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.sql.Date;
import java.text.ParseException;
import java.time.Instant;


import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.Entitys.ApiConfiguration;

public class CreateTableConfiguration {

	public static CloudTable createTable(CloudTableClient tableClient, String tableName, String country)
			throws StorageException, RuntimeException, IOException, InvalidKeyException, IllegalArgumentException,
			URISyntaxException, IllegalStateException, ParseException {
		Instant instant = Instant.now();
		Date date = new Date(1583572325000L);

		// Create a new table
		CloudTable table = tableClient.getTableReference(tableName);

		try {// APIConfiguration
			if (tableName != "APIConfiguration" && table.exists() == false) {
				table.create();
				System.out.println(instant.toString() + " CreateTableConfiguration.createTable: Se creo Exitosamente la tabla: " + tableName+" en:"+country);
			} else {
				if (table.exists() == false) {
					table.createIfNotExists();
					System.out.println(instant.toString() + " CreateTableConfiguration.createTable: Se creo Exitosamente la tabla: " + tableName+" en:"+country);

					// Inserta registros
					try {
						// business_units
						ApiConfiguration apiConfiguration = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfiguration.setPartitionKey("1");
						apiConfiguration.setRowKey("1");
						apiConfiguration.setAplicaActualizacionCompleta(0);
						apiConfiguration.setAplicaFiltroFecha(1);
						apiConfiguration.setEntidad("business_units");
						apiConfiguration.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfiguration));
						// users
						ApiConfiguration apiConfigurationU = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationU.setPartitionKey("2");
						apiConfigurationU.setRowKey("2");
						apiConfigurationU.setAplicaActualizacionCompleta(0);
						apiConfigurationU.setAplicaFiltroFecha(1);
						apiConfigurationU.setEntidad("users");
						apiConfigurationU.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationU));
						// customersUS
						ApiConfiguration apiConfigurationC = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationC.setPartitionKey("3");
						apiConfigurationC.setRowKey("3");
						apiConfigurationC.setAplicaActualizacionCompleta(0);
						apiConfigurationC.setAplicaFiltroFecha(1);
						apiConfigurationC.setEntidad("customers");
						apiConfigurationC.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationC));
						// materials
						ApiConfiguration apiConfigurationM = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationM.setPartitionKey("4");
						apiConfigurationM.setRowKey("4");
						apiConfigurationM.setAplicaActualizacionCompleta(0);
						apiConfigurationM.setAplicaFiltroFecha(1);
						apiConfigurationM.setEntidad("materials");
						apiConfigurationM.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationM));
						// cases
						ApiConfiguration apiConfigurationCas = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationCas.setPartitionKey("5");
						apiConfigurationCas.setRowKey("5");
						apiConfigurationCas.setAplicaActualizacionCompleta(0);
						apiConfigurationCas.setAplicaFiltroFecha(1);
						apiConfigurationCas.setEntidad("cases");
						apiConfigurationCas.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationCas));
						// service_categories
						ApiConfiguration apiConfigurationCat = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationCat.setPartitionKey("6");
						apiConfigurationCat.setRowKey("6");
						apiConfigurationCat.setAplicaActualizacionCompleta(0);
						apiConfigurationCat.setAplicaFiltroFecha(1);
						apiConfigurationCat.setEntidad("service_categories");
						apiConfigurationCat.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationCat));
						// definition_services
						ApiConfiguration apiConfigurationDef = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationDef.setPartitionKey("7");
						apiConfigurationDef.setRowKey("7");
						apiConfigurationDef.setAplicaActualizacionCompleta(0);
						apiConfigurationDef.setAplicaFiltroFecha(1);
						apiConfigurationDef.setEntidad("definition_services");
						apiConfigurationDef.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationDef));
						// professionals
						ApiConfiguration apiConfigurationProf = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationProf.setPartitionKey("8");
						apiConfigurationProf.setRowKey("8");
						apiConfigurationProf.setAplicaActualizacionCompleta(0);
						apiConfigurationProf.setAplicaFiltroFecha(1);
						apiConfigurationProf.setEntidad("professionals");
						apiConfigurationProf.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationProf));
						// providers
						ApiConfiguration apiConfigurationProv = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationProv.setPartitionKey("9");
						apiConfigurationProv.setRowKey("9");
						apiConfigurationProv.setAplicaActualizacionCompleta(0);
						apiConfigurationProv.setAplicaFiltroFecha(1);
						apiConfigurationProv.setEntidad("providers");
						apiConfigurationProv.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationProv));
						// countries
						ApiConfiguration apiConfigurationCoun = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationCoun.setPartitionKey("10");
						apiConfigurationCoun.setRowKey("10");
						apiConfigurationCoun.setAplicaActualizacionCompleta(0);
						apiConfigurationCoun.setAplicaFiltroFecha(1);
						apiConfigurationCoun.setEntidad("countries");
						apiConfigurationCoun.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationCoun));
						// cities
						ApiConfiguration apiConfigurationCit = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationCit.setPartitionKey("11");
						apiConfigurationCit.setRowKey("11");
						apiConfigurationCit.setAplicaActualizacionCompleta(0);
						apiConfigurationCit.setAplicaFiltroFecha(1);
						apiConfigurationCit.setEntidad("cities");
						apiConfigurationCit.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationCit));
						// comunes
						ApiConfiguration apiConfigurationCom = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationCom.setPartitionKey("12");
						apiConfigurationCom.setRowKey("12");
						apiConfigurationCom.setAplicaActualizacionCompleta(0);
						apiConfigurationCom.setAplicaFiltroFecha(1);
						apiConfigurationCom.setEntidad("comunes");
						apiConfigurationCom.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationCom));
						// services
						ApiConfiguration apiConfigurationSer = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationSer.setPartitionKey("13");
						apiConfigurationSer.setRowKey("13");
						apiConfigurationSer.setAplicaActualizacionCompleta(0);
						apiConfigurationSer.setAplicaFiltroFecha(1);
						apiConfigurationSer.setEntidad("services");
						apiConfigurationSer.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationSer));
						// subscriptions
						ApiConfiguration apiConfigurationSub = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationSub.setPartitionKey("14");
						apiConfigurationSub.setRowKey("14");
						apiConfigurationSub.setAplicaActualizacionCompleta(0);
						apiConfigurationSub.setAplicaFiltroFecha(1);
						apiConfigurationSub.setEntidad("subscriptions");
						apiConfigurationSub.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationSub));
						// subscriptions_consumed
						ApiConfiguration apiConfigurationSubCon =  new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationSubCon.setPartitionKey("15");
						apiConfigurationSubCon.setRowKey("15");
						apiConfigurationSubCon.setAplicaActualizacionCompleta(0);
						apiConfigurationSubCon.setAplicaFiltroFecha(1);
						apiConfigurationSubCon.setEntidad("subscriptions_consumed");
						apiConfigurationSubCon.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationSubCon));
						// EncuentasDeServicios
						ApiConfiguration apiConfigurationEnc =  new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationEnc.setPartitionKey("16");
						apiConfigurationEnc.setRowKey("16");
						apiConfigurationEnc.setAplicaActualizacionCompleta(0);
						apiConfigurationEnc.setAplicaFiltroFecha(1);
						apiConfigurationEnc.setEntidad("polls");
						apiConfigurationEnc.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationEnc));
						// planes
						ApiConfiguration apiConfigurationPlans = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationPlans.setPartitionKey("17");
						apiConfigurationPlans.setRowKey("17");
						apiConfigurationPlans.setAplicaActualizacionCompleta(0);
						apiConfigurationPlans.setAplicaFiltroFecha(1);
						apiConfigurationPlans.setEntidad("plans");
						apiConfigurationPlans.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationPlans));
						
						// Evaluaciones
						ApiConfiguration apiConfigurationEva =  new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationEva.setPartitionKey("18");
						apiConfigurationEva.setRowKey("18");
						apiConfigurationEva.setAplicaActualizacionCompleta(0);
						apiConfigurationEva.setAplicaFiltroFecha(1);
						apiConfigurationEva.setEntidad("satisfactions");
						apiConfigurationEva.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationEva));
						// Reclamos
						ApiConfiguration apiConfigurationRecla =  new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationRecla.setPartitionKey("19");
						apiConfigurationRecla.setRowKey("19");
						apiConfigurationRecla.setAplicaActualizacionCompleta(0);
						apiConfigurationRecla.setAplicaFiltroFecha(1);
						apiConfigurationRecla.setEntidad("claims");
						apiConfigurationRecla.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationRecla));
						// Ofertas a profecionales
						ApiConfiguration apiConfigurationOprof =  new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationOprof.setPartitionKey("20");
						apiConfigurationOprof.setRowKey("20");
						apiConfigurationOprof.setAplicaActualizacionCompleta(0);
						apiConfigurationOprof.setAplicaFiltroFecha(1);
						apiConfigurationOprof.setEntidad("professional_offers");
						apiConfigurationOprof.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationOprof));
						//PresupuestosDeServicios
						ApiConfiguration apiConfigurationPreDeServ =  new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationPreDeServ.setPartitionKey("21");
						apiConfigurationPreDeServ.setRowKey("21");
						apiConfigurationPreDeServ.setAplicaActualizacionCompleta(0);
						apiConfigurationPreDeServ.setAplicaFiltroFecha(1);
						apiConfigurationPreDeServ.setEntidad("service_budgets");
						apiConfigurationPreDeServ.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationPreDeServ));
						//PagoAlProveedor
						ApiConfiguration apiConfigurationPagAlProv =  new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationPagAlProv.setPartitionKey("22");
						apiConfigurationPagAlProv.setRowKey("22");
						apiConfigurationPagAlProv.setAplicaActualizacionCompleta(0);
						apiConfigurationPagAlProv.setAplicaFiltroFecha(1);
						apiConfigurationPagAlProv.setEntidad("provider_payments");
						apiConfigurationPagAlProv.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationPagAlProv));
						//RespuestasWizard
						ApiConfiguration apiConfigurationRespuestasWizard =  new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationRespuestasWizard.setPartitionKey("23");
						apiConfigurationRespuestasWizard.setRowKey("23");
						apiConfigurationRespuestasWizard.setAplicaActualizacionCompleta(0);
						apiConfigurationRespuestasWizard.setAplicaFiltroFecha(1);
						apiConfigurationRespuestasWizard.setEntidad("wizard_responses");
						apiConfigurationRespuestasWizard.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationRespuestasWizard));
						//Checklists
						ApiConfiguration apiConfigurationChecklists = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationChecklists.setPartitionKey("24");
						apiConfigurationChecklists.setRowKey("24");
						apiConfigurationChecklists.setAplicaActualizacionCompleta(0);
						apiConfigurationChecklists.setEntidad("checklists");
						apiConfigurationChecklists.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationChecklists));
						//ChecklistsItems
						ApiConfiguration apiConfigurationChecklistsItems = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationChecklistsItems.setPartitionKey("25");
						apiConfigurationChecklistsItems.setRowKey("25");
						apiConfigurationChecklistsItems.setAplicaActualizacionCompleta(0);
						apiConfigurationChecklistsItems.setEntidad("checklist_items");
						apiConfigurationChecklistsItems.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationChecklists));
						//CostConcepts
						ApiConfiguration apiConfigurationCostConcepts = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationCostConcepts.setPartitionKey("26");
						apiConfigurationCostConcepts.setRowKey("26");
						apiConfigurationCostConcepts.setAplicaActualizacionCompleta(0);
						apiConfigurationCostConcepts.setEntidad("costConcepts");
						apiConfigurationCostConcepts.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationCostConcepts));
						//Historial de servicios
						ApiConfiguration apiConfigurationHistorial = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationHistorial.setPartitionKey("27");
						apiConfigurationHistorial.setRowKey("27");
						apiConfigurationHistorial.setAplicaActualizacionCompleta(0);
						apiConfigurationHistorial.setEntidad("service_status_change_history_logs");
						apiConfigurationHistorial.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationHistorial));
						
						//Professional_schedules
						ApiConfiguration apiConfigurationPs = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationPs.setPartitionKey("28");
						apiConfigurationPs.setRowKey("28");
						apiConfigurationPs.setAplicaActualizacionCompleta(0);
						apiConfigurationPs.setEntidad("professional_schedules");
						apiConfigurationPs.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationPs));
						//professional_surges
						ApiConfiguration apiConfigurationProSur = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationProSur.setPartitionKey("29");
						apiConfigurationProSur.setRowKey("29");
						apiConfigurationProSur.setAplicaActualizacionCompleta(0);
						apiConfigurationProSur.setEntidad("professional_surges");
						apiConfigurationProSur.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationProSur));
						//Match 
						ApiConfiguration apiConfigurationMatch = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationMatch.setPartitionKey("30");
						apiConfigurationMatch.setRowKey("30");
						apiConfigurationMatch.setAplicaActualizacionCompleta(0);
						apiConfigurationMatch.setEntidad("motor_match");
						apiConfigurationMatch.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationMatch));
						//Wizards 
						ApiConfiguration apiConfigurationWizards = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationWizards.setPartitionKey("31");
						apiConfigurationWizards.setRowKey("31");
						apiConfigurationWizards.setAplicaActualizacionCompleta(0);
						apiConfigurationWizards.setEntidad("wizards");
						apiConfigurationWizards.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationWizards));
						//Estatus de presupuesto
						ApiConfiguration apiConfigurationEp = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationEp.setPartitionKey("32");
						apiConfigurationEp.setRowKey("32");
						apiConfigurationEp.setAplicaActualizacionCompleta(0);
						apiConfigurationEp.setEntidad("budget_status");
						apiConfigurationEp.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationEp));
						//Variables de Wizards
						ApiConfiguration apiConfigurationVw = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationVw.setPartitionKey("33");
						apiConfigurationVw.setRowKey("33");
						apiConfigurationVw.setAplicaActualizacionCompleta(0);
						apiConfigurationVw.setEntidad("wizards_vars");
						apiConfigurationVw.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationVw));
						//Skus de presupuesto
						ApiConfiguration apiConfigurationSkus = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationSkus.setPartitionKey("34");
						apiConfigurationSkus.setRowKey("34");
						apiConfigurationSkus.setAplicaActualizacionCompleta(0);
						apiConfigurationSkus.setEntidad("budget_skus");
						apiConfigurationSkus.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationSkus));
						//Siniestralidad
						ApiConfiguration apiConfigurationCoverageBalances = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationCoverageBalances.setPartitionKey("35");
						apiConfigurationCoverageBalances.setRowKey("35");
						apiConfigurationCoverageBalances.setAplicaActualizacionCompleta(0);
						apiConfigurationCoverageBalances.setEntidad("coverage_balances");
						apiConfigurationCoverageBalances.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationCoverageBalances));
						//Topes
						ApiConfiguration apiConfigurationMaxCove = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationMaxCove.setPartitionKey("36");
						apiConfigurationMaxCove.setRowKey("36");
						apiConfigurationMaxCove.setAplicaActualizacionCompleta(0);
						apiConfigurationMaxCove.setEntidad("maximum_coverages");
						apiConfigurationMaxCove.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationMaxCove));
						//Budgets
						ApiConfiguration apiConfigurationBudgets = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationBudgets.setPartitionKey("37");
						apiConfigurationBudgets.setRowKey("37");
						apiConfigurationBudgets.setAplicaActualizacionCompleta(0);
						apiConfigurationBudgets.setEntidad("budgets");
						apiConfigurationBudgets.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationBudgets));
						//sku_comune_names
						ApiConfiguration apiConfigurationScn = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationScn.setPartitionKey("38");
						apiConfigurationScn.setRowKey("38");
						apiConfigurationScn.setAplicaActualizacionCompleta(0);
						apiConfigurationScn.setEntidad("sku_comune_names");
						apiConfigurationScn.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationScn));
						//claim_associated_services
						ApiConfiguration apiConfigurationClaim = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationClaim.setPartitionKey("39");
						apiConfigurationClaim.setRowKey("39");
						apiConfigurationClaim.setAplicaActualizacionCompleta(0);
						apiConfigurationClaim.setEntidad("claim_associated_services");
						apiConfigurationClaim.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationClaim));
						//Checklist del servicio
						ApiConfiguration apiConfigurationCheckListSer = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationCheckListSer.setPartitionKey("40");
						apiConfigurationCheckListSer.setRowKey("40");
						apiConfigurationCheckListSer.setAplicaActualizacionCompleta(0);
						apiConfigurationCheckListSer.setEntidad("checklist_services");
						apiConfigurationCheckListSer.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationCheckListSer));
						//Pagos de excedentes para servicios
						ApiConfiguration apiConfigurationServicesEx = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationServicesEx.setPartitionKey("41");
						apiConfigurationServicesEx.setRowKey("41");
						apiConfigurationServicesEx.setAplicaActualizacionCompleta(0);
						apiConfigurationServicesEx.setEntidad("service_excess_payments");
						apiConfigurationServicesEx.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationServicesEx));
						//Servicios De Profesionales
						ApiConfiguration apiConfigurationSdp = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationSdp.setPartitionKey("42");
						apiConfigurationSdp.setRowKey("42");
						apiConfigurationSdp.setAplicaActualizacionCompleta(0);
						apiConfigurationSdp.setEntidad("professional_services");
						apiConfigurationSdp.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationSdp));
						//Agrupador de Servicios
						ApiConfiguration apiConfigurationAgrupadorDs = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationAgrupadorDs.setPartitionKey("43");
						apiConfigurationAgrupadorDs.setRowKey("43");
						apiConfigurationAgrupadorDs.setAplicaActualizacionCompleta(0);
						apiConfigurationAgrupadorDs.setEntidad("generic_coverages");
						apiConfigurationAgrupadorDs.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationAgrupadorDs));
						//Coberturas
						ApiConfiguration apiConfigurationAgrupadorCoberturas = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationAgrupadorCoberturas.setPartitionKey("44");
						apiConfigurationAgrupadorCoberturas.setRowKey("44");
						apiConfigurationAgrupadorCoberturas.setAplicaActualizacionCompleta(0);
						apiConfigurationAgrupadorCoberturas.setEntidad("coverages");
						apiConfigurationAgrupadorCoberturas.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationAgrupadorCoberturas));
						//Services_Order
						ApiConfiguration apiConfigurationServicesOrder = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationServicesOrder.setPartitionKey("45");
						apiConfigurationServicesOrder.setRowKey("45");
						apiConfigurationServicesOrder.setAplicaActualizacionCompleta(0);
						apiConfigurationServicesOrder.setEntidad("services_order");
						apiConfigurationServicesOrder.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationServicesOrder));
						//Applicants
						ApiConfiguration apiConfigurationApplicants = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationApplicants.setPartitionKey("46");
						apiConfigurationApplicants.setRowKey("46");
						apiConfigurationApplicants.setAplicaActualizacionCompleta(0);
						apiConfigurationApplicants.setEntidad("applicants");
						apiConfigurationApplicants.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationApplicants));
						//Sucursals
						ApiConfiguration apiConfigurationSucursals = new ApiConfiguration("PartitionKey", "RowKey");
						apiConfigurationSucursals.setPartitionKey("47");
						apiConfigurationSucursals.setRowKey("47");
						apiConfigurationSucursals.setAplicaActualizacionCompleta(0);
						apiConfigurationSucursals.setEntidad("applicants");
						apiConfigurationSucursals.setUltimaFechaActualizacion(date);
						table.execute(TableOperation.insert(apiConfigurationSucursals));
						
						System.out.println(instant.toString() + "---Se inserto datos en Azure");
					} catch (Exception e) {
						System.out.println(instant.toString()
								+ "---CreateTableConfiguration.createTable = Error al insertar registros tabla "
								+ tableName + " en Azure "+country);
					}
				}
			}
		} catch (StorageException s) {
			System.out.println(instant.toString() + "---CreateTableConfiguration.createTable = Error al crear tabla "
					+ tableName + " en Azure "+country);
			if (s.getCause() instanceof java.net.ConnectException) {
				System.out.println(
						"Se detectó una excepción de conexión del cliente. Si se ejecuta con la configuración predeterminada, asegúrese de haber iniciado el emulador de almacenamiento "+country+".");
			}
			throw s;
		}
		return table;
	}
}
