package es.ike.api.report.services.impls;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthRequest;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthResult;
import com.amazonaws.services.cognitoidp.model.AuthFlowType;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import es.ike.api.report.constants.Constants; 
import es.ike.api.report.dto.User;
import es.ike.api.report.services.AuthenticationService;
import es.ike.api.report.services.CognitoUserService;


@Service
public class AuthenticationServiceImpls implements AuthenticationService {

	@Override
	public JsonObject getToken(String username, String password) {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");

		CognitoUserService cognitoUserService = new CognitoUserServiceImpls();
		AWSCognitoIdentityProvider awsCognitoIdentityProvider = cognitoUserService.createIdentityProvider();
		User user = new User(username, password);

		JsonArray jsonArrayDetails = new JsonArray();
		JsonObject jsonReason = new JsonObject();
		JsonObject jsonData = new JsonObject();
		JsonObject jsonResponse = new JsonObject();
		jsonData.addProperty("success", false);

		if (user == null || (user.getUser_name().isEmpty() || user.getUser_name() == null)
				|| (user.getPassword().isEmpty() || user.getPassword() == null)) {
			jsonArrayDetails.add("User data and password are required");
			jsonReason.addProperty("description", "Template's parameter validation encounter errors");
			jsonReason.addProperty("error", "validation_error");

			jsonReason.add("details", jsonArrayDetails);
			jsonData.add("reason", jsonReason);
			jsonResponse.add("data", jsonData);

			jsonResponse.addProperty("code", 400);
			jsonResponse.addProperty("message", "Invalid authentication.");
			return jsonResponse;
		}

		try {
			Map<String, String> params = new HashMap<String, String>();
			params.put("USERNAME", user.getUser_name());
			params.put("PASSWORD", user.getPassword());
			params.put("SECRET_HASH", cognitoUserService.calculateSecretHash(Constants.AWS_COGNITO_CLIENTID,
					Constants.AWS_COGNITO_CLIENT_SECRET, user.getUser_name()));

			AdminInitiateAuthRequest adminInitiateAuthRequest = new AdminInitiateAuthRequest()
					.withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH).withAuthParameters(params)
					.withClientId(Constants.AWS_COGNITO_CLIENTID).withUserPoolId(Constants.AWS_COGNITO_USERPOOLID);

			AdminInitiateAuthResult adminInitiateAuthResult = awsCognitoIdentityProvider
					.adminInitiateAuth(adminInitiateAuthRequest);

			JsonObject tokenJson = new JsonObject();
			tokenJson.addProperty("access_token", adminInitiateAuthResult.getAuthenticationResult().getAccessToken());
			tokenJson.addProperty("refresh_token", adminInitiateAuthResult.getAuthenticationResult().getRefreshToken());
			tokenJson.addProperty("id_token", adminInitiateAuthResult.getAuthenticationResult().getIdToken());
			tokenJson.addProperty("token_type", adminInitiateAuthResult.getAuthenticationResult().getTokenType());
			tokenJson.addProperty("expires_in", adminInitiateAuthResult.getAuthenticationResult().getExpiresIn());

			jsonResponse.add("data", tokenJson);
			jsonResponse.addProperty("code", 200);
			jsonResponse.addProperty("message", "Valid authentication.");
			return jsonResponse;

		} catch (Exception e) {
			jsonReason.addProperty("description", "Parameter validation encounter errors");
			jsonReason.addProperty("error", "parameter_error");
			jsonArrayDetails.add("User data or password are invalid");

			jsonReason.add("details", jsonArrayDetails);
			jsonData.add("reason", jsonReason);
			jsonResponse.add("data", jsonData);

			jsonResponse.addProperty("code", 400);
			jsonResponse.addProperty("message", "Invalid authentication.");
			return jsonResponse;

		}
	}
	
	@Override
	public JsonObject getRefreshToken(String username, String refreshToken) {
		
		CognitoUserService cognitoUserService = new CognitoUserServiceImpls();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");

		AWSCognitoIdentityProvider awsCognitoIdentityProvider = cognitoUserService.createIdentityProvider();
		User user = new User();
		user.setUser_name(username);
		user.setRefreshToken(refreshToken);

		JsonArray jsonArrayDetails = new JsonArray();
		JsonObject jsonReason = new JsonObject();
		JsonObject jsonData = new JsonObject();
		JsonObject jsonResponse = new JsonObject();
		jsonData.addProperty("success", false);
		
		if(user == null || (user.getUser_name() == null || user.getUser_name().isEmpty()) || (user.getRefreshToken() == null || user.getRefreshToken().isEmpty())){
			jsonArrayDetails.add("User data and refresh token are required");
            jsonReason.addProperty("description", "Template's parameter validation encounter errors");
            jsonReason.addProperty("error", "validation_error");

            jsonReason.add("details", jsonArrayDetails);
            jsonData.add("reason", jsonReason);
            jsonResponse.add("data", jsonData);

            jsonResponse.addProperty("code", 400);
            jsonResponse.addProperty("message", "Refresh Token error.");
            return jsonResponse;
			
		}
		
		try {
			Map<String, String> params = new HashMap<String, String>();
            params.put("REFRESH_TOKEN", user.getRefreshToken());
            params.put("SECRET_HASH", cognitoUserService.calculateSecretHash(Constants.AWS_COGNITO_CLIENTID, Constants.AWS_COGNITO_CLIENT_SECRET, user.getUser_name()));
            AdminInitiateAuthRequest adminInitiateAuthRequest = new AdminInitiateAuthRequest()
                    .withAuthFlow(AuthFlowType.REFRESH_TOKEN_AUTH).withAuthParameters(params)
                    .withClientId(Constants.AWS_COGNITO_CLIENTID).withUserPoolId(Constants.AWS_COGNITO_USERPOOLID);

            AdminInitiateAuthResult adminInitiateAuthResult = awsCognitoIdentityProvider.adminInitiateAuth(adminInitiateAuthRequest);

            JsonObject tokenJson = new JsonObject();
            tokenJson.addProperty("access_token", adminInitiateAuthResult.getAuthenticationResult().getAccessToken());
            tokenJson.addProperty("id_token", adminInitiateAuthResult.getAuthenticationResult().getIdToken());
            tokenJson.addProperty("token_type", adminInitiateAuthResult.getAuthenticationResult().getTokenType());
            tokenJson.addProperty("expires_in", adminInitiateAuthResult.getAuthenticationResult().getExpiresIn());
            jsonResponse.add("data", tokenJson);
            jsonResponse.addProperty("code", 200);
            jsonResponse.addProperty("message", "Refresh Token succesfull.");
            return jsonResponse;
		} catch (Exception e) {
			jsonReason.addProperty("description", "Parameter validation encounter errors");
            jsonReason.addProperty("error", "parameter_error");
            jsonArrayDetails.add("User data or refresh token are invalid");

            jsonReason.add("details", jsonArrayDetails);
            jsonData.add("reason", jsonReason);
            jsonResponse.add("data", jsonData);

            jsonResponse.addProperty("code", 400);
            jsonResponse.addProperty("message", "Refresh Token error.");
            return jsonResponse;
		}
	}
	
	
}
