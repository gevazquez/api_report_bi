package es.ike.api.report.services;


import com.google.gson.JsonObject;

public interface AuthenticationService {
	
	public JsonObject getToken(String username, String password);
	public JsonObject getRefreshToken(String username, String refreshToken);
	

}
