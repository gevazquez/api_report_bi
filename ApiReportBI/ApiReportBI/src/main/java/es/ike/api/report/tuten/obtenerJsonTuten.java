package es.ike.api.report.tuten;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.azure.data.tables.models.TableServiceException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.BusinessUnitsEntity;
import es.ike.api.report.utilerias.JsonUtility;

public class obtenerJsonTuten {
	//----------------------------------------------------------
	public static String getJson(String StrToken, String StrJsonQuery, String country) throws Exception {
		String StrSalida = "";
		int intHttpCode = 0;
		URL url = null;
		HttpsURLConnection conn = null;
		StringBuilder response = null;
		byte[] input = null;	
		// Lectura del archivo de configuración con los datos de resources
		Thread currentThread = Thread.currentThread();
		ClassLoader contextClassLoader = currentThread.getContextClassLoader();
		URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+ country);
		Properties prop = new Properties();
		if (resource == null) {			
			throw new Exception("obtenerJsonTuten.getJson= No se pudo leer el archivo de configuración TutenLabsconfig/TutenLabsConfig.properties"+ country);		}
		try (InputStream is = resource.openStream()) {
			prop.load(is);
			url = new URL(prop.getProperty("urlTutenGraphQL"));
			conn = (HttpsURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setInstanceFollowRedirects(false);
			conn.setRequestMethod("POST");
			conn.setUseCaches(false);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("token", StrToken);
			try (OutputStream osTuten = conn.getOutputStream()) {
				input = StrJsonQuery.getBytes("utf-8");
				osTuten.write(input, 0, input.length);
				}
			intHttpCode = conn.getResponseCode();
			if (intHttpCode == java.net.HttpURLConnection.HTTP_OK) {
				// ---------------
				try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"))) {
					String Stroutput;
					response = new StringBuilder();
					while ((Stroutput = in.readLine()) != null) { response.append(Stroutput);}
					StrSalida = response.toString();
					JSONParser  parser = new JSONParser();
					JSONObject json = (JSONObject) parser.parse(response.toString());
					if(JsonUtility.validateJsonData(json,"errors" )) {
						System.out.println("obtenerJsonTuten.getJson = Error al obtener Respuesta JsonInput "+country+" :"+StrJsonQuery);
						System.out.println("obtenerJsonTuten.getJson = Error al obtener Respuesta JsonOutput "+country+" :"+json.toString());
						StrSalida="";
						}
				} catch (Exception e) {
					System.out.println("obtenerJsonTuten.getJson = Error desconocido");
					e.printStackTrace();
					// --------------
				}
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
				String builder = in.readLine();
				System.out.println("obtenerJsonTuten.getJson =Error de Codigo HTTPS - "+builder.toString());
			}
		}catch(Exception e) {
			System.out.println("obtenerJsonTuten.getJson= Error en obtencion de Json Respuesta "+ country);	
			e.printStackTrace();
		}
		return StrSalida;		
	}
	//----------------------------------------------------------
}
