package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.HistorialDeServiciosEntity;
import es.ike.api.report.utilerias.JsonUtility;

public class LoadHistorialDeServicios {
	public static void HistorialDeServicios(String jsonResponse, String country) throws Exception {
		// Se carga jsonResponse en Arreglo Json
				Integer iError = 0;
				Instant instant = Instant.now();
				// Obtiene configuracion de Entidad
				ApiConfiguration apiConfiguration = null;
				apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("27","27","service_status_change_history_logs",country);
				Logger logger = Logger.getLogger("Logger apiReport LoadHistorialDeServicios.HistorialDeServicios");
				//Crea tabla Countries si no existe en Azure
				CloudTableClient tableClient1 = null;
				CreateTableConfiguration tableConfiguration = null;
				tableClient1 = TableClientProvider.getTableClientReference(country);
				tableConfiguration.createTable(tableClient1, "ServiceStatusChangeHistoryLogs",country);
				// Se carga jsonResponse en Arreglo Json
				try {
					if (String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")) {
						JSONArray jsonarray = new JSONArray(jsonResponse);
						CloudTableClient tableClient = null;
						tableClient = TableClientProvider.getTableClientReference(country);
						// Se genera la variable tabla, con el nombre de la tabla en AZURE
						CloudTable table = tableClient.getTableReference("ServiceStatusChangeHistoryLogs");
						for (int a = 0; a<jsonarray.length(); a++) {
		            		JSONParser parser=null;
		            		JSONObject json=null;
		            		String strId="";
		            		String strBusinessUnit="";
		            		String strUserId="";
		            		String strType="";
		            		String strNewState="";
		            		String strPreviousState="";
		            		String strServiceId="";
		            		String strInitialDateAndTime="";
		            		String strCreatedAt="";
		            		String strUpdatedAt="";
		            		
		            		
		            		parser = new JSONParser();
		            		json = (JSONObject)parser.parse(jsonarray.get(a).toString());
		            		strId = JsonUtility.JsonValidaExisteLlave(json,"id" );
		            		strBusinessUnit = JsonUtility.JsonValidaExisteLlave(json,"businessUnit" );
		            		strUserId= JsonUtility.JsonValidaExisteLlave(json,"userId" );
		            		strType= JsonUtility.JsonValidaExisteLlave(json,"type" );
		            		strNewState= JsonUtility.JsonValidaExisteLlave(json,"newState" );
		            		strPreviousState= JsonUtility.JsonValidaExisteLlave(json,"previousState" );
		            		strServiceId = JsonUtility.JsonValidaExisteLlave(json,"serviceId" );
		            		strInitialDateAndTime = JsonUtility.JsonValidaExisteLlave(json,"initialDateAndTime" );
		            	    strCreatedAt = JsonUtility.JsonValidaExisteLlave(json,"createdAt" );
		            	    strUpdatedAt = JsonUtility.JsonValidaExisteLlave(json,"updatedAt" );
		            		
		            		HistorialDeServiciosEntity historialDeServiciosEntity = new HistorialDeServiciosEntity(strId, strBusinessUnit); //PartitionKey & RowKey
		            		historialDeServiciosEntity.setEtag(historialDeServiciosEntity.getEtag());
		            		historialDeServiciosEntity.setId(strId != null ? Integer.parseInt(strId) : null);
		            		historialDeServiciosEntity.setBusinessUnit(strBusinessUnit != null ? UUID.fromString(strBusinessUnit): null);
		            		historialDeServiciosEntity.setUserId(strUserId != null ? Integer.parseInt(strUserId): 0);
		            		historialDeServiciosEntity.setType(strType != null ? strType: "null" );
		            		historialDeServiciosEntity.setNewState(strNewState != null ? strNewState: "null" );
		            		historialDeServiciosEntity.setPreviousState(strPreviousState != null ? strPreviousState: "null" );
		            		historialDeServiciosEntity.setServiceId(strServiceId != null ? Integer.parseInt(strServiceId): 0);
		            		historialDeServiciosEntity.setInitialDateAndTime(strInitialDateAndTime != null ? strInitialDateAndTime: "null");
		            		historialDeServiciosEntity.setCreatedAt(strCreatedAt != null ? strCreatedAt: "null");
		            		historialDeServiciosEntity.setUpdatedAt(strUpdatedAt != null ? strUpdatedAt: "null");
		                    table.execute(TableOperation.insertOrReplace(historialDeServiciosEntity));
						}
					} else {
						iError = 1;
						logger.log(Level.WARNING, "LoadHistorialDeServicios.HistorialDeServicios= Respuesta No identificada " + country + "=" + jsonResponse);
					}
				} catch (Exception e) {
					iError = 1;
					logger.log(Level.WARNING, "LoadHistorialDeServicios.HistorialDeServicios= Error al procesar arreglo e insertar en AZURE " + country + "=" + jsonResponse);
					e.printStackTrace();
				}
				// Fin de proceso de Entidad
				try {
					if (iError == 0) {
						// ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
						logger.log(Level.INFO, " Se cargan registros entidad HistorialDeServicios con filtro ProcessFilterBu "+ country + "!!!");
					}
				} catch (Exception e) {
					iError = 1;
					logger.log(Level.WARNING, "LoadHistorialDeServicios.HistorialDeServicios = Error al actualizar Fecha" + country);
					e.printStackTrace();
				}
				if (iError == 0) {
					
							logger.log(Level.INFO," Proceso Carga de Entidad con filtro HistorialDeServicios Finalizada " + country + "!!!!");
				}

		}

		}