package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ServiciosDeProfesionalesEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public ServiciosDeProfesionalesEntity (String id,String businessUnit) {
		this.partitionKey=id;
		this.rowKey=businessUnit;
		
	}
	// --------------------------------------------------------------------
	public ServiciosDeProfesionalesEntity() {
	
	}
	// ------------------------------------------------------------------------------------------------------------

	public Integer id;
	public UUID businessUnit;
	public String serviceName;
	public Integer professionalId;
	public Integer serviceId;
	public String baseServiceCost;
	public String baseProCost;
	public String expressProCost;
	public String wizardPercentage;
	public String updatedAt;
	public String createdAt;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public Integer getProfessionalId() {
		return professionalId;
	}
	public void setProfessionalId(Integer professionalId) {
		this.professionalId = professionalId;
	}
	public Integer getServiceId() {
		return serviceId;
	}
	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}
	public String getBaseServiceCost() {
		return baseServiceCost;
	}
	public void setBaseServiceCost(String baseServiceCost) {
		this.baseServiceCost = baseServiceCost;
	}
	public String getBaseProCost() {
		return baseProCost;
	}
	public void setBaseProCost(String baseProCost) {
		this.baseProCost = baseProCost;
	}
	public String getExpressProCost() {
		return expressProCost;
	}
	public void setExpressProCost(String expressProCost) {
		this.expressProCost = expressProCost;
	}
	public String getWizardPercentage() {
		return wizardPercentage;
	}
	public void setWizardPercentage(String wizardPercentage) {
		this.wizardPercentage = wizardPercentage;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
	public List<String>  getColumns(){
		List<String> list = new ArrayList<String>();
		list.add("id");
		list.add("businessUnit");
		list.add("serviceName");
		list.add("professionalId");
		list.add("serviceId");
		list.add("baseProCost");
		list.add("expressProCost");
		list.add("wizardPercentage");
		list.add("updatedAt");
		list.add("createdAt");
		return list;
	}

}
