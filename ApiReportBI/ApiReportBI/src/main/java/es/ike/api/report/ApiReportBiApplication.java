package es.ike.api.report;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.microsoft.azure.storage.table.CloudTableClient;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.TableClientProvider;

@SpringBootApplication
@EnableScheduling
public class ApiReportBiApplication  extends SpringBootServletInitializer {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ApiReportBiApplication.class, args);
		
		try {
			CloudTableClient tableClient = null;
			
			
			// Crear una Conexion para interactuar con el AZURE table service Mexico
	        tableClient = TableClientProvider.getTableClientReference("MX");
	        CreateTableConfiguration.createTable(tableClient, "APIConfiguration","MX");
	        
	        CloudTableClient tableClientAR = null;
			
	      //Crear una Conexion para interactuar con el AZURE table service Argentina
	        tableClientAR = TableClientProvider.getTableClientReference("AR");
	        CreateTableConfiguration.createTable(tableClientAR, "APIConfiguration","AR");
	        
	        CloudTableClient tableClientCO = null;
			
	     // Crear una Conexion para interactuar con el AZURE table service Colombia
	        tableClientCO = TableClientProvider.getTableClientReference("CO");
	        CreateTableConfiguration.createTable(tableClientCO, "APIConfiguration","CO");

	        
	        CloudTableClient tableClientCOH = null;
			
	     // Crear una Conexion para interactuar con el AZURE table service ColombiaHasura
	        tableClientCOH = TableClientProvider.getTableClientReference("COH");
	        CreateTableConfiguration.createTable(tableClientCOH, "APIConfiguration","COH");
	        

			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(TimerAplication.class);
	}

}
