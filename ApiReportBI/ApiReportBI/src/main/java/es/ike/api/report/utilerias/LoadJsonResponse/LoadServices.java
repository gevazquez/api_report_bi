package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.ServiciosEntity;
import es.ike.api.report.utilerias.ActualizaFecha;
import es.ike.api.report.utilerias.JsonUtility;

public class LoadServices {
	public static void Services(String jsonResponse,String country ) throws Exception {
		//Se carga jsonResponse en Arreglo Json
		Integer iError=0;
		Instant instant=Instant.now();
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("13","13","services",country);
		Logger logger = Logger.getLogger("Logger apiReport LoadServices.Services");
		//Crea tabla Countries si no existe en Azure
				CloudTableClient tableClient1 = null;
				CreateTableConfiguration tableConfiguration = null;
				tableClient1 = TableClientProvider.getTableClientReference(country);
				tableConfiguration.createTable(tableClient1, "Services",country);
	    try {
	    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
            	JSONArray jsonarray= new JSONArray(jsonResponse);  
            	CloudTableClient tableClient = null;
                tableClient = TableClientProvider.getTableClientReference(country);
                //Se genera la variable tabla, con el nombre de la tabla en AZURE
                CloudTable table = tableClient.getTableReference("Services");
            	for (int a = 0; a<jsonarray.length(); a++) {
            		JSONParser parser=null;
            		JSONObject jsonServices=null;
            		String StrId="";
            		String StrBusinessUnit="";
            		String StrCaseId="";
            		String StrComunaId="";
            		String StrCustomerId="";
            		String StrInitialPrice="";
            		String BooleanIsExpress="";
            		String StrProfessionalId="";
            		String StrProviderId="";
            		String StrServiceTypeId="";
            		String BooleanAssignmentManual="";
            		String StrAcceptedDate="";
            		String StrServiceOrigin="";
            		String StrCancellationDate="";
            		//String StrColorSLA="";
            		//String StrComunaName="";
            		String StrCostLines="";
            		String StrDesiredDate="";
            		//String StrIncidence="";
            		//String StrIsBudget="";
            		//String StrServiceOriginLatitude="";
            		//String StrServiceOriginLongitude="";
            		String StrRealFinishDate="";
            		String StrRealStartDate="";
            		//String StrReasonCancellation="";
            		//String StrReasonClosedService="";
            		String StrRejectedDate="";
            		String StrRescheduleDate="";
            		String StrScheduleDate="";
            		String StrState="";
            		//String StrTimeSLA="";
            		String StrTransferDate="";
            		//String StrUsernameReschedule="";
            		String StrCreatedAt="";
            		String StrUpdatedAt="";
            		String StrAssignmentDate = "";
            		String StrConclusionDate = "";
            		String StrCreationDate = "";
            		String StrProfessionalOnTheWayDate = "";
            		//String StrServiceStatus = "";
            		String StrStartedDate = "";
            		String StrPlannedStartDateTime = "";
            		String StrPlannedFinishDateTime = "";
            		//String StrProfessionalCost = "";
            		String StrBookingDate="";
            		//String StrIsAccepted="";
            		//String StrIsRejected="";
            		//String StrUsernameManualAssignment="";
            		//String StrDetailsIncidence="";
            		String StrManualAssignmentDate="";
            		//String StrDetailsCancellation="";
            		//String StrEmergencyService="";
            		String StrPlanId="";
            		
            		//String StrSubscriptionPlanId="";
            		//String StrServicePurchased="";
            		//String StrServiceDestinationLatitude="";
            		//String StrServiceDestinationLongitude="";
            		//String StrServiceDestination="";
            		
            		
            		parser = new JSONParser();
                    jsonServices = (JSONObject)parser.parse(jsonarray.get(a).toString());
                    StrId = JsonUtility.JsonValidaExisteLlave(jsonServices,"id" );
                    StrBusinessUnit = JsonUtility.JsonValidaExisteLlave(jsonServices,"businessUnit" );
                    StrCaseId = JsonUtility.JsonValidaExisteLlave(jsonServices,"caseId" );
                    StrComunaId = JsonUtility.JsonValidaExisteLlave(jsonServices,"comunaId" );
                    StrCustomerId = JsonUtility.JsonValidaExisteLlave(jsonServices,"customerId" );
            		StrInitialPrice = JsonUtility.JsonValidaExisteLlave(jsonServices,"initialPrice" );
            		BooleanIsExpress = JsonUtility.JsonValidaExisteLlave(jsonServices,"isExpress" );
            		StrProfessionalId = JsonUtility.JsonValidaExisteLlave(jsonServices,"professionalId" );
            		StrProviderId = JsonUtility.JsonValidaExisteLlave(jsonServices,"providerId" );
            		StrServiceTypeId = JsonUtility.JsonValidaExisteLlave(jsonServices,"serviceTypeId" ); 
            		BooleanAssignmentManual = JsonUtility.JsonValidaExisteLlave(jsonServices,"assignmentManual" );
            		StrAcceptedDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"acceptedDate" );
            		StrServiceOrigin = JsonUtility.JsonValidaExisteLlave(jsonServices,"serviceOrigin" );
            		StrCancellationDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"cancellationDate" );
            		//StrColorSLA = JsonUtility.JsonValidaExisteLlave(jsonServices,"colorSLA" );
            		//StrComunaName = JsonUtility.JsonValidaExisteLlave(jsonServices,"comunaName" );
            		StrCostLines = JsonUtility.JsonValidaExisteLlave(jsonServices,"costLines" );
            		StrDesiredDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"desiredDate" );
            		//StrIncidence = JsonUtility.JsonValidaExisteLlave(jsonServices,"incidence" );
            		//StrIsBudget = JsonUtility.JsonValidaExisteLlave(jsonServices,"isBudget" );
            		//StrServiceOriginLatitude = JsonUtility.JsonValidaExisteLlave(jsonServices,"serviceOriginLatitude" );
            		//StrServiceOriginLongitude = JsonUtility.JsonValidaExisteLlave(jsonServices,"serviceOriginLongitude" );
            		StrRealFinishDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"realFinishDate" );
            		StrRealStartDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"realStartDate" );
            		//StrReasonCancellation = JsonUtility.JsonValidaExisteLlave(jsonServices,"reasonCancellation" );
            		//StrReasonClosedService = JsonUtility.JsonValidaExisteLlave(jsonServices,"reasonClosedService" );
            		StrRejectedDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"rejectedDate" );
            		StrRescheduleDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"rescheduleDate" );
            		StrScheduleDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"scheduleDate" );
            		StrState = JsonUtility.JsonValidaExisteLlave(jsonServices,"state" );
            		//StrTimeSLA = JsonUtility.JsonValidaExisteLlave(jsonServices,"timeSLA" );
            		StrTransferDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"transferDate" );
            		//StrUsernameReschedule = JsonUtility.JsonValidaExisteLlave(jsonServices,"usernameReschedule" );
            		StrCreatedAt = JsonUtility.JsonValidaExisteLlave(jsonServices,"createdAt" );
            		StrUpdatedAt = JsonUtility.JsonValidaExisteLlave(jsonServices,"updatedAt" );
            		
            		StrAssignmentDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"assignmentDate" );
            		StrConclusionDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"conclusionDate" );
            		StrCreationDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"creationDate" ); 
            		StrProfessionalOnTheWayDate = JsonUtility.JsonValidaExisteLlave(jsonServices, "professionalOnTheWayDate"); 
            		//StrServiceStatus = JsonUtility.JsonValidaExisteLlave(jsonServices, "serviceStatus");
            		StrStartedDate = JsonUtility.JsonValidaExisteLlave(jsonServices, "startedDate");
            		StrPlannedStartDateTime = JsonUtility.JsonValidaExisteLlave(jsonServices, "plannedStartDateTime");
            		StrPlannedFinishDateTime = JsonUtility.JsonValidaExisteLlave(jsonServices, "plannedFinishDateTime");
            		//StrProfessionalCost = JsonUtility.JsonValidaExisteLlave(jsonServices, "professionalCost");
            		
            		StrBookingDate = JsonUtility.JsonValidaExisteLlave(jsonServices, "bookingDate");
            		//StrIsAccepted = JsonUtility.JsonValidaExisteLlave(jsonServices, "isAccepted");
            		//StrIsRejected = JsonUtility.JsonValidaExisteLlave(jsonServices, "isRejected");
            		//StrUsernameManualAssignment = JsonUtility.JsonValidaExisteLlave(jsonServices, "usernameManualAssignment");
            		//StrDetailsIncidence = JsonUtility.JsonValidaExisteLlave(jsonServices, "detailsIncidence");
            		StrManualAssignmentDate = JsonUtility.JsonValidaExisteLlave(jsonServices, "manualAssignmentDate");
            		//StrDetailsCancellation = JsonUtility.JsonValidaExisteLlave(jsonServices, "detailsCancellation");
            		//StrEmergencyService = JsonUtility.JsonValidaExisteLlave(jsonServices, "emergencyService");
            		StrPlanId = JsonUtility.JsonValidaExisteLlave(jsonServices, "planId");
            		//StrSubscriptionPlanId= JsonUtility.JsonValidaExisteLlave(jsonServices, "subscriptionPlanId");
            		//StrServicePurchased= JsonUtility.JsonValidaExisteLlave(jsonServices, "servicePurchased");
            		//StrServiceDestinationLatitude= JsonUtility.JsonValidaExisteLlave(jsonServices, "serviceDestinationLatitude");
            		//StrServiceDestinationLongitude= JsonUtility.JsonValidaExisteLlave(jsonServices, "serviceDestinationLongitude");
            		//StrServiceDestination= JsonUtility.JsonValidaExisteLlave(jsonServices, "serviceDestination");
            		
            		//---------------------- Asignacion de valores 
            		ServiciosEntity serviciosEntity = new ServiciosEntity(StrId, StrBusinessUnit); //PartitionKey & RowKey
            		serviciosEntity.setEtag(serviciosEntity.getEtag());
            		serviciosEntity.setId(StrId != null ? Integer.parseInt(StrId) : null);
            		serviciosEntity.setBusinessUnit(StrBusinessUnit !=null ? UUID.fromString(StrBusinessUnit):null);
            		serviciosEntity.setCaseId(StrCaseId != null ? StrCaseId : null); 
            		serviciosEntity.setComunaId(StrComunaId != null ? Integer.parseInt(StrComunaId) : null);
            		serviciosEntity.setCustomerId(StrCustomerId != null ? Integer.parseInt(StrCustomerId) : null);
            		serviciosEntity.setInitialPrice(StrInitialPrice);
            		serviciosEntity.setIsExpress(BooleanIsExpress != null ? Boolean.valueOf(BooleanIsExpress) : null);
            		serviciosEntity.setProfessionalId(StrProfessionalId != null ? Integer.parseInt(StrProfessionalId) : null);
            		serviciosEntity.setProviderId(StrProviderId != null ? Integer.parseInt(StrProviderId) : null);
            		serviciosEntity.setServiceTypeId(StrServiceTypeId != null ? Integer.parseInt(StrServiceTypeId) : null);
            		serviciosEntity.setAssignmentManual(BooleanAssignmentManual != null ? Boolean.valueOf(BooleanAssignmentManual) : null);
            		serviciosEntity.setAcceptedDate(StrAcceptedDate);
            		serviciosEntity.setServiceOrigin(StrServiceOrigin != null ? StrServiceOrigin: "null");
            		serviciosEntity.setCancellationDate(StrCancellationDate);
            		//serviciosEntity.setColorSLA(StrColorSLA);
            		//serviciosEntity.setComunaName(StrComunaName);
            		serviciosEntity.setCostLines(StrCostLines);
            		serviciosEntity.setDesiredDate(StrDesiredDate);
            		//serviciosEntity.setIncidence(StrIncidence);
            		//serviciosEntity.setIsBudget(StrIsBudget != null ?  Boolean.valueOf(StrIsBudget) : false);
            		//serviciosEntity.setServiceOriginLatitude(StrServiceOriginLatitude != null ? StrServiceOriginLatitude: "null");
            		//serviciosEntity.setServiceOriginLongitude(StrServiceOriginLongitude != null ? StrServiceOriginLongitude: "null");
            		serviciosEntity.setRealFinishDate(StrRealFinishDate);
            		serviciosEntity.setRealStartDate(StrRealStartDate);
            		//serviciosEntity.setReasonCancellation(StrReasonCancellation);
            		//serviciosEntity.setReasonClosedService(StrReasonClosedService);
            		serviciosEntity.setRejectedDate(StrRejectedDate  != null ? StrRejectedDate : "null");
            		serviciosEntity.setRescheduleDate(StrRescheduleDate);
            		serviciosEntity.setScheduleDate(StrScheduleDate);
            		serviciosEntity.setState(StrState);
            		//serviciosEntity.setTimeSLA(StrTimeSLA != null ? Integer.valueOf(StrTimeSLA) : -1);
            		serviciosEntity.setTransferDate(StrTransferDate);
            		//serviciosEntity.setUsernameReschedule(StrUsernameReschedule);
            		serviciosEntity.setCreatedAt(StrCreatedAt);
            		serviciosEntity.setUpdatedAt(StrUpdatedAt);
            		
            		serviciosEntity.setAssignmentDate(StrAssignmentDate != null ? StrAssignmentDate : null);
            		serviciosEntity.setConclusionDate(StrConclusionDate != null ? StrConclusionDate : null);
            		serviciosEntity.setCreationDate(StrCreationDate);
            		serviciosEntity.setProfessionalOnTheWayDate(StrProfessionalOnTheWayDate != null ? StrProfessionalOnTheWayDate : null);
            		//serviciosEntity.setServiceStatus(StrServiceStatus);
            		serviciosEntity.setStartedDate(StrStartedDate != null ? StrStartedDate : null);
            		serviciosEntity.setPlannedStartDateTime(StrPlannedStartDateTime);
            		serviciosEntity.setPlannedFinishDateTime(StrPlannedFinishDateTime);
            		//serviciosEntity.setProfessionalCost(StrProfessionalCost  != null ? StrProfessionalCost : null);
            		
            		
            		serviciosEntity.setBookingDate(StrBookingDate != null ? StrBookingDate: "null");
            		//serviciosEntity.setIsAccepted(StrIsAccepted != null ? StrIsAccepted: "null");
            		//serviciosEntity.setIsRejected(StrIsRejected != null ? StrIsRejected: "null");
            		//serviciosEntity.setUsernameManualAssignment(StrUsernameManualAssignment != null ? StrUsernameManualAssignment: "null");
            		//serviciosEntity.setDetailsIncidence(StrDetailsIncidence != null ? StrDetailsIncidence: "null");
            		serviciosEntity.setManualAssignmentDate(StrManualAssignmentDate != null ? StrManualAssignmentDate: "null");
            		//serviciosEntity.setDetailsCancellation(StrDetailsCancellation != null ? StrDetailsCancellation: "null");
            		//serviciosEntity.setEmergencyService(StrEmergencyService  != null ?  Boolean.valueOf(StrEmergencyService) : false);
            		serviciosEntity.setPlanId(StrPlanId != null ? Integer.parseInt(StrPlanId): 0);
            		//serviciosEntity.setSubscriptionPlanId(StrSubscriptionPlanId  != null ? Integer.parseInt(StrSubscriptionPlanId): 0);
            		//serviciosEntity.setServicePurchased(StrServicePurchased != null ? Boolean.valueOf(StrServicePurchased) :false);
            		//serviciosEntity.setServiceDestinationLatitude(StrServiceDestinationLatitude != null ? StrServiceDestinationLatitude: "null");
            		//serviciosEntity.setServiceDestinationLongitude(StrServiceDestinationLongitude != null ? StrServiceDestinationLongitude: "null");
            		//serviciosEntity.setServiceDestination(StrServiceDestination != null ? StrServiceDestination: "null");
            		table.execute(TableOperation.insertOrReplace(serviciosEntity));

            		}
	    	} else {
				iError = 1;
				logger.log(Level.WARNING, "LoadServices.Services= Respuesta No identificada " + country + "=" + jsonResponse);
			}
		} catch (Exception e) {
			iError = 1;
			logger.log(Level.WARNING, "LoadServices.Services= Error al procesar arreglo e insertar en AZURE " + country + "=" + jsonResponse);
			e.printStackTrace();
		}
		// Fin de proceso de Entidad
		try {
			if (iError == 0) {
				// ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
				logger.log(Level.INFO, " Se cargan registros entidad Services con filtro ProcessFilterBu "+ country + "!!!");
			}
		} catch (Exception e) {
			iError = 1;
			logger.log(Level.WARNING, "LoadServices.Services = Error al actualizar Fecha" + country);
			e.printStackTrace();
		}
		if (iError == 0) {
			
					logger.log(Level.INFO," Proceso Carga de Entidad con filtro Services Finalizada " + country + "!!!!");
		}

}

}
