package es.ike.api.report.dto;

public class GetTokenDTO {

	private String user_name;
	private String password;
	
	public GetTokenDTO() {}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "GetTokenDTO [user_name=" + user_name + ", password=" + password + "]";
	}
	
	
}
