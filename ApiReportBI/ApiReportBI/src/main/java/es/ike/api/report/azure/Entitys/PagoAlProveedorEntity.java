package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class PagoAlProveedorEntity extends TableServiceEntity{
	public PagoAlProveedorEntity(String id,String businessUuid) {
		this.partitionKey=id;
		this.rowKey=businessUuid;
	}
	public PagoAlProveedorEntity() {
		
	}
	
	public UUID businessUnit;
	public Integer paymentId;
	public String paymentStatus;
	public Integer serviceId;
	public String createdAt;
	public String updatedAt;
	public String servicePrice;
	public String serviceCost;
	

	
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public Integer getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(Integer paymentId) {
		this.paymentId = paymentId;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public Integer getServiceId() {
		return serviceId;
	}
	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getServicePrice() {
		return servicePrice;
	}
	public void setServicePrice(String servicePrice) {
		this.servicePrice = servicePrice;
	}
	public String getServiceCost() {
		return serviceCost;
	}
	public void setServiceCost(String serviceCost) {
		this.serviceCost = serviceCost;
	}

	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("paymentId");
		list.add("businessUnit");
		list.add("paymentStatus");
		list.add("serviceId");
		list.add("servicePrice");
		list.add("serviceCost");
		list.add("createdAt");
		list.add("updatedAt");
		return list;
	}
	
	
	
}
