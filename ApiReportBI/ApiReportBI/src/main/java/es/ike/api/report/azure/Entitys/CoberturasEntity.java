package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

import io.swagger.v3.oas.models.security.SecurityScheme.In;

public class CoberturasEntity extends TableServiceEntity{

	// --------------------------------------------------------------------
		public CoberturasEntity (String id,String businessUnit) {
			this.partitionKey=id;
			this.rowKey=businessUnit;
			
		}
		// --------------------------------------------------------------------
		public CoberturasEntity() {
		
		}
		// ------------------------------------------------------------------------------------------------------------
				public Integer id;
				public UUID businessUnit;
				public Integer planId;
				public String coverageName;
				public String serviceIds;
				public String updatedAt;
				public String createdAt;
				public Double lack;
				public Double deductible;
				public String currency;
				public Integer serviceCategoryId;
				public Integer materialTypeId;
				
			
				public Integer getId() {
					return id;
				}
				public void setId(Integer id) {
					this.id = id;
				}
				public UUID getBusinessUnit() {
					return businessUnit;
				}
				public void setBusinessUnit(UUID businessUnit) {
					this.businessUnit = businessUnit;
				}
				public Integer getPlanId() {
					return planId;
				}
				public void setPlanId(Integer planId) {
					this.planId = planId;
				}
				public String getCoverageName() {
					return coverageName;
				}
				public void setCoverageName(String coverageName) {
					this.coverageName = coverageName;
				}
				public String getServiceIds() {
					return serviceIds;
				}
				public void setServiceIds(String serviceIds) {
					this.serviceIds = serviceIds;
				}
				public String getUpdatedAt() {
					return updatedAt;
				}
				public void setUpdatedAt(String updatedAt) {
					this.updatedAt = updatedAt;
				}
				public String getCreatedAt() {
					return createdAt;
				}
				public void setCreatedAt(String createdAt) {
					this.createdAt = createdAt;
				}
				public Double getLack() {
					return lack;
				}
				public void setLack(Double lack) {
					this.lack = lack;
				}
				public Double getDeductible() {
					return deductible;
				}
				public void setDeductible(Double deductible) {
					this.deductible = deductible;
				}
				public String getCurrency() {
					return currency;
				}
				public void setCurrency(String currency) {
					this.currency = currency;
				}
				public Integer getServiceCategoryId() {
					return serviceCategoryId;
				}
				public void setServiceCategoryId(Integer serviceCategoryId) {
					this.serviceCategoryId = serviceCategoryId;
				}
				public Integer getMaterialTypeId() {
					return materialTypeId;
				}
				public void setMaterialTypeId(Integer materialTypeId) {
					this.materialTypeId = materialTypeId;
				}
				
				public List<String>  getColumns(){
					List<String> list = new ArrayList<String>();
					list.add("id");
					list.add("businessUnit");
					list.add("planId");
					list.add("coverageName");
					list.add("serviceIds");
					list.add("updatedAt");
					list.add("createdAt");
					list.add("lack");
					list.add("deductible");
					list.add("currency");
					list.add("serviceCategoryId");
					list.add("materialTypeId");
					return list;
				}
				
			
				

	}


