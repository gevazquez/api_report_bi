package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class CasosEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public CasosEntity (String id,String businessUnit) {
		this.partitionKey=id;
		this.rowKey=businessUnit;
		
	}
	// --------------------------------------------------------------------
	public CasosEntity() {
	
	}
	// ------------------------------------------------------------------------------------------------------------
			public Integer id;
			public UUID businessUnit;
			public String caseNumber;
			public Integer materiaId;
			public Integer customerId;
			public String statusName;
			public String typeName;
			public String casualtyAt;
			public Integer createdBy;
			public String createdAt;
			public String updatedAt;
			public String externalCode;
			public String correlativeNumber;
			
			
			public Integer getId() {
				return id;
			}
			public void setId(Integer id) {
				this.id = id;
			}
			
			public String getCaseNumber() {
				return caseNumber;
			}
			public void setCaseNumber(String caseNumber) {
				this.caseNumber = caseNumber;
			}
			public Integer getMateriaId() {
				return materiaId;
			}
			public void setMateriaId(Integer materiaId) {
				this.materiaId = materiaId;
			}
			public Integer getCustomerId() {
				return customerId;
			}
			public void setCustomerId(Integer customerId) {
				this.customerId = customerId;
			}
			public String getStatusName() {
				return statusName;
			}
			public void setStatusName(String statusName) {
				this.statusName = statusName;
			}
			public String getTypeName() {
				return typeName;
			}
			public void setTypeName(String typeName) {
				this.typeName = typeName;
			}
			public String getCasualtyAt() {
				return casualtyAt;
			}
			public void setCasualtyAt(String casualtyAt) {
				this.casualtyAt = casualtyAt;
			}
			public Integer getCreatedBy() {
				return createdBy;
			}
			public void setCreatedBy(Integer createdBy) {
				this.createdBy = createdBy;
			}
			public String getCreatedAt() {
				return createdAt;
			}
			public void setCreatedAt(String createdAt) {
				this.createdAt = createdAt;
			}
			public String getUpdatedAt() {
				return updatedAt;
			}
			public void setUpdatedAt(String updatedAt) {
				this.updatedAt = updatedAt;
			}
			public UUID getBusinessUnit() {
				return businessUnit;
			}
			public void setBusinessUnit(UUID businessUnit) {
				this.businessUnit = businessUnit;
			}
				
			public String getExternalCode() {
				return externalCode;
			}
			public void setExternalCode(String externalCode) {
				this.externalCode = externalCode;
			}
			
			public String getCorrelativeNumber() {
				return correlativeNumber;
			}
			public void setCorrelativeNumber(String correlativeNumber) {
				this.correlativeNumber = correlativeNumber;
			}
			public List<String>  getColumns(){
				List<String> list = new ArrayList<String>();
				list.add("id");
				list.add("businessUnit");
				list.add("caseNumber");
				list.add("materiaId");
				list.add("customerId");
				list.add("statusName");
				list.add("typeName");
				list.add("casualtyAt");
				list.add("createdBy");
				list.add("createdAt");
				list.add("updatedAt");
				list.add("externalCode");
				list.add("correlativeNumber");
				return list;
			}
				
			

}
