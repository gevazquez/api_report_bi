package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class AgendaDeProfesionalesEntity extends TableServiceEntity{
	//------------------------------------------------------------------------------------------------------------
	public AgendaDeProfesionalesEntity(String id, String businessUnit) {
		this.partitionKey=id;
		this.rowKey=businessUnit;
	}
	
	//------------------------------------------------------------------------------------------------------------
	public AgendaDeProfesionalesEntity() {
		
	}
	
	public UUID businessUnit;
    public String day;
    public Integer hourFrom;
    public Integer hourTo;
    public Integer id;
    public Integer professionalId;
    public String updatedAt;
	public String createdAt;
	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}


	public Integer getHourFrom() {
		return hourFrom;
	}

	public void setHourFrom(Integer hourFrom) {
		this.hourFrom = hourFrom;
	}

	public Integer getHourTo() {
		return hourTo;
	}

	public void setHourTo(Integer hourTo) {
		this.hourTo = hourTo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProfessionalId() {
		return professionalId;
	}

	public void setProfessionalId(Integer professionalId) {
		this.professionalId = professionalId;
	}
	
	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public List<String> getColumns(){
	List<String> list =  new ArrayList<String>();
	list.add("businessUnit");
	list.add("day");
	list.add("hourFrom");
	list.add("hourTo");
	list.add("id");
	list.add("professionalId");
	list.add("createdAt");
	list.add("updatedAt");
		return list;
	}
    
    

}
