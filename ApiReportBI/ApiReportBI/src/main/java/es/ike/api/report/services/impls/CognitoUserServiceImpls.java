package es.ike.api.report.services.impls;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Service;

import es.ike.api.report.constants.Constants;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.JwkProviderBuilder;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.RSAKeyProvider;

import es.ike.api.report.services.CognitoUserService;

@Service
public class CognitoUserServiceImpls implements CognitoUserService {

	private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";

	@Override
	public String calculateSecretHash(String awsCongitoClientId, String awsCognitoClientSecret, String userName) {
		try {
			SecretKeySpec secret_key_spec = new SecretKeySpec(awsCognitoClientSecret.getBytes(StandardCharsets.UTF_8),
					HMAC_SHA256_ALGORITHM);
			Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
			mac.init(secret_key_spec);
			mac.update(userName.getBytes(StandardCharsets.UTF_8));
			byte[] byte_mac = mac.doFinal(awsCongitoClientId.getBytes(StandardCharsets.UTF_8));
			return Base64.getEncoder().encodeToString(byte_mac);
		} catch (Exception e) {
			throw new RuntimeException("Error while calculating");
		}
	}

	@Override
	public boolean validateToken(String token) {

		try {
			RSAKeyProvider rsaKeyProvider = new RSAKeyProvider() {
				@Override
				public RSAPublicKey getPublicKeyById(String keyId) {
					try {
						URL url = new URL(Constants.AWS_JWKS);
						JwkProvider provider = new JwkProviderBuilder(url).build();
						Jwk jwk = provider.get(keyId);
						return (RSAPublicKey) jwk.getPublicKey();
					} catch (MalformedURLException | JwkException e) {
						e.printStackTrace();
					}
					return null;
				}

				@Override
				public RSAPrivateKey getPrivateKey() {
					return null;
				}

				@Override
				public String getPrivateKeyId() {
					return null;
				}
			};
			Algorithm algorithm = Algorithm.RSA256(rsaKeyProvider);
			JWTVerifier jwtVerifier = JWT.require(algorithm).build();
			DecodedJWT decodedJWT = jwtVerifier.verify(token);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public AWSCognitoIdentityProvider createIdentityProvider() {
		BasicAWSCredentials awsCredentials = new BasicAWSCredentials(Constants.AWS_ACCESS_KEY,
				Constants.AWS_ACCESS_SECRET);
		AWSCognitoIdentityProvider aws_cognito_identity_provider = AWSCognitoIdentityProviderClientBuilder.standard()
				.withRegion(Constants.AWS_REGION).withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
				.build();
		return aws_cognito_identity_provider;
	}
}
