package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ConsumosDeSuscripcionesEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public ConsumosDeSuscripcionesEntity (String id, String businessUnit) {
		this.partitionKey=id  ;
		this.rowKey=businessUnit;
	}
	// --------------------------------------------------------------------
	public ConsumosDeSuscripcionesEntity() {
		
	}
	// ------------------------------------------------------------------------------------------------------------

	public Integer id;
	public UUID businessUnit;
	public Integer serviceid;
	public String createdAt;
    public String updatedAt;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public Integer getServiceid() {
		return serviceid;
	}
	public void setServiceid(Integer serviceid) {
		this.serviceid = serviceid;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
    
    public List<String> getColumns(){
    	List<String> list = new ArrayList<>();
    	list.add("id");
    	list.add("serviceId");
    	list.add("businessUnit");
    	list.add("createdAt");
    	list.add("updatedAt");
    	return list;
    	
    }

}
