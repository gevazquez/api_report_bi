package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class RecargosDeProfesionalesEntity extends TableServiceEntity{
	// --------------------------------------------------------------------
	public RecargosDeProfesionalesEntity(String id, String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;
	}
	// --------------------------------------------------------------------
	public RecargosDeProfesionalesEntity() {
		
	}
	// --------------------------------------------------------------------
	public Integer	id;
	public String description;
	public UUID    businessUnit;
	public Integer professionalId;
	public String surgeAmount;
	public String surgePercent;
	public String surgeDayOfWeek;
	public String surgeDate;
	public String updatedAt;
	public String createdAt;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public Integer getProfessionalId() {
		return professionalId;
	}
	public void setProfessionalId(Integer professionalId) {
		this.professionalId = professionalId;
	}
	public String getSurgeAmount() {
		return surgeAmount;
	}
	public void setSurgeAmount(String surgeAmount) {
		this.surgeAmount = surgeAmount;
	}
	public String getSurgePercent() {
		return surgePercent;
	}
	public void setSurgePercent(String surgePercent) {
		this.surgePercent = surgePercent;
	}
	public String getSurgeDayOfWeek() {
		return surgeDayOfWeek;
	}
	public void setSurgeDayOfWeek(String surgeDayOfWeek) {
		this.surgeDayOfWeek = surgeDayOfWeek;
	}
	public String getSurgeDate() {
		return surgeDate;
	}
	public void setSurgeDate(String surgeDate) {
		this.surgeDate = surgeDate;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}    
	    
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("description");
		list.add("businessUnit");
		list.add("professionalId");
		list.add("surgeAmount");
		list.add("surgePercent");
		list.add("surgeDayOfWeek");
		list.add("surgeDate");
		list.add("updatedAt");
		list.add("createdAt");
		return list;
	}
	    
	    
	    

}
