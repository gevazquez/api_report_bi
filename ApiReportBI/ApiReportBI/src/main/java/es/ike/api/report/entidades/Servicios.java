package es.ike.api.report.entidades;

import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.azure.data.tables.models.TableServiceException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.ProfesionalesEntity;
import es.ike.api.report.azure.Entitys.ServiciosEntity;
import es.ike.api.report.azure.Entitys.SuscripcionesEntity;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;
import es.ike.api.report.utilerias.ActualizaFecha;
import es.ike.api.report.utilerias.DatosPaginacion;
import es.ike.api.report.utilerias.Fecha;
import es.ike.api.report.utilerias.JsonUtility;
import es.ike.api.report.utilerias.Paginacion;

public class Servicios {
	public static void ProcesoEntidadServicios(String StrToken, String country)throws Exception,TableServiceException{
		Integer iError=0;
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		Instant instant=Instant.now();
		apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("13","13","services",country);
		System.out.println(instant.toString()+" Obtiene Configuacion "+country+" de Entidad "+apiConfiguration.getEntidad());
		//Crea tabla Services si no existe en Azure
	  	CloudTableClient tableClient1 = null;
	  	CreateTableConfiguration tableConfiguration = null;
	  	tableClient1 = TableClientProvider.getTableClientReference(country);
	  	tableConfiguration.createTable(tableClient1, "Services",country);
		//Obtenemos Token de OAuth de TL
		String token="";
		token=ObtenerOauthTL.OAuthAPI_Report(country);
		System.out.println(instant.toString()+" Servicios.ProcesoEntidadServicios=Obtiene Token "+token);
		//Inserta conteo total
				if(!token.equalsIgnoreCase("")) {
					String StrJSONContoTotal="{"
							+"\n"+"\"query\":\"{"+
							"services_aggregate {"+
							"aggregate {"+
							"count"+
								"}"+
							"}"+
			                	"}\""+
							 "}";
					String jsonResponseConteoTotal="";
					String StrConteoTotal="";
					Integer numConteoTotal = 0;
					jsonResponseConteoTotal=obtenerJsonTuten.getJson(token,StrJSONContoTotal,country);
					//Inicia Parseo de conteo Total
				    try {
				    	JSONParser parser = new JSONParser();
				    	JSONObject json=(JSONObject) parser.parse(jsonResponseConteoTotal);
				    	jsonResponseConteoTotal = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
				    	parser=null;
			            json=null;
			            parser = new JSONParser();
			            json = (JSONObject) parser.parse(jsonResponseConteoTotal);
			            jsonResponseConteoTotal=(JsonUtility.validateJsonData(json,"services_aggregate"))?json.get("services_aggregate").toString():"";
			            parser=null;
		                json=null;
		                parser = new JSONParser();
		                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
		                jsonResponseConteoTotal=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
		                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
		                StrConteoTotal=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
		                numConteoTotal = Integer.parseInt(StrConteoTotal);
		                try {
		                	apiConfiguration.setConteoTotal(numConteoTotal);
		                }catch(Exception e) {
		                	iError=1;
					    	System.out.println("Servicios.ProcesoEntidadServicios = Error al cargar conteo total en Azure Entidad Servicios "+country);
		                }
				    }catch(Exception e) {
				    	iError=1;
				    	System.out.println("Servicios.ProcesoEntidadServicios = Error al parsear JsonResponseConteoTotal de Entidad Servicios "+country);}
					
				}else {System.out.println("Servicios.ProcesoEntidadServicios= Problema en obtener el Token del conteo Total "+country);	}
		//Implementación de filtro where
		String StrWhere=Fecha.filtroFecha(apiConfiguration, "id");
		String StrWhereCount="";
		String StrWhereQuery="";
		if (!StrWhere.equalsIgnoreCase("")) {
		   	StrWhereCount="("+StrWhere+")"; }
		if (!StrWhere.equalsIgnoreCase("")) {
		   	StrWhereQuery=","+StrWhere; }
		if(!token.equalsIgnoreCase("")) {
			String StrJSONCount="{"
					+"\n"+"\"query\":\"{"+
					"services_aggregate "+StrWhereCount+"{"+
						"aggregate {"+
							" count"+
						"}"+
					"}"+
				"}\""+
	 "}";
			//Implementacion de paginación
			try {
				String jsonResponseCount="";
				Integer paginas=0;
				String StrCount="";
				Integer numCount = 0;
				String jsonResponse="";
				Integer numPaginacion=0;
				Integer offset=0;
				jsonResponseCount=obtenerJsonTuten.getJson(token,StrJSONCount,country);
				if(!jsonResponseCount.equalsIgnoreCase("")) { 
					//---------------------------------------------------------------------
					//Inicia Parseo de count
				    try {
				    	JSONParser parser = new JSONParser();
				    	JSONObject json=(JSONObject) parser.parse(jsonResponseCount);
				    	jsonResponseCount = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
				    	parser=null;
			            json=null;
			            parser = new JSONParser();
			            json = (JSONObject) parser.parse(jsonResponseCount);
			            jsonResponseCount=(JsonUtility.validateJsonData(json,"services_aggregate"))?json.get("services_aggregate").toString():"";
			            parser=null;
		                json=null;
		                parser = new JSONParser();
		                json= (JSONObject) parser.parse(jsonResponseCount);
		                jsonResponseCount=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
		                json= (JSONObject) parser.parse(jsonResponseCount);
		                StrCount=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
		                numCount = Integer.parseInt(StrCount);
				    }catch(Exception e) {
				    	iError=1;
				    	System.out.println("Servicios.ProcesoEntidadServicios = Error al parsear JsonResponseCount de Entidad Servicios "+country);}
				    //llamada al archivo properties
					Thread currentThread = Thread.currentThread();
					ClassLoader contextClassLoader = currentThread.getContextClassLoader();
					URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+country);
					Properties prop = new Properties();
					if (resource == null) {	
						iError=1;
						throw new Exception("Servicios.ProcesoEntidadServicios= No se pudo leer el archivo de configuración TutenLabsconfig/TutenLabsConfig.properties"+country);		}
					try (InputStream is = resource.openStream()) {
						prop.load(is);
					//Se obtiene el numero de paginacion del archivo properties
						numPaginacion=Integer.parseInt(prop.getProperty("intPaginacion"));
					}catch(Exception e) {
						iError=1;
						System.out.println("Servicios.ProcesoEntidadServicios= Error al obtener intPaginacion del archivo TutenLabsconfig/TutenLabsConfig.properties"+country);
					}
					//Se llama y ejecuta el metodo calculaPaginacion
					Paginacion paginacion = new Paginacion();
					DatosPaginacion datosPaginacion = null;
					datosPaginacion= paginacion.calculaPaginacion(numCount, numPaginacion);
					if(datosPaginacion.getResto() ==0) { 	paginas=datosPaginacion.getEnteros();;
					}else {	paginas=datosPaginacion.getEnteros()+1;			}
					for(int i =0;i<paginas;i++){
							String StrJSON="{"
									+"\n"+"\"query\":\"{"+
									apiConfiguration.getEntidad() +"(limit:"+numPaginacion+",offset:"+offset+" "+StrWhereQuery+")" +"{ "+
									" id"+
									" businessUnit"+
									" caseId"+
									" comunaId"+
							        " customerId"+
							        " initialPrice"+
							        " isExpress"+
							        " professionalId"+
							        " providerId"+
							        " serviceTypeId"+
							        " assignmentManual"+
							        
									
							        " acceptedDate"+
									" serviceOrigin"+
									" cancellationDate"+
									//" colorSLA"+
									//" comunaName"+
									" costLines"+
									" desiredDate"+
									//" incidence"+
									//" isBudget"+
									//" serviceOriginLatitude"+
									//" serviceOriginLongitude"+
									" realFinishDate"+
									" realStartDate"+
									//" reasonCancellation"+
									//" reasonClosedService"+
									" rejectedDate"+
									" rescheduleDate"+
									" scheduleDate"+
									" state"+
									//" timeSLA"+
									" transferDate"+
									//" usernameReschedule"+
							        " createdAt"+
							        " updatedAt"+							        
									" assignmentDate"+
									" conclusionDate"+
									" creationDate"+
									" professionalOnTheWayDate"+
									//" serviceStatus"+
									" startedDate"+
									" plannedStartDateTime"+
									" plannedFinishDateTime"+
									//" professionalCost"+
									
									" bookingDate"+
									//" isAccepted"+
									//" isRejected"+
									//" usernameManualAssignment"+
									//" detailsIncidence"+
									" manualAssignmentDate"+
									//" detailsCancellation"+
									//" emergencyService"+
									" planId"+
									//" subscriptionPlanId"+
									//" servicePurchased"+
									//" serviceDestinationLatitude"+
									//" serviceDestinationLongitude"+
									//" serviceDestination"+

											" }"+
					                	"}\""+
									 "}";
							offset=offset+numPaginacion;
							jsonResponse=obtenerJsonTuten.getJson(StrToken, StrJSON,country);  
							if(!jsonResponse.equalsIgnoreCase("")) {
								//Parsear para obtener Arreglo json de Entidad
							    try {
							    	JSONParser parser = new JSONParser();
							        JSONObject json=(JSONObject) parser.parse(jsonResponse);
							        jsonResponse = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
							        parser=null;
						            json=null;
						            parser = new JSONParser();
						            json = (JSONObject) parser.parse(jsonResponse);
						            jsonResponse = (JsonUtility.validateJsonData(json,apiConfiguration.getEntidad()))?json.get(apiConfiguration.getEntidad()).toString():"";
							    }catch(Exception e){
							    	iError=1;
							    	System.out.println("Servicios.ProcesoEntidadServicios = Error al parsear JsonResponse de Entidad Servicios "+country);
							    	e.printStackTrace();
							    	}
								//Se carga jsonResponse en Arreglo Json
							    try {
							    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
						            	JSONArray jsonarray= new JSONArray(jsonResponse);  
						            	CloudTableClient tableClient = null;
						                tableClient = TableClientProvider.getTableClientReference(country);
						                //Se genera la variable tabla, con el nombre de la tabla en AZURE
						                CloudTable table = tableClient.getTableReference("Services");
						            	for (int a = 0; a<jsonarray.length(); a++) {
						            		JSONParser parser=null;
						            		JSONObject jsonServices=null;
						            		String StrId="";
						            		String StrBusinessUnit="";
						            		String StrCaseId="";
						            		String StrComunaId="";
						            		String StrCustomerId="";
						            		String StrInitialPrice="";
						            		String BooleanIsExpress="";
						            		String StrProfessionalId="";
						            		String StrProviderId="";
						            		String StrServiceTypeId="";
						            		String BooleanAssignmentManual="";
						            		String StrAcceptedDate="";
						            		String StrServiceOrigin="";
						            		String StrCancellationDate="";
						            		//String StrColorSLA="";
						            		//String StrComunaName="";
						            		String StrCostLines="";
						            		String StrDesiredDate="";
						            		//String StrIncidence="";
						            		//String StrIsBudget="";
						            		//String StrServiceOriginLatitude="";
						            		//String StrServiceOriginLongitude="";
						            		String StrRealFinishDate="";
						            		String StrRealStartDate="";
						            		//String StrReasonCancellation="";
						            		//String StrReasonClosedService="";
						            		String StrRejectedDate="";
						            		String StrRescheduleDate="";
						            		String StrScheduleDate="";
						            		String StrState="";
						            		//String StrTimeSLA="";
						            		String StrTransferDate="";
						            		//String StrUsernameReschedule="";
						            		String StrCreatedAt="";
						            		String StrUpdatedAt="";
						            		String StrAssignmentDate = "";
						            		String StrConclusionDate = "";
						            		String StrCreationDate = "";
						            		String StrProfessionalOnTheWayDate = "";
						            		//String StrServiceStatus = "";
						            		String StrStartedDate = "";
						            		String StrPlannedStartDateTime = "";
						            		String StrPlannedFinishDateTime = "";
						            		//String StrProfessionalCost = "";
						            		String StrBookingDate="";
						            		//String StrIsAccepted="";
						            		//String StrIsRejected="";
						            		//String StrUsernameManualAssignment="";
						            		//String StrDetailsIncidence="";
						            		String StrManualAssignmentDate="";
						            		//String StrDetailsCancellation="";
						            		//String StrEmergencyService="";
						            		String StrPlanId="";
						            		
						            		//String StrSubscriptionPlanId="";
						            		//String StrServicePurchased="";
						            		//String StrServiceDestinationLatitude="";
						            		//String StrServiceDestinationLongitude="";
						            		//String StrServiceDestination="";
						            		
						            		parser = new JSONParser();
						                    jsonServices = (JSONObject)parser.parse(jsonarray.get(a).toString());
						                    StrId = JsonUtility.JsonValidaExisteLlave(jsonServices,"id" );
						                    StrBusinessUnit = JsonUtility.JsonValidaExisteLlave(jsonServices,"businessUnit" );
						                    StrCaseId = JsonUtility.JsonValidaExisteLlave(jsonServices,"caseId" );
						                    StrComunaId = JsonUtility.JsonValidaExisteLlave(jsonServices,"comunaId" );
						                    StrCustomerId = JsonUtility.JsonValidaExisteLlave(jsonServices,"customerId" );
						            		StrInitialPrice = JsonUtility.JsonValidaExisteLlave(jsonServices,"initialPrice" );
						            		BooleanIsExpress = JsonUtility.JsonValidaExisteLlave(jsonServices,"isExpress" );
						            		StrProfessionalId = JsonUtility.JsonValidaExisteLlave(jsonServices,"professionalId" );
						            		StrProviderId = JsonUtility.JsonValidaExisteLlave(jsonServices,"providerId" );
						            		StrServiceTypeId = JsonUtility.JsonValidaExisteLlave(jsonServices,"serviceTypeId" ); 
						            		BooleanAssignmentManual = JsonUtility.JsonValidaExisteLlave(jsonServices,"assignmentManual" );
						            		StrAcceptedDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"acceptedDate" );
						            		StrServiceOrigin = JsonUtility.JsonValidaExisteLlave(jsonServices,"serviceOrigin" );
						            		StrCancellationDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"cancellationDate" );
						            		//StrColorSLA = JsonUtility.JsonValidaExisteLlave(jsonServices,"colorSLA" );
						            		//StrComunaName = JsonUtility.JsonValidaExisteLlave(jsonServices,"comunaName" );
						            		StrCostLines = JsonUtility.JsonValidaExisteLlave(jsonServices,"costLines" );
						            		StrDesiredDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"desiredDate" );
						            		//StrIncidence = JsonUtility.JsonValidaExisteLlave(jsonServices,"incidence" );
						            		//StrIsBudget = JsonUtility.JsonValidaExisteLlave(jsonServices,"isBudget" );
						            		//StrServiceOriginLatitude = JsonUtility.JsonValidaExisteLlave(jsonServices,"serviceOriginLatitude" );
						            		//StrServiceOriginLongitude = JsonUtility.JsonValidaExisteLlave(jsonServices,"serviceOriginLongitude" );
						            		StrRealFinishDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"realFinishDate" );
						            		StrRealStartDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"realStartDate" );
						            		//StrReasonCancellation = JsonUtility.JsonValidaExisteLlave(jsonServices,"reasonCancellation" );
						            		//StrReasonClosedService = JsonUtility.JsonValidaExisteLlave(jsonServices,"reasonClosedService" );
						            		StrRejectedDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"rejectedDate" );
						            		StrRescheduleDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"rescheduleDate" );
						            		StrScheduleDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"scheduleDate" );
						            		StrState = JsonUtility.JsonValidaExisteLlave(jsonServices,"state" );
						            		//StrTimeSLA = JsonUtility.JsonValidaExisteLlave(jsonServices,"timeSLA" );
						            		StrTransferDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"transferDate" );
						            		//StrUsernameReschedule = JsonUtility.JsonValidaExisteLlave(jsonServices,"usernameReschedule" );
						            		StrCreatedAt = JsonUtility.JsonValidaExisteLlave(jsonServices,"createdAt" );
						            		StrUpdatedAt = JsonUtility.JsonValidaExisteLlave(jsonServices,"updatedAt" );
						            		StrAssignmentDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"assignmentDate" );
						            		StrConclusionDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"conclusionDate" );
						            		StrCreationDate = JsonUtility.JsonValidaExisteLlave(jsonServices,"creationDate" ); 
						            		StrProfessionalOnTheWayDate = JsonUtility.JsonValidaExisteLlave(jsonServices, "professionalOnTheWayDate"); 
						            		//StrServiceStatus = JsonUtility.JsonValidaExisteLlave(jsonServices, "serviceStatus");
						            		StrStartedDate = JsonUtility.JsonValidaExisteLlave(jsonServices, "startedDate");
						            		StrPlannedStartDateTime = JsonUtility.JsonValidaExisteLlave(jsonServices, "plannedStartDateTime");
						            		StrPlannedFinishDateTime = JsonUtility.JsonValidaExisteLlave(jsonServices, "plannedFinishDateTime");
						            		//StrProfessionalCost = JsonUtility.JsonValidaExisteLlave(jsonServices, "professionalCost");
						            		StrBookingDate = JsonUtility.JsonValidaExisteLlave(jsonServices, "bookingDate");
						            		//StrIsAccepted = JsonUtility.JsonValidaExisteLlave(jsonServices, "isAccepted");
						            		//StrIsRejected = JsonUtility.JsonValidaExisteLlave(jsonServices, "isRejected");
						            		//StrUsernameManualAssignment = JsonUtility.JsonValidaExisteLlave(jsonServices, "usernameManualAssignment");
						            		//StrDetailsIncidence = JsonUtility.JsonValidaExisteLlave(jsonServices, "detailsIncidence");
						            		StrManualAssignmentDate = JsonUtility.JsonValidaExisteLlave(jsonServices, "manualAssignmentDate");
						            		//StrDetailsCancellation = JsonUtility.JsonValidaExisteLlave(jsonServices, "detailsCancellation");
						            		//StrEmergencyService = JsonUtility.JsonValidaExisteLlave(jsonServices, "emergencyService");
						            		StrPlanId = JsonUtility.JsonValidaExisteLlave(jsonServices, "planId");
						            		
						            		//StrSubscriptionPlanId= JsonUtility.JsonValidaExisteLlave(jsonServices, "subscriptionPlanId");
						            		//StrServicePurchased= JsonUtility.JsonValidaExisteLlave(jsonServices, "servicePurchased");
						            		//StrServiceDestinationLatitude= JsonUtility.JsonValidaExisteLlave(jsonServices, "serviceDestinationLatitude");
						            		//StrServiceDestinationLongitude= JsonUtility.JsonValidaExisteLlave(jsonServices, "serviceDestinationLongitude");
						            		//StrServiceDestination= JsonUtility.JsonValidaExisteLlave(jsonServices, "serviceDestination");
						            		
						            		
						            		ServiciosEntity serviciosEntity = new ServiciosEntity(StrId, StrBusinessUnit); //PartitionKey & RowKey
						            		serviciosEntity.setEtag(serviciosEntity.getEtag());
						            		serviciosEntity.setId(StrId != null ? Integer.parseInt(StrId) : null);
						            		serviciosEntity.setBusinessUnit(StrBusinessUnit !=null ? UUID.fromString(StrBusinessUnit):null);
						            		serviciosEntity.setCaseId(StrCaseId != null ? StrCaseId : null); 
						            		serviciosEntity.setComunaId(StrComunaId != null ? Integer.parseInt(StrComunaId) : null);
						            		serviciosEntity.setCustomerId(StrCustomerId != null ? Integer.parseInt(StrCustomerId) : null);
						            		serviciosEntity.setInitialPrice(StrInitialPrice);
						            		serviciosEntity.setIsExpress(BooleanIsExpress != null ? Boolean.valueOf(BooleanIsExpress) : null);
						            		serviciosEntity.setProfessionalId(StrProfessionalId != null ? Integer.parseInt(StrProfessionalId) : null);
						            		serviciosEntity.setProviderId(StrProviderId != null ? Integer.parseInt(StrProviderId) : null);
						            		serviciosEntity.setServiceTypeId(StrServiceTypeId != null ? Integer.parseInt(StrServiceTypeId) : null);
						            		serviciosEntity.setAssignmentManual(BooleanAssignmentManual != null ? Boolean.valueOf(BooleanAssignmentManual) : null);
						            		serviciosEntity.setAcceptedDate(StrAcceptedDate);
						            		serviciosEntity.setServiceOrigin(StrServiceOrigin != null ? StrServiceOrigin: "null");
						            		serviciosEntity.setCancellationDate(StrCancellationDate);
						            		//serviciosEntity.setColorSLA(StrColorSLA);
						            		//serviciosEntity.setComunaName(StrComunaName);
						            		serviciosEntity.setCostLines(StrCostLines);
						            		serviciosEntity.setDesiredDate(StrDesiredDate);
						            		//serviciosEntity.setIncidence(StrIncidence);
						            		//serviciosEntity.setIsBudget(StrIsBudget != null ?  Boolean.valueOf(StrIsBudget) : false);
						            		//serviciosEntity.setServiceOriginLatitude(StrServiceOriginLatitude != null ? StrServiceOriginLatitude: "null");
						            		//serviciosEntity.setServiceOriginLongitude(StrServiceOriginLongitude != null ? StrServiceOriginLongitude: "null");
						            		serviciosEntity.setRealFinishDate(StrRealFinishDate);
						            		serviciosEntity.setRealStartDate(StrRealStartDate);
						            		//serviciosEntity.setReasonCancellation(StrReasonCancellation);
						            		//serviciosEntity.setReasonClosedService(StrReasonClosedService);
						            		//serviciosEntity.setRejectedDate(StrRejectedDate  != null ? StrRejectedDate : "null");
						            		serviciosEntity.setRescheduleDate(StrRescheduleDate);
						            		serviciosEntity.setScheduleDate(StrScheduleDate);
						            		serviciosEntity.setState(StrState);
						            		//serviciosEntity.setTimeSLA(StrTimeSLA != null ? Integer.valueOf(StrTimeSLA) : -1);
						            		serviciosEntity.setTransferDate(StrTransferDate);
						            		//serviciosEntity.setUsernameReschedule(StrUsernameReschedule);
						            		serviciosEntity.setCreatedAt(StrCreatedAt);
						            		serviciosEntity.setUpdatedAt(StrUpdatedAt);
						            		serviciosEntity.setAssignmentDate(StrAssignmentDate != null ? StrAssignmentDate : null);
						            		serviciosEntity.setConclusionDate(StrConclusionDate != null ? StrConclusionDate : null);
						            		serviciosEntity.setCreationDate(StrCreationDate);
						            		serviciosEntity.setProfessionalOnTheWayDate(StrProfessionalOnTheWayDate != null ? StrProfessionalOnTheWayDate : null);
						            		//serviciosEntity.setServiceStatus(StrServiceStatus);
						            		serviciosEntity.setStartedDate(StrStartedDate != null ? StrStartedDate : null);
						            		serviciosEntity.setPlannedStartDateTime(StrPlannedStartDateTime);
						            		serviciosEntity.setPlannedFinishDateTime(StrPlannedFinishDateTime);
						            		//serviciosEntity.setProfessionalCost(StrProfessionalCost  != null ? StrProfessionalCost : null);
						            		serviciosEntity.setBookingDate(StrBookingDate != null ? StrBookingDate: "null");
						            		//serviciosEntity.setIsAccepted(StrIsAccepted != null ? StrIsAccepted: "null");
						            		//serviciosEntity.setIsRejected(StrIsRejected != null ? StrIsRejected: "null");
						            		//serviciosEntity.setUsernameManualAssignment(StrUsernameManualAssignment != null ? StrUsernameManualAssignment: "null");
						            		//serviciosEntity.setDetailsIncidence(StrDetailsIncidence != null ? StrDetailsIncidence: "null");
						            		serviciosEntity.setManualAssignmentDate(StrManualAssignmentDate != null ? StrManualAssignmentDate: "null");
						            		//serviciosEntity.setDetailsCancellation(StrDetailsCancellation != null ? StrDetailsCancellation: "null");
						            		//serviciosEntity.setEmergencyService(StrEmergencyService  != null ?  Boolean.valueOf(StrEmergencyService) : false);
						            		serviciosEntity.setPlanId(StrPlanId != null ? Integer.parseInt(StrPlanId): 0);
						            		//serviciosEntity.setSubscriptionPlanId(StrSubscriptionPlanId  != null ? Integer.parseInt(StrSubscriptionPlanId): 0);
						            		//serviciosEntity.setServicePurchased(StrServicePurchased != null ? Boolean.valueOf(StrServicePurchased) :false);
						            		//serviciosEntity.setServiceDestinationLatitude(StrServiceDestinationLatitude != null ? StrServiceDestinationLatitude: "null");
						            		//serviciosEntity.setServiceDestinationLongitude(StrServiceDestinationLongitude != null ? StrServiceDestinationLongitude: "null");
						            		//serviciosEntity.setServiceDestination(StrServiceDestination != null ? StrServiceDestination: "null");
						            		table.execute(TableOperation.insertOrReplace(serviciosEntity));

						            		}
						            }else {
						            	iError=1;
						            	System.out.println("Servicios.ProcesoEntidadServicios= Respuesta No identificada "+country+"="+jsonResponse);      }
							    }catch(Exception e) {
							    	iError=1;
							    	System.out.println("Servicios.ProcesoServicios= Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse);
							    	e.printStackTrace();
							    }	
							}else {
								iError=1;
						    	System.out.println("Servicios.ProcesoEntidadServicios= Error en JSON QUERY GRAPHQL "+country);
							}
					}
				}else {
					iError=1;
					System.out.println("Servicios.ProcesoEntidadServicios = Error en JSON QUERY COUNT "+country);
				}
			}catch(Exception e){
				iError=1;
				System.out.println("Servicios.ProcesoServicios = Error ejecutar paginación "+country);
				e.printStackTrace();
				}
	        //Fin de proceso de Entidad
			try { 	
				if(iError==0) {
					ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
					System.out.println(instant.toString()+" Actualizacion de Fecha completada "+country+"!!!");
					}
				}
			catch(Exception e) {
				iError=1;
				System.out.println("Servicios.ProcesoEntidadServicios = Error al actualizar Fecha "+country);
				e.printStackTrace();
			}
			if(iError==0) {
				System.out.println(instant.toString()+" Proceso de Entidad Servicios Finalizada "+country+"!!!!");}
		}else {System.out.println("Servicios.ProcesoEntidadServicios= Problema en obtener el Token "+country);	}

	}
	
}
