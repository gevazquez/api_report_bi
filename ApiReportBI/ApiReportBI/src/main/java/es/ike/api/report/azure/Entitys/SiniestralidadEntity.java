package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class SiniestralidadEntity extends TableServiceEntity {
	//------------------------------------------------------------------------------------------------------------
	public SiniestralidadEntity(String id, String businessUuid) {
		this.partitionKey = id;
		this.rowKey = businessUuid;  
	}
//------------------------------------------------------------------------------------------------------------
	public SiniestralidadEntity() {		}
//------------------------------------------------------------------------------------------------------------

	public  UUID businessUnit;
    public String createdAt;
    public Integer id;
    public Integer available;
    public String behavior;
    public Integer consumed;
    public Integer coverageId;
    public String coverageName;
    public String coverageStatus;
    public Integer limitAmount;
    public Integer maximumCoverageId;
    public String periodicity;
    public Integer planId;
    public Integer planSubscriptionId;
    public Boolean restrictive;
    public Integer subscriptionId;
    public String unitCode;
    public String updatedAt;

    
	
    
    
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAvailable() {
		return available;
	}
	public void setAvailable(Integer available) {
		this.available = available;
	}
	public String getBehavior() {
		return behavior;
	}
	public void setBehavior(String behavior) {
		this.behavior = behavior;
	}
	public Integer getConsumed() {
		return consumed;
	}
	public void setConsumed(Integer consumed) {
		this.consumed = consumed;
	}
	public Integer getCoverageId() {
		return coverageId;
	}
	public void setCoverageId(Integer coverageId) {
		this.coverageId = coverageId;
	}
	public String getCoverageName() {
		return coverageName;
	}
	public void setCoverageName(String coverageName) {
		this.coverageName = coverageName;
	}
	public String getCoverageStatus() {
		return coverageStatus;
	}
	public void setCoverageStatus(String coverageStatus) {
		this.coverageStatus = coverageStatus;
	}
	public Integer getLimitAmount() {
		return limitAmount;
	}
	public void setLimitAmount(Integer limitAmount) {
		this.limitAmount = limitAmount;
	}
	public Integer getMaximumCoverageId() {
		return maximumCoverageId;
	}
	public void setMaximumCoverageId(Integer maximumCoverageId) {
		this.maximumCoverageId = maximumCoverageId;
	}
	public String getPeriodicity() {
		return periodicity;
	}
	public void setPeriodicity(String periodicity) {
		this.periodicity = periodicity;
	}
	public Integer getPlanId() {
		return planId;
	}
	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
	public Integer getPlanSubscriptionId() {
		return planSubscriptionId;
	}
	public void setPlanSubscriptionId(Integer planSubscriptionId) {
		this.planSubscriptionId = planSubscriptionId;
	}
	public Boolean getRestrictive() {
		return restrictive;
	}
	public void setRestrictive(Boolean restrictive) {
		this.restrictive = restrictive;
	}
	public Integer getSubscriptionId() {
		return subscriptionId;
	}
	public void setSubscriptionId(Integer subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	public String getUnitCode() {
		return unitCode;
	}
	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("businessUnit");
		list.add("available");
		list.add("behavior");
		list.add("consumed");
		list.add("coverageId");
		list.add("coverageName");
		list.add("coverageStatus");
		list.add("limitAmount");
		list.add("maximumCoverageId");
		list.add("periodicity");
		list.add("planId");
		list.add("planSubscriptionId");
		list.add("restrictive");
		list.add("subscriptionId");
		list.add("unitCode");
		list.add("createdAt");
		list.add("updatedAt");
		return list;
	}


}
