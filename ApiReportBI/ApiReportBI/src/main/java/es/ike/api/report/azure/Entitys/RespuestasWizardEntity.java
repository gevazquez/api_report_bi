package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class RespuestasWizardEntity extends TableServiceEntity{
	public RespuestasWizardEntity(String id, String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;
		
	}
	
	public RespuestasWizardEntity() {
		
	}
	
	public UUID businessUnit;
	public Integer wizardId;
	public Integer serviceId;
	public String questionId;
	public String wizardAnswer;
	public String createdAt;
	public String updatedAt;
	public String wizardQuestion;
	
	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}



	public Integer getWizardId() {
		return wizardId;
	}

	public void setWizardId(Integer wizardId) {
		this.wizardId = wizardId;
	}

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	

	public String getWizardAnswer() {
		return wizardAnswer;
	}

	public void setWizardAnswer(String wizardAnswer) {
		this.wizardAnswer = wizardAnswer;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getWizardQuestion() {
		return wizardQuestion;
	}

	public void setWizardQuestion(String wizardQuestion) {
		this.wizardQuestion = wizardQuestion;
	}
	
	
	
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("businessUnit");
		list.add("wizarId");
	    list.add("serviceId");
	    list.add("questionId");
	    list.add("wizardAnswer");
	    list.add("createdAt");
	    list.add("updatedAt");
	    list.add("wizardQuestion");
		return list;
	}

}
