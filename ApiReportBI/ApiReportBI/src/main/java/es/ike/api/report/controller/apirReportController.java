package es.ike.api.report.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;

import es.ike.api.report.entidades.UnidadDeNegocio;
import es.ike.api.report.entidades.Usuarios;
import es.ike.api.report.services.CognitoUserService;
import es.ike.api.report.entidades.Clientes;
import es.ike.api.report.entidades.Materias;
import es.ike.api.report.entidades.AgendaDeProfesionales;
import es.ike.api.report.entidades.Casos;
import es.ike.api.report.entidades.CategoriasDeServicios;
import es.ike.api.report.entidades.Checklists;
import es.ike.api.report.entidades.ChecklistItems;
import es.ike.api.report.entidades.DefinicionDeServicios;
import es.ike.api.report.entidades.Profesionales;
import es.ike.api.report.entidades.Proveedores;
import es.ike.api.report.entidades.Paises;
import es.ike.api.report.entidades.Ciudades;
import es.ike.api.report.entidades.Comunas;
import es.ike.api.report.entidades.Servicios;
import es.ike.api.report.entidades.SkusDePresupuestos;
import es.ike.api.report.entidades.Suscripciones;
import es.ike.api.report.entidades.ConsumosDeSuscripciones;
import es.ike.api.report.entidades.CostConcepts;
import es.ike.api.report.entidades.EncuestasDeServicios;
import es.ike.api.report.entidades.Evaluaciones;
import es.ike.api.report.entidades.Reclamos;
import es.ike.api.report.entidades.OfertasAProfesionales;
import es.ike.api.report.entidades.PresupuestosDeServicios;
import es.ike.api.report.entidades.PagoDelProveedor;
import es.ike.api.report.entidades.RespuestasWizard;
import es.ike.api.report.entidades.RecargosDeProfesionales;
import es.ike.api.report.entidades.Wizards;
import es.ike.api.report.entidades.EstatusDePresupuesto;
import es.ike.api.report.entidades.VariablesWizards;
import es.ike.api.report.entidades.Planes;
import es.ike.api.report.entidades.HistorialDeServicios;
import es.ike.api.report.entidades.Siniestralidad;
import es.ike.api.report.entidades.Topes;
import es.ike.api.report.entidades.Budgets;
import es.ike.api.report.entidades.NombreDeComunaDeSku;
import es.ike.api.report.entidades.ServiciosAsociadosAlReclamo;
import es.ike.api.report.entidades.ChecklistDelServicio;
import es.ike.api.report.entidades.PagosDeExcedentesParaServicios;
import es.ike.api.report.entidades.ServiciosDeProfesionales;
import es.ike.api.report.entidades.AgrupadorDeServicios;
import es.ike.api.report.entidades.Coberturas;
import es.ike.api.report.entidades.Match;
import es.ike.api.report.entidades.ServicesOrder;
import es.ike.api.report.entidades.Solicitantes;
import es.ike.api.report.entidades.Sucursales;
import es.ike.api.report.tuten.ObtenerOauthTL;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
@RequestMapping("/Entidad")
public class apirReportController {
	String token = "";

	@Autowired
	private CognitoUserService cognitoUserService;

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/unidadDeNegocio")
	// De esta manera acepto valores desde la peticion
	public String UnidadNegocio(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				UnidadDeNegocio.ProcesoEntidadUnidadNegocio(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					UnidadDeNegocio.ProcesoEntidadUnidadNegocio(token, "COH");
					return "Ejecución Correcta de Entidad Unidad de Negocio Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Unidad de Negocio " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Unidad de Negocio = Error al ejecutar entidad Unidad de Negocio " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/usuarios")
	public String Usuarios(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				String token = ObtenerOauthTL.OAuthAPI_Report(country);
				Usuarios.ProcesoEntidadUsuarios(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Usuarios.ProcesoEntidadUsuarios(token, "COH");
					return "Ejecución Correcta de Entidad Usuarios Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Usuarios " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Usuarios = Error al ejecutar entidad Usuarios " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/clientes")
	public String Clientes(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				String token = ObtenerOauthTL.OAuthAPI_Report(country);
				Clientes.ProcesoEntidadClientes(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Clientes.ProcesoEntidadClientes(token, "COH");
					return "Ejecución Correcta de Entidad Clientes Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Clientes " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}

		} catch (Exception e) {
			return "apiReportController.Clientes = Error al ejecutar entidad Clientes " + country;
		}
	}
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/materias")
	public String Materias(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				String token = ObtenerOauthTL.OAuthAPI_Report(country);
				Materias.ProcesoEntidadMaterias(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Materias.ProcesoEntidadMaterias(token, "COH");
					return "Ejecución Correcta de Entidad Materias Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Materias " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Materias = Error al ejecutar entidad Materias " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/casos")
	public String Casos(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				String token = ObtenerOauthTL.OAuthAPI_Report(country);
				Casos.ProcesoEntidadCasos(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Casos.ProcesoEntidadCasos(token, "COH");
					return "Ejecución Correcta de Entidad Casos Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Casos " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Casos = Error al ejecutar entidad Casos " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/categoriaDeServicios")
	public String CategoriaDeServicios(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				CategoriasDeServicios.ProcesoEntidadCategoriaDeServicios(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					CategoriasDeServicios.ProcesoEntidadCategoriaDeServicios(token, "COH");
					return "Ejecución Correcta de Entidad CategoriaDeServicios Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad CategoriaDeServicios " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.CategoriaDeServicios = Error al ejecutar entidad CategoriaDeServicios "
					+ country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/definicionDeServicios")
	public String DefinicionDeServicios(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios(token, "COH");
					return "Ejecución Correcta de Entidad DefinicionDeServicios Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad DefinicionDeServicios " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.DefinicionDeServicios = Error al ejecutar entidad DefinicionDeServicios "
					+ country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/profesionales")
	public String Profesionales(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Profesionales.ProcesoEntidadProfesionales(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Profesionales.ProcesoEntidadProfesionales(token, "COH");
					return "Ejecución Correcta de Entidad Profesionales Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Profesionales " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Profesionales = Error al ejecutar entidad Profesionales " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/proveedores")
	public String Proveedores(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Proveedores.ProcesoEntidadProveedores(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Proveedores.ProcesoEntidadProveedores(token, "COH");
					return "Ejecución Correcta de Entidad Proveedores Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Proveedores " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Proveedores = Error al ejecutar entidad Proveedores " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/paises")
	public String Paises(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Paises.ProcesoEntidadPaises(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Paises.ProcesoEntidadPaises(token, "COH");
					return "Ejecución Correcta de Entidad Paises Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Paises " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Paises = Error al ejecutar entidad Paises " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/ciudades")
	public String Ciudades(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Ciudades.ProcesoEntidadCiudades(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Ciudades.ProcesoEntidadCiudades(token, "COH");
					return "Ejecución Correcta de Entidad Ciudades Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Ciudades " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Ciudades = Error al ejecutar entidad Ciudades " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/comunas")
	public String Comunas(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Comunas.ProcesoEntidadComunas(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Comunas.ProcesoEntidadComunas(token, "COH");
					return "Ejecución Correcta de Entidad Comunas Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Comunas " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Comunas = Error al ejecutar entidad Comunas " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/servicios")
	public String Servicios(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Servicios.ProcesoEntidadServicios(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Servicios.ProcesoEntidadServicios(token, "COH");
					return "Ejecución Correcta de Entidad Servicios Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Servicios " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Servicios = Error al ejecutar entidad Servicios " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/suscripciones")
	public String Suscripciones(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Suscripciones.ProcesoEntidadSuscripciones(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Suscripciones.ProcesoEntidadSuscripciones(token, "COH");
					return "Ejecución Correcta de Entidad Suscripciones Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Suscripciones " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Suscripciones = Error al ejecutar entidad Suscripciones " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/consumosDeSuscripciones")
	public String ConsumosDeSuscripciones(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				ConsumosDeSuscripciones.ProcesoEntidadConsumosDeSuscripciones(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					ConsumosDeSuscripciones.ProcesoEntidadConsumosDeSuscripciones(token, "COH");
					return "Ejecución Correcta de Entidad ConsumosDeSuscripciones Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad ConsumosDeSuscripciones " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.ConsumosDeSuscripciones = Error al ejecutar entidad ConsumosDeSuscripciones "
					+ country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/encuestasDeServicios")
	public String EncuestasDeServicios(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				EncuestasDeServicios.ProcesoEntidadEncuestasDeServicios(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					EncuestasDeServicios.ProcesoEntidadEncuestasDeServicios(token, "COH");
					return "Ejecución Correcta de Entidad EncuestasDeServicios Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad EncuestasDeServicios " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.EncuestasDeServicios = Error al ejecutar entidad EncuestasDeServicios "
					+ country;
		}
	}



	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/evaluaciones")
	public String Evaluaciones(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Evaluaciones.ProcesoEntidadEvaluaciones(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Evaluaciones.ProcesoEntidadEvaluaciones(token, "COH");
					return "Ejecución Correcta de Entidad Evaluaciones Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Evaluaciones " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Evaluaciones = Error al ejecutar entidad Evaluaciones " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/reclamos")
	public String Reclamos(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Reclamos.ProcesoEntidadReclamos(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Reclamos.ProcesoEntidadReclamos(token, "COH");
					return "Ejecución Correcta de Entidad Reclamos Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Reclamos " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Reclamos = Error al ejecutar entidad Reclamos " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/ofertasAProfesionales")
	public String OfertasAProfecionales(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales(token, "COH");
					return "Ejecución Correcta de Entidad OfertasAProfecionalesColombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad OfertasAProfecionales " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.OfertasAProfecionales = Error al ejecutar entidad OfertasAProfecionales "
					+ country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/presupuestosDeServicios")
	public String PresupuestosDeServicios(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				PresupuestosDeServicios.ProcesoEntidadPresupuestosDeServicios(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					PresupuestosDeServicios.ProcesoEntidadPresupuestosDeServicios(token, "COH");
					return "Ejecución Correcta de Entidad PresupuestosDeServicios Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad PresupuestosDeServicios " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.PresupuestosDeServicios = Error al ejecutar entidad PresupuestosDeServicios "
					+ country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/pagoDelProveedor")
	public String PagoDelProveedor(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				PagoDelProveedor.ProcesoEntidadPagoDelProveedor(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					PagoDelProveedor.ProcesoEntidadPagoDelProveedor(token, "COH");
					return "Ejecución Correcta de Entidad PagoDelProveedor Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad PagoDelProveedor " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.PagoDelProveedor = Error al ejecutar entidad PagoDelProveedor " + country;
		}
	}

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/respuestasWizard")
	public String RespuestasWizard(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				RespuestasWizard.ProcesoEntidadRespuestasWizard(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					RespuestasWizard.ProcesoEntidadRespuestasWizard(token, "COH");
					return "Ejecución Correcta de Entidad RespuestasWizard Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad RespuestasWizard " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.RespuestasWizard = Error al ejecutar entidad RespuestasWizard " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/checklists")
	public String Checklists(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Checklists.ProcesoEntidadChecklists(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Checklists.ProcesoEntidadChecklists(token, "COH");
					return "Ejecución Correcta de Entidad Checklists Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Checklists " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Checklists = Error al ejecutar entidad Checklists " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/checkListsItems")
	public String CheckListsItems(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				ChecklistItems.ProcesoEntidadCheckListsItems(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					ChecklistItems.ProcesoEntidadCheckListsItems(token, "COH");
					return "Ejecución Correcta de Entidad CheckListsItems Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad CheckListsItems " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.CheckListsItems = Error al ejecutar entidad CheckListsItems " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/costConcepts")
	public String CostConcepts(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				CostConcepts.ProcesoEntidadCostConcepts(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					CostConcepts.ProcesoEntidadCostConcepts(token, "COH");
					return "Ejecución Correcta de Entidad CostConcepts Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad CostConcepts " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.CostConcepts = Error al ejecutar entidad CostConcepts " + country;
		}
	}
	

	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/agendaDeProfesionales")
	public String AgendaDeProfesionales(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				AgendaDeProfesionales.ProcesoEntidadAgendaDeProfesionales(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					AgendaDeProfesionales.ProcesoEntidadAgendaDeProfesionales(token, "COH");
					return "Ejecución Correcta de Entidad AgendaDeProfesionales Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad AgendaDeProfesionales " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.AgendaDeProfesionales = Error al ejecutar entidad AgendaDeProfesionales " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/recargoDeProfesionales")
	public String RecargoDeProfesionales(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				RecargosDeProfesionales.ProcesoEntidadRecargosDeProfesionales(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					RecargosDeProfesionales.ProcesoEntidadRecargosDeProfesionales(token, "COH");
					return "Ejecución Correcta de Entidad RecargoDeProfesionales Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad RecargoDeProfesionales " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.RecargoDeProfesionales = Error al ejecutar entidad RecargoDeProfesionales " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/match")
	public String Match(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				if(country.equals("MX")) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Match.ProcesoEntidadMatch(token, country);
				}
				// Ejecuta Entidad de Colombia Hasura
				else if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Match.ProcesoEntidadMatch(token, "COH");
					return "Ejecución Correcta de Entidad Match Colombia";
				}else {
					return "Pais incorrecto, introduzca MX o CO ";
				}
				return "Ejecución Correcta de Entidad Match " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Match = Error al ejecutar entidad Match " + country;
		}
	}
	
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/wizards")
	public String Wizards(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Wizards.ProcesoEntidadWizards(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Wizards.ProcesoEntidadWizards(token, "COH");
					return "Ejecución Correcta de Entidad Wizards Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Wizards " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Wizards = Error al ejecutar entidad Wizards " + country;
		}
	}
	

	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/estatusDePresupuesto")
	public String EstatusDePresupuesto(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				
				if (country.equals("CO")  || country.equals("AR")) {
					return "La Entidad Estatus De Presupuesto solo se puede ejecutar en ambiente MX";
				}else {
					token = ObtenerOauthTL.OAuthAPI_Report(country);
					EstatusDePresupuesto.ProcesoEntidadEstatusDePresupuesto(token, country);
				}
				return "Ejecución Correcta de Entidad EstatusDePresupuesto " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.EstatusDePresupuesto = Error al ejecutar entidad EstatusDePresupuesto " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/variablesWizards")
	public String VariablesWizards(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				VariablesWizards.ProcesoEntidadVariablesWizards(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					VariablesWizards.ProcesoEntidadVariablesWizards(token, "COH");
					return "Ejecución Correcta de Entidad Variables Wizard Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Variables Wizard " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.EstatusDePresupuesto = Error al ejecutar entidad EstatusDePresupuesto " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/skusDePresupuestos")
	public String SkusDePresupuestos(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				
				// Filtro ejecucion por pais
				if (country.equals("CO")  || country.equals("AR")) {
					return "La Entidad SkusDePresupuestos solo se puede ejecutar en ambiente MX";
				}else {
					token = ObtenerOauthTL.OAuthAPI_Report(country);
					SkusDePresupuestos.ProcesoEntidadSkusDePresupuestos(token, country);
				}
				return "Ejecución Correcta de Entidad SkusDePresupuestos " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.SkusDePresupuestos = Error al ejecutar entidad SkusDePresupuestos " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/planes")
	public String Planes(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Planes.ProcesoEntidadPlanes(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Planes.ProcesoEntidadPlanes(token, "COH");
					return "Ejecución Correcta de Entidad Planes Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Planes " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Planes = Error al ejecutar entidad Planes " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/historialDeServicios")
	public String HistorialDeServicios(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				HistorialDeServicios.ProcesoEntidadHistorialDeServicios(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					HistorialDeServicios.ProcesoEntidadHistorialDeServicios(token, "COH");
					return "Ejecución Correcta de Entidad HistorialDeServicios Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad HistorialDeServicios " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.HistorialDeServicios = Error al ejecutar entidad HistorialDeServicios " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/siniestralidad")
	public String Siniestralidad(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Siniestralidad.ProcesoEntidadSiniestralidad(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Siniestralidad.ProcesoEntidadSiniestralidad(token, "COH");
					return "Ejecución Correcta de Entidad Siniestralidad Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Siniestralidad " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Siniestralidad = Error al ejecutar entidad Siniestralidad " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/topes")
	public String Topes(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Topes.ProcesoEntidadTopes(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Topes.ProcesoEntidadTopes(token, "COH");
					return "Ejecución Correcta de Entidad Topes Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Topes " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Topes = Error al ejecutar entidad Topes " + country;
		}
	}
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/budgets")
	public String Budgets(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Budgets.ProcesoEntidadBudgets(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Budgets.ProcesoEntidadBudgets(token, "COH");
					return "Ejecución Correcta de Entidad Budgets Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad Budgets " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Budgets = Error al ejecutar entidad Budgets " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/nombreDeComunaDeSku")
	public String NombreDeComunaDeSku(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				NombreDeComunaDeSku.ProcesoEntidadNombreDeComunaDeSku(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					NombreDeComunaDeSku.ProcesoEntidadNombreDeComunaDeSku(token, "COH");
					return "Ejecución Correcta de Entidad NombreDeComunaDeSku Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad NombreDeComunaDeSku " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.NombreDeComunaDeSku = Error al ejecutar entidad NombreDeComunaDeSku " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/serviciosAsociadosAlReclamo")
	public String ServiciosAsociadosAlReclamo(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				ServiciosAsociadosAlReclamo.ProcesoEntidadServiciosAsociadosAlReclamo(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					ServiciosAsociadosAlReclamo.ProcesoEntidadServiciosAsociadosAlReclamo(token, "COH");
					return "Ejecución Correcta de Entidad ServiciosAsociadosAlReclamo Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad ServiciosAsociadosAlReclamo " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.ServiciosAsociadosAlReclamo = Error al ejecutar entidad ServiciosAsociadosAlReclamo " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/checkListDelServicio")
	public String CheckListDelServicio(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				ChecklistDelServicio.ProcesoEntidadChecklistDelServicio(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					ChecklistDelServicio.ProcesoEntidadChecklistDelServicio(token, "COH");
					return "Ejecución Correcta de Entidad CheckListDelServicio Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad CheckListDelServicio " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.CheckListDelServicio = Error al ejecutar entidad CheckListDelServicio " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/pagosDeExcedentesParaServicios")
	public String PagosDeExcedentesParaServicios(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				PagosDeExcedentesParaServicios.ProcesoEntidadPagosDeExcedentesParaServicios(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					PagosDeExcedentesParaServicios.ProcesoEntidadPagosDeExcedentesParaServicios(token, "COH");
					return "Ejecución Correcta de Entidad PagosDeExcedentesParaServicios Colombia y Colombia Enel";
				}
				return "Ejecución Correcta de Entidad PagosDeExcedentesParaServicios " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.PagosDeExcedentesParaServicios = Error al ejecutar entidad PagosDeExcedentesParaServicios " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/serviciosDeProfesionales")
	public String ServiciosDeProfesionales(@RequestHeader(name = "Authorization", required = false) String authorization) 
	{
		String country="COH";
		try {
			
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				ServiciosDeProfesionales.ProcesoEntidadServiciosDeProfesionales(token, country);
				return "Ejecución Correcta de Entidad ServiciosDeProfesionales Colombia";
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.ServiciosDeProfesionales = Error al ejecutar entidad ServiciosDeProfesionales Colombia";
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/agrupadorDeServicios")
	public String AgrupadorDeServicios(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios(token, "COH");
					return "Ejecución Correcta de Entidad AgrupadorDeServicios Colombia";
				}
				return "Ejecución Correcta de Entidad AgrupadorDeServicios " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.PagosDeExcedentesParaServicios = Error al ejecutar entidad AgrupadorDeServicios " + country;
		}
	}
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/coberturas")
	public String Coberturas(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				Coberturas.ProcesoEntidadCoberturas(token, country);
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO")) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Coberturas.ProcesoEntidadCoberturas(token, "COH");
					return "Ejecución Correcta de Entidad Coberturas Colombia ";
				}
				return "Ejecución Correcta de Entidad Coberturas " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Coberturas = Error al ejecutar entidad Coberturas " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/serivicesOrder")
	public String ServicesOrder(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO") || country.equals("COH") ) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					ServicesOrder.ProcesoEntidadServicesOrder(token, "COH");
					return "Ejecución Correcta de Entidad ServicesOrder Colombia ";
				}
				else if(country.equals("MX")) {
					ServicesOrder.ProcesoEntidadServicesOrder(token, "MX");
					return "Ejecución Correcta de Entidad ServicesOrder Mexíco ";
				}
				return "Ejecución Correcta de Entidad ServicesOrder " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.ServicesOrder = Error al ejecutar entidad ServicesOrder " + country;
		}
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/applicants")
	public String Applicants(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO") || country.equals("COH") ) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Solicitantes.ProcesoEntidadSolicitantes(token, "COH");
					return "Ejecución Correcta de Entidad Applicants Colombia ";
				}
				else if(country.equals("MX")) {
					Solicitantes.ProcesoEntidadSolicitantes(token, "MX");
					return "Ejecución Correcta de Entidad Applicants Mexíco ";
				}
				return "Ejecución Correcta de Entidad Applicants " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Applicants = Error al ejecutar entidad Applicants " + country;
		}
	}
	
	/*@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/sucursals")
	public String Sucursals(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if (bool == true) {
				token = ObtenerOauthTL.OAuthAPI_Report(country);
				
				// Ejecuta Entidad de Colombia Hasura
				if (country.equals("CO") || country.equals("COH") ) {
					token = ObtenerOauthTL.OAuthAPI_Report("COH");
					Sucursales.ProcesoEntidadSucursales(token, "COH");
					return "Ejecución Correcta de Entidad Sucursals Colombia ";
				}
				else if(country.equals("MX")) {
					Sucursales.ProcesoEntidadSucursales(token, "MX");
					return "Ejecución Correcta de Entidad Sucursals Mexíco ";
				}
				return "Ejecución Correcta de Entidad Sucursals " + country;
			} else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		} catch (Exception e) {
			return "apiReportController.Applicants = Error al ejecutar entidad Sucursals " + country;
		}
	}*/

}