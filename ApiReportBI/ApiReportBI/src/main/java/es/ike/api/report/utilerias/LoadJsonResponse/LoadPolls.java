package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.EncuentasDeServiciosEntity;
import es.ike.api.report.utilerias.JsonUtility;

public class LoadPolls {
	public static void Polls(String jsonResponse, String country) throws Exception{
		// Se carga jsonResponse en Arreglo Json
				Integer iError = 0;
				Instant instant = Instant.now();
				// Obtiene configuracion de Entidad
				ApiConfiguration apiConfiguration = null;
				apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("16","16","polls", country);
				Logger logger = Logger.getLogger("Logger apiReport LoadPolls.Polls");
				// Se carga jsonResponse en Arreglo Json
				try {
					if (String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")) {
						JSONArray jsonarray = new JSONArray(jsonResponse);
						CloudTableClient tableClient = null;
						tableClient = TableClientProvider.getTableClientReference(country);
						// Se genera la variable tabla, con el nombre de la tabla en AZURE
						CloudTable table = tableClient.getTableReference("Polls");
						for (int a = 0; a<jsonarray.length(); a++) {
		            		JSONParser parser=null;
		            		JSONObject jsonServices=null;
		            		String strPollQuestionId ="";
		            		String strPollResponseId ="";
		            		String strCreatedAt ="";
		            		String strUpdatedAt ="";
		            		String strServiceId ="";
		            		String strQuestionId ="";
		            		String strAsk	="";
		            		String strAnswer	="";
		            		String strIdTemplate	="";
		            		String strBusinessUnit	="";
		            		String strProfessionalId	="";
		            		String strQualifyingSubjectName="";
		            		String strType	="";

		            		parser = new JSONParser();
		                    jsonServices = (JSONObject)parser.parse(jsonarray.get(a).toString());
		                    strPollQuestionId = JsonUtility.JsonValidaExisteLlave(jsonServices,"pollQuestionId" );
		                    strPollResponseId = JsonUtility.JsonValidaExisteLlave(jsonServices,"pollResponseId" );
		                    strCreatedAt = JsonUtility.JsonValidaExisteLlave(jsonServices,"createdAt" );
		                    strUpdatedAt = JsonUtility.JsonValidaExisteLlave(jsonServices,"updatedAt" );
		                    strServiceId = JsonUtility.JsonValidaExisteLlave(jsonServices,"serviceId" );
		                    strQuestionId = JsonUtility.JsonValidaExisteLlave(jsonServices,"questionId" );
		                    strAsk = JsonUtility.JsonValidaExisteLlave(jsonServices,"ask" );
		                    strAnswer = JsonUtility.JsonValidaExisteLlave(jsonServices,"answer" );
		                    strIdTemplate = JsonUtility.JsonValidaExisteLlave(jsonServices,"idTemplate" );
		                    strBusinessUnit = JsonUtility.JsonValidaExisteLlave(jsonServices,"businessUnit" );
		                    strProfessionalId = JsonUtility.JsonValidaExisteLlave(jsonServices,"professionalId" );
		            		strQualifyingSubjectName = JsonUtility.JsonValidaExisteLlave(jsonServices,"qualifyingSubjectName" );
		            		strType = JsonUtility.JsonValidaExisteLlave(jsonServices,"type" );
		            		
		            		
		            		EncuentasDeServiciosEntity encuestasDeServiciosEntity = new EncuentasDeServiciosEntity(strPollResponseId+"_"+strPollQuestionId, strBusinessUnit); //PartitionKey & RowKey
		            		encuestasDeServiciosEntity.setEtag(encuestasDeServiciosEntity.getEtag());
		            		encuestasDeServiciosEntity.setPollQuestionId(strPollQuestionId != null ? Integer.parseInt(strPollQuestionId) : null);
		            		encuestasDeServiciosEntity.setPollResponseId(strPollResponseId != null ? Integer.parseInt(strPollResponseId) : null);
		            		encuestasDeServiciosEntity.setCreatedAt(strCreatedAt);
		            		encuestasDeServiciosEntity.setUpdatedAt(strUpdatedAt);
		            		encuestasDeServiciosEntity.setServiceId(strServiceId != null ? Integer.parseInt(strServiceId) : null);
		            		encuestasDeServiciosEntity.setQuestionId(strQuestionId != null ? Integer.parseInt(strQuestionId) : null);
		            		encuestasDeServiciosEntity.setAsk(strAsk);
		            		encuestasDeServiciosEntity.setAnswer(strAnswer);
		            		encuestasDeServiciosEntity.setIdTemplate(strIdTemplate != null ? Integer.parseInt(strIdTemplate) : null);
		            		encuestasDeServiciosEntity.setBusinessUnit(strBusinessUnit !=null ? UUID.fromString(strBusinessUnit):null);
		            		encuestasDeServiciosEntity.setProfessionalId(strProfessionalId != null ? Integer.parseInt(strProfessionalId) : null);
		            		encuestasDeServiciosEntity.setQualifyingSubjectName(strQualifyingSubjectName);
		            		encuestasDeServiciosEntity.setType(strType);
		    
		            		table.execute(TableOperation.insertOrReplace(encuestasDeServiciosEntity));
						}
					} else {
						iError = 1;
						logger.log(Level.WARNING, "LoadPolls.Polls= Respuesta No identificada " + country + "=" + jsonResponse);
					}
				} catch (Exception e) {
					iError = 1;
					logger.log(Level.WARNING, "LoadPolls.Polls= Error al procesar arreglo e insertar en AZURE " + country + "=" + jsonResponse);
					e.printStackTrace();
				}
				// Fin de proceso de Entidad
				try {
					if (iError == 0) {
						// ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
						logger.log(Level.INFO, " Se cargan registros entidad Polls con filtro ProcessFilterBu "+ country + "!!!");
					}
				} catch (Exception e) {
					iError = 1;
					logger.log(Level.WARNING, "LoadPolls.Polls = Error al actualizar Fecha" + country);
					e.printStackTrace();
				}
				if (iError == 0) {
					
							logger.log(Level.INFO," Proceso Carga de Entidad con filtro Polls Finalizada " + country + "!!!!");
				}

    	}

}