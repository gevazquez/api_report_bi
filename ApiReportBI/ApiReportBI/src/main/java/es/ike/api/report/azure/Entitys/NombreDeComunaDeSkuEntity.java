package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class NombreDeComunaDeSkuEntity extends TableServiceEntity {
	//------------------------------------------------------------------------------------------------------------
	public NombreDeComunaDeSkuEntity(String skuComuneId, String businessUuid) {
		this.partitionKey = skuComuneId;
		this.rowKey = businessUuid;  
	}
//------------------------------------------------------------------------------------------------------------
	public NombreDeComunaDeSkuEntity() {		}
//------------------------------------------------------------------------------------------------------------

	public UUID businessUnit;
    public Integer skuComuneId;
    public Integer skuId;
    public String skuName;
    public String measureUnit;
    public String createdAt;
    public String updatedAt;
    
    
	
    
  
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public Integer getSkuComuneId() {
		return skuComuneId;
	}
	public void setSkuComuneId(Integer skuComuneId) {
		this.skuComuneId = skuComuneId;
	}
	public Integer getSkuId() {
		return skuId;
	}
	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}
	public String getSkuName() {
		return skuName;
	}
	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}
	public String getMeasureUnit() {
		return measureUnit;
	}
	public void setMeasureUnit(String measureUnit) {
		this.measureUnit = measureUnit;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("businessUnit");
		list.add("skuComuneId");
		list.add("skuId");
		list.add("skuName");
		list.add("measureUnit");
		list.add("createdAt");
		list.add("updatedAt");
		return list;
	}
	
}
