package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class PagosDeExcedenteParaServiciosEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public PagosDeExcedenteParaServiciosEntity (String id,String businessUnit) {
		this.partitionKey=id;
		this.rowKey=businessUnit;
		
	}
	// --------------------------------------------------------------------
	public PagosDeExcedenteParaServiciosEntity() {
	
	}
	// ------------------------------------------------------------------------------------------------------------
			public UUID businessUnit;
			public Integer id;
			public String paidBy;
			public Integer budgetId;
			public String amount;
			public String createdAt;
			public String updatedAt;
			
			
			
			public String getPaidBy() {
				return paidBy;
			}
			public void setPaidBy(String paidBy) {
				this.paidBy = paidBy;
			}
			public UUID getBusinessUnit() {
				return businessUnit;
			}
			public void setBusinessUnit(UUID businessUnit) {
				this.businessUnit = businessUnit;
			}
			public Integer getId() {
				return id;
			}
			public void setId(Integer id) {
				this.id = id;
			}
			
			public Integer getBudgetId() {
				return budgetId;
			}
			public void setBudgetId(Integer budgetId) {
				this.budgetId = budgetId;
			}
			public String getAmount() {
				return amount;
			}
			public void setAmount(String amount) {
				this.amount = amount;
			}
			public String getCreatedAt() {
				return createdAt;
			}
			public void setCreatedAt(String createdAt) {
				this.createdAt = createdAt;
			}
			public String getUpdatedAt() {
				return updatedAt;
			}
			public void setUpdatedAt(String updatedAt) {
				this.updatedAt = updatedAt;
			}
			public List<String>  getColumns(){
				List<String> list = new ArrayList<String>();
				list.add("id");
				list.add("businessUnit");
				list.add("paidBy");
				list.add("budgetId");
				list.add("amount");
				list.add("updatedAt");
				list.add("createdAT");
				return list;
			}
				
			

}
