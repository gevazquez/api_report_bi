package es.ike.api.report.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;

import es.ike.api.report.services.CognitoUserService;
import es.ike.api.report.utilerias.ProcessFilterId;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
@RequestMapping("/FiltroById")
public class FiltroByIdController {
	
	@Autowired
	private CognitoUserService cognitoUserService;
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroByIdCases")
	public String FiltroByIdCases(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "id") String id) {
		try {
			String where ="where: {id: {_eq: \\\""+id+"\\\"}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"cases");
				return "Ejecución Correcta de filtro ById Entidad Cases " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.FiltroByIdCases = Error al ejecutar filtro by ID entidad Cases " + country;
		}
		
	}
	
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroByIdCustomers")
	public String FiltroByIdCustomers(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "id") String id)
	{
		try {
			String where ="where: {id: {_eq: \\\""+id+"\\\"}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"customers");
				return "Ejecución Correcta de filtro ById Entidad Customers " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.FiltroByIdCustomers = Error al ejecutar filtro by ID entidad Customers " + country;
		}
		
	}
	
	
	
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroByIdMaterials")
	public String FiltroByIdMaterials(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "id") String id)
	{
		try {
			String where ="where: {id: {_eq: \\\""+id+"\\\"}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"materials");
				return "Ejecución Correcta de filtro ById Entidad Materials " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.FiltroByIdMaterials = Error al ejecutar filtro by ID entidad Materials " + country;
		}
		
		
		
	}
	
	
	@Operation(security = { @SecurityRequirement(name = "Token") })///revisar como sera el id
	@GetMapping("/FiltroByIdPolls")
	public String FiltroByIdPolls(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "pollResponseId") String pollResponseId,
			@RequestParam(value = "pollQuestionId") String pollQuestionId
			)
	{
		try {
			String where ="where: {pollResponseId: {_eq: \\\""+pollResponseId+"\\\"}, _and: {pollQuestionId: {_eq: \\\""+pollQuestionId+"\\\"}}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"polls");
				return "Ejecución Correcta de filtro ById Entidad polls " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.FiltroByIdPolls = Error al ejecutar filtro by ID entidad polls " + country;
		}
		
		
		
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroByIdProviderPayments")
	public String FiltroByIdProviderPayments(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "paymentId") String paymentId)
	{
		try {
			String where ="where: {paymentId: {_eq: \\\""+paymentId+"\\\"}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"provider_payments");
				return "Ejecución Correcta de filtro ById Entidad provider_payments " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.FiltroByIdProviderPayments = Error al ejecutar filtro by ID entidad provider_payments " + country;
		}
		
		
		
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroByIdServiceBudgets")
	public String FiltroByIdServiceBudgets(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "Id") String Id)
	{
		try {
			String where ="where: {id: {_eq: \\\""+Id+"\\\"}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"service_budgets");
				return "Ejecución Correcta de filtro ById Entidad service_budgets " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.FiltroByIdServiceBudgets = Error al ejecutar filtro by ID entidad service_budgets " + country;
		}
		
		
		
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroByIdServices")
	public String FiltroByIdServices(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "Id") String Id)
	{
		try {
			String where ="where: {id: {_eq: \\\""+Id+"\\\"}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"services");
				return "Ejecución Correcta de filtro ById Entidad services " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.FiltroByIdServices = Error al ejecutar filtro by ID entidad services " + country;
		}
		
		
		
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroByIdSubscriptions")
	public String FiltroByIdSubscriptions(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "Id") String Id)
	{
		try {
			String where ="where: {id: {_eq: \\\""+Id+"\\\"}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"subscriptions");
				return "Ejecución Correcta de filtro ById Entidad subscriptions " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.FiltroByIdSubscriptions = Error al ejecutar filtro by ID entidad subscriptions " + country;
		}
		
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroByIdSubscriptionsConsumed")//revisar id
	public String FiltroByIdSubscriptionsConsumed(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "Id") String Id,
			@RequestParam(value = "serviceId") String serviceId)
	{
		try {
			String where ="where: {id: {_eq: \\\""+Id+"\\\"}, _and: {serviceId: {_eq: \\\""+serviceId+"\\\"}}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"subscriptions_consumed");
				return "Ejecución Correcta de filtro ById Entidad FiltroByIdSubscriptionsConsumed " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.subscriptions_consumed = Error al ejecutar filtro by ID entidad FiltroByIdSubscriptionsConsumed " + country;
		}
		
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroByIdCoverageBalances")
	public String FiltroByIdCoverageBalances(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "Id") String Id)
	{
		try {
			String where ="where: {id: {_eq: \\\""+Id+"\\\"}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"coverage_balances");
				return "Ejecución Correcta de filtro ById Entidad FiltroByIdCoverageBalances " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.coverage_balances = Error al ejecutar filtro by ID entidad FiltroByIdCoverageBalancescover " + country;
		}
		
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroById_Service_status_change_history_logs")
	public String FiltroById_Service_status_change_history_logs(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "Id") String Id)
	{
		try {
			String where ="where: {id: {_eq: \\\""+Id+"\\\"}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"service_status_change_history_logs");
				return "Ejecución Correcta de filtro ById Entidad FiltroById_Service_status_change_history_logs " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.service_status_change_history_logs = Error al ejecutar filtro by ID entidad FiltroById_Service_status_change_history_logs " + country;
		}
		
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroById_Professional_services")
	public String FiltroById_Professional_services(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "Id") String Id)
	{
		String country="MX";
		try {
			String where ="where: {id: {_eq: \\\""+Id+"\\\"}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"professional_services");
				return "Ejecución Correcta de filtro ById Entidad Professional_services " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.Professional_services = Error al ejecutar filtro by ID entidad Professional_services " + country;
		}
		
		
	}
	
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroById_ServicesOrder")
	public String FiltroById_ServicesOrder(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "Id") String Id)
	{
		try {
			String where ="where: {id: {_eq: \\\""+Id+"\\\"}}";
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterId.FilterById(country, where,"services_order");
				return "Ejecución Correcta de filtro ById Entidad FiltroById_ServicesOrder " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
			
		}catch(Exception e) {
			return "FiltroByIdController.ServicesOrder = Error al ejecutar filtro by ID entidad FiltroById_ServicesOrder " + country;
		}
		
	}
	
	
}
