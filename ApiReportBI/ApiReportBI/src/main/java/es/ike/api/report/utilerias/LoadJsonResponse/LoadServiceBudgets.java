package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.PresupuestosDeServiciosEntity;
import es.ike.api.report.utilerias.JsonUtility;

public class LoadServiceBudgets {
	public static void ServiceBudgets(String jsonResponse, String country) throws Exception{
		// Se carga jsonResponse en Arreglo Json
				Integer iError = 0;
				Instant instant = Instant.now();
				// Obtiene configuracion de Entidad
				ApiConfiguration apiConfiguration = null;
				apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("21","21","service_budgets",country);
				Logger logger = Logger.getLogger("Logger apiReport LoadServiceBudgets.ServiceBudgets");
				// Se carga jsonResponse en Arreglo Json
				try {
					if (String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")) {
						JSONArray jsonarray = new JSONArray(jsonResponse);
						CloudTableClient tableClient = null;
						tableClient = TableClientProvider.getTableClientReference(country);
						// Se genera la variable tabla, con el nombre de la tabla en AZURE
						CloudTable table = tableClient.getTableReference("ServiceBudgets");
						for (int a = 0; a<jsonarray.length(); a++) {
		                	JSONParser parser=null;
		            		JSONObject json=null;
		            		String strId="";
		            		String strCreatedAt="";
		            		String strBusinessUnit="";
		            		String strBookingId="";
		            		String strTotalCost="";
		            		String strTotalPrice="";
		            		String strUdatedAt="";
		            		String strServiceDuration="";
		            		
		            		
		            		parser = new JSONParser();
		                    json = (JSONObject)parser.parse(jsonarray.get(a).toString());
		                    strId=JsonUtility.JsonValidaExisteLlave(json,"id");
		                    strCreatedAt=JsonUtility.JsonValidaExisteLlave(json,"createdAt");
		                    strBusinessUnit=JsonUtility.JsonValidaExisteLlave(json,"businessUnit");
		                    strBookingId=JsonUtility.JsonValidaExisteLlave(json,"bookingId");
		                    strTotalCost=JsonUtility.JsonValidaExisteLlave(json,"totalCost");
		                    strTotalPrice=JsonUtility.JsonValidaExisteLlave(json,"totalPrice");
		                    strUdatedAt =JsonUtility.JsonValidaExisteLlave(json,"updatedAt");
		                    strServiceDuration =JsonUtility.JsonValidaExisteLlave(json,"serviceDuration");
		                    
		                    PresupuestosDeServiciosEntity presupuestosDeServiciosEntity = new PresupuestosDeServiciosEntity(strId,strBusinessUnit);
		                    presupuestosDeServiciosEntity.setEtag(presupuestosDeServiciosEntity.getEtag());
		                    presupuestosDeServiciosEntity.setId(strId != null ? Integer.parseInt(strId): null);
		                    presupuestosDeServiciosEntity.setCreatedAt(strCreatedAt);
		                    presupuestosDeServiciosEntity.setBusinessUnit(strBusinessUnit != null ? UUID.fromString(strBusinessUnit): null);
		                    presupuestosDeServiciosEntity.setBookingId(strBookingId != null ? Integer.parseInt(strBookingId): null);
		                    presupuestosDeServiciosEntity.setTotalCost(strTotalCost != null ? Integer.parseInt(strTotalCost): null);
		                    presupuestosDeServiciosEntity.setTotalPrice(strTotalPrice != null ? Integer.parseInt(strTotalPrice): null);
		                    presupuestosDeServiciosEntity.setUpdatedAt(strUdatedAt);
		                    presupuestosDeServiciosEntity.setServiceDuration(strServiceDuration != null ? Integer.parseInt(strServiceDuration): null);
		                    table.execute(TableOperation.insertOrReplace(presupuestosDeServiciosEntity));
						}
					} else {
						iError = 1;
						logger.log(Level.WARNING, "LoadServiceBudgets.ServiceBudgets= Respuesta No identificada " + country + "=" + jsonResponse);
					}
				} catch (Exception e) {
					iError = 1;
					logger.log(Level.WARNING, "LoadServiceBudgets.ServiceBudgets= Error al procesar arreglo e insertar en AZURE " + country + "=" + jsonResponse);
					e.printStackTrace();
				}
				// Fin de proceso de Entidad
				try {
					if (iError == 0) {
						// ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
						logger.log(Level.INFO, " Se cargan registros entidad ServiceBudgets con filtro ProcessFilterBu "+ country + "!!!");
					}
				} catch (Exception e) {
					iError = 1;
					logger.log(Level.WARNING, "LoadServiceBudgets.ServiceBudgets = Error al actualizar Fecha" + country);
					e.printStackTrace();
				}
				if (iError == 0) {
					
							logger.log(Level.INFO," Proceso Carga de Entidad con filtro ServiceBudgets Finalizada " + country + "!!!!");
				}

    	}

}