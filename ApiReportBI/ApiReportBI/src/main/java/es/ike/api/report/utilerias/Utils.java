package es.ike.api.report.utilerias;

import java.util.List;

import es.ike.api.report.azure.Entitys.AgendaDeProfesionalesEntity;
import es.ike.api.report.azure.Entitys.BusinessUnitsEntity;
import es.ike.api.report.azure.Entitys.CasosEntity;
import es.ike.api.report.azure.Entitys.CategoriasDeServiciosEntity;
import es.ike.api.report.azure.Entitys.ChecklistItemsEntity;
import es.ike.api.report.azure.Entitys.ChecklistsEntity;
import es.ike.api.report.azure.Entitys.CiudadesEntity;
import es.ike.api.report.azure.Entitys.ClientesEntity;
import es.ike.api.report.azure.Entitys.ComunasEntity;
import es.ike.api.report.azure.Entitys.ConsumosDeSuscripcionesEntity;
import es.ike.api.report.azure.Entitys.CostConceptsEntity;
import es.ike.api.report.azure.Entitys.DefinicionDeServiciosEntity;
import es.ike.api.report.azure.Entitys.EncuentasDeServiciosEntity;
import es.ike.api.report.azure.Entitys.EstatusDePresupustoEntity;
import es.ike.api.report.azure.Entitys.EvaluacionesEntity;
import es.ike.api.report.azure.Entitys.MateriasEntity;
import es.ike.api.report.azure.Entitys.OfertasAProfesionalesEntity;
import es.ike.api.report.azure.Entitys.PagoAlProveedorEntity;
import es.ike.api.report.azure.Entitys.PaisesEntity;
import es.ike.api.report.azure.Entitys.PresupuestosDeServiciosEntity;
import es.ike.api.report.azure.Entitys.ProfesionalesEntity;
import es.ike.api.report.azure.Entitys.ProveedoresEntity;
import es.ike.api.report.azure.Entitys.RecargosDeProfesionalesEntity;
import es.ike.api.report.azure.Entitys.ReclamosEntity;
import es.ike.api.report.azure.Entitys.RespuestasWizardEntity;
import es.ike.api.report.azure.Entitys.ServiciosDeProfesionalesEntity;
import es.ike.api.report.azure.Entitys.ServiciosEntity;
import es.ike.api.report.azure.Entitys.SiniestralidadEntity;
import es.ike.api.report.azure.Entitys.SuscripcionesEntity;
import es.ike.api.report.azure.Entitys.UsersEntity;
import es.ike.api.report.azure.Entitys.VariablesWizardsEntity;
import es.ike.api.report.azure.Entitys.WizardsEntity;
import es.ike.api.report.entidades.ServiciosDeProfesionales;
import es.ike.api.report.azure.Entitys.HistorialDeServiciosEntity;
import es.ike.api.report.azure.Entitys.ServicesOrderEntity;


public class Utils {
	public static List<String> getColumns(String entity) {
		List<String> result = null;

		switch (entity) {

		case "costConcepts":
			CostConceptsEntity conceptsEntity = new CostConceptsEntity();
			result = conceptsEntity.getColumns();
			break;
		case "professional_schedules":
			AgendaDeProfesionalesEntity agendaDeProfesionalesEntity = new AgendaDeProfesionalesEntity();
			result = agendaDeProfesionalesEntity.getColumns();
			break;
		case "business_units":
			BusinessUnitsEntity businessUnitsEntity = new BusinessUnitsEntity();
			result = businessUnitsEntity.getColumns();
			break;
		case "cases":
			CasosEntity casosEntity = new CasosEntity();
			result= casosEntity.getColumns();
			break;
		case "service_categories":
			CategoriasDeServiciosEntity categoriasDeServiciosEntity = new CategoriasDeServiciosEntity();
			result= categoriasDeServiciosEntity.getColumns();
			break;
		case "checklist_items":
			ChecklistItemsEntity checklistItemsEntity = new ChecklistItemsEntity();
			result= checklistItemsEntity.getColumns();
			break;
		case "checklists":
			ChecklistsEntity checklistsEntity = new ChecklistsEntity();
			result = checklistsEntity.getColumns();
			break;
		case "cities":
			CiudadesEntity ciudadesEntity = new CiudadesEntity();
			result = ciudadesEntity.getColumns();
			break;
		case "customers":
			ClientesEntity clientesEntity = new ClientesEntity();
			result = clientesEntity.getColumns();
			break;
		case "comunes":
			ComunasEntity comunasEntity = new ComunasEntity();
			result = comunasEntity.getColumns();
			break;
		case "subscriptions_consumed":
			ConsumosDeSuscripcionesEntity consumosDeSuscripcionesEntity = new ConsumosDeSuscripcionesEntity(); 
			result = consumosDeSuscripcionesEntity.getColumns();
			break;
		case "definition_services":
			DefinicionDeServiciosEntity definicionDeServiciosEntity = new DefinicionDeServiciosEntity();
			result = definicionDeServiciosEntity.getColumns();
			break;
		case "polls":
			EncuentasDeServiciosEntity encuestasDeServiciosEntity = new EncuentasDeServiciosEntity();
			result= encuestasDeServiciosEntity.getColumns();
			break;
		case "budget_status":
			EstatusDePresupustoEntity estatusDePresupustoEntity = new EstatusDePresupustoEntity();
			result= estatusDePresupustoEntity.getColumns();
			break;
		case "satisfactions":
			EvaluacionesEntity evaluacionesEntity = new EvaluacionesEntity();
			result=evaluacionesEntity.getColumns();
			break;
		case "materials":
			MateriasEntity materiasEntity = new MateriasEntity();
			result=materiasEntity.getColumns();
			break;
		case "professional_offers":
			OfertasAProfesionalesEntity ofertasAProfesionalesEntity = new OfertasAProfesionalesEntity();
			result=ofertasAProfesionalesEntity.getColumns();
			break;
		case "provider_payments":
			PagoAlProveedorEntity pagoDelProveedorEntity = new PagoAlProveedorEntity();
			result=pagoDelProveedorEntity.getColumns();
			break;
		case "countries":
			PaisesEntity paisesEntity = new PaisesEntity();
			result=paisesEntity.getColumns();
			break;
		case "service_budgets":
			PresupuestosDeServiciosEntity presupuestosDeServiciosEntity = new PresupuestosDeServiciosEntity();
			result=presupuestosDeServiciosEntity.getColumns();
			break;
		case "professionals":
			ProfesionalesEntity profesionalesEntity = new ProfesionalesEntity();
			result=profesionalesEntity.getColumns();
			break;
		case "providers":
			ProveedoresEntity proveedoresEntity = new ProveedoresEntity();
			result=proveedoresEntity.getColumns();
			break;
		case "professional_surges":
			RecargosDeProfesionalesEntity recargosDeProfesionalesEntity = new RecargosDeProfesionalesEntity();
			result=recargosDeProfesionalesEntity.getColumns();
			break;
		case "claims":
			ReclamosEntity reclamosEntity = new ReclamosEntity();
			result= reclamosEntity.getColumns();
			break;
		case "wizard_responses":
			RespuestasWizardEntity respuestasWizardEntity = new RespuestasWizardEntity();
			result= respuestasWizardEntity.getColumns();
			break;
		case "services":
			ServiciosEntity serviciosEntity = new ServiciosEntity();
			result=serviciosEntity.getColumns();
			break;
		case "subscriptions":
			SuscripcionesEntity suscripcionesEntity = new SuscripcionesEntity();
			result=suscripcionesEntity.getColumns();
			break;
		case "users":
			UsersEntity usersEntity = new UsersEntity();
			result=usersEntity.getColumns();
			break;
		case "wizards_vars":
			VariablesWizardsEntity variablesWizardsEntity = new VariablesWizardsEntity();
			result=variablesWizardsEntity.getColumns();
			break;
		case "wizards":
			WizardsEntity wizardsEntity = new WizardsEntity();
			result=wizardsEntity.getColumns();
			break;
		case "coverage_balances":
			SiniestralidadEntity siniestralidadEntity = new SiniestralidadEntity();
			result=siniestralidadEntity.getColumns();
			break;
		case "service_status_change_history_logs":
			 HistorialDeServiciosEntity historialDeServiciosEntity = new HistorialDeServiciosEntity();
			result=historialDeServiciosEntity.getColumns();
			break;
		case "professional_services":
			 ServiciosDeProfesionalesEntity serviciosDeProfesionalesEntity = new ServiciosDeProfesionalesEntity();
			result=serviciosDeProfesionalesEntity.getColumns();
			break;
		case "services_order":
			 ServicesOrderEntity servicesOrderEntity = new ServicesOrderEntity();
			result=servicesOrderEntity.getColumns();
			break;
		}
		return result;
	}

}
