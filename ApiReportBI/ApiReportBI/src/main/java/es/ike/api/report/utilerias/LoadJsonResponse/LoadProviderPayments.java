package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.PagoAlProveedorEntity;
import es.ike.api.report.utilerias.JsonUtility;

public class LoadProviderPayments {
	public static void ProviderPayments(String jsonResponse, String country) throws Exception{
		// Se carga jsonResponse en Arreglo Json
				Integer iError = 0;
				Instant instant = Instant.now();
				// Obtiene configuracion de Entidad
				ApiConfiguration apiConfiguration = null;
				apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("22","22","provider_payments",country);
				Logger logger = Logger.getLogger("Logger apiReport LoadProviderPayments.ProviderPayments");
				// Se carga jsonResponse en Arreglo Json
				try {
					if (String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")) {
						JSONArray jsonarray = new JSONArray(jsonResponse);
						CloudTableClient tableClient = null;
						tableClient = TableClientProvider.getTableClientReference(country);
						// Se genera la variable tabla, con el nombre de la tabla en AZURE
						CloudTable table = tableClient.getTableReference("ProviderPayments");
						for (int a = 0; a<jsonarray.length(); a++) {
		                	JSONParser parser=null;
		            		JSONObject json=null;
		            		String strBusinessUnit="";
		            		String strPaymentId="";
		            		String strPaymentStatus="";
		            		String strServiceId="";
		            		String strCreatedAt="";
		            		String strUpdatedAt="";
		            		String strServicePrice="";
		            		String strServiceCost="";
		            		
		            		parser = new JSONParser();
		                    json = (JSONObject)parser.parse(jsonarray.get(a).toString());
		                    strBusinessUnit=JsonUtility.JsonValidaExisteLlave(json,"businessUnit");
		                    strPaymentId=JsonUtility.JsonValidaExisteLlave(json,"paymentId");
		                    strPaymentStatus=JsonUtility.JsonValidaExisteLlave(json, "paymentStatus");
		                    strServiceId=JsonUtility.JsonValidaExisteLlave(json, "serviceId");
		                    strCreatedAt=JsonUtility.JsonValidaExisteLlave(json, "createdAt");
		                    strUpdatedAt=JsonUtility.JsonValidaExisteLlave(json, "updatedAt");
		                    strServicePrice=JsonUtility.JsonValidaExisteLlave(json, "servicePrice");
		                    strServiceCost=JsonUtility.JsonValidaExisteLlave(json, "serviceCost");
		                   
		                    
		                    PagoAlProveedorEntity pagoAlProveedorEntity = new PagoAlProveedorEntity(strPaymentId,strBusinessUnit);
		                    pagoAlProveedorEntity.setEtag(pagoAlProveedorEntity.getEtag());
		                    pagoAlProveedorEntity.setBusinessUnit(strBusinessUnit != null ? UUID.fromString(strBusinessUnit): null);
		                    pagoAlProveedorEntity.setPaymentId(strPaymentId != null ? Integer.parseInt(strPaymentId): null);
		                    pagoAlProveedorEntity.setPaymentStatus(strPaymentStatus);
		                    pagoAlProveedorEntity.setServiceId(strServiceId != null ? Integer.parseInt(strServiceId): null);
		                    pagoAlProveedorEntity.setCreatedAt(strCreatedAt);
		                    pagoAlProveedorEntity.setUpdatedAt(strUpdatedAt);
		                    pagoAlProveedorEntity.setServicePrice(strServicePrice != null ? strServicePrice: "null");
		                    pagoAlProveedorEntity.setServiceCost(strServiceCost != null ? strServiceCost: "null");
		                    table.execute(TableOperation.insertOrReplace(pagoAlProveedorEntity));
						}
					} else {
						iError = 1;
						logger.log(Level.WARNING,"LoadProviderPayments.ProviderPayments= Respuesta No identificada " + country + "=" + jsonResponse);
					}
				} catch (Exception e) {
					iError = 1;
					logger.log(Level.WARNING,"LoadProviderPayments.ProviderPayments= Error al procesar arreglo e insertar en AZURE " + country + "=" + jsonResponse);
					e.printStackTrace();
				}
				// Fin de proceso de Entidad
				try {
					if (iError == 0) {
						// ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
						logger.log(Level.INFO," Se cargan registros entidad ProviderPayments con filtro ProcessFilterBu "+ country + "!!!");
					}
				} catch (Exception e) {
					iError = 1;
					logger.log(Level.WARNING,"LoadProviderPayments.ProviderPayments = Error al actualizar Fecha" + country);
					e.printStackTrace();
				}
				if (iError == 0) {
					logger.log(Level.INFO," Proceso Carga de Entidad con filtro ProviderPayments Finalizada " + country + "!!!!");
				}

		}

}
