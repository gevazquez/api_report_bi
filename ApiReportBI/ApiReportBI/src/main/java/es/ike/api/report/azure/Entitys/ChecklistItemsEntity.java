package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ChecklistItemsEntity extends TableServiceEntity{
	
	// --------------------------------------------------------------------
	public ChecklistItemsEntity(String id, String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;
	}
	// --------------------------------------------------------------------
	public ChecklistItemsEntity() {
		
	}
	
	public Integer id;
	public UUID businessUnit;
	public Integer checklistId;
	public Integer formularyId;
	public String description;
	public String createdAt;
	public String updatedAt;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public Integer getChecklistId() {
		return checklistId;
	}
	public void setChecklistId(Integer checklistId) {
		this.checklistId = checklistId;
	}
	public Integer getFormularyId() {
		return formularyId;
	}
	public void setFormularyId(Integer formularyId) {
		this.formularyId = formularyId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("businessUnit");
		list.add("checklistId");
		list.add("formularyId");
		list.add("description");
		list.add("createdAt");
		list.add("updatedAt");
		return list;
	}
	
	

}
