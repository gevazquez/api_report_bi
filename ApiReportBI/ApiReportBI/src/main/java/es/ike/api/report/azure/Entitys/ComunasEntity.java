package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ComunasEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public ComunasEntity (String id,String cityId) {
		this.partitionKey=id;
		this.rowKey=cityId;
		
	}
	// --------------------------------------------------------------------
	public ComunasEntity() {
	
	}
	// ------------------------------------------------------------------------------------------------------------
			public Integer id;
			public String name;
			public Integer cityId;
			public UUID tenantId;
			public String updatedAt;
			public String createdAt;
			
		
			
		public Integer getId() {
				return id;
			}
			public void setId(Integer id) {
				this.id = id;
			}
			public String getName() {
				return name;
			}
			public void setName(String name) {
				this.name = name;
			}
			public Integer getCityId() {
				return cityId;
			}
			public void setCityId(Integer cityId) {
				this.cityId = cityId;
			}
			public UUID getTenantId() {
				return tenantId;
			}
			public void setTenantId(UUID tenantId) {
				this.tenantId = tenantId;
			}
			public String getUpdatedAt() {
				return updatedAt;
			}
			public void setUpdatedAt(String updatedAt) {
				this.updatedAt = updatedAt;
			}
			public String getCreatedAt() {
				return createdAt;
			}
			public void setCreatedAt(String createdAt) {
				this.createdAt = createdAt;
			}
		public List<String> getColumns(){
			List<String> list = new ArrayList<>();
			list.add("id");
			list.add("name");
			list.add("cityId");
			list.add("tenantId");
			list.add("updatedAt");
			list.add("createdAt");
			return list;
		}
			

}
