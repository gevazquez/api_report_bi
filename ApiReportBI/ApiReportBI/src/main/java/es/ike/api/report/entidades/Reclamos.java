package es.ike.api.report.entidades;

import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.azure.data.tables.models.TableServiceException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.ReclamosEntity;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;
import es.ike.api.report.utilerias.ActualizaFecha;
import es.ike.api.report.utilerias.DatosPaginacion;
import es.ike.api.report.utilerias.Fecha;
import es.ike.api.report.utilerias.JsonUtility;
import es.ike.api.report.utilerias.Paginacion;

public class Reclamos {

	public static void ProcesoEntidadReclamos(String StrToken,String country) throws Exception,TableServiceException{
		Integer iError=0;
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		Instant instant=Instant.now();
		apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("19","19","claims",country);
		//agregar parametro 
		System.out.println(instant.toString()+" Obtiene Configuacion "+country+" de Entidad "+apiConfiguration.getEntidad());
		//Crea tabla Reclamos si no existe en Azure
		CloudTableClient tableClient1 = null;
		CreateTableConfiguration tableConfiguration = null;
        tableClient1 = TableClientProvider.getTableClientReference(country);
        tableConfiguration.createTable(tableClient1, "Claims",country);
		//Obtenemos Token de OAuth de TL
		String token="";
		token=ObtenerOauthTL.OAuthAPI_Report(country);
		System.out.println(instant.toString()+" Reclamos.ProcesoEntidadReclamos=Obtiene Token "+country+" "+token);
		//Inserta conteo total
			if(!token.equalsIgnoreCase("")) {
				String StrJSONContoTotal="{"
						+"\n"+"\"query\":\"{"+
						"claims_aggregate {"+
						"aggregate {"+
						"count"+
							"}"+
						"}"+
		                	"}\""+
						 "}";
				String jsonResponseConteoTotal="";
				String StrConteoTotal="";
				Integer numConteoTotal = 0;
				jsonResponseConteoTotal=obtenerJsonTuten.getJson(token,StrJSONContoTotal,country);
				//Inicia Parseo de conteo Total
			    try {
			    	JSONParser parser = new JSONParser();
			    	JSONObject json=(JSONObject) parser.parse(jsonResponseConteoTotal);
			    	jsonResponseConteoTotal = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
			    	parser=null;
		            json=null;
		            parser = new JSONParser();
		            json = (JSONObject) parser.parse(jsonResponseConteoTotal);
		            jsonResponseConteoTotal=(JsonUtility.validateJsonData(json,"claims_aggregate"))?json.get("claims_aggregate").toString():"";
		            parser=null;
	                json=null;
	                parser = new JSONParser();
	                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
	                jsonResponseConteoTotal=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
	                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
	                StrConteoTotal=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
	                numConteoTotal = Integer.parseInt(StrConteoTotal);
	                try {
	                	apiConfiguration.setConteoTotal(numConteoTotal);
	                }catch(Exception e) {
	                	iError=1;
				    	System.out.println("Reclamos.ProcesoEntidadReclamos = Error al cargar conteo total en Azure Entidad Reclamos "+country);
	                }
			    }catch(Exception e) {
			    	iError=1;
			    	System.out.println("Reclamos.ProcesoEntidadReclamos = Error al parsear JsonResponseConteoTotal de Entidad Reclamos "+country);}
				
			}else {System.out.println("Reclamos.ProcesoEntidadReclamos = Problema en obtener el Token del conteo Total "+country);	}
			
		//Implementación de filtro where
		String StrWhere=Fecha.filtroFecha(apiConfiguration, "businessUnit");
		String StrWhereCount="";
		String StrWhereQuery="";
		if (!StrWhere.equalsIgnoreCase("")) {
		   	StrWhereCount="("+StrWhere+")"; }
		if (!StrWhere.equalsIgnoreCase("")) {
		   	StrWhereQuery=","+StrWhere; }
		if(!token.equalsIgnoreCase("")) {
			String StrJSONCount="{"
					+"\n"+"\"query\":\"{"+
					"claims_aggregate "+StrWhereCount+"{"+
					"aggregate {"+
					"count"+
						"}"+
					"}"+
	                	"}\""+
					 "}";
			//Implementacion de paginación
			try {
				String jsonResponseCount="";
				Integer paginas=0;
				String StrCount="";
				Integer numCount = 0;
				String jsonResponse="";
				Integer numPaginacion=0;
				Integer offset=0;
				jsonResponseCount=obtenerJsonTuten.getJson(token,StrJSONCount,country);
				if(!jsonResponseCount.equalsIgnoreCase("")) { 
					//---------------------------------------------------------------------
					//Inicia Parseo de count
				    try {
				    	JSONParser parser = new JSONParser();
				    	JSONObject json=(JSONObject) parser.parse(jsonResponseCount);
				    	jsonResponseCount = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
				    	parser=null;
			            json=null;
			            parser = new JSONParser();
			            json = (JSONObject) parser.parse(jsonResponseCount);
			            jsonResponseCount=(JsonUtility.validateJsonData(json,"claims_aggregate"))?json.get("claims_aggregate").toString():"";
			            parser=null;
		                json=null;
		                parser = new JSONParser();
		                json= (JSONObject) parser.parse(jsonResponseCount);
		                jsonResponseCount=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
		                json= (JSONObject) parser.parse(jsonResponseCount);
		                StrCount=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
		                numCount = Integer.parseInt(StrCount);
				    }catch(Exception e) {
				    	iError=1;
				    	System.out.println("Reclamos.ProcesoEntidadReclamos = Error al parsear JsonResponseCount de Entidad Reclamos "+country);}
				    //llamada al archivo properties
					Thread currentThread = Thread.currentThread();
					ClassLoader contextClassLoader = currentThread.getContextClassLoader();
					URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+country);
					Properties prop = new Properties();
					if (resource == null) {	
						iError=1;
						throw new Exception("Reclamos.ProcesoEntidadReclamos = No se pudo leer el archivo de configuración TutenLabsconfig/TutenLabsConfig.properties"+country);		}
					try (InputStream is = resource.openStream()) {
						prop.load(is);
					//Se obtiene el numero de paginacion del archivo properties
						numPaginacion=Integer.parseInt(prop.getProperty("intPaginacion"));
					}catch(Exception e) {
						iError=1;
						System.out.println("Reclamos.ProcesoEntidadReclamos = Error al obtener intPaginacion del archivo TutenLabsconfig/TutenLabsConfig.properties"+country);
					}
					//Se llama y ejecuta el metodo calculaPaginacion
					Paginacion paginacion = new Paginacion();
					DatosPaginacion datosPaginacion = null;
					datosPaginacion= paginacion.calculaPaginacion(numCount, numPaginacion);
					if(datosPaginacion.getResto() ==0) { 	paginas=datosPaginacion.getEnteros();;
					}else {	paginas=datosPaginacion.getEnteros()+1;			}
					for(int i =0;i<paginas;i++){
							String StrJSON="{"
									+"\n"+"\"query\":\"{"+
									apiConfiguration.getEntidad()+"(limit:"+numPaginacion+",offset:"+offset+" "+StrWhereQuery+ ")" +"{ "+
									  " id"+
									  " businessUnit"+
									  " createdAt"+
									  " updatedAt"+
									  " accountNumber"+
									  " claimAccountTypeId"+
									  " claimAccountTypeName"+
									  " claimActioTypeName"+
									  " claimActionTypeId"+
									  " claimBankId"+
									  " claimBankName"+
									  " claimBeneficiaryName"+
									  " claimCompensationTypeId"+
									  " claimCompensationTypeName"+
									  " claimDocumentTypeId"+
									  " claimDocumentTypeName"+
									  " claimPaymentTypeId"+
									  " claimPaymentTypeName"+
									  " claimPriorityId"+
									  " claimPriorityName"+
									  " claimResponseTypeId"+
									  " claimResponseTypeName"+
									  " claimStatusId"+
									  " claimStatusName"+
									  " claimSubTypificationId"+
									  " claimSubTypificationName"+
									  " claimTypeId"+
									  " claimTypeName"+
									  " claimTypificationId"+
									  " claimTypificationName"+
									  " beneficiaryDocumentNumber"+
									  " beneficiaryName"+
									  " description"+
									  " monetaryAmount"+
									  " monetaryUnit"+
									  " claimResponsibleAreaId"+
									  " claimResponsibleAreaName"+
									  " caseNumber"+
									  " caseId"+
								  
									  
		                            " }"+
		                        "}\""+
		                    "}";
							offset=offset+numPaginacion;
							jsonResponse=obtenerJsonTuten.getJson(StrToken, StrJSON,country);  
							if(!jsonResponse.equalsIgnoreCase("")) {
								//Parsear para obtener Arreglo json de Entidad
							    try {
							    	JSONParser parser = new JSONParser();
							        JSONObject json=(JSONObject) parser.parse(jsonResponse);
							        jsonResponse = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
							        parser=null;
						            json=null;
						            parser = new JSONParser();
						            json = (JSONObject) parser.parse(jsonResponse);
						            jsonResponse = (JsonUtility.validateJsonData(json,apiConfiguration.getEntidad()))?json.get(apiConfiguration.getEntidad()).toString():"";
							    }catch(Exception e){
							    	iError=1;
							    	System.out.println("Reclamos.ProcesoEntidadReclamos = Error al parsear JsonResponse de Entidad Reclamos "+country);
							    	e.printStackTrace();
							    	}
								//Se carga jsonResponse en Arreglo Json
							    try {
							    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
						            	JSONArray jsonarray= new JSONArray(jsonResponse);  
						            	CloudTableClient tableClient = null;
						                tableClient = TableClientProvider.getTableClientReference(country);
						                //Se genera la variable tabla, con el nombre de la tabla en AZURE
						                CloudTable table = tableClient.getTableReference("Claims");
						            	for (int a = 0; a<jsonarray.length(); a++) {
						            		JSONParser parser=null;
						            		JSONObject json=null;
						            		  String strId="";
						            		  String strBusinessUnit="";
						            		  String strCreatedAt="";
						            		  String strUpdatedAt="";
						            		  String strAccountNumber="";
						            		  String strClaimAccountTypeId="";
						            		  String strClaimAccountTypeName="";
						            		  String strClaimActioTypeName="";
						            		  String strClaimActionTypeId="";
						            		  String strClaimBankId="";
						            		  String strClaimBankName="";
						            		  String strClaimBeneficiaryName="";
						            		  String strClaimCompensationTypeId="";
						            		  String strClaimCompensationTypeName="";
						            		  String strClaimDocumentTypeId="";
						            		  String strClaimDocumentTypeName="";
						            		  String strClaimPaymentTypeId="";
						            		  String strClaimPaymentTypeName="";
						            		  String strClaimPriorityId="";
						            		  String strClaimPriorityName="";
						            		  String strClaimResponseTypeId="";
						            		  String strClaimResponseTypeName="";
						            		  String strClaimStatusId="";
						            		  String strClaimStatusName="";
						            		  String strClaimSubTypificationId="";
						            		  String strClaimSubTypificationName="";
						            		  String strClaimTypeId="";
						            		  String strClaimTypeName="";
						            		  String strClaimTypificationId="";
						            		  String strClaimTypificationName="";
						            		  String strBeneficiaryDocumentNumber="";
						            		  String strBeneficiaryName="";
						            		  String strDescription="";
						            		  String strMonetaryAmount="";
						            		  String strMonetaryUnit="";
						            		  String strClaimResponsibleAreaId="";
						            		  String strClaimResponsibleAreaName="";
						            		  String strCaseNumber= "";
						            		  String strCaseId="";

						            		  
						                    
						                    parser = new JSONParser();
						                    json = (JSONObject)parser.parse(jsonarray.get(a).toString());
						                    strId =JsonUtility.JsonValidaExisteLlave(json,"id");
						                    strBusinessUnit=JsonUtility.JsonValidaExisteLlave(json,"businessUnit");
						                    strCreatedAt=JsonUtility.JsonValidaExisteLlave(json,"createdAt");
						                    strUpdatedAt=JsonUtility.JsonValidaExisteLlave(json,"updatedAt");
						                    strAccountNumber=JsonUtility.JsonValidaExisteLlave(json,"accountNumber");
						                    strClaimAccountTypeId =JsonUtility.JsonValidaExisteLlave(json,"claimAccountTypeId");
						                    strClaimAccountTypeName =JsonUtility.JsonValidaExisteLlave(json,"claimAccountTypeName");
						                    strClaimActioTypeName =JsonUtility.JsonValidaExisteLlave(json,"claimActioTypeName");
						                    strClaimActionTypeId =JsonUtility.JsonValidaExisteLlave(json,"claimActionTypeId");
											strClaimBankId =JsonUtility.JsonValidaExisteLlave(json,"claimBankId");
											strClaimBankName =JsonUtility.JsonValidaExisteLlave(json,"claimBankName");
											strClaimBeneficiaryName =JsonUtility.JsonValidaExisteLlave(json,"claimBeneficiaryName");
						                    strClaimCompensationTypeId = JsonUtility.JsonValidaExisteLlave(json,"claimCompensationTypeId");
						                    strClaimCompensationTypeName =JsonUtility.JsonValidaExisteLlave(json,"claimCompensationTypeName");
						                    strClaimDocumentTypeId =JsonUtility.JsonValidaExisteLlave(json,"claimDocumentTypeId");
						                    strClaimDocumentTypeName =JsonUtility.JsonValidaExisteLlave(json,"claimDocumentTypeName");
						                    strClaimPaymentTypeId =JsonUtility.JsonValidaExisteLlave(json,"claimPaymentTypeId");
						                    strClaimPaymentTypeName =JsonUtility.JsonValidaExisteLlave(json,"claimPaymentTypeName");
						                    strClaimPriorityId =JsonUtility.JsonValidaExisteLlave(json,"claimPriorityId");
						                    strClaimPriorityName =JsonUtility.JsonValidaExisteLlave(json,"claimPriorityName");
						                    strClaimResponseTypeId =JsonUtility.JsonValidaExisteLlave(json,"claimResponseTypeId");
						                    strClaimResponseTypeName =JsonUtility.JsonValidaExisteLlave(json,"claimResponseTypeName");
						                    strClaimStatusId =JsonUtility.JsonValidaExisteLlave(json,"claimStatusId");
						                    strClaimStatusName =JsonUtility.JsonValidaExisteLlave(json,"claimStatusName");
						                    strClaimSubTypificationId =JsonUtility.JsonValidaExisteLlave(json,"claimSubTypificationId");
						                    strClaimSubTypificationName =JsonUtility.JsonValidaExisteLlave(json,"claimSubTypificationName");
						                    strClaimTypeId =JsonUtility.JsonValidaExisteLlave(json,"claimTypeId");
						                    strClaimTypeName =JsonUtility.JsonValidaExisteLlave(json,"claimTypeName");
						                    strClaimTypificationId =JsonUtility.JsonValidaExisteLlave(json,"claimTypificationId");
						                    strClaimTypificationName =JsonUtility.JsonValidaExisteLlave(json,"claimTypificationName");
						                    strBeneficiaryDocumentNumber  =JsonUtility.JsonValidaExisteLlave(json,"beneficiaryDocumentNumber");
						                    strBeneficiaryName  =JsonUtility.JsonValidaExisteLlave(json,"beneficiaryName");
						                    strDescription =JsonUtility.JsonValidaExisteLlave(json,"description");
						                    strMonetaryAmount =JsonUtility.JsonValidaExisteLlave(json,"monetaryAmount");
						                    strMonetaryUnit =JsonUtility.JsonValidaExisteLlave(json,"monetaryUnit");
						                    strClaimResponsibleAreaId =JsonUtility.JsonValidaExisteLlave(json,"claimResponsibleAreaId");
						                    strClaimResponsibleAreaName =JsonUtility.JsonValidaExisteLlave(json,"claimResponsibleAreaName");
						                    strCaseNumber = JsonUtility.JsonValidaExisteLlave(json,"caseNumber");
						                    strCaseId = JsonUtility.JsonValidaExisteLlave(json,"caseId");
						                    
						                    
						                    
						                    
						            		ReclamosEntity reclamosEntity = new ReclamosEntity(strId,strBusinessUnit);
						            		reclamosEntity.setEtag(reclamosEntity.getEtag());
						            		reclamosEntity.setId(strId != null ? Integer.parseInt(strId): null );
						            		reclamosEntity.setBusinessUnit(strBusinessUnit != null ? UUID.fromString(strBusinessUnit): null);
						            		reclamosEntity.setCreatedAt(strCreatedAt);
						            		reclamosEntity.setUpdatedAt(strUpdatedAt);
						            		reclamosEntity.setAccountNumber(strAccountNumber  != null ? strAccountNumber: "null");
						            		reclamosEntity.setClaimAccountTypeId(strClaimAccountTypeId != null ? Integer.parseInt(strClaimAccountTypeId) : -1);
						            		reclamosEntity.setClaimAccountTypeName(strClaimAccountTypeName  != null ? strClaimAccountTypeName: "null");
						            		reclamosEntity.setClaimActioTypeName(strClaimActioTypeName != null ? strClaimActioTypeName: "null");
						            		reclamosEntity.setClaimActionTypeId(strClaimActionTypeId != null ? Integer.parseInt(strClaimActionTypeId) : -1);
						            		reclamosEntity.setClaimBankId(strClaimBankId != null ? Integer.parseInt(strClaimBankId) : -1 );
						            		reclamosEntity.setClaimBankName(strClaimBankName != null ? strClaimBankName : "null" );
						            		reclamosEntity.setClaimBeneficiaryName(strClaimBeneficiaryName != null ? Boolean.valueOf(strClaimBeneficiaryName) : false);
						            		reclamosEntity.setClaimCompensationTypeId(strClaimCompensationTypeId != null ? Integer.parseInt(strClaimCompensationTypeId) : -1);
						            		reclamosEntity.setClaimCompensationTypeName(strClaimCompensationTypeName != null ? strClaimCompensationTypeName : "null");
						            		reclamosEntity.setClaimDocumentTypeId(strClaimDocumentTypeId != null ? Integer.parseInt(strClaimDocumentTypeId) : -1);
						            		reclamosEntity.setClaimDocumentTypeName(strClaimDocumentTypeName != null ? strClaimDocumentTypeName : "null");
						            		reclamosEntity.setClaimPaymentTypeId(strClaimPaymentTypeId != null ? Integer.parseInt(strClaimPaymentTypeId) : -1);
						            		reclamosEntity.setClaimPaymentTypeName(strClaimPaymentTypeName != null ? strClaimPaymentTypeName : "null");
						            		reclamosEntity.setClaimPriorityId(strClaimPriorityId != null ? Integer.parseInt(strClaimPriorityId) : -1);
						            		reclamosEntity.setClaimPriorityName(strClaimPriorityName != null ? strClaimPriorityName : "null");
						            		reclamosEntity.setClaimResponseTypeId(strClaimResponseTypeId != null ? Integer.parseInt(strClaimResponseTypeId) : -1);
						            		reclamosEntity.setClaimResponseTypeName(strClaimResponseTypeName != null ? strClaimResponseTypeName : "null");
						            		reclamosEntity.setClaimStatusId(strClaimStatusId != null ? Integer.parseInt(strClaimStatusId) : -1);
						            		reclamosEntity.setClaimStatusName(strClaimStatusName != null ? strClaimStatusName : "null");
						            		reclamosEntity.setClaimSubTypificationId(strClaimSubTypificationId != null ? Integer.parseInt(strClaimSubTypificationId) : -1);
						            		reclamosEntity.setClaimSubTypificationName(strClaimSubTypificationName != null ? strClaimSubTypificationName : "null");
						            		reclamosEntity.setClaimTypeId(strClaimTypeId != null ? Integer.parseInt(strClaimTypeId) : -1);
						            		reclamosEntity.setClaimTypeName(strClaimTypeName != null ? strClaimSubTypificationName : "null");
						            		reclamosEntity.setClaimTypificationId(strClaimTypificationId != null ? Integer.parseInt(strClaimTypificationId) : -1);
						            		reclamosEntity.setClaimTypificationName(strClaimTypificationName != null ? strClaimTypificationName : "null");
						            	    reclamosEntity.setBeneficiaryDocumentNumber(strBeneficiaryDocumentNumber != null ? strBeneficiaryDocumentNumber : "null");
						            		reclamosEntity.setBeneficiaryName(strBeneficiaryName != null ? strBeneficiaryName : "null");
						            		reclamosEntity.setDescription(strDescription != null ? strDescription : "null");
						            		reclamosEntity.setMonetaryAmount(strMonetaryAmount != null ? Integer.parseInt(strMonetaryAmount) : -1);
						            		reclamosEntity.setMonetaryUnit(strMonetaryUnit != null ? strMonetaryUnit : "null");
						            		reclamosEntity.setClaimResponsibleAreaId(strClaimResponsibleAreaId != null ? Integer.parseInt(strClaimResponsibleAreaId) : -1);
						            		reclamosEntity.setClaimResponsibleAreaName(strClaimResponsibleAreaName != null ? strClaimResponsibleAreaName : "null");
						            		reclamosEntity.setCaseNumber(strCaseNumber != null ? strCaseNumber : "null");
						            		reclamosEntity.setCaseId(strCaseId != null ? Integer.parseInt(strCaseId): 0);
						            		
						            		table.execute(TableOperation.insertOrReplace(reclamosEntity));
						            		}
						            }else {
						            	iError=1;
						            	System.out.println("Reclamos.ProcesoEntidadReclamos = Respuesta No identificada "+country+"="+jsonResponse);      }
							    }catch(Exception e) {
							    	iError=1;
							    	System.out.println("Reclamos.ProcesoEntidadReclamos = Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse);
							    	e.printStackTrace();
							    }	
							}else {
								iError=1;
						    	System.out.println("Reclamos.ProcesoEntidadReclamos = Error en JSON QUERY GRAPHQL "+country);
							}
					}
				}else {
					iError=1;
					System.out.println("Reclamos.ProcesoEntidadReclamos = Error en JSON QUERY COUNT "+country);
				}
			}catch(Exception e){
				iError=1;
				System.out.println("Reclamos.ProcesoEntidadReclamos = Error ejecutar paginación "+country);
				e.printStackTrace();
				}
	        //Fin de proceso de Entidad
			try { 	
				if(iError==0) {
					ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
					System.out.println(instant.toString()+" Actualizacion de Fecha completada "+country+"!!!");
					}
				}
			catch(Exception e) {
				iError=1;
				System.out.println("Reclamos.ProcesoEntidadReclamos = Error al actualizar Fecha "+country);
				e.printStackTrace();
			}
			if(iError==0) {
				System.out.println(instant.toString()+" Proceso de Entidad Reclamos Finalizada "+country+"!!!!");}
		}else {System.out.println("Reclamos.ProcesoEntidadReclamos = Problema en obtener el Token "+country);	}
	}
}