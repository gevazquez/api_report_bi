package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class SuscripcionesEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public SuscripcionesEntity(String id, String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;

	}

	// --------------------------------------------------------------------
	public SuscripcionesEntity() {

	}

	// ------------------------------------------------------------------------------------------------------------{
	public Integer id;
	public UUID businessUnit;
	public String planName;
	public String category;
	public String planRcu;
	public String subscriptionRcu;
	public String start;
	public String end;
	public String createdAt;
	public String updatedAt;
	public Boolean validity;
	public Boolean status;
	public String beneficiaryIds;
	public String materialIds;
	public Integer titularId;
	public Boolean planSubscriptionStatus;
	public String purchaseChannel;
	public String planSubscriptionCoverageStatus;
	public Integer contractorId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPlanRcu() {
		return planRcu;
	}

	public void setPlanRcu(String planRcu) {
		this.planRcu = planRcu;
	}

	public String getSubscriptionRcu() {
		return subscriptionRcu;
	}

	public void setSubscriptionRcu(String subscriptionRcu) {
		this.subscriptionRcu = subscriptionRcu;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Boolean getValidity() {
		return validity;
	}

	public void setValidity(Boolean validity) {
		this.validity = validity;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getBeneficiaryIds() {
		return beneficiaryIds;
	}

	public void setBeneficiaryIds(String beneficiaryIds) {
		this.beneficiaryIds = beneficiaryIds;
	}

	public String getMaterialIds() {
		return materialIds;
	}

	public void setMaterialIds(String materialIds) {
		this.materialIds = materialIds;
	}

	public Integer getTitularId() {
		return titularId;
	}

	public void setTitularId(Integer titularId) {
		this.titularId = titularId;
	}

	public Boolean getPlanSubscriptionStatus() {
		return planSubscriptionStatus;
	}

	public void setPlanSubscriptionStatus(Boolean planSubscriptionStatus) {
		this.planSubscriptionStatus = planSubscriptionStatus;
	}

	public String getPurchaseChannel() {
		return purchaseChannel;
	}

	public void setPurchaseChannel(String purchaseChannel) {
		this.purchaseChannel = purchaseChannel;
	}

	public String getPlanSubscriptionCoverageStatus() {
		return planSubscriptionCoverageStatus;
	}

	public void setPlanSubscriptionCoverageStatus(String planSubscriptionCoverageStatus) {
		this.planSubscriptionCoverageStatus = planSubscriptionCoverageStatus;
	}

	public Integer getContractorId() {
		return contractorId;
	}

	public void setContractorId(Integer contractorId) {
		this.contractorId = contractorId;
	}

	public List<String> getColumns() {
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("businessUnit");
		list.add("planName");
		list.add("category");
		list.add("planRcu");
		list.add("subscriptionRcu");
		list.add("start");
		list.add("end");
		list.add("createdAt");
		list.add("updatedAt");
		list.add("validity");
		list.add("status");
		list.add("beneficiaryIds");
		list.add("materialIds");
		list.add("titularId");
		list.add("planSubscriptionStatus");
		list.add("purchaseChannel");
		list.add("planSubscriptionCoverageStatus");
		list.add("contractorId");
		return list;
	}

}
