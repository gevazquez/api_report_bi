package es.ike.api.report.entidades;

import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.azure.data.tables.models.TableServiceException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;
import es.ike.api.report.azure.CreateTableConfiguration;


import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.BusinessUnitsEntity;
import es.ike.api.report.azure.Entitys.ProfesionalesEntity;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;
import es.ike.api.report.utilerias.ActualizaFecha;
import es.ike.api.report.utilerias.DatosPaginacion;
import es.ike.api.report.utilerias.Fecha;
import es.ike.api.report.utilerias.JsonUtility;
import es.ike.api.report.utilerias.Paginacion;

public class Profesionales {
	public static void ProcesoEntidadProfesionales(String StrToken,String country) throws Exception,TableServiceException{
		Integer iError=0;
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		Instant instant=Instant.now();
		apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("8","8","professionals",country);
		System.out.println(instant.toString()+" Obtiene Configuacion "+country+" de Entidad "+apiConfiguration.getEntidad());
		//Crea tabla Professionals si no existe en Azure
		CloudTableClient tableClient1 = null;
		CreateTableConfiguration tableConfiguration = null;
		tableClient1 = TableClientProvider.getTableClientReference(country);
		tableConfiguration.createTable(tableClient1, "Professionals",country);
		//Obtenemos Token de OAuth de TL
		String token="";
		token=ObtenerOauthTL.OAuthAPI_Report(country);
		System.out.println(instant.toString()+" Profesionales.ProcesoEntidadProfesionales=Obtiene Token "+country+" "+token);
		//Inserta conteo total
		if(!token.equalsIgnoreCase("")) {
			String StrJSONContoTotal="{"
					+"\n"+"\"query\":\"{"+
					"professionals_aggregate {"+
					"aggregate {"+
					"count"+
						"}"+
					"}"+
	                	"}\""+
					 "}";
			String jsonResponseConteoTotal="";
			String StrConteoTotal="";
			Integer numConteoTotal = 0;
			jsonResponseConteoTotal=obtenerJsonTuten.getJson(token,StrJSONContoTotal,country);
			//Inicia Parseo de conteo Total
		    try {
		    	JSONParser parser = new JSONParser();
		    	JSONObject json=(JSONObject) parser.parse(jsonResponseConteoTotal);
		    	jsonResponseConteoTotal = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
		    	parser=null;
	            json=null;
	            parser = new JSONParser();
	            json = (JSONObject) parser.parse(jsonResponseConteoTotal);
	            jsonResponseConteoTotal=(JsonUtility.validateJsonData(json,"professionals_aggregate"))?json.get("professionals_aggregate").toString():"";
	            parser=null;
                json=null;
                parser = new JSONParser();
                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
                jsonResponseConteoTotal=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
                StrConteoTotal=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
                numConteoTotal = Integer.parseInt(StrConteoTotal);
                try {
                	apiConfiguration.setConteoTotal(numConteoTotal);
                }catch(Exception e) {
                	iError=1;
			    	System.out.println("Profesionales.ProcesoEntidadProfesionales = Error al cargar conteo total en Azure Entidad Profesionales "+country);
                }
		    }catch(Exception e) {
		    	iError=1;
		    	System.out.println("Profesionales.ProcesoEntidadProfesionales = Error al parsear JsonResponseConteoTotal de Entidad Profesionales "+country);}
			
		}else {System.out.println("Profesionales.ProcesoEntidadProfesionales= Problema en obtener el Token del conteo Total "+country);	}
		//Implementación de filtro where
		String StrWhere=Fecha.filtroFecha(apiConfiguration, "id");
		String StrWhereCount="";
		String StrWhereQuery="";
		if (!StrWhere.equalsIgnoreCase("")) {
		   	StrWhereCount="("+StrWhere+")"; }
		if (!StrWhere.equalsIgnoreCase("")) {
		   	StrWhereQuery=","+StrWhere; }
		if(!token.equalsIgnoreCase("")) {
			String StrJSONCount="{"
					+"\n"+"\"query\":\"{"+
					"professionals_aggregate "+StrWhereCount+" {"+
					"aggregate {"+
					" count"+
						 "}"+		
					"}"+
						 "}\""+
					 "}";
			//Implementacion de paginación
			try {
				String jsonResponseCount="";
				Integer paginas=0;
				String StrCount="";
				Integer numCount = 0;
				String jsonResponse="";
				Integer numPaginacion=0;
				Integer offset=0;
				jsonResponseCount=obtenerJsonTuten.getJson(token,StrJSONCount,country);
				if(!jsonResponseCount.equalsIgnoreCase("")) { 
					//---------------------------------------------------------------------
					//Inicia Parseo de count
				    try {
				    	JSONParser parser = new JSONParser();
				    	JSONObject json=(JSONObject) parser.parse(jsonResponseCount);
				    	jsonResponseCount = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
				    	parser=null;
			            json=null;
			            parser = new JSONParser();
			            json = (JSONObject) parser.parse(jsonResponseCount);
			            jsonResponseCount=(JsonUtility.validateJsonData(json,"professionals_aggregate"))?json.get("professionals_aggregate").toString():"";
			            parser=null;
		                json=null;
		                parser = new JSONParser();
		                json= (JSONObject) parser.parse(jsonResponseCount);
		                jsonResponseCount=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
		                json= (JSONObject) parser.parse(jsonResponseCount);
		                StrCount=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
		                numCount = Integer.parseInt(StrCount);
				    }catch(Exception e) {
				    	iError=1;
				    	System.out.println("Profesionales.ProcesoEntidadProfesionales = Error al parsear JsonResponseCount de Entidad Profesionales "+country);}
				    //llamada al archivo properties
					Thread currentThread = Thread.currentThread();
					ClassLoader contextClassLoader = currentThread.getContextClassLoader();
					URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+country);
					Properties prop = new Properties();
					if (resource == null) {	
						iError=1;
						throw new Exception("Profesionales.ProcesoEntidadProfesionales= No se pudo leer el archivo de configuración TutenLabsconfig/TutenLabsConfig.properties"+country);		}
					try (InputStream is = resource.openStream()) {
						prop.load(is);
					//Se obtiene el numero de paginacion del archivo properties
						numPaginacion=Integer.parseInt(prop.getProperty("intPaginacion"));
					}catch(Exception e) {
						iError=1;
						System.out.println("Profesionales.ProcesoEntidadProfesionales= Error al obtener intPaginacion del archivo TutenLabsconfig/TutenLabsConfig.properties"+country);
					}
					//Se llama y ejecuta el metodo calculaPaginacion
					Paginacion paginacion = new Paginacion();
					DatosPaginacion datosPaginacion = null;
					datosPaginacion= paginacion.calculaPaginacion(numCount, numPaginacion);
					if(datosPaginacion.getResto() ==0) { 	paginas=datosPaginacion.getEnteros();;
					}else {	paginas=datosPaginacion.getEnteros()+1;			}
					for(int i =0;i<paginas;i++){
							String StrJSON="{"
									+"\n"+"\"query\":\"{"+
									apiConfiguration.getEntidad()+"(limit:"+numPaginacion+",offset:"+offset+" "+StrWhereQuery+")" +"{ "+
									" id"+
									" businessUnit"+
									" name"+
									" lastName"+
									" email"+
									" phoneNumber"+
									" createdAt"+
									" updatedAt"+
									" providerId"+
									" status"+
									" approvalAt"+
									" approved"+
									" bank"+
									" branchId"+
									" identificationTypeId"+
									" identificationTypeNumber"+
									" numberAccount"+
									" typeAccount"+
									" address"+
									" hourApproved"+
											" }"+
					                	"}\""+
									 "}";
							offset=offset+numPaginacion;
							jsonResponse=obtenerJsonTuten.getJson(StrToken, StrJSON,country);  
							if(!jsonResponse.equalsIgnoreCase("")) {
								//Parsear para obtener Arreglo json de Entidad
							    try {
							    	JSONParser parser = new JSONParser();
							        JSONObject json=(JSONObject) parser.parse(jsonResponse);
							        jsonResponse = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
							        parser=null;
						            json=null;
						            parser = new JSONParser();
						            json = (JSONObject) parser.parse(jsonResponse);
						            jsonResponse = (JsonUtility.validateJsonData(json,apiConfiguration.getEntidad()))?json.get(apiConfiguration.getEntidad()).toString():"";
							    }catch(Exception e){
							    	iError=1;
							    	System.out.println("Profesionales.ProcesoEntidadProfesionales = Error al parsear JsonResponse de Entidad Profesionales "+country);
							    	e.printStackTrace();
							    	}
								//Se carga jsonResponse en Arreglo Json
							    try {
							    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
						            	JSONArray jsonarray= new JSONArray(jsonResponse);  
						            	CloudTableClient tableClient = null;
						                tableClient = TableClientProvider.getTableClientReference(country);
						                //Se genera la variable tabla, con el nombre de la tabla en AZURE
						                CloudTable table = tableClient.getTableReference("Professionals");
						            	for (int a = 0; a<jsonarray.length(); a++) {
						            		JSONParser parser=null;
						            		JSONObject jsonProfessionals=null;
						            		String StrId="";
						            		String StrUuidBusinessUnit="";
						            		String StrName="";
						            		String StrLastName="";
						            		String StrEmail="";
						            		String StrPhoneNumber="";
						            		String StrCreatedAt="";
						            		String StrUpdatedAt="";
						            		
						            	    String StrProviderId="";
						            		String StrStatus="";
						            		String StrApprovalAt="";
						            		String StrApproved="";
						            		String StrBank="";
						            		String StrBranchId="";
						            		String StrIdentificationTypeId="";
						            		String StrIdentificationTypeNumber="";
						            		String StrNumberAccount="";
						            		String StrTypeAccount="";
						            		String StrAddress="";
						            		String StrHourApproved="";
						            		
						            		parser = new JSONParser();
						                    jsonProfessionals = (JSONObject)parser.parse(jsonarray.get(a).toString());
						                    StrId = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"id");
						                    StrUuidBusinessUnit = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"businessUnit");
						            		StrName = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"name" );
						            		StrLastName = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"lastName" );
						            		StrEmail = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"email" );
						            		StrPhoneNumber = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"phoneNumber" );
						            		StrCreatedAt = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"createdAt" );
						            		StrUpdatedAt = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"updatedAt" );
						            		StrProviderId = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"providerId" );
						            		StrStatus = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"status" );
						            		StrApprovalAt = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"approvalAt" );
						            		StrApproved = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"approved" );
						            		StrBank = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"bank" );
						            		StrBranchId = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"branchId" );
						            		StrIdentificationTypeId = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"identificationTypeId" );
						            		StrIdentificationTypeNumber = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"identificationTypeNumber" );
						            		StrNumberAccount = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"numberAccount" );
						            		StrTypeAccount = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"typeAccount" );
						            		StrAddress = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"address" );
						            		StrHourApproved = JsonUtility.JsonValidaExisteLlave(jsonProfessionals,"hourApproved" );
						            		
						                    ProfesionalesEntity profesionalesEntity = new ProfesionalesEntity(StrId,StrUuidBusinessUnit);   //PartitionKey & RowKey
						                    profesionalesEntity.setEtag(profesionalesEntity.getEtag());
						                    profesionalesEntity.setId(StrId != null ? Integer.parseInt(StrId) : null);
						                    profesionalesEntity.setBusinessUnit(StrUuidBusinessUnit !=null ? UUID.fromString(StrUuidBusinessUnit):null);
						                    profesionalesEntity.setName(StrName);
						                    profesionalesEntity.setLastName(StrLastName);
						                    profesionalesEntity.setEmail(StrEmail);
						                    profesionalesEntity.setPhoneNumber(StrPhoneNumber);
						                    profesionalesEntity.setCreatedAt(StrCreatedAt);
						                    profesionalesEntity.setUpdatedAt(StrUpdatedAt);
						                    profesionalesEntity.setProviderId(StrProviderId !=null ? StrProviderId: "null");
						                    profesionalesEntity.setStatus(StrStatus !=null ? StrStatus: "null");
						                    profesionalesEntity.setApprovalAt(StrApprovalAt !=null ? StrApprovalAt: "null");
						                    profesionalesEntity.setApproved(StrApproved !=null ? StrApproved: "null");
						                    profesionalesEntity.setBank(StrBank !=null ? StrBank: "null");
						                    profesionalesEntity.setBranchId(StrBranchId !=null ? StrBranchId: "null");
						                    profesionalesEntity.setIdentificationTypeId(StrIdentificationTypeId !=null ? StrIdentificationTypeId: "null");
						                    profesionalesEntity.setIdentificationTypeNumber(StrIdentificationTypeNumber !=null ? StrIdentificationTypeNumber: "null");
						                    profesionalesEntity.setNumberAccount(StrNumberAccount !=null ? StrNumberAccount: "null");
						                    profesionalesEntity.setTypeAccount(StrTypeAccount !=null ? StrTypeAccount: "null");
						                    profesionalesEntity.setAddress(StrAddress !=null ? StrAddress: "null");
						                    profesionalesEntity.setHourApproved(StrHourApproved !=null ? StrHourApproved: "null");
						                    table.execute(TableOperation.insertOrReplace(profesionalesEntity ));
						            		}
						            }else {
						            	iError=1;
						            	System.out.println("Profesionales.ProcesoEntidadProfesionales= Respuesta No identificada "+country+"="+jsonResponse);      }
							    }catch(Exception e) {
							    	iError=1;
							    	System.out.println("Profesionales.ProcesoProfesionales= Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse);
							    	e.printStackTrace();
							    }	
							}else {
								iError=1;
						    	System.out.println("Profesionales.ProcesoEntidadProfesionales= Error en JSON QUERY GRAPHQL "+country);
							}
					}
				}else {
					iError=1;
					System.out.println("Profesionales.ProcesoEntidadProfesionales = Error en JSON QUERY COUNT "+country);
				}
			}catch(Exception e){
				iError=1;
				System.out.println("Profesionales.ProcesoProfesionales = Error ejecutar paginación "+country);
				e.printStackTrace();
				}
	        //Fin de proceso de Entidad
			try { 	
				if(iError==0) {
					ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
					System.out.println(instant.toString()+" Actualizacion de Fecha completada "+country+"!!!");
					}
				}
			catch(Exception e) {
				iError=1;
				System.out.println("Profesionales.ProcesoEntidadProfesionales = Error al actualizar Fecha "+country);
				e.printStackTrace();
			}
			if(iError==0) {
				System.out.println(instant.toString()+" Proceso de Entidad Profesionales Finalizada "+country+"!!!!");}
		}else {System.out.println("Profesionales.ProcesoEntidadProfesionales= Problema en obtener el Token "+country);	}
	}
}
