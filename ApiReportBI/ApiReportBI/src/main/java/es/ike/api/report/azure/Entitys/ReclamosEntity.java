package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ReclamosEntity extends TableServiceEntity {
	public ReclamosEntity(String id, String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;
	}

	public ReclamosEntity() {

	}

	public Integer id;
	public UUID businessUnit;
	public String createdAt;
	public String updatedAt;
	public String accountNumber;
	public Integer claimAccountTypeId;
	public String claimAccountTypeName;
	public String claimActioTypeName;
	public Integer claimActionTypeId;
	public Integer claimBankId;
	public String claimBankName;
	public Boolean claimBeneficiaryName;
	public Integer claimCompensationTypeId;
	public String claimCompensationTypeName;
	public Integer claimDocumentTypeId;
	public String claimDocumentTypeName;
	public Integer claimPaymentTypeId;
	public String claimPaymentTypeName;
	public Integer claimPriorityId;
	public String claimPriorityName;
	public Integer claimResponseTypeId;
	public String claimResponseTypeName;
	public Integer claimStatusId;
	public String claimStatusName;
	public Integer claimSubTypificationId;
	public String claimSubTypificationName;
	public Integer claimTypeId;
	public String claimTypeName;
	public Integer claimTypificationId;
	public String claimTypificationName;
	public String beneficiaryDocumentNumber;
	public String beneficiaryName;
	public String description;
	public Integer monetaryAmount;
	public String monetaryUnit;
	public Integer claimResponsibleAreaId;
	public String claimResponsibleAreaName;
	public String caseNumber;
	public Integer caseId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public Integer getClaimAccountTypeId() {
		return claimAccountTypeId;
	}

	public void setClaimAccountTypeId(Integer claimAccountTypeId) {
		this.claimAccountTypeId = claimAccountTypeId;
	}

	public String getClaimAccountTypeName() {
		return claimAccountTypeName;
	}

	public void setClaimAccountTypeName(String claimAccountTypeName) {
		this.claimAccountTypeName = claimAccountTypeName;
	}

	public String getClaimActioTypeName() {
		return claimActioTypeName;
	}

	public void setClaimActioTypeName(String claimActioTypeName) {
		this.claimActioTypeName = claimActioTypeName;
	}

	public Integer getClaimActionTypeId() {
		return claimActionTypeId;
	}

	public void setClaimActionTypeId(Integer claimActionTypeId) {
		this.claimActionTypeId = claimActionTypeId;
	}

	public Integer getClaimBankId() {
		return claimBankId;
	}

	public void setClaimBankId(Integer claimBankId) {
		this.claimBankId = claimBankId;
	}

	public String getClaimBankName() {
		return claimBankName;
	}

	public void setClaimBankName(String claimBankName) {
		this.claimBankName = claimBankName;
	}

	public Boolean getClaimBeneficiaryName() {
		return claimBeneficiaryName;
	}

	public void setClaimBeneficiaryName(Boolean claimBeneficiaryName) {
		this.claimBeneficiaryName = claimBeneficiaryName;
	}

	public Integer getClaimCompensationTypeId() {
		return claimCompensationTypeId;
	}

	public void setClaimCompensationTypeId(Integer claimCompensationTypeId) {
		this.claimCompensationTypeId = claimCompensationTypeId;
	}

	public String getClaimCompensationTypeName() {
		return claimCompensationTypeName;
	}

	public void setClaimCompensationTypeName(String claimCompensationTypeName) {
		this.claimCompensationTypeName = claimCompensationTypeName;
	}

	public Integer getClaimDocumentTypeId() {
		return claimDocumentTypeId;
	}

	public void setClaimDocumentTypeId(Integer claimDocumentTypeId) {
		this.claimDocumentTypeId = claimDocumentTypeId;
	}

	public String getClaimDocumentTypeName() {
		return claimDocumentTypeName;
	}

	public void setClaimDocumentTypeName(String claimDocumentTypeName) {
		this.claimDocumentTypeName = claimDocumentTypeName;
	}

	public Integer getClaimPaymentTypeId() {
		return claimPaymentTypeId;
	}

	public void setClaimPaymentTypeId(Integer claimPaymentTypeId) {
		this.claimPaymentTypeId = claimPaymentTypeId;
	}

	public String getClaimPaymentTypeName() {
		return claimPaymentTypeName;
	}

	public void setClaimPaymentTypeName(String claimPaymentTypeName) {
		this.claimPaymentTypeName = claimPaymentTypeName;
	}

	public Integer getClaimPriorityId() {
		return claimPriorityId;
	}

	public void setClaimPriorityId(Integer claimPriorityId) {
		this.claimPriorityId = claimPriorityId;
	}

	public String getClaimPriorityName() {
		return claimPriorityName;
	}

	public void setClaimPriorityName(String claimPriorityName) {
		this.claimPriorityName = claimPriorityName;
	}

	public Integer getClaimResponseTypeId() {
		return claimResponseTypeId;
	}

	public void setClaimResponseTypeId(Integer claimResponseTypeId) {
		this.claimResponseTypeId = claimResponseTypeId;
	}

	public String getClaimResponseTypeName() {
		return claimResponseTypeName;
	}

	public void setClaimResponseTypeName(String claimResponseTypeName) {
		this.claimResponseTypeName = claimResponseTypeName;
	}

	public Integer getClaimStatusId() {
		return claimStatusId;
	}

	public void setClaimStatusId(Integer claimStatusId) {
		this.claimStatusId = claimStatusId;
	}

	public String getClaimStatusName() {
		return claimStatusName;
	}

	public void setClaimStatusName(String claimStatusName) {
		this.claimStatusName = claimStatusName;
	}

	public Integer getClaimSubTypificationId() {
		return claimSubTypificationId;
	}

	public void setClaimSubTypificationId(Integer claimSubTypificationId) {
		this.claimSubTypificationId = claimSubTypificationId;
	}

	public String getClaimSubTypificationName() {
		return claimSubTypificationName;
	}

	public void setClaimSubTypificationName(String claimSubTypificationName) {
		this.claimSubTypificationName = claimSubTypificationName;
	}

	public Integer getClaimTypeId() {
		return claimTypeId;
	}

	public void setClaimTypeId(Integer claimTypeId) {
		this.claimTypeId = claimTypeId;
	}

	public String getClaimTypeName() {
		return claimTypeName;
	}

	public void setClaimTypeName(String claimTypeName) {
		this.claimTypeName = claimTypeName;
	}

	public Integer getClaimTypificationId() {
		return claimTypificationId;
	}

	public void setClaimTypificationId(Integer claimTypificationId) {
		this.claimTypificationId = claimTypificationId;
	}

	public String getClaimTypificationName() {
		return claimTypificationName;
	}

	public void setClaimTypificationName(String claimTypificationName) {
		this.claimTypificationName = claimTypificationName;
	}

	public String getBeneficiaryDocumentNumber() {
		return beneficiaryDocumentNumber;
	}

	public void setBeneficiaryDocumentNumber(String beneficiaryDocumentNumber) {
		this.beneficiaryDocumentNumber = beneficiaryDocumentNumber;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMonetaryAmount() {
		return monetaryAmount;
	}

	public void setMonetaryAmount(Integer monetaryAmount) {
		this.monetaryAmount = monetaryAmount;
	}

	public String getMonetaryUnit() {
		return monetaryUnit;
	}

	public void setMonetaryUnit(String monetaryUnit) {
		this.monetaryUnit = monetaryUnit;
	}

	public Integer getClaimResponsibleAreaId() {
		return claimResponsibleAreaId;
	}

	public void setClaimResponsibleAreaId(Integer claimResponsibleAreaId) {
		this.claimResponsibleAreaId = claimResponsibleAreaId;
	}

	public String getClaimResponsibleAreaName() {
		return claimResponsibleAreaName;
	}

	public void setClaimResponsibleAreaName(String claimResponsibleAreaName) {
		this.claimResponsibleAreaName = claimResponsibleAreaName;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public Integer getCaseId() {
		return caseId;
	}

	public void setCaseId(Integer caseId) {
		this.caseId = caseId;
	}

	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("businessUnit");
		list.add("createdAt");
		list.add("updatedAt");
		list.add("accountNumber");
		list.add("claimAccountTypeId");
		list.add("claimAccountTypeName");
		list.add("claimActioTypeName");
	    list.add("claimActionTypeId");
	    list.add("claimBankId");
	    list.add("claimBankName");
        list.add("claimBeneficiaryName");
        list.add("claimCompensationTypeId");
        list.add("claimCompensationTypeName");
        list.add("claimDocumentTypeId");
        list.add("claimDocumentTypeName");
        list.add("claimPaymentTypeId");
        list.add("claimPaymentTypeName");
        list.add("claimPriorityId");
        list.add("claimPriorityName");
        list.add("claimResponseTypeName");
        list.add("claimStatusId");
        list.add("claimStatusName");
        list.add("claimSubTypificationId");
        list.add("claimSubTypificationName");
        list.add("claimTypeId");
        list.add("claimTypeName");
        list.add("claimTypificationId");
        list.add("claimTypificationName");
        list.add("beneficiaryDocumentNumber");
        list.add("beneficiaryName");
        list.add("description");
        list.add("monetaryAmount");
        list.add("monetaryUnit");
        list.add("claimResponsibleAreaId");
        list.add("claimResponsibleAreaName");
        list.add("caseNumber");
        list.add("caseId");
	
		return list;
	}

}
