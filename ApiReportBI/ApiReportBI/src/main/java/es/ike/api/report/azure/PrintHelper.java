package  es.ike.api.report.azure;

import com.microsoft.azure.storage.StorageException;
import java.io.PrintWriter;
import java.io.StringWriter;

/** * A class which provides utility methods **/
final class PrintHelper {
	//-------------------------------------------------------------
    /** * Prints out the sample start information .*/
    static void printSampleStartInfo(String sampleName) {
        System.out.println(String.format("PrintHelper.printSampleStartInfo= %s muestra iniciando...",sampleName));
    }
	//-------------------------------------------------------------
    /** * Prints out the sample complete information . */
    static void printSampleCompleteInfo(String sampleName) {
        System.out.println(String.format("PrintHelper.printSampleStartInfo= %s muestra completada.",sampleName));
    }
	//-------------------------------------------------------------
    /** * Print the exception stack trace  * * @param t Exception to be printed  */
    static void printException(Throwable t) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        t.printStackTrace(printWriter);
        if (t instanceof StorageException) {
            if (((StorageException) t).getExtendedErrorInformation() != null) {
                System.out.println(String.format("PrintHelper.printException= Error: %s", ((StorageException) t).getExtendedErrorInformation().getErrorMessage()));
            }
        }
        System.out.println(String.format("PrintHelper.printException= Detalles de la excepción:\n%s", stringWriter.toString()));
    }
	//-------------------------------------------------------------
}