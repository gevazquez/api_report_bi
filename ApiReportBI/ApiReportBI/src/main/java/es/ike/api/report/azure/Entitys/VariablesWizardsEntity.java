package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class VariablesWizardsEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public VariablesWizardsEntity(String id,String businessUuid) {
		this.partitionKey=id;
		this.rowKey=businessUuid;
		
	}
	// --------------------------------------------------------------------
	public VariablesWizardsEntity() {
		
	}
	
	public String id;
	public UUID businessUnit;
	public Integer wizardVersionId;
	public Integer versionId;
	//public String data;
	//public String unitCost;
	public String options;
	public String intervals;
	public String optionName;
	public String optionTime;
	public String optionPrice;
	public String createdAt;
	public String updatedAt;
	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public Integer getWizardVersionId() {
		return wizardVersionId;
	}
	public void setWizardVersionId(Integer wizardVersionId) {
		this.wizardVersionId = wizardVersionId;
	}
	public Integer getVersionId() {
		return versionId;
	}
	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getIntervals() {
		return intervals;
	}
	public void setIntervals(String intervals) {
		this.intervals = intervals;
	}
	public String getOptionName() {
		return optionName;
	}
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	public String getOptionTime() {
		return optionTime;
	}
	public void setOptionTime(String optionTime) {
		this.optionTime = optionTime;
	}
	public String getOptionPrice() {
		return optionPrice;
	}
	public void setOptionPrice(String optionPrice) {
		this.optionPrice = optionPrice;
	}
	
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public String getOptions() {
		return options;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("businessUnit");
		list.add("wizardVersionId");
		list.add("versionId");
		list.add("options");
		list.add("intervals");
		list.add("optionName");
		list.add("optionTime");
		list.add("optionPrice");
		list.add("createdAt");
		list.add("updatedAt");
		return list;
	}
	

}
