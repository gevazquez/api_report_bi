package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class PaisesEntity extends TableServiceEntity {
	//------------------------------------------------------------------------------------------------------------
	public PaisesEntity(String id, String name) {
		this.partitionKey = id;
		// Se quita el uso del name, para evitar errores de caracteres no validos para el Rowkey
		//this.rowKey = name;
		this.rowKey = id;  
	}
//------------------------------------------------------------------------------------------------------------
	public PaisesEntity() {		}

//------------------------------------------------------------------------------------------------------------
	public Integer id;
	public String name;
	public UUID tenantId;
	public String updatedAt;
	public String createdAT;
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public UUID getTenantId() {
		return tenantId;
	}
	public void setTenantId(UUID tenantId) {
		this.tenantId = tenantId;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getCreatedAT() {
		return createdAT;
	}
	public void setCreatedAT(String createdAT) {
		this.createdAT = createdAT;
	}
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("name");
		list.add("tenantId");
		list.add("updatedAt");
		list.add("createdAT");
		return list;
	}

}
