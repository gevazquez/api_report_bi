package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ChecklistDelServicioEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public ChecklistDelServicioEntity (String id,String businessUnit) {
		this.partitionKey=id;
		this.rowKey=businessUnit;
		
	}
	// --------------------------------------------------------------------
	public ChecklistDelServicioEntity() {
	
	}
	// ------------------------------------------------------------------------------------------------------------
			public UUID businessUnit;
			public Integer serviceId;
			public Integer serial;
			public Integer typeQuestionId;
			public String typeQuestionName;
			public String question; 
			public Boolean optional;
			public Boolean checked;
			public String dateResponse;
			public Boolean haveOption;
			public String option;
			public String fileName;
			public String fileNameAux;
			public String fileUrl;
			public String fileCreationTime;
			public String placeholder;
			public String value;
			public String createdAt;
			public String updatedAt;
			
			
			
			public UUID getBusinessUnit() {
				return businessUnit;
			}
			public void setBusinessUnit(UUID businessUnit) {
				this.businessUnit = businessUnit;
			}
			public Integer getServiceId() {
				return serviceId;
			}
			public void setServiceId(Integer serviceId) {
				this.serviceId = serviceId;
			}
			public Integer getSerial() {
				return serial;
			}
			public void setSerial(Integer serial) {
				this.serial = serial;
			}
			
			
			public String getTypeQuestionName() {
				return typeQuestionName;
			}
			public void setTypeQuestionName(String typeQuestionName) {
				this.typeQuestionName = typeQuestionName;
			}
			public Integer getTypeQuestionId() {
				return typeQuestionId;
			}
			public void setTypeQuestionId(Integer typeQuestionId) {
				this.typeQuestionId = typeQuestionId;
			}
			public String getQuestion() {
				return question;
			}
			public void setQuestion(String question) {
				this.question = question;
			}
			public Boolean getOptional() {
				return optional;
			}
			public void setOptional(Boolean optional) {
				this.optional = optional;
			}
			public Boolean getChecked() {
				return checked;
			}
			public void setChecked(Boolean checked) {
				this.checked = checked;
			}
			public String getDateResponse() {
				return dateResponse;
			}
			public void setDateResponse(String dateResponse) {
				this.dateResponse = dateResponse;
			}
			public Boolean getHaveOption() {
				return haveOption;
			}
			public void setHaveOption(Boolean haveOption) {
				this.haveOption = haveOption;
			}
			public String getOption() {
				return option;
			}
			public void setOption(String option) {
				this.option = option;
			}
			public String getFileName() {
				return fileName;
			}
			public void setFileName(String fileName) {
				this.fileName = fileName;
			}
			public String getFileNameAux() {
				return fileNameAux;
			}
			public void setFileNameAux(String fileNameAux) {
				this.fileNameAux = fileNameAux;
			}
			public String getFileUrl() {
				return fileUrl;
			}
			public void setFileUrl(String fileUrl) {
				this.fileUrl = fileUrl;
			}
			public String getFileCreationTime() {
				return fileCreationTime;
			}
			public void setFileCreationTime(String fileCreationTime) {
				this.fileCreationTime = fileCreationTime;
			}
			public String getPlaceholder() {
				return placeholder;
			}
			public void setPlaceholder(String placeholder) {
				this.placeholder = placeholder;
			}
			public String getValue() {
				return value;
			}
			public void setValue(String value) {
				this.value = value;
			}
			public String getCreatedAt() {
				return createdAt;
			}
			public void setCreatedAt(String createdAt) {
				this.createdAt = createdAt;
			}
			public String getUpdatedAt() {
				return updatedAt;
			}
			public void setUpdatedAt(String updatedAt) {
				this.updatedAt = updatedAt;
			}
			public List<String>  getColumns(){
				List<String> list = new ArrayList<String>();
				list.add("businessUnit");
				list.add("serviceId");
				list.add("serial");
				list.add("typeQuestionId");
				list.add("typeQuestionName");
				list.add("question");
				list.add("optional");
				list.add("checked");
				list.add("dateResponse");
				list.add("haveOption");
				list.add("option");
				list.add("fileName");
				list.add("fileNameAux");
				list.add("fileUrl");
				list.add("fileCreationTime");
				list.add("placeholder");
				list.add("value");
				list.add("updatedAt");
				list.add("createdAT");
				return list;
			}
				
			

}
