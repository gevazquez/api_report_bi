package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.SiniestralidadEntity;
import es.ike.api.report.utilerias.JsonUtility;

public class LoadSiniestralidad {
	public static void Siniestralidad(String jsonResponse,String country ) throws Exception {
		//Se carga jsonResponse en Arreglo Json
				Integer iError=0;
				Instant instant=Instant.now();
				//Obtiene configuracion de Entidad
				ApiConfiguration apiConfiguration =null;
				apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("35","35","coverage_balances",country);
				Logger logger = Logger.getLogger("Logger apiReport LoadSiniestralidad.Siniestralidad");
				//Crea tabla Countries si no existe en Azure
				CloudTableClient tableClient1 = null;
				CreateTableConfiguration tableConfiguration = null;
				tableClient1 = TableClientProvider.getTableClientReference(country);
				tableConfiguration.createTable(tableClient1, "CoverageBalances",country);
				 //Se carga jsonResponse en Arreglo Json
		        try {
			    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
		            	JSONArray jsonarray= new JSONArray(jsonResponse);  
		            	CloudTableClient tableClient = null;
		                tableClient = TableClientProvider.getTableClientReference(country);
		              //Se genera la variable tabla, con el nombre de la tabla en AZURE
		                CloudTable table = tableClient.getTableReference("CoverageBalances");
		                for (int a = 0; a<jsonarray.length(); a++) {
		            		JSONParser parser=null;
		            		JSONObject json=null;
		            		String strId="";
		            		String strBusinessUnit="";
		            		String strAvailable="";
							String strBehavior="";
							String strConsumed="";
							String strCoverageId="";
							String strCoverageName="";
							String strCoverageStatus="";
							String strLimitAmount="";
							String strMaximumCoverageId="";
							String strPeriodicity="";
							String strPlanId="";
							String strPlanSubscriptionId="";
							String strRestrictive="";
							String strSubscriptionId="";
							String strUnitCode="";
		            		String strCreatedAt="";
		            		String strUpdatedAt="";
		            		
		            		
		            		parser = new JSONParser();
		            		json = (JSONObject)parser.parse(jsonarray.get(a).toString());
		            		strId = JsonUtility.JsonValidaExisteLlave(json,"id" );
		            		strBusinessUnit = JsonUtility.JsonValidaExisteLlave(json,"businessUnit" );
		            		strAvailable = JsonUtility.JsonValidaExisteLlave(json,"available" );
		            		strBehavior = JsonUtility.JsonValidaExisteLlave(json,"behavior" );
		            		strConsumed= JsonUtility.JsonValidaExisteLlave(json,"consumed" );
		            		strCoverageId = JsonUtility.JsonValidaExisteLlave(json,"coverageId" );
		            		strCoverageName = JsonUtility.JsonValidaExisteLlave(json,"coverageName" );
		            		strCoverageStatus = JsonUtility.JsonValidaExisteLlave(json,"coverageStatus" );
		            		strLimitAmount= JsonUtility.JsonValidaExisteLlave(json,"limitAmount" );
		            		strMaximumCoverageId = JsonUtility.JsonValidaExisteLlave(json,"maximumCoverageId" );
		            		strPeriodicity = JsonUtility.JsonValidaExisteLlave(json,"periodicity" );
		            		strPlanId = JsonUtility.JsonValidaExisteLlave(json,"planId" );
		            		strPlanSubscriptionId = JsonUtility.JsonValidaExisteLlave(json,"planSubscriptionId" );
		            		strRestrictive = JsonUtility.JsonValidaExisteLlave(json,"restrictive" );
		            		strSubscriptionId = JsonUtility.JsonValidaExisteLlave(json,"subscriptionId" );
		            		strUnitCode = JsonUtility.JsonValidaExisteLlave(json,"unitCode" );						            	
		            	    strCreatedAt = JsonUtility.JsonValidaExisteLlave(json,"createdAt" );
		            	    strUpdatedAt = JsonUtility.JsonValidaExisteLlave(json,"updatedAt" );
		            		
		            		SiniestralidadEntity siniestralidadEntity = new SiniestralidadEntity(strId, strBusinessUnit); //PartitionKey & RowKey
		            		siniestralidadEntity.setEtag(siniestralidadEntity.getEtag());
		            		siniestralidadEntity.setId(strId != null ? Integer.parseInt(strId): 0);
		            		siniestralidadEntity.setBusinessUnit(strBusinessUnit != null ?  UUID.fromString(strBusinessUnit): null);
		            		siniestralidadEntity.setAvailable(strAvailable != null ? Integer.parseInt(strAvailable): 0);
		            		siniestralidadEntity.setBehavior(strBehavior != null ? strBehavior: "null");
		            		siniestralidadEntity.setConsumed(strConsumed != null ? Integer.parseInt(strConsumed): 0);
		            		siniestralidadEntity.setCoverageId(strCoverageId != null ? Integer.parseInt(strCoverageId): 0);
		            		siniestralidadEntity.setCoverageName(strCoverageName != null ? strCoverageName: "null");
		            		siniestralidadEntity.setCoverageStatus(strCoverageStatus != null ? strCoverageStatus: "null");
		            		siniestralidadEntity.setLimitAmount(strLimitAmount != null ? Integer.parseInt(strLimitAmount): 0);
		            		siniestralidadEntity.setMaximumCoverageId(strMaximumCoverageId != null ? Integer.parseInt(strMaximumCoverageId): 0);
		            		siniestralidadEntity.setPeriodicity(strPeriodicity != null ? strPeriodicity: "null");
		            		siniestralidadEntity.setPlanId(strPlanId != null ? Integer.parseInt(strPlanId): 0);
		            		siniestralidadEntity.setPlanSubscriptionId(strPlanSubscriptionId != null ? Integer.parseInt(strPlanSubscriptionId): 0);
		            		siniestralidadEntity.setRestrictive(strRestrictive != null ? Boolean.valueOf(strRestrictive): false);
		            		siniestralidadEntity.setSubscriptionId(strSubscriptionId != null ? Integer.parseInt(strSubscriptionId): 0);
		            		siniestralidadEntity.setUnitCode(strUnitCode != null ? strUnitCode: "null");
		            		siniestralidadEntity.setCreatedAt(strCreatedAt != null ? strCreatedAt: "null");
		            		siniestralidadEntity.setUpdatedAt(strUpdatedAt != null ? strUpdatedAt: "null");
		                    table.execute(TableOperation.insertOrReplace(siniestralidadEntity));
		                }
		            }else {
		            	iError=1;
		            	logger.log(Level.WARNING, "LoadSiniestralidad.Siniestralidad= Respuesta No identificada "+country+"="+jsonResponse);
		            }
			    }catch(Exception e) {
			    	iError=1;
			    	logger.log(Level.WARNING, "LoadSiniestralidad.Siniestralidad= Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse);
			    	e.printStackTrace();
			    }
			  //Fin de proceso de Entidad
				try { 	
					if(iError==0) {
						logger.log(Level.INFO, instant.toString()+" Se cargan registros entidad Siniestralidad con filtro ProcessFilterBu "+country+"!!!");
						
						}
					}
				catch(Exception e) {
					iError=1;
					
					logger.log(Level.WARNING, "LoadSiniestralidad.Siniestralidad = Error al actualizar Fecha"+country);
					e.printStackTrace();
				}
				if(iError==0) {
					logger.log(Level.INFO, instant.toString()+" Proceso Carga de Entidad con filtro Siniestralidad Finalizada "+country+"!!!!");
					}
			
	}

}
