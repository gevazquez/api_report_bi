package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.ClientesEntity;
import es.ike.api.report.azure.Entitys.UsersEntity;
import es.ike.api.report.utilerias.JsonUtility;

public class LoadCustomers {
	public static void Customers(String jsonResponse, String country) throws Exception {
		// Se carga jsonResponse en Arreglo Json
		Integer iError = 0;
		Instant instant = Instant.now();
		// Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration = null;
		apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("3","3","customers",country);
		Logger logger = Logger.getLogger("Logger apiReport LoadCustomers.Customers");
		//Crea tabla Countries si no existe en Azure
		CloudTableClient tableClient1 = null;
		CreateTableConfiguration tableConfiguration = null;
		tableClient1 = TableClientProvider.getTableClientReference(country);
		tableConfiguration.createTable(tableClient1, "Customers",country);
		// Se carga jsonResponse en Arreglo Json
		try {
			if (String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")) {
				JSONArray jsonarray = new JSONArray(jsonResponse);
				CloudTableClient tableClient = null;
				tableClient = TableClientProvider.getTableClientReference(country);
				// Se genera la variable tabla, con el nombre de la tabla en AZURE
				CloudTable table = tableClient.getTableReference("Customers");
				for (int a = 0; a<jsonarray.length(); a++) {
                	JSONParser parser=null;
            		JSONObject json=null;
            		String StrId="";
            		String StrBusinessUnit="";
            		String StrIdentificationType="";
            		String StrIdentificationNumber="";
            		String StrName="";
            		String StrLastName="";
            		String StrEmail="";
            		String StrMobilePhone="";
            		String StrAddress="";
            		String StrCreatedAt="";
            		String StrUpdatedAt="";
            		parser = new JSONParser();
                    json = (JSONObject)parser.parse(jsonarray.get(a).toString());
                    StrId=JsonUtility.JsonValidaExisteLlave(json,"id");
                    StrBusinessUnit=JsonUtility.JsonValidaExisteLlave(json,"businessUnit");
                    StrIdentificationType=JsonUtility.JsonValidaExisteLlave(json,"identificationType");
                    StrIdentificationNumber=JsonUtility.JsonValidaExisteLlave(json,"identificationNumber"); 
                    StrName=JsonUtility.JsonValidaExisteLlave(json,"name");
                    StrLastName=JsonUtility.JsonValidaExisteLlave(json,"lastName");
                    StrEmail=JsonUtility.JsonValidaExisteLlave(json,"email");
                    StrMobilePhone=JsonUtility.JsonValidaExisteLlave(json,"mobilePhone");
                    StrAddress=JsonUtility.JsonValidaExisteLlave(json,"address");
                    StrCreatedAt=JsonUtility.JsonValidaExisteLlave(json,"createdAt");
                    StrUpdatedAt=JsonUtility.JsonValidaExisteLlave(json,"updatedAt");
                    ClientesEntity clientesEntity = new ClientesEntity(StrId,StrBusinessUnit);
                    clientesEntity.setEtag(clientesEntity.getEtag());
                    clientesEntity.setId(StrId != null ? Integer.parseInt(StrId): null);
                    clientesEntity.setBusinessUnit(StrBusinessUnit != null ? UUID.fromString(StrBusinessUnit): null);
                    clientesEntity.setIdentificationType(StrIdentificationType != null ? StrIdentificationType: "null");
                    clientesEntity.setIdentificationNumber(StrIdentificationNumber != null ? StrIdentificationNumber: "null");
                    clientesEntity.setName(StrName != null ? StrName: "null");
                    clientesEntity.setLastName(StrLastName != null ? StrLastName: "null");
                    clientesEntity.setEmail(StrEmail != null ? StrEmail: "null");
                    clientesEntity.setMobilePhone(StrMobilePhone != null ? StrMobilePhone: "null");
                    clientesEntity.setAddress(StrAddress != null ? StrAddress: "null");
                    clientesEntity.setCreatedAt(StrCreatedAt != null ? StrCreatedAt: "null");
                    clientesEntity.setUpdatedAt(StrUpdatedAt != null ? StrUpdatedAt: "null");
                    table.execute(TableOperation.insertOrReplace(clientesEntity));
				}
			} else {
				iError = 1;
				logger.log(Level.WARNING, "LoadCustomers.Customers= Respuesta No identificada "+country+"="+jsonResponse);
			}
		} catch (Exception e) {
			iError = 1;
			logger.log(Level.WARNING, "LoadCustomers.Customers= Error al procesar arreglo e insertar en AZURE " + country + "=" + jsonResponse);
			e.printStackTrace();
		}
		// Fin de proceso de Entidad
		try {
			if (iError == 0) {
				// ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
				logger.log(Level.INFO, instant.toString() + " Se cargan registros entidad Customers con filtro ProcessFilterBu "+ country + "!!!");
			}
		} catch (Exception e) {
			iError = 1;
			logger.log(Level.WARNING, "LoadCustomers.Customers = Error al actualizar Fecha" + country);
			e.printStackTrace();
		}
		if (iError == 0) {
			logger.log(Level.INFO, instant.toString() + " Proceso Carga de Entidad con filtro Customers Finalizada " + country + "!!!!");
		}

	}

}
