package es.ike.api.report.entidades;

import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.azure.data.tables.models.TableServiceException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.DefinicionDeServiciosEXTCategoriesEntity;
import es.ike.api.report.azure.Entitys.DefinicionDeServiciosEntity;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;
import es.ike.api.report.utilerias.ActualizaFecha;
import es.ike.api.report.utilerias.DatosPaginacion;
import es.ike.api.report.utilerias.Fecha;
import es.ike.api.report.utilerias.JsonUtility;
import es.ike.api.report.utilerias.Paginacion;

public class DefinicionDeServicios {
	public static void ProcesoEntidadDefinicionDeServicios(String StrToken,String country)throws Exception,TableServiceException{
		Integer iError=0;
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		Instant instant=Instant.now();
	    apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("7","7","definition_services",country);
	    System.out.println(instant.toString()+" Obtiene Configuacion "+country+" de Entidad "+apiConfiguration.getEntidad());
	  //Crea tabla DefinitionServices si no existe en Azure
		CloudTableClient tableClient1 = null;
		CreateTableConfiguration tableConfiguration = null;
		tableClient1 = TableClientProvider.getTableClientReference(country);
		tableConfiguration.createTable(tableClient1, "DefinitionServices",country);
	  //Crea tabla DefinitionServiceEXTCategories si no existe en Azure
		tableConfiguration.createTable(tableClient1, "DefinitionServiceEXTCategories",country);
		//Obtenemos Token de OAuth de TL
		String token="";
		token=ObtenerOauthTL.OAuthAPI_Report(country);
		System.out.println(instant.toString()+" DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios=Obtiene Token "+country+" "+token);
		//Inserta conteo total
		if(!token.equalsIgnoreCase("")) {
			String StrJSONContoTotal="{"
					+"\n"+"\"query\":\"{"+
					"definition_services_aggregate {"+
					"aggregate {"+
					"count"+
						"}"+
					"}"+
	                	"}\""+
					 "}";
			String jsonResponseConteoTotal="";
			String StrConteoTotal="";
			Integer numConteoTotal = 0;
			jsonResponseConteoTotal=obtenerJsonTuten.getJson(token,StrJSONContoTotal,country);
			//Inicia Parseo de conteo Total
		    try {
		    	JSONParser parser = new JSONParser();
		    	JSONObject json=(JSONObject) parser.parse(jsonResponseConteoTotal);
		    	jsonResponseConteoTotal = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
		    	parser=null;
	            json=null;
	            parser = new JSONParser();
	            json = (JSONObject) parser.parse(jsonResponseConteoTotal);
	            jsonResponseConteoTotal=(JsonUtility.validateJsonData(json,"definition_services_aggregate"))?json.get("definition_services_aggregate").toString():"";
	            parser=null;
                json=null;
                parser = new JSONParser();
                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
                jsonResponseConteoTotal=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
                StrConteoTotal=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
                numConteoTotal = Integer.parseInt(StrConteoTotal);
                try {
                	apiConfiguration.setConteoTotal(numConteoTotal);
                }catch(Exception e) {
                	iError=1;
			    	System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios = Error al cargar conteo total en Azure Entidad DefinicionDeServicios "+country);
                }
		    }catch(Exception e) {
		    	iError=1;
		    	System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios = Error al parsear JsonResponseConteoTotal de Entidad DefinicionDeServicios "+country);}
			
		}else {System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios = Problema en obtener el Token del conteo Total "+country);	}
		//Implementación de filtro where
		String StrWhere=Fecha.filtroFecha(apiConfiguration,"id");
		String StrWhereCount="";
		String StrWhereQuery="";
		if (!StrWhere.equalsIgnoreCase("")) {
		   	StrWhereCount="("+StrWhere+")"; }
		if (!StrWhere.equalsIgnoreCase("")) {
		   	StrWhereQuery=","+StrWhere; }
		if(!token.equalsIgnoreCase("")) {
			String StrJSONCount="{"
					+"\n"+"\"query\":\"{"+
					"definition_services_aggregate "+StrWhereCount+"{"+
					"aggregate {"+
					"count"+
						"}"+
					"}"+
	                	"}\""+
					 "}";
			//Implementacion de paginación
			try {
				String jsonResponseCount="";
				Integer paginas=0;
				String StrCount="";
				Integer numCount = 0;
				String jsonResponse="";
				Integer numPaginacion=0;
				Integer offset=0;
				jsonResponseCount=obtenerJsonTuten.getJson(token,StrJSONCount,country);
				if(!jsonResponseCount.equalsIgnoreCase("")) { 
				//---------------------------------------------------------------------
				//Inicia Parseo de count
			    try {
			    	JSONParser parser = new JSONParser();
			    	JSONObject json=(JSONObject) parser.parse(jsonResponseCount);
			    	jsonResponseCount = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
			    	parser=null;
		            json=null;
		            parser = new JSONParser();
		            json = (JSONObject) parser.parse(jsonResponseCount);
		            jsonResponseCount=(JsonUtility.validateJsonData(json, "definition_services_aggregate"))?json.get("definition_services_aggregate").toString():"";
		            parser=null;
	                json=null;
	                parser = new JSONParser();
	                json= (JSONObject) parser.parse(jsonResponseCount);
	                jsonResponseCount=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
	                json= (JSONObject) parser.parse(jsonResponseCount);
	                StrCount=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
	                numCount = Integer.parseInt(StrCount);
			    }catch(Exception e) {
			    	iError=1;
			    	System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios = Error al parsear JsonResponseCount de Entidad DefinicionDeServicios "+country);}
			    	//llamada al archivo properties
			    	Thread currentThread = Thread.currentThread();
					ClassLoader contextClassLoader = currentThread.getContextClassLoader();
					URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+country);
					Properties prop = new Properties();
					if (resource == null) {
						iError=1;
						throw new Exception("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios= No se pudo leer el archivo de configuración TutenLabsconfig/TutenLabsConfig.properties"+country);		}
					try (InputStream is = resource.openStream()) {
						prop.load(is);
					//Se obtiene el numero de paginacion del archivo properties
						numPaginacion=Integer.parseInt(prop.getProperty("intPaginacion"));
					}catch(Exception e) {
						iError=1;
						System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios= Error en obtencion intPaginacion del archivo TutenLabsconfig/TutenLabsConfig.properties"+country);
					}
					//Se llama y ejecuta el metodo calculaPaginacion
					Paginacion paginacion = new Paginacion();
					DatosPaginacion datosPaginacion = null;
					datosPaginacion= paginacion.calculaPaginacion(numCount, numPaginacion);
					if(datosPaginacion.getResto() ==0) { 	paginas=datosPaginacion.getEnteros();;
					}else {	paginas=datosPaginacion.getEnteros()+1;			}
					for(int i =0;i<paginas;i++){
						String StrJSON="{"
								+"\n"+"\"query\":\"{"+
								apiConfiguration.getEntidad()+"(limit:"+numPaginacion+",offset:"+offset+" "+StrWhereQuery+ ")" +"{ "+
								" id"+
								" businessUnit"+
								" name"+
								" price"+
								" cost"+
								" categories"+
								" duration"+
								" createdAt"+
								" updatedAt"+
								" description"+
							    " wizardId"+
							    " checklistId"+
							    " isExpress"+
							    " active"+
							    " minExpressBookingTime"+
							    " maxExpressStartTime"+
							    " avgExpressArrivalTime"+
							    " motorId"+
							    " sendsPublicLink"+
							    " isAutomatic"+
							    " automaticConceptCost"+
							    " topeMinSatisfactory"+
							    " topeMaxSatisfactory"+
							    " topeMinUnsatisfactory"+
							    " topeMaxUnsatisfactory"+
							    //" questionnaire"+ Se comenta campo hasta que tuten lo libere 
										" }"+
				                	"}\""+
								 "}";
						offset=offset+numPaginacion;
						jsonResponse=obtenerJsonTuten.getJson(StrToken, StrJSON,country);
						if(!jsonResponse.equalsIgnoreCase("")) {
						//Parsear para obtener Arreglo json de Entidad
					    try {
					    	JSONParser parser = new JSONParser();
					        JSONObject json=(JSONObject) parser.parse(jsonResponse);
					        jsonResponse = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
					        parser=null;
				            json=null;
				            parser = new JSONParser();
				            json = (JSONObject) parser.parse(jsonResponse);
				            jsonResponse = (JsonUtility.validateJsonData(json,apiConfiguration.getEntidad()))?json.get(apiConfiguration.getEntidad()).toString():"";
					}catch(Exception e){
						iError=1;
						System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios = Error al parsear JsonResponse de Entidad DefinicionDeServicios "+country);
						e.printStackTrace();
					}
						//Se carga jsonResponse en Arreglo Json
					    try {
					    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
				            	JSONArray jsonarray= new JSONArray(jsonResponse);  
				            	CloudTableClient tableClient = null;
				                tableClient = TableClientProvider.getTableClientReference(country);
				              //Se genera la variable tabla, con el nombre de la tabla en AZURE
				                CloudTable table = tableClient.getTableReference("DefinitionServices");
				                for (int a = 0; a<jsonarray.length(); a++) {
				            		JSONParser parser=null;
				            		JSONObject json=null;
				            		String StrId="";
				        		    String StrBusinessUnit="";
				        			String StrName="";
				        			String StrPrice="";
				        			String StrCost="";
				        			String StrCategories="";
				        			String arrayCategories="";
				        			String StrDuration="";
				        			String StrCreatedAt="";
				        			String StrUpdatedAt="";
				        			Integer countCategories =0;
				        			String StrDescription="";
				        			String StrWizardId="";
				        			String StrChecklistId="";
				        			String StrIsExpress="";
				        			String StrActive="";
				        			String StrMinExpressBookingTime="";
				        			String StrMaxExpressStartTime="";
				        			String StrAvgExpressArrivalTime="";
				        			String StrMotorId="";
				        			String StrSendsPublicLink="";
				        			String StrIsAutomatic="";
				        			String StrAutomaticConceptCost="";
				        			String StrTopeMinSatisfactory="";
				        			String StrTopeMaxSatisfactory="";
				        			String StrTopeMinUnsatisfactory="";
				        			String StrTopeMaxUnsatisfactory="";
				        			String StrQuestionnaire="";
				        			
				        			parser = new JSONParser();
				                    json = (JSONObject)parser.parse(jsonarray.get(a).toString());
				                    StrId=JsonUtility.JsonValidaExisteLlave(json, "id");
				                    StrBusinessUnit=JsonUtility.JsonValidaExisteLlave(json, "businessUnit");
				                    StrName=JsonUtility.JsonValidaExisteLlave(json, "name");
				                    StrPrice=JsonUtility.JsonValidaExisteLlave(json, "price");
				                    StrCost=JsonUtility.JsonValidaExisteLlave(json, "cost");
				                    //Inicia Proceso arreglo categories
				                    StrCategories=JsonUtility.JsonValidaExisteLlave(json, "categories");
				                   try {
				                	if (StrCategories==null) {
				                		StrCategories="";
				                	}else { StrCategories= StrCategories.substring(1,StrCategories.length()-1); }
				                    if(!StrCategories.equalsIgnoreCase("")) {
				                    	String strArray [] =StrCategories.split(", ");//Convierte un string a un array separandolo (caracteres)
				                    	CloudTable table2 = tableClient.getTableReference("DefinitionServiceEXTCategories");
				                    	for (int b = 0; b < strArray.length; b++) {
					                        arrayCategories= strArray[b];
					                        DefinicionDeServiciosEXTCategoriesEntity definicionDeServiciosEXTCategoriesEntity = new DefinicionDeServiciosEXTCategoriesEntity(StrId+"_"+StrBusinessUnit,arrayCategories);
					                        definicionDeServiciosEXTCategoriesEntity.setEtag(definicionDeServiciosEXTCategoriesEntity.getEtag());
					                        definicionDeServiciosEXTCategoriesEntity.setCategories(arrayCategories !=null ? Integer.parseInt(arrayCategories):null);
					                        definicionDeServiciosEXTCategoriesEntity.setIdExt(StrId != null ? Integer.parseInt(StrId): null);
					                        definicionDeServiciosEXTCategoriesEntity.setBusinessUnitExt(StrBusinessUnit != null ? UUID.fromString(StrBusinessUnit): null);
					                        table2.execute(TableOperation.insertOrReplace(definicionDeServiciosEXTCategoriesEntity));
					                        countCategories++;
				                    		}
				                    	}
				                   }catch(Exception e) {
				                	   iError=1;
										System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios = Error al insertar en tabla DefinitionServiceEXTCategories campo arrayCategories de Entidad DefinicionDeServicios "+country);
										e.printStackTrace();
				                   		}
				                   	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				                    StrDuration=JsonUtility.JsonValidaExisteLlave(json, "duration");
				                    StrCreatedAt=JsonUtility.JsonValidaExisteLlave(json, "createdAt");
				                    StrUpdatedAt=JsonUtility.JsonValidaExisteLlave(json, "updatedAt");
				                    StrDescription=JsonUtility.JsonValidaExisteLlave(json, "description");
				                    StrWizardId=JsonUtility.JsonValidaExisteLlave(json, "wizardId");
				                    StrChecklistId=JsonUtility.JsonValidaExisteLlave(json, "checklistId");
				                    StrIsExpress=JsonUtility.JsonValidaExisteLlave(json, "isExpress");
				                    StrActive=JsonUtility.JsonValidaExisteLlave(json, "active");
				                    StrMinExpressBookingTime=JsonUtility.JsonValidaExisteLlave(json, "minExpressBookingTime");
				                    StrMaxExpressStartTime=JsonUtility.JsonValidaExisteLlave(json, "maxExpressStartTime");
				                    StrAvgExpressArrivalTime=JsonUtility.JsonValidaExisteLlave(json, "avgExpressArrivalTime");
				                    StrMotorId=JsonUtility.JsonValidaExisteLlave(json, "motorId");
				                    StrSendsPublicLink=JsonUtility.JsonValidaExisteLlave(json, "sendsPublicLink");
				                    StrIsAutomatic=JsonUtility.JsonValidaExisteLlave(json, "isAutomatic");
				                    StrAutomaticConceptCost=JsonUtility.JsonValidaExisteLlave(json, "automaticConceptCost");
				                    StrTopeMinSatisfactory=JsonUtility.JsonValidaExisteLlave(json, "topeMinSatisfactory");
				                    StrTopeMaxSatisfactory=JsonUtility.JsonValidaExisteLlave(json, "topeMaxSatisfactory");
				                    StrTopeMinUnsatisfactory=JsonUtility.JsonValidaExisteLlave(json, "topeMinUnsatisfactory");
				                    StrTopeMaxUnsatisfactory=JsonUtility.JsonValidaExisteLlave(json, "topeMaxUnsatisfactory");
				                    //StrQuestionnaire=JsonUtility.JsonValidaExisteLlave(json, "questionnaire");

				                    
				                    
				                    DefinicionDeServiciosEntity definicionDeServiciosEntity = new DefinicionDeServiciosEntity(StrId,StrBusinessUnit);
				                    definicionDeServiciosEntity.setEtag(definicionDeServiciosEntity.getEtag());
				                    definicionDeServiciosEntity.setId(StrId != null ? Integer.parseInt(StrId): null);
				                    definicionDeServiciosEntity.setBusinessUnit(StrBusinessUnit != null ? UUID.fromString(StrBusinessUnit): null);
				                    definicionDeServiciosEntity.setName(StrName);
				                    definicionDeServiciosEntity.setPrice(StrPrice);
				                    definicionDeServiciosEntity.setCost(StrCost);
				                    definicionDeServiciosEntity.setDuration(StrDuration);
				                    definicionDeServiciosEntity.setCreatedAt(StrCreatedAt);
				                    definicionDeServiciosEntity.setUpdatedAt(StrUpdatedAt);
				                    definicionDeServiciosEntity.setCountCategories(countCategories);
				                    definicionDeServiciosEntity.setDescription(StrDescription != null ? StrDescription: "null");
				                    definicionDeServiciosEntity.setWizardId(StrWizardId != null ? StrWizardId: "null");
				                    definicionDeServiciosEntity.setChecklistId(StrChecklistId != null ? StrChecklistId: "null");
				                    definicionDeServiciosEntity.setIsExpress(StrIsExpress != null ? StrIsExpress: "null");
				                    definicionDeServiciosEntity.setActive(StrActive != null ? StrActive: "null");
				                    definicionDeServiciosEntity.setMinExpressBookingTime(StrMinExpressBookingTime != null ? StrMinExpressBookingTime: "null");
				                    definicionDeServiciosEntity.setMaxExpressStartTime(StrMaxExpressStartTime != null ? StrMaxExpressStartTime: "null");
				                    definicionDeServiciosEntity.setAvgExpressArrivalTime(StrAvgExpressArrivalTime != null ? StrAvgExpressArrivalTime: "null");
				                    definicionDeServiciosEntity.setMotorId(StrMotorId != null ? StrMotorId: "");
				                    definicionDeServiciosEntity.setSendsPublicLink(StrSendsPublicLink != null ? StrSendsPublicLink: "null");
				                    definicionDeServiciosEntity.setIsAutomatic(StrIsAutomatic != null ? Boolean.valueOf(StrIsAutomatic): false  );
				                    definicionDeServiciosEntity.setAutomaticConceptCost(StrAutomaticConceptCost != null ? Boolean.valueOf(StrAutomaticConceptCost): false  );
				                    definicionDeServiciosEntity.setTopeMinSatisfactory(StrTopeMinSatisfactory != null ? Integer.parseInt(StrTopeMinSatisfactory): 0);
				                    definicionDeServiciosEntity.setTopeMaxSatisfactory(StrTopeMaxSatisfactory != null ? Integer.parseInt(StrTopeMaxSatisfactory): 0);
				                    definicionDeServiciosEntity.setTopeMinUnsatisfactory(StrTopeMinUnsatisfactory != null ? Integer.parseInt(StrTopeMinUnsatisfactory): 0);
				                    definicionDeServiciosEntity.setTopeMaxUnsatisfactory(StrTopeMaxUnsatisfactory != null ? Integer.parseInt(StrTopeMaxUnsatisfactory): 0);
				                    //definicionDeServiciosEntity.setQuestionnaire(StrQuestionnaire != null ? UUID.fromString(StrQuestionnaire): null);
				                    
				                    table.execute(TableOperation.insertOrReplace(definicionDeServiciosEntity));
				                }
					    	}else {
					    		iError=1;
					    		System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios= Respuesta No identificada "+country+"="+jsonResponse);}
					    }catch(Exception e) {
					    	iError=1;
					    	System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios= Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse);
					    	e.printStackTrace();
					    }
						}else {
							iError=1;
					    	System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios= Error en JSON QUERY GRAPHQL "+country);
						}
					}
				}else {
					iError=1;
					System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios = Error en JSON QUERY COUNT "+country);
				}
			}catch (Exception e) {
				System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios = Error ejecutar paginación "+country);			}		  
			//Fin de proceso de Entidad
			try { 	
				if(iError==0) {
					ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
					System.out.println(instant.toString()+" Actualizacion de Fecha completada "+country+"!!!");
					}
				}
			catch(Exception e) {
				iError=1;
				System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios = Error al actualizar Fecha "+country);
				e.printStackTrace();
			}
			if(iError==0) {
				System.out.println(instant.toString()+" Proceso de Entidad Definicion De Servicios Finalizada "+country+"!!!!");}
		}else {
			System.out.println("DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios= Problema en obtener el Token "+country);
		}		
	}
}
