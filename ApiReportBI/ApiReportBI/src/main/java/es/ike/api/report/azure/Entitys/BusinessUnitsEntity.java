package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class BusinessUnitsEntity extends TableServiceEntity {
	// ------------------------------------------------------------------------------------------------------------
	public BusinessUnitsEntity(String businessUnit, String name) {
		this.partitionKey = businessUnit;
		// Se quita el uso del name, para evitar errores de caracteres no validos para
		// el Rowkey
		// this.rowKey = name;
		this.rowKey = businessUnit;
	}

	// ------------------------------------------------------------------------------------------------------------
	public BusinessUnitsEntity() {
	}

	// ------------------------------------------------------------------------------------------------------------
	public UUID businessUnit;
	public String name;
	public String shortName;
	public String category;
	public String currency;
	public String tenant;
	public String createdAt;
	public String updatedAt;
	public String timezoneIdName;
	public String offset;
	public Boolean active;
	public String code;
	public Boolean hasCustomerPortal;

	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getTenant() {
		return tenant;
	}

	public void setTenant(String tenant) {
		this.tenant = tenant;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getTimezoneIdName() {
		return timezoneIdName;
	}

	public void setTimezoneIdName(String timezoneIdName) {
		this.timezoneIdName = timezoneIdName;
	}

	public String getOffset() {
		return offset;
	}

	public void setOffset(String offset) {
		this.offset = offset;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getHasCustomerPortal() {
		return hasCustomerPortal;
	}

	public void setHasCustomerPortal(Boolean hasCustomerPortal) {
		this.hasCustomerPortal = hasCustomerPortal;
	}

	public List<String> getColumns() {
		List<String> list = new ArrayList<String>();
		list.add("businessUnit");
		list.add("name");
		list.add("shortName");
		list.add("category");
		list.add("currency");
		list.add("tenant");
		list.add("createdAt");
		list.add("updatedAt");
		list.add("timezoneIdName");
		list.add("offset");
		list.add("active");
		list.add("code");
		list.add("hasCustomerPortal");
		return list;
	}

}