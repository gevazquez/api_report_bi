package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ProveedoresEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public ProveedoresEntity (String id,String businessUnit) {
		this.partitionKey=id;
		this.rowKey=businessUnit;
		
	}
	// --------------------------------------------------------------------
	public ProveedoresEntity() {
	
	}
	// ------------------------------------------------------------------------------------------------------------{
	public Integer id;
	public UUID businessUnit;
    public String name;
    public String businessName;
    public String address;
    public String legalRepresentativeName;
    public String legalRepresentativeLastname;
    public String legalRepresentativePhone;
    public String createdAt;
    public String updatedAt;
    public String status;
    public String identificationTypeId;
    public String identificationNumber;
    public Boolean punished;
    public Boolean catalogNotUpdated;
    public Boolean quotation;
    public Boolean onTime;
    public Boolean doesNotAnswer;
    public Boolean doesNotGiveResponseTime;
    public Boolean noCredit;
    public Boolean serviceByAppointment;
    public Boolean withoutSpecializedTechnician;
    public Boolean doesNotCoverArea;
    public Boolean onlyUnderAuthorization;
	
    
	public String getIdentificationTypeId() {
		return identificationTypeId;
	}
	public void setIdentificationTypeId(String identificationTypeId) {
		this.identificationTypeId = identificationTypeId;
	}
	public String getIdentificationNumber() {
		return identificationNumber;
	}
	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLegalRepresentativeName() {
		return legalRepresentativeName;
	}
	public void setLegalRepresentativeName(String legalRepresentativeName) {
		this.legalRepresentativeName = legalRepresentativeName;
	}
	public String getLegalRepresentativeLastname() {
		return legalRepresentativeLastname;
	}
	public void setLegalRepresentativeLastname(String legalRepresentativeLastname) {
		this.legalRepresentativeLastname = legalRepresentativeLastname;
	}
	public String getLegalRepresentativePhone() {
		return legalRepresentativePhone;
	}
	public void setLegalRepresentativePhone(String legalRepresentativePhone) {
		this.legalRepresentativePhone = legalRepresentativePhone;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getPunished() {
		return punished;
	}
	public void setPunished(Boolean punished) {
		this.punished = punished;
	}
	public Boolean getCatalogNotUpdated() {
		return catalogNotUpdated;
	}
	public void setCatalogNotUpdated(Boolean catalogNotUpdated) {
		this.catalogNotUpdated = catalogNotUpdated;
	}
	public Boolean getQuotation() {
		return quotation;
	}
	public void setQuotation(Boolean quotation) {
		this.quotation = quotation;
	}
	public Boolean getOnTime() {
		return onTime;
	}
	public void setOnTime(Boolean onTime) {
		this.onTime = onTime;
	}
	public Boolean getDoesNotAnswer() {
		return doesNotAnswer;
	}
	public void setDoesNotAnswer(Boolean doesNotAnswer) {
		this.doesNotAnswer = doesNotAnswer;
	}
	public Boolean getDoesNotGiveResponseTime() {
		return doesNotGiveResponseTime;
	}
	public void setDoesNotGiveResponseTime(Boolean doesNotGiveResponseTime) {
		this.doesNotGiveResponseTime = doesNotGiveResponseTime;
	}
	public Boolean getNoCredit() {
		return noCredit;
	}
	public void setNoCredit(Boolean noCredit) {
		this.noCredit = noCredit;
	}
	public Boolean getServiceByAppointment() {
		return serviceByAppointment;
	}
	public void setServiceByAppointment(Boolean serviceByAppointment) {
		this.serviceByAppointment = serviceByAppointment;
	}
	public Boolean getWithoutSpecializedTechnician() {
		return withoutSpecializedTechnician;
	}
	public void setWithoutSpecializedTechnician(Boolean withoutSpecializedTechnician) {
		this.withoutSpecializedTechnician = withoutSpecializedTechnician;
	}
	public Boolean getDoesNotCoverArea() {
		return doesNotCoverArea;
	}
	public void setDoesNotCoverArea(Boolean doesNotCoverArea) {
		this.doesNotCoverArea = doesNotCoverArea;
	}
	public Boolean getOnlyUnderAuthorization() {
		return onlyUnderAuthorization;
	}
	public void setOnlyUnderAuthorization(Boolean onlyUnderAuthorization) {
		this.onlyUnderAuthorization = onlyUnderAuthorization;
	}
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("businessUnit");
		list.add("name");
		list.add("businessName");
		list.add("address");
		list.add("legalRepresentativeName");
		list.add("legalRepresentativeLastname");
		list.add("legalRepresentativePhone");
		list.add("status");
		list.add("identificationTypeId");
		list.add("identificationNumber");
		list.add("createdAt");
		list.add("updatedAt");
		list.add("punished");
		list.add("catalogNotUpdated");
		list.add("quotation");
		list.add("onTime");
		list.add("doesNotAnswer");
		list.add("doesNotGiveResponseTime");
		list.add("noCredit");
		list.add("serviceByAppointment");
		list.add("withoutSpecializedTechnician");
		list.add("doesNotCoverArea");
		list.add("onlyUnderAuthorization");
		return list;
	}
    
    

}
