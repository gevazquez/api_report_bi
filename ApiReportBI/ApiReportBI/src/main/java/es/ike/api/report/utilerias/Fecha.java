package es.ike.api.report.utilerias;

import java.text.SimpleDateFormat;
import java.time.Instant;

import com.azure.data.tables.implementation.models.TransactionalBatchAction.UpdateEntity;

import es.ike.api.report.azure.Entitys.ApiConfiguration;

public class Fecha {
	public static String filtroFecha(ApiConfiguration apiConfiguration,String StrNombreCampoId) {
		String StrWhere="";
		Integer aplicaFiltroFecha=0;
		Integer aplicaActualizacionCompleta=0;		
		String fechaConver="";
		try {
		aplicaFiltroFecha=apiConfiguration.getAplicaFiltroFecha();
		aplicaActualizacionCompleta=apiConfiguration.getAplicaActualizacionCompleta();
		//aplicaActualizacionCompleta=0; variable temporal para pruebas
		if(aplicaFiltroFecha == 1) {
			if(aplicaActualizacionCompleta==0) {
				StrWhere="";
			}else {
				Instant instant = apiConfiguration.ultimaFechaActualizacion.toInstant();
				StrWhere="where: {updatedAt: {_gt: \\\""+instant+"\\\"}},order_by: {"+StrNombreCampoId+": asc}";				
			}
		}else {  StrWhere=""; }
		}catch(Exception e) {
			System.out.println("Fecha.filtroFecha =Error al generar filtro de Fecha WHERE");
			e.printStackTrace();
		}
		return StrWhere;
	}

}
