package es.ike.api.report.entidades;

import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.azure.data.tables.models.TableServiceException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.CostConceptsEntity;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;
import es.ike.api.report.utilerias.ActualizaFecha;
import es.ike.api.report.utilerias.DatosPaginacion;
import es.ike.api.report.utilerias.Fecha;
import es.ike.api.report.utilerias.JsonUtility;
import es.ike.api.report.utilerias.Paginacion;

public class CostConcepts {

	public static void ProcesoEntidadCostConcepts(String StrToken,String country) throws Exception,TableServiceException{
		Integer iError=0;
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		Instant instant=Instant.now();
		apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("26","26","costConcepts",country);
		System.out.println(instant.toString()+" Obtiene Configuacion "+country+" de Entidad "+apiConfiguration.getEntidad());
		//Crea tabla Countries si no existe en Azure
		CloudTableClient tableClient1 = null;
		CreateTableConfiguration tableConfiguration = null;
		tableClient1 = TableClientProvider.getTableClientReference(country);
		tableConfiguration.createTable(tableClient1, "CostConcepts",country);
		//Obtenemos Token de OAuth de TL
		String token="";
		token=ObtenerOauthTL.OAuthAPI_Report(country);
		System.out.println(instant.toString()+" CostConcepts.ProcesoEntidadCostConcepts=Obtiene Token "+country+" "+token);
		//Inserta conteo total
			if(!token.equalsIgnoreCase("")) {
				String StrJSONContoTotal="{"
						+"\n"+"\"query\":\"{"+
						"costConcepts_aggregate {"+
						"aggregate {"+
						"count"+
							"}"+
						"}"+
		                	"}\""+
						 "}";
				String jsonResponseConteoTotal="";
				String StrConteoTotal="";
				Integer numConteoTotal = 0;
				jsonResponseConteoTotal=obtenerJsonTuten.getJson(token,StrJSONContoTotal,country);
				//Inicia Parseo de conteo Total
			    try {
			    	JSONParser parser = new JSONParser();
			    	JSONObject json=(JSONObject) parser.parse(jsonResponseConteoTotal);
			    	jsonResponseConteoTotal = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
			    	parser=null;
		            json=null;
		            parser = new JSONParser();
		            json = (JSONObject) parser.parse(jsonResponseConteoTotal);
		            jsonResponseConteoTotal=(JsonUtility.validateJsonData(json,"costConcepts_aggregate"))?json.get("costConcepts_aggregate").toString():"";
		            parser=null;
	                json=null;
	                parser = new JSONParser();
	                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
	                jsonResponseConteoTotal=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
	                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
	                StrConteoTotal=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
	                numConteoTotal = Integer.parseInt(StrConteoTotal);
	                try {
	                	apiConfiguration.setConteoTotal(numConteoTotal);
	                }catch(Exception e) {
	                	iError=1;
				    	System.out.println("CostConcepts.ProcesoEntidadCostConcepts = Error al cargar conteo total en Azure Entidad CostConcepts "+country);
	                }
			    }catch(Exception e) {
			    	iError=1;
			    	System.out.println("CostConcepts.ProcesoEntidadCostConcepts = Error al parsear JsonResponseConteoTotal de Entidad CostConcepts "+country);}
				
			}else {System.out.println("CostConcepts.ProcesoEntidadCostConcepts= Problema en obtener el Token del conteo Total "+country);	}
			//Implementación de filtro where
			String StrWhere=Fecha.filtroFecha(apiConfiguration, "id");
			String StrWhereCount="";
			String StrWhereQuery="";
			if (!StrWhere.equalsIgnoreCase("")) {
			   	StrWhereCount="("+StrWhere+")"; }
			if (!StrWhere.equalsIgnoreCase("")) {
			   	StrWhereQuery=","+StrWhere; }
			if(!token.equalsIgnoreCase("")) {
				String StrJSONCount="{"
						+"\n"+"\"query\":\"{"+
						"costConcepts_aggregate "+StrWhereCount+" {"+
							"aggregate {"+
								" count"+
								"}"+
							"}"+
						"}\""+
						"}";
				//Implementacion de paginación
				try {
					String jsonResponseCount="";
					Integer paginas=0;
					String StrCount="";
					Integer numCount = 0;
					String jsonResponse="";
					Integer numPaginacion=0;
					Integer offset=0;
					jsonResponseCount=obtenerJsonTuten.getJson(token,StrJSONCount,country);
					if(!jsonResponseCount.equalsIgnoreCase("")) {
						//---------------------------------------------------------------------
						//Inicia Parseo de count
					    try {
					    	JSONParser parser = new JSONParser();
					    	JSONObject json=(JSONObject) parser.parse(jsonResponseCount);
					    	jsonResponseCount = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
					    	parser=null;
				            json=null;
				            parser = new JSONParser();
				            json = (JSONObject) parser.parse(jsonResponseCount);
				            jsonResponseCount=(JsonUtility.validateJsonData(json,"costConcepts_aggregate"))?json.get("costConcepts_aggregate").toString():"";
				            parser=null;
			                json=null;
			                parser = new JSONParser();
			                json= (JSONObject) parser.parse(jsonResponseCount);
			                jsonResponseCount=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
			                json= (JSONObject) parser.parse(jsonResponseCount);
			                StrCount=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
			                numCount = Integer.parseInt(StrCount);
					    }catch(Exception e) {
					    	iError=1;
					    	System.out.println("CostConcepts.ProcesoEntidadCostConcepts = Error al parsear JsonResponseCount de Entidad CostConcepts "+country);}
					  //llamada al archivo properties
						Thread currentThread = Thread.currentThread();
						ClassLoader contextClassLoader = currentThread.getContextClassLoader();
						URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+country);
						Properties prop = new Properties();
						if (resource == null) {	
							iError=1;
							throw new Exception("CostConcepts.ProcesoEntidadCostConcepts= No se pudo leer el archivo de configuración TutenLabsconfig/TutenLabsConfig.properties"+country);		}
						try (InputStream is = resource.openStream()) {
							prop.load(is);
						//Se obtiene el numero de paginacion del archivo properties
							numPaginacion=Integer.parseInt(prop.getProperty("intPaginacion"));
						}catch(Exception e) {
							iError=1;
							System.out.println("CostConcepts.ProcesoEntidadCostConcepts= Error al obtener intPaginacion del archivo TutenLabsconfig/TutenLabsConfig.properties"+country);
						}
						//Se llama y ejecuta el metodo calculaPaginacion
						Paginacion paginacion = new Paginacion();
						DatosPaginacion datosPaginacion = null;
						datosPaginacion= paginacion.calculaPaginacion(numCount, numPaginacion);
						if(datosPaginacion.getResto() ==0) { 	paginas=datosPaginacion.getEnteros();
						}else {	paginas=datosPaginacion.getEnteros()+1;			}
						for(int i =0;i<paginas;i++){
							String StrJSON="{"
									+"\n"+"\"query\":\"{"+
									apiConfiguration.getEntidad()+"(limit:"+numPaginacion+",offset:"+offset+" "+StrWhereQuery+")" +"{ "+
									" active"+
									" actualStatus"+
									" baseCostProvider"+
									" baseSale"+
									" budgetCCAmount"+
									" budgetCCBaseCost"+
									" budgetCCExcessBaseCost"+
									" budgetCCRateType"+
									" budgetCCTop"+
									" budgetCCTotal"+
									" budgetMargin"+
									" budgetTotalCostProvider"+
									" businessUnit"+
									" calculateIva"+
									" comments"+
									" costConceptBaseCost"+
									" costConceptCost"+
									" costConceptId"+
									" costConceptName"+
									" costConceptRateType"+
									" costConceptSale"+
									" costConceptTop"+
									" costConceptTotalCostProvider"+
									" createdAt"+
									" creationDate"+
									" excessBaseCost"+
									" excessBaseSale"+
									" idBudget"+
									" idBudgetCc"+
									" integrationMap"+
									" isCovered"+
									" isValidated"+
									" iva"+
									" ivaCost"+
									" margin"+
									" materialsCost"+
									" providerPayment"+
									" question"+
									" runtimeMin"+
									" saleDiscount"+
									" serviceDuration"+
									" state"+
									" statusName"+
									" subtotal"+
									" subtotalCost"+
									" totalBudget"+
									" totalCost"+
									" unitCode"+
									" unitName"+
									" updatedAt"+
									" validCoverage"+
									" workforce"+
											" }"+
					                	"}\""+
									 "}";
							
							offset=offset+numPaginacion;
							jsonResponse=obtenerJsonTuten.getJson(StrToken, StrJSON,country);  
							if(!jsonResponse.equalsIgnoreCase("")) {
								//Parsear para obtener Arreglo json de Entidad
							    try {
							    	JSONParser parser = new JSONParser();
							        JSONObject json=(JSONObject) parser.parse(jsonResponse);
							        jsonResponse = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
							        parser=null;
						            json=null;
						            parser = new JSONParser();
						            json = (JSONObject) parser.parse(jsonResponse);
						            jsonResponse = (JsonUtility.validateJsonData(json,apiConfiguration.getEntidad()))?json.get(apiConfiguration.getEntidad()).toString():"";
							    }catch(Exception e){
							    	iError=1;
							    	System.out.println("CostConcepts.ProcesoEntidadCostConcepts = Error al parsear JsonResponse de Entidad CostConcepts "+country);
							    	e.printStackTrace();
							    	}
								//Se carga jsonResponse en Arreglo Json
							    try {
							    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
						            	JSONArray jsonarray= new JSONArray(jsonResponse);  
						            	CloudTableClient tableClient = null;
						                tableClient = TableClientProvider.getTableClientReference(country);
						                //Se genera la variable tabla, con el nombre de la tabla en AZURE
						                CloudTable table = tableClient.getTableReference("CostConcepts");
						            	for (int a = 0; a<jsonarray.length(); a++) {
						            		JSONParser parser=null;
						            		JSONObject jsonCostConcepts=null;
						            		String strActive="";
						            		String strActualStatus="";
						            		String strBaseCostProvider="";
						            		String strBaseSale="";
						            		String strBudgetCCAmount="";
						            		String strBudgetCCBaseCost="";
						            		String strBudgetCCExcessBaseCost="";
						            		String strBudgetCCRateType="";
						            		String strBudgetCCTop="";
						            		String strBudgetCCTotal="";
						            		String strBudgetMargin="";
						            		String strBudgetTotalCostProvider="";
						            		String strBusinessUnit="";
						            		String strCalculateIva="";
						            		String strComments="";
						            		String strCostConceptBaseCost="";
						            		String strCostConceptCost="";
						            		String strCostConceptId="";
						            		String strCostConceptName="";
						            		String strCostConceptRateType="";
						            		String strCostConceptSale="";
						            		String strCostConceptTop="";
						            		String strCostConceptTotalCostProvider="";
						            		String strCreatedAt="";
						            		String strCreationDate="";
						            		String strExcessBaseCost="";
						            		String strExcessBaseSale="";
						            		String strIdBudget="";
						            		String strIdBudgetCc="";
						            		String strIntegrationMap="";
						            		String strIsCovered="";
						            		String strIsValidated="";
						            		String strIva="";
						            		String strIvaCost="";
						            		String strMargin="";
						            		String strMaterialsCost="";
						            		String strProviderPayment="";
						            		String strQuestion="";
						            		String strRuntimeMin="";
						            		String strSaleDiscount="";
						            		String strServiceDuration="";
						            		String strState="";
						            		String strStatusName="";
						            		String strSubtotal="";
						            		String strSubtotalCost="";
						            		String strTotalBudget="";
						            		String strTotalCost="";
						            		String strUnitCode="";
						            		String strUnitName="";
						            		String strUpdatedAt="";
						            		String strValidCoverage="";
						            		String strWorkforce="";

						            		
						            		parser = new JSONParser();
						            		jsonCostConcepts = (JSONObject)parser.parse(jsonarray.get(a).toString());
						            		strActive= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"active" );
						            		strActualStatus= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"actualStatus" );
						            		strBaseCostProvider= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"baseCostProvider" );
						            		strBaseSale= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"baseSale" );
						            		strBudgetCCAmount= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"budgetCCAmount" );
						            		strBudgetCCBaseCost= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"budgetCCBaseCost" );
						            		strBudgetCCExcessBaseCost= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"budgetCCExcessBaseCost" );
						            		strBudgetCCRateType= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"budgetCCRateType" );
						            		strBudgetCCTop= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"budgetCCTop" );
						            		strBudgetCCTotal= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"budgetCCTotal" );
						            		strBudgetMargin= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"budgetMargin" );
						            		strBudgetTotalCostProvider= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"budgetTotalCostProvider" );
						            		strBusinessUnit= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"businessUnit" );
						            		strCalculateIva= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"calculateIva" );
						            		strComments= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"comments" );
						            		strCostConceptBaseCost= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"costConceptBaseCost" );
						            		strCostConceptCost= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"costConceptCost" );
						            		strCostConceptId= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"costConceptId" );
						            		strCostConceptName= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"costConceptName" );
						            		strCostConceptRateType= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"costConceptRateType" );
						            		strCostConceptSale= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"costConceptSale" );
						            		strCostConceptTop= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"costConceptTop" );
						            		strCostConceptTotalCostProvider= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"costConceptTotalCostProvider" );
						            		strCreatedAt= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"createdAt" );
						            		strCreationDate= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"creationDate" );
						            		strExcessBaseCost= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"excessBaseCost" );
						            		strExcessBaseSale= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"excessBaseSale" );
						            		strIdBudget= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"idBudget" );
						            		strIdBudgetCc= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"idBudgetCc" );
						            		strIntegrationMap= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"integrationMap" );
						            		strIsCovered= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"isCovered" );
						            		strIsValidated= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"isValidated" );
						            		strIva= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"iva" );
						            		strIvaCost= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"ivaCost" );
						            		strMargin= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"margin" );
						            		strMaterialsCost= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"materialsCost" );
						            		strProviderPayment= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"providerPayment" );
						            		strQuestion= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"question" );
						            		strRuntimeMin= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"runtimeMin" );
						            		strSaleDiscount= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"saleDiscount" );
						            		strServiceDuration= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"serviceDuration" );
						            		strState= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"state" );
						            		strStatusName= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"statusName" );
						            		strSubtotal= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"subtotal" );
						            		strSubtotalCost= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"subtotalCost" );
						            		strTotalBudget= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"totalBudget" );
						            		strTotalCost= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"totalCost" );
						            		strUnitCode= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"unitCode" );
						            		strUnitName= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"unitName" );
						            		strUpdatedAt= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"updatedAt" );
						            		strValidCoverage= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"validCoverage" );
						            		strWorkforce= JsonUtility.JsonValidaExisteLlave(jsonCostConcepts,"workforce" );

						            		
						            		
						            		
						            		CostConceptsEntity costConceptsEntity = new CostConceptsEntity(strIdBudget+"_"+strIdBudgetCc+"_"+strCostConceptId, strBusinessUnit); //PartitionKey & RowKey
						            		costConceptsEntity.setEtag(costConceptsEntity.getEtag());
						            		costConceptsEntity.setActive(strActive != null ? Boolean.parseBoolean(strActive): null);
						            		costConceptsEntity.setActualStatus(strActualStatus != null ? Integer.parseInt(strActualStatus): null);
						            		costConceptsEntity.setBaseCostProvider(strBaseCostProvider != null ? Integer.parseInt(strBaseCostProvider): null);
						            		costConceptsEntity.setBaseSale(strBaseSale != null ? Integer.parseInt(strBaseSale): null);
						            		costConceptsEntity.setBudgetCCAmount(strBudgetCCAmount != null ? Integer.parseInt(strBudgetCCAmount): null);
						            		costConceptsEntity.setBudgetCCBaseCost(strBudgetCCBaseCost != null ? Integer.parseInt(strBudgetCCBaseCost): null);
						            		costConceptsEntity.setBudgetCCExcessBaseCost(strBudgetCCExcessBaseCost != null ? Integer.parseInt(strBudgetCCExcessBaseCost):null);
						            		costConceptsEntity.setBudgetCCRateType(strBudgetCCRateType != null ? Integer.parseInt(strBudgetCCRateType): null);
						            		costConceptsEntity.setBudgetCCTop(strBudgetCCTop != null ? Integer.parseInt(strBudgetCCTop): null);
						            		costConceptsEntity.setBudgetCCTotal(strBudgetCCTotal != null ? Integer.parseInt(strBudgetCCTotal): null);
						            		costConceptsEntity.setBudgetMargin(strBudgetMargin != null ? strBudgetMargin: "null");
						            		costConceptsEntity.setBudgetTotalCostProvider(strBudgetTotalCostProvider != null ? Integer.parseInt(strBudgetTotalCostProvider): null);
						            		costConceptsEntity.setBusinessUnit(strBusinessUnit != null ? UUID.fromString(strBusinessUnit): null);
						            		costConceptsEntity.setCalculateIva(strCalculateIva != null ? Boolean.parseBoolean(strCalculateIva):null);
						            		costConceptsEntity.setComments(strComments != null ? strComments: "null");
						            		costConceptsEntity.setCostConceptBaseCost(strCostConceptBaseCost != null ? strCostConceptBaseCost: "null");
						            		costConceptsEntity.setCostConceptCost(strCostConceptCost != null ? Integer.parseInt(strCostConceptCost):null);
						            		costConceptsEntity.setCostConceptId(strCostConceptId != null ? Integer.parseInt(strCostConceptId):null);
						            		costConceptsEntity.setCostConceptName(strCostConceptName != null ? strCostConceptName: "null");
						            		costConceptsEntity.setCostConceptRateType(strCostConceptRateType != null ? Integer.parseInt(strCostConceptRateType):null);
						            		costConceptsEntity.setCostConceptSale(strCostConceptSale != null ? Integer.parseInt(strCostConceptSale): null);
						            		costConceptsEntity.setCostConceptTop(strCostConceptTop != null ? Integer.parseInt(strCostConceptTop): null);
						            		costConceptsEntity.setCostConceptTotalCostProvider(strCostConceptTotalCostProvider != null ? Integer.parseInt(strCostConceptTotalCostProvider): null);
						            		costConceptsEntity.setCreatedAt(strCreatedAt != null ? strCreatedAt: null);
						            		costConceptsEntity.setCreationDate(strCreationDate != null ? strCreationDate: null);
						            		costConceptsEntity.setExcessBaseCost(strExcessBaseCost != null ? Integer.parseInt(strExcessBaseCost): null);
						            		costConceptsEntity.setExcessBaseSale(strExcessBaseSale != null ? Integer.parseInt(strExcessBaseSale): null);
						            		costConceptsEntity.setIdBudget(strIdBudget != null ? Integer.parseInt(strIdBudget): null);
						            		costConceptsEntity.setIdBudgetCc(strIdBudgetCc != null ? Integer.parseInt(strIdBudgetCc): null);
						            		costConceptsEntity.setIntegrationMap(strIntegrationMap != null ? Boolean.parseBoolean(strIntegrationMap):null);
						            		costConceptsEntity.setIsCovered(strIsCovered != null ? Boolean.parseBoolean(strIsCovered): null );
						            		costConceptsEntity.setIsValidated(strIsValidated != null ? Boolean.parseBoolean(strIsValidated): null);
						            		costConceptsEntity.setIva(strIva != null ? Double.parseDouble(strIva): null);
						            		costConceptsEntity.setIvaCost(strIvaCost != null ? strIvaCost: null);
						            		costConceptsEntity.setMargin(strMargin != null ? strMargin: "null");
						            		costConceptsEntity.setMaterialsCost(strMaterialsCost != null ? Integer.parseInt(strMaterialsCost): null);
						            		costConceptsEntity.setProviderPayment(strProviderPayment != null ? Boolean.parseBoolean(strProviderPayment): null);
						            		costConceptsEntity.setQuestion(strQuestion != null ? Boolean.parseBoolean(strQuestion): null);
						            		costConceptsEntity.setRuntimeMin(strRuntimeMin != null ? Integer.parseInt(strRuntimeMin): null);
						            		costConceptsEntity.setSaleDiscount(strSaleDiscount != null ? Integer.parseInt(strSaleDiscount): null);
						            		costConceptsEntity.setServiceDuration(strServiceDuration != null ? Integer.parseInt(strServiceDuration): null);
						            		costConceptsEntity.setState(strState != null ? strState: "null");
						            		costConceptsEntity.setStatusName(strStatusName != null ? strStatusName: "null");
						            		costConceptsEntity.setSubtotal(strSubtotal != null ? Integer.parseInt(strSubtotal)  : null);
						            		costConceptsEntity.setSubtotalCost(strSubtotalCost != null ? Integer.parseInt(strSubtotalCost):null);
						            		costConceptsEntity.setTotalBudget(strTotalBudget != null ? Integer.parseInt(strTotalBudget): null);
						            		costConceptsEntity.setTotalCost(strTotalCost != null ? Integer.parseInt(strTotalCost): null);
						            		costConceptsEntity.setUnitCode(strUnitCode != null ? strUnitCode: "null");
						            		costConceptsEntity.setUnitName(strUnitName != null ? strUnitName: "null");
						            		costConceptsEntity.setUpdatedAt(strUpdatedAt != null ? strUpdatedAt: "null");
						            		costConceptsEntity.setValidCoverage(strValidCoverage != null ? Boolean.parseBoolean(strValidCoverage): null);
						            		costConceptsEntity.setWorkforce(strWorkforce != null ? Integer.parseInt(strWorkforce): null);
						                    table.execute(TableOperation.insertOrReplace(costConceptsEntity));
						            		}
						            }else {
						            	iError=1;
						            	System.out.println("CostConcepts.ProcesoEntidadCostConcepts= Respuesta No identificada "+country+"="+jsonResponse);      }
							    }catch(Exception e) {
							    	iError=1;
							    	System.out.println("CostConcepts.ProcesoEntidadCostConcepts= Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse);
							    	e.printStackTrace();
							    }	
							}else {
								iError=1;
						    	System.out.println("CostConcepts.ProcesoEntidadCostConcepts= Error en JSON QUERY GRAPHQL "+country);
							}
						}
					}else {
						iError=1;
						System.out.println("CostConcepts.ProcesoEntidadCostConcepts = Error en JSON QUERY COUNT "+country);
					}
				}catch(Exception e){
					iError=1;
					System.out.println("CostConcepts.ProcesoEntidadCostConcepts = Error ejecutar paginación "+country);
					e.printStackTrace();
				}
				//Fin de proceso de Entidad
				try { 	
					if(iError==0) {
						ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
						System.out.println(instant.toString()+" Actualizacion de Fecha completada "+country+"!!!");
						}
					}
				catch(Exception e) {
					iError=1;
					System.out.println("CostConcepts.ProcesoEntidadCostConcepts = Error al actualizar Fecha "+country);
					e.printStackTrace();
				}
				if(iError==0) {
					System.out.println(instant.toString()+" Proceso de Entidad CostConcepts Finalizada "+country+"!!!!");}
			}else {System.out.println("CostConcepts.ProcesoEntidadCostConcepts= Problema en obtener el Token "+country);	}
		
	
	}
}
