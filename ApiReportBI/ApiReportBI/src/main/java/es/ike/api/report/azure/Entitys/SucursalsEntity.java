package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;
public class SucursalsEntity extends TableServiceEntity{

	// --------------------------------------------------------------------
			public SucursalsEntity(String id, String businessUnit) {
				this.partitionKey = id;
				this.rowKey = businessUnit;
			}

			// --------------------------------------------------------------------
			public SucursalsEntity() {

			}

			// --------------------------------------------------------------------
			public UUID businessUnit;
			public Integer id;
			public String name;
			public String address;
			public String canalPhone;
			public String contactEmail;
			public String contactLastName;
			public String contactName;
			public String contactPhone;
			public String externalId;
			public Integer providerId;
			public String upadtedAT;
			public String createdAt;
			
			
			public UUID getBusinessUnit() {
				return businessUnit;
			}

			public void setBusinessUnit(UUID businessUnit) {
				this.businessUnit = businessUnit;
			}

			public Integer getId() {
				return id;
			}

			public void setId(Integer id) {
				this.id = id;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			public String getAddress() {
				return address;
			}

			public void setAddress(String address) {
				this.address = address;
			}

			public String getCanalPhone() {
				return canalPhone;
			}

			public void setCanalPhone(String canalPhone) {
				this.canalPhone = canalPhone;
			}

			public String getContactEmail() {
				return contactEmail;
			}

			public void setContactEmail(String contactEmail) {
				this.contactEmail = contactEmail;
			}

			public String getContactLastName() {
				return contactLastName;
			}

			public void setContactLastName(String contactLastName) {
				this.contactLastName = contactLastName;
			}

			public String getContactName() {
				return contactName;
			}

			public void setContactName(String contactName) {
				this.contactName = contactName;
			}

			public String getContactPhone() {
				return contactPhone;
			}

			public void setContactPhone(String contactPhone) {
				this.contactPhone = contactPhone;
			}

			public String getExternalId() {
				return externalId;
			}

			public void setExternalId(String externalId) {
				this.externalId = externalId;
			}

			public Integer getProviderId() {
				return providerId;
			}

			public void setProviderId(Integer providerId) {
				this.providerId = providerId;
			}

			public String getUpadtedAT() {
				return upadtedAT;
			}

			public void setUpadtedAT(String upadtedAT) {
				this.upadtedAT = upadtedAT;
			}

			public String getCreatedAt() {
				return createdAt;
			}

			public void setCreatedAt(String createdAt) {
				this.createdAt = createdAt;
			}
			
			public List<String> getColumns(){
				List<String> list = new ArrayList<String>();
				list.add("id");
				list.add("businessUnit");
				list.add("name");
				list.add("address");
				list.add("canalPhone");
				list.add("contactEmail");
				list.add("contactLastName");
				list.add("contactName");
				list.add("contactPhone");
				list.add("externalId");
				list.add("providerId");
				list.add("createdAt");
				list.add("upadtedAT");
				return list;
			}

}
