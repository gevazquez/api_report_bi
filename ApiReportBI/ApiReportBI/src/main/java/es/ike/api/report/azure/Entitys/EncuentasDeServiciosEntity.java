package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class EncuentasDeServiciosEntity extends TableServiceEntity {
	public EncuentasDeServiciosEntity (String id, String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;
	}
//----------------------------------------------------------------------------------
	public EncuentasDeServiciosEntity() {
		
	}
	
//-----------------------------------------------------------------------------------
	
	public Integer pollQuestionId; 
	public Integer pollResponseId;
	public String createdAt;
	public String updatedAt;
	public Integer serviceId;
	public Integer questionId;
	public String ask;
	public String answer;
	public Integer idTemplate;
    public UUID businessUnit;
    public Integer professionalId;
    public String qualifyingSubjectName;
    public String type;
    
	public Integer getPollQuestionId() {
		return pollQuestionId;
	}
	public void setPollQuestionId(Integer pollQuestionId) {
		this.pollQuestionId = pollQuestionId;
	}
	public Integer getPollResponseId() {
		return pollResponseId;
	}
	public void setPollResponseId(Integer pollResponseId) {
		this.pollResponseId = pollResponseId;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Integer getServiceId() {
		return serviceId;
	}
	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public String getAsk() {
		return ask;
	}
	public void setAsk(String ask) {
		this.ask = ask;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public Integer getIdTemplate() {
		return idTemplate;
	}
	public void setIdTemplate(Integer idTemplate) {
		this.idTemplate = idTemplate;
	}
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public Integer getProfessionalId() {
		return professionalId;
	}
	public void setProfessionalId(Integer professionalId) {
		this.professionalId = professionalId;
	}
	public String getQualifyingSubjectName() {
		return qualifyingSubjectName;
	}
	public void setQualifyingSubjectName(String qualifyingSubjectName) {
		this.qualifyingSubjectName = qualifyingSubjectName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
    
    public List<String> getColumns(){
    	List<String> list = new ArrayList<>();
    	list.add("pollQuestionId");
    	list.add("pollResponseId");
    	list.add("createdAt");
    	list.add("updatedAt");
    	list.add("serviceId");
    	list.add("questionId");
    	list.add("ask");
    	list.add("answer");
    	list.add("idTemplate");
    	list.add("businessUnit");
    	list.add("professionalId");
    	list.add("qualifyingSubjectName");
    	list.add("type");
    	return list; 
    }
    
}
