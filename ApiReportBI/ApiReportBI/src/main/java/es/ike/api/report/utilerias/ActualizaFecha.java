package es.ike.api.report.utilerias;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.Entitys.ApiConfiguration;

public class ActualizaFecha {
	public static String actualizaUltimaFecha(ApiConfiguration apiConfiguration,Instant instant,String country) {
		int aplicaActualizacionCompleta=0;
		int ValidaGuarda=0;
		Date date=null;
		try {
			date=Date.from(instant);
			aplicaActualizacionCompleta=apiConfiguration.getAplicaActualizacionCompleta();
			if(aplicaActualizacionCompleta==0) {
				apiConfiguration.setAplicaActualizacionCompleta(1);			}
			apiConfiguration.setUltimaFechaActualizacion(date);
			//Guardar en AZURE ApiConfiguration
			ValidaGuarda=GetAPIConfigurationTable.SaveApiConfiguration(apiConfiguration,country);
		}catch(Exception e) {
			System.out.println("No se actualizo campo UltimaFechaActualizacion "+country);
		}
		return "Se actualizo campo UltimaFechaActualizacion azure "+country;
	}
}
