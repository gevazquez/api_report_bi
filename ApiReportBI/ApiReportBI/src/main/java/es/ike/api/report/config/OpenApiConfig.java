package es.ike.api.report.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;

@Configuration
public class OpenApiConfig {
	@Bean
	public OpenAPI customOpenAPI() {
		return new OpenAPI().
					info(new Info()
							.title("API REPORT MULTIPAIS")
							.version("2.0.0")
							.description(""))
	                .components(new Components()
	                		.addSecuritySchemes("Token", new SecurityScheme()
	                        .type(SecurityScheme.Type.APIKEY)
	                        .in(SecurityScheme.In.HEADER)
	                        .name("Authorization")))
	                .addSecurityItem(new SecurityRequirement()
	                		.addList("Api Report Multipais Security"));
	}

}
