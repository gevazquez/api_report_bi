package es.ike.api.report;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;
import es.ike.api.report.entidades.UnidadDeNegocio;
import es.ike.api.report.entidades.Usuarios;
import es.ike.api.report.entidades.Casos;
import es.ike.api.report.entidades.Clientes;
import es.ike.api.report.entidades.DefinicionDeServicios;
import es.ike.api.report.entidades.EncuestasDeServicios;
import es.ike.api.report.entidades.Materias;
import es.ike.api.report.entidades.CategoriasDeServicios;
import es.ike.api.report.entidades.Checklists;
import es.ike.api.report.entidades.ChecklistItems;
import es.ike.api.report.entidades.CostConcepts;
import es.ike.api.report.entidades.Profesionales;
import es.ike.api.report.entidades.Proveedores;
import es.ike.api.report.entidades.Paises;
import es.ike.api.report.entidades.Ciudades;
import es.ike.api.report.entidades.Comunas;
import es.ike.api.report.entidades.Servicios;
import es.ike.api.report.entidades.Suscripciones;
import es.ike.api.report.entidades.ConsumosDeSuscripciones;
import es.ike.api.report.entidades.Evaluaciones;
import es.ike.api.report.entidades.Reclamos;
import es.ike.api.report.entidades.OfertasAProfesionales;
import es.ike.api.report.entidades.PresupuestosDeServicios;
import es.ike.api.report.entidades.PagoDelProveedor;
import es.ike.api.report.entidades.RespuestasWizard;
import es.ike.api.report.entidades.AgendaDeProfesionales;
import es.ike.api.report.entidades.Wizards;
import es.ike.api.report.entidades.RecargosDeProfesionales;
import es.ike.api.report.entidades.EstatusDePresupuesto;
import es.ike.api.report.entidades.VariablesWizards;
import es.ike.api.report.entidades.SkusDePresupuestos;
import es.ike.api.report.entidades.Planes;
import es.ike.api.report.entidades.Siniestralidad;
import es.ike.api.report.entidades.Topes;
import es.ike.api.report.entidades.Budgets;
import es.ike.api.report.entidades.NombreDeComunaDeSku;
import es.ike.api.report.entidades.PagosDeExcedentesParaServicios;
import es.ike.api.report.entidades.ServiciosDeProfesionales;
import es.ike.api.report.entidades.AgrupadorDeServicios;
import es.ike.api.report.entidades.Coberturas;
import es.ike.api.report.entidades.Match;
import es.ike.api.report.entidades.ServiciosAsociadosAlReclamo;
import es.ike.api.report.entidades.HistorialDeServicios;
import es.ike.api.report.entidades.ServicesOrder;
import es.ike.api.report.entidades.Solicitantes;
import es.ike.api.report.entidades.Sucursales;
import es.ike.api.report.tuten.ObtenerOauthTL;


@RestController
@Component
public class TimerAplication {

	String tokenMX = "";
	String tokenAR = "";
	String tokenCO = "";
	String tokenCOH = "";

	
	@Scheduled(cron = "0 15 0 ? * MON")
	// @Scheduled(cron = "0 0/1 * * * ? ")
	public void UnidadNegocio() throws Exception {

		// Ejecuta entidad de Mexico
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		UnidadDeNegocio.ProcesoEntidadUnidadNegocio(tokenMX, "MX");
		// Ejecuta entidad de ColombiaHasura
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		UnidadDeNegocio.ProcesoEntidadUnidadNegocio(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 15 1 ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Usuarios() throws Exception {

		// Ejecuta entidad de Mexico
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Usuarios.ProcesoEntidadUsuarios(tokenMX, "MX");
		// Ejecuta entidad de ColombiaHasura
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Usuarios.ProcesoEntidadUsuarios(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 0 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Clientes() throws Exception {

		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Clientes.ProcesoEntidadClientes(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Clientes.ProcesoEntidadClientes(tokenCOH, "COH");

	}

	@Scheduled(cron = "0 50 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Materias() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Materias.ProcesoEntidadMaterias(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Materias.ProcesoEntidadMaterias(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 30 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Casos() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Casos.ProcesoEntidadCasos(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Casos.ProcesoEntidadCasos(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 0 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void CategoriaDeServicios() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		CategoriasDeServicios.ProcesoEntidadCategoriaDeServicios(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		CategoriasDeServicios.ProcesoEntidadCategoriaDeServicios(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 0 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void DefinicionDeServicios() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		DefinicionDeServicios.ProcesoEntidadDefinicionDeServicios(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 15 3/12 ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Profesionales() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Profesionales.ProcesoEntidadProfesionales(tokenMX, "MX");
//		tokenCO = ObtenerOauthTL.OAuthAPI_Report("CO");
//		Profesionales.ProcesoEntidadProfesionales(tokenCO,"CO");
//		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
//		Profesionales.ProcesoEntidadProfesionales(tokenCOH,"COH");

	}

	@Scheduled(cron = "0 15 4/12 ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Proveedores() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Proveedores.ProcesoEntidadProveedores(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Proveedores.ProcesoEntidadProveedores(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 15 0 ? * TUE")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Paises() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Paises.ProcesoEntidadPaises(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Paises.ProcesoEntidadPaises(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 15 0 ? * WED")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Ciudades() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Ciudades.ProcesoEntidadCiudades(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Ciudades.ProcesoEntidadCiudades(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 15 0 ? * THU")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Comunas() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Comunas.ProcesoEntidadComunas(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Comunas.ProcesoEntidadComunas(tokenCOH, "COH");
	}

	//@Scheduled(cron = "0 0 * ? * *")
	 @Scheduled(cron = "0 0/1 * * * ?")
	public void Servicios() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Servicios.ProcesoEntidadServicios(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Servicios.ProcesoEntidadServicios(tokenCOH, "COH");
	}
    
	@Scheduled(cron = "0 0 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Suscripciones() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Suscripciones.ProcesoEntidadSuscripciones(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Suscripciones.ProcesoEntidadSuscripciones(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 0 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void ConsumosDeSuscripciones() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		ConsumosDeSuscripciones.ProcesoEntidadConsumosDeSuscripciones(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		ConsumosDeSuscripciones.ProcesoEntidadConsumosDeSuscripciones(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 0 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void EncuestasDeServicios() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		EncuestasDeServicios.ProcesoEntidadEncuestasDeServicios(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		EncuestasDeServicios.ProcesoEntidadEncuestasDeServicios(tokenCOH, "COH");
	}

	

	@Scheduled(cron = "0 0 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Evaluaciones() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Evaluaciones.ProcesoEntidadEvaluaciones(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Evaluaciones.ProcesoEntidadEvaluaciones(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 0 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Reclamos() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Reclamos.ProcesoEntidadReclamos(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Reclamos.ProcesoEntidadReclamos(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 0 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void OfertasAProfecionales() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		OfertasAProfesionales.ProcesoEntidadOfertasAProfesionales(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 0 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void PresupuestosDeServicios() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		PresupuestosDeServicios.ProcesoEntidadPresupuestosDeServicios(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		PresupuestosDeServicios.ProcesoEntidadPresupuestosDeServicios(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 0 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void PagoAlProveedor() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		PagoDelProveedor.ProcesoEntidadPagoDelProveedor(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		PagoDelProveedor.ProcesoEntidadPagoDelProveedor(tokenCOH, "COH");
	}

	@Scheduled(cron = "0 0 * ? * *")
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void RespuestasWizard() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		RespuestasWizard.ProcesoEntidadRespuestasWizard(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		RespuestasWizard.ProcesoEntidadRespuestasWizard(tokenCOH, "COH");

	} 
	
	 @Scheduled(cron = "0 0 * ? * *") 
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void Checklists() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		Checklists.ProcesoEntidadChecklists(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		Checklists.ProcesoEntidadChecklists(tokenCOH, "COH");

	}

	@Scheduled(cron = "0 0 * ? * *") 
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void CheckListsItems() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		ChecklistItems.ProcesoEntidadCheckListsItems(tokenMX, "MX");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		ChecklistItems.ProcesoEntidadCheckListsItems(tokenCOH, "COH");

	}

	/*@Scheduled(cron = "0 0 * ? * *")-- revisar cuandos e puede actualizar esta entidad
	// @Scheduled(cron = "0 0/1 * * * ?")
	public void CostConcepts() throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		CostConcepts.ProcesoEntidadCostConcepts(tokenMX, "MX");
		tokenAR = ObtenerOauthTL.OAuthAPI_Report("AR");
		CostConcepts.ProcesoEntidadCostConcepts(tokenAR, "AR");
		tokenCO = ObtenerOauthTL.OAuthAPI_Report("CO");
		CostConcepts.ProcesoEntidadCostConcepts(tokenCO, "CO");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		CostConcepts.ProcesoEntidadCostConcepts(tokenCOH, "COH");

	}*/
	

	/*
	//@Scheduled(cron = "0 0 * ? * *")-- revisar cuandos e puede actualizar esta entidad
		//@Scheduled(cron = "0 0/1 * * * ?")
	public void AgendaDeProfesionales()  throws Exception {
		tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
		AgendaDeProfesionales.ProcesoEntidadAgendaDeProfesionales(tokenMX, "MX");
		tokenAR = ObtenerOauthTL.OAuthAPI_Report("AR");
		AgendaDeProfesionales.ProcesoEntidadAgendaDeProfesionales(tokenAR, "AR");
		tokenCO = ObtenerOauthTL.OAuthAPI_Report("CO");
		AgendaDeProfesionales.ProcesoEntidadAgendaDeProfesionales(tokenCO, "CO");
		tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
		AgendaDeProfesionales.ProcesoEntidadAgendaDeProfesionales(tokenCOH, "COH");
		
	}*/
	  
		@Scheduled(cron = "0 0 17 ? * *")
		//@Scheduled(cron = "0 0/1 * * * ?")
		public void RecargosDeProfesionales()  throws Exception {
			tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
			RecargosDeProfesionales.ProcesoEntidadRecargosDeProfesionales(tokenMX, "MX");
			tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
			RecargosDeProfesionales.ProcesoEntidadRecargosDeProfesionales(tokenCOH, "COH");
	
		}
		
		//@Scheduled(cron = "0 0 * ? * *")-- revisar cuandos e puede actualizar esta entidad
		@Scheduled(cron = "0 0/1 * * * ?")
		public void Match() throws Exception{
			tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
			Match.ProcesoEntidadMatch(tokenMX, "MX");
			tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
			Match.ProcesoEntidadMatch(tokenCOH, "COH");
		}
		
		@Scheduled(cron = "0 0 18 ? * THU")
		//@Scheduled(cron = "0 0/1 * * * ?")
		public void Wizards() throws Exception{
			tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
			Wizards.ProcesoEntidadWizards(tokenMX, "MX");
			tokenAR = ObtenerOauthTL.OAuthAPI_Report("AR");
			Wizards.ProcesoEntidadWizards(tokenAR, "AR");
			tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
			Wizards.ProcesoEntidadWizards(tokenCOH, "COH");
			
		}
	 
		@Scheduled(cron = "0 0 18 ? * * ")
		//@Scheduled(cron = "0 0/1 * * * ?")
			public void EstatusDePresupuesto() throws Exception{
				tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
				EstatusDePresupuesto.ProcesoEntidadEstatusDePresupuesto(tokenMX, "MX");
			
			}
	
			@Scheduled(cron = "0 0 18 ? * FRI")
			//@Scheduled(cron = "0 0/1 * * * ?")
				public void VariablesWizards() throws Exception{
					tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
					VariablesWizards.ProcesoEntidadVariablesWizards(tokenMX, "MX");
					tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
					VariablesWizards.ProcesoEntidadVariablesWizards(tokenCOH, "COH");
				}
	
		@Scheduled(cron = "0 0 14 ? * SUN")
		//@Scheduled(cron = "0 0/1 * * * ?")
			public void SkusDePresupuesto() throws Exception{
				tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
				SkusDePresupuestos.ProcesoEntidadSkusDePresupuestos(tokenMX, "MX");
			
			}
		
		@Scheduled(cron = "0 0 15 ? * SUN")
		//@Scheduled(cron = "0 0/1 * * * ?")
			public void Planes() throws Exception{
				tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
				Planes.ProcesoEntidadPlanes(tokenMX, "MX");
				tokenCO = ObtenerOauthTL.OAuthAPI_Report("CO");
				Planes.ProcesoEntidadPlanes(tokenCO, "CO");
				tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
				Planes.ProcesoEntidadPlanes(tokenCO, "COH");
			
			}
		
		@Scheduled(cron = "0 0 16 ? * SUN")
		//@Scheduled(cron = "0 0/1 * * * ?")
			public void Siniestralidad() throws Exception{
				tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
				Siniestralidad.ProcesoEntidadSiniestralidad(tokenCOH, "COH");
			
			}
		
		@Scheduled(cron = "0 0 17 ? * MON")
		//@Scheduled(cron = "0 0/1 * * * ?")
			public void Topes() throws Exception{
				tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
				Topes.ProcesoEntidadTopes(tokenMX, "MX");
				tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
				Topes.ProcesoEntidadTopes(tokenCO, "COH");
			
			}
		
		@Scheduled(cron = "0 0 6 ? * *")
		//@Scheduled(cron = "0 0/1 * * * ?")
			public void Budgets() throws Exception{
				tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
				Budgets.ProcesoEntidadBudgets(tokenMX, "MX");
			
			}
		@Scheduled(cron = "0 0 18 ? * MON")
		//@Scheduled(cron = "0 0/1 * * * ?")
			public void NobreDeComunaDeSku() throws Exception{
				tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
				NombreDeComunaDeSku.ProcesoEntidadNombreDeComunaDeSku(tokenMX, "MX");
			
			}
		
		@Scheduled(cron = "0 0 17 ? * MON")
		//@Scheduled(cron = "0 0/1 * * * ?")
			public void PagosDeExcedentesParaServicios() throws Exception{
				tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
				PagosDeExcedentesParaServicios.ProcesoEntidadPagosDeExcedentesParaServicios(tokenMX, "MX");
				tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
				PagosDeExcedentesParaServicios.ProcesoEntidadPagosDeExcedentesParaServicios(tokenCOH, "COH");
			
			} 
	
	@Scheduled(cron = "0 0 18 ? * TUE") 
	//@Scheduled(cron = "0 0/1 * * * ?")
		public void ServiciosDeProfesionales() throws Exception{
			tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
			ServiciosDeProfesionales.ProcesoEntidadServiciosDeProfesionales(tokenCOH, "COH");
		
		}
	
	@Scheduled(cron = "0 0 17 ? * MON")
	//@Scheduled(cron = "0 0/1 * * * ?")
		public void AgrupadorDeServicios() throws Exception{
			tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
			AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios(tokenMX, "MX");
			tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
			AgrupadorDeServicios.ProcesoEntidadAgrupadorDeServicios(tokenCOH, "COH");
		
		}
	@Scheduled(cron = "0 0 18 ? * WED")
	//@Scheduled(cron = "0 0/1 * * * ?")
		public void Coberturas() throws Exception{
			tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
			Coberturas.ProcesoEntidadCoberturas(tokenMX, "MX");
			tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
			Coberturas.ProcesoEntidadCoberturas(tokenCOH, "COH");
		
		}
	@Scheduled(cron = "0 0 18 ? * THU")
	//@Scheduled(cron = "0 0/1 * * * ?")
		public void ServiciosAsociadosAlReclamo() throws Exception{
			tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
			ServiciosAsociadosAlReclamo.ProcesoEntidadServiciosAsociadosAlReclamo(tokenMX, "MX");
		}
	
	@Scheduled(cron = "0 0 18 ? * FRI")
	//@Scheduled(cron = "0 0/1 * * * ?")
		public void HistorialDeServicios() throws Exception{
			tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
			HistorialDeServicios.ProcesoEntidadHistorialDeServicios(tokenCOH, "COH");
		}
	
	@Scheduled(cron = "0 0 21 ? * FRI")
	//@Scheduled(cron = "0 0/1 * * * ?")
		public void ServicesOrder() throws Exception{
			tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
			ServicesOrder.ProcesoEntidadServicesOrder(tokenMX, "MX");
			tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
			ServicesOrder.ProcesoEntidadServicesOrder(tokenCOH, "COH");
		}
	
	@Scheduled(cron = "0 0 01 ? * WED")
	//@Scheduled(cron = "0 0/1 * * * ?")
		public void Solicitantes() throws Exception{
			tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
			Solicitantes.ProcesoEntidadSolicitantes(tokenMX, "MX");
			tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
			Solicitantes.ProcesoEntidadSolicitantes(tokenCOH, "COH");
		}
	
	/*@Scheduled(cron = "0 0 02 ? * WED")
	//@Scheduled(cron = "0 0/1 * * * ?")
		public void Sucursales() throws Exception{
			tokenMX = ObtenerOauthTL.OAuthAPI_Report("MX");
			Sucursales.ProcesoEntidadSucursales(tokenMX, "MX");
			tokenCOH = ObtenerOauthTL.OAuthAPI_Report("COH");
			Sucursales.ProcesoEntidadSucursales(tokenCOH, "COH");
		}*/



}