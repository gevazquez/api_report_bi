package es.ike.api.report.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.microsoft.azure.storage.table.CloudTableClient;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.services.CognitoUserService;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.utilerias.ProcessFilterBu;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
@RequestMapping("/FiltroBuRange")
public class FiltroBuController {
	//String token = "";
	
	@Autowired
	private CognitoUserService cognitoUserService;
	@Operation(security = { @SecurityRequirement(name = "Token") })
	@GetMapping("/FiltroBu")
	public String FiltroBu(@RequestHeader(name = "Authorization", required = false) String authorization,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "bUnit") String bUnit,
			@RequestParam(value = "startDate") String startDate,
			@RequestParam(value = "endDate") String endDate,
			@RequestParam(value = "nameEntity") String nameEntity
			) {
		try {
			boolean bool = cognitoUserService.validateToken(authorization);
			if(bool == true) {
				ProcessFilterBu.StartFilterBu(country, bUnit, startDate, endDate, nameEntity);
				return "Ejecución Correcta de filtro FiltroBu Entidad "+nameEntity+" " + country;
			}else {
				JsonObject response = new JsonObject();
				response.addProperty("response", "Token Invalid.");
				return response.toString();
			}
		}catch(Exception e) {
			return "FiltroBuController.FiltroBu = Error al ejecutar entidad "+nameEntity+" " + country;
		}
		
	}

}
