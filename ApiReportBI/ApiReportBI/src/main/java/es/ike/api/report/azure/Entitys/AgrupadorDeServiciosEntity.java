package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class AgrupadorDeServiciosEntity extends TableServiceEntity{

	// --------------------------------------------------------------------
		public AgrupadorDeServiciosEntity (String id,String businessUnit) {
			this.partitionKey=id;
			this.rowKey=businessUnit;
			
		}
		// --------------------------------------------------------------------
		public AgrupadorDeServiciosEntity() {
		
		}
		// ------------------------------------------------------------------------------------------------------------
				public Integer id;
				public UUID businessUnit;
				public Integer serviceId;
				public Integer serviceCategoryId;
				public String serviceAggregatorName;
				public String materialType;
				public String updatedAt;
				public String createdAt;
				
				
			
				
				
				
				public Integer getId() {
					return id;
				}
				public void setId(Integer id) {
					this.id = id;
				}
				public UUID getBusinessUnit() {
					return businessUnit;
				}
				public void setBusinessUnit(UUID businessUnit) {
					this.businessUnit = businessUnit;
				}
				public Integer getServiceId() {
					return serviceId;
				}
				public void setServiceId(Integer serviceId) {
					this.serviceId = serviceId;
				}
				public Integer getServiceCategoryId() {
					return serviceCategoryId;
				}
				public void setServiceCategoryId(Integer serviceCategoryId) {
					this.serviceCategoryId = serviceCategoryId;
				}
				public String getServiceAggregatorName() {
					return serviceAggregatorName;
				}
				public void setServiceAggregatorName(String serviceAggregatorName) {
					this.serviceAggregatorName = serviceAggregatorName;
				}
				public String getMaterialType() {
					return materialType;
				}
				public void setMaterialType(String materialType) {
					this.materialType = materialType;
				}
				public String getUpdatedAt() {
					return updatedAt;
				}
				public void setUpdatedAt(String updatedAt) {
					this.updatedAt = updatedAt;
				}
				public String getCreatedAt() {
					return createdAt;
				}
				public void setCreatedAt(String createdAt) {
					this.createdAt = createdAt;
				}
				public List<String>  getColumns(){
					List<String> list = new ArrayList<String>();
					list.add("id");
					list.add("businessUnit");
					list.add("serviceId");
					list.add("serviceCategoryId");
					list.add("serviceAggregatorName");
					list.add("materialType");
					list.add("updatedAt");
					list.add("createdAt");
					return list;
				}
				
			
				

	}

