package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class MatchEntity  extends TableServiceEntity{
	// --------------------------------------------------------------------
			public MatchEntity (String id,String businessUnit) {
				this.partitionKey=id;
				this.rowKey=businessUnit;
				
			}
			// --------------------------------------------------------------------
			public MatchEntity() {
			
			}
			// ------------------------------------------------------------------------------------------------------------
					public Integer id;
					public UUID businessUnit;
					public String name;
				    public String operationalDays;
				    public Integer operatingStartTime;
				    public Integer  operatingEndTime;
				    public Integer  maxSearchTime;
				    public Integer  maxNumberOfOffers;
				    public Integer  maxTimeOfferAcceptance;
				    public Integer  timeBetweenOffers;
				    public Integer  maxNumberOfTries;
				    public String  type;
				    public String numberOfOffers;
				    public String  updatedAt;
				    public String createdAt;
				    
					
				    
					public Integer getId() {
						return id;
					}
					public void setId(Integer id) {
						this.id = id;
					}
					public UUID getBusinessUnit() {
						return businessUnit;
					}
					public void setBusinessUnit(UUID businessUnit) {
						this.businessUnit = businessUnit;
					}
					public String getName() {
						return name;
					}
					public void setName(String name) {
						this.name = name;
					}
					public String getOperationalDays() {
						return operationalDays;
					}
					public void setOperationalDays(String operationalDays) {
						this.operationalDays = operationalDays;
					}
					public Integer getOperatingStartTime() {
						return operatingStartTime;
					}
					public void setOperatingStartTime(Integer operatingStartTime) {
						this.operatingStartTime = operatingStartTime;
					}
					public Integer getOperatingEndTime() {
						return operatingEndTime;
					}
					public void setOperatingEndTime(Integer operatingEndTime) {
						this.operatingEndTime = operatingEndTime;
					}
					public Integer getMaxSearchTime() {
						return maxSearchTime;
					}
					public void setMaxSearchTime(Integer maxSearchTime) {
						this.maxSearchTime = maxSearchTime;
					}
					public Integer getMaxNumberOfOffers() {
						return maxNumberOfOffers;
					}
					public void setMaxNumberOfOffers(Integer maxNumberOfOffers) {
						this.maxNumberOfOffers = maxNumberOfOffers;
					}
					public Integer getMaxTimeOfferAcceptance() {
						return maxTimeOfferAcceptance;
					}
					public void setMaxTimeOfferAcceptance(Integer maxTimeOfferAcceptance) {
						this.maxTimeOfferAcceptance = maxTimeOfferAcceptance;
					}
					public Integer getTimeBetweenOffers() {
						return timeBetweenOffers;
					}
					public void setTimeBetweenOffers(Integer timeBetweenOffers) {
						this.timeBetweenOffers = timeBetweenOffers;
					}
					public Integer getMaxNumberOfTries() {
						return maxNumberOfTries;
					}
					public void setMaxNumberOfTries(Integer maxNumberOfTries) {
						this.maxNumberOfTries = maxNumberOfTries;
					}
					public String getType() {
						return type;
					}
					public void setType(String type) {
						this.type = type;
					}
					
					public String getNumberOfOffers() {
						return numberOfOffers;
					}
					public void setNumberOfOffers(String numberOfOffers) {
						this.numberOfOffers = numberOfOffers;
					}
					public String getUpdatedAt() {
						return updatedAt;
					}
					public void setUpdatedAt(String updatedAt) {
						this.updatedAt = updatedAt;
					}
					public String getCreatedAt() {
						return createdAt;
					}
					public void setCreatedAt(String createdAt) {
						this.createdAt = createdAt;
					}
					public List<String>  getColumns(){
						List<String> list = new ArrayList<String>();
						list.add("id");
						list.add("businessUnit");
						list.add("name");
					    list.add("operationalDays");
					    list.add("operatingStartTime");
					    list.add("operatingEndTime");
					    list.add("maxSearchTime");
					    list.add("maxNumberOfOffers");
					    list.add("maxTimeOfferAcceptance");
					    list.add("timeBetweenOffers");
					    list.add("maxNumberOfTries");
					    list.add("type");
					    list.add("numberOfOffers");
					    list.add("updatedAt");
					    list.add("createdAt");
						return list;
					}
				      

}
