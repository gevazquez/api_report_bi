package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class HistorialDeServiciosEntity extends TableServiceEntity {
	//------------------------------------------------------------------------------------------------------------
	public HistorialDeServiciosEntity(String id, String businessUuid) {
		this.partitionKey = id;
		this.rowKey = businessUuid;  
	}
//------------------------------------------------------------------------------------------------------------
	public HistorialDeServiciosEntity() {		}
//------------------------------------------------------------------------------------------------------------

	public  UUID businessUnit;
    public String createdAt;
    public Integer id;
    public String initialDateAndTime;
    public Integer serviceId;
    public String type;
    public String updatedAt;
    public Integer userId;
    public String newState;
    public String previousState;
    
	
    
    
	public UUID getBusinessUnit() {
		return businessUnit;
	}
	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getInitialDateAndTime() {
		return initialDateAndTime;
	}
	public void setInitialDateAndTime(String initialDateAndTime) {
		this.initialDateAndTime = initialDateAndTime;
	}
	public Integer getServiceId() {
		return serviceId;
	}
	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getNewState() {
		return newState;
	}
	public void setNewState(String newState) {
		this.newState = newState;
	}
	public String getPreviousState() {
		return previousState;
	}
	public void setPreviousState(String previousState) {
		this.previousState = previousState;
	}
	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("id");
		list.add("businessUnit");
		list.add("initialDateAndTime");
		list.add("serviceId");
		list.add("newState");
		list.add("previousState");
		list.add("type");
		list.add("userId");
		list.add("createdAt");
		list.add("updatedAt");
		return list;
	}
	
}
