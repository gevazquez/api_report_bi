package es.ike.api.report.utilerias.LoadJsonResponse;

import java.time.Instant;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.CasosEntity;

import es.ike.api.report.utilerias.JsonUtility;

public class LoadCases {
	public static void Cases(String jsonResponse,String country ) throws Exception {
		//Se carga jsonResponse en Arreglo Json
				Integer iError=0;
				Instant instant=Instant.now();
				//Obtiene configuracion de Entidad
				ApiConfiguration apiConfiguration =null;
				apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("5","5","cases",country);
				Logger logger = Logger.getLogger("Logger apiReport LoadCases.Cases");
				
				//Crea tabla Countries si no existe en Azure
				CloudTableClient tableClient1 = null;
				CreateTableConfiguration tableConfiguration = null;
				tableClient1 = TableClientProvider.getTableClientReference(country);
				tableConfiguration.createTable(tableClient1, "Cases",country);
				 //Se carga jsonResponse en Arreglo Json
		        try {
			    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
		            	JSONArray jsonarray= new JSONArray(jsonResponse);  
		            	CloudTableClient tableClient = null;
		                tableClient = TableClientProvider.getTableClientReference(country);
		              //Se genera la variable tabla, con el nombre de la tabla en AZURE
		                CloudTable table = tableClient.getTableReference("Cases");
		                for (int a = 0; a<jsonarray.length(); a++) {
		            		JSONParser parser=null;
		            		JSONObject json=null;
		            		String strId="";
		        		    String strBusinessUnit="";
		        			String strCaseNumber="";
		        		    String strMateriaId="";
		        			String strCustomerId="";
		        			String strStatusName="";
		        			String strTypeName="";
		        			String strCasualtyAt="";
		        			String strCreatedBy="";
		        			String strCreatedAt="";
		        			String strUpdatedAt="";
		        			String strExternalCode="";
		        			String strCorrelativeNumber="";
		        			
		        			parser = new JSONParser();
		                    json = (JSONObject)parser.parse(jsonarray.get(a).toString());
		                    strId=JsonUtility.JsonValidaExisteLlave(json, "id");
		                    strBusinessUnit=JsonUtility.JsonValidaExisteLlave(json,"businessUnit");
		                    strCaseNumber=JsonUtility.JsonValidaExisteLlave(json,"caseNumber");
		                    strMateriaId=JsonUtility.JsonValidaExisteLlave(json,"materiaId");
		                    strCustomerId=JsonUtility.JsonValidaExisteLlave(json,"customerId");
		                    strStatusName=JsonUtility.JsonValidaExisteLlave(json,"statusName");
		                    strTypeName=JsonUtility.JsonValidaExisteLlave(json,"typeName");
		                    strCasualtyAt=JsonUtility.JsonValidaExisteLlave(json,"casualtyAt");
		                    strCreatedAt=JsonUtility.JsonValidaExisteLlave(json,"createdAt");
		                    strCreatedBy=JsonUtility.JsonValidaExisteLlave(json,"createdBy");
		                    strUpdatedAt=JsonUtility.JsonValidaExisteLlave(json,"updatedAt");
		                    strExternalCode=JsonUtility.JsonValidaExisteLlave(json,"externalCode");
		                    strCorrelativeNumber=JsonUtility.JsonValidaExisteLlave(json,"correlativeNumber");

		                    CasosEntity casosEntity = new CasosEntity(strId,strBusinessUnit);
		                    casosEntity.setEtag(casosEntity.getEtag());
		                    casosEntity.setId(strId != null ? Integer.parseInt(strId):null);
		                    casosEntity.setBusinessUnit(strBusinessUnit != null ? UUID.fromString(strBusinessUnit):null);
		                    casosEntity.setCaseNumber(strCaseNumber);
		                    casosEntity.setMateriaId(strMateriaId != null ? Integer.parseInt(strMateriaId):null);
		                    casosEntity.setCustomerId(strCustomerId != null ? Integer.parseInt(strCustomerId):null);
		                    casosEntity.setStatusName(strStatusName);
		                    casosEntity.setTypeName(strTypeName);
		                    casosEntity.setCasualtyAt(strCasualtyAt  != null ?strCasualtyAt :"null");
		                    casosEntity.setCreatedBy(strCreatedBy != null ? Integer.parseInt(strCreatedBy):null);
		                    casosEntity.setCreatedAt(strCreatedAt);
		                    casosEntity.setUpdatedAt(strUpdatedAt);
		                    casosEntity.setExternalCode(strExternalCode != null ? strExternalCode: "null");
		                    casosEntity.setCorrelativeNumber(strCorrelativeNumber != null ? strCorrelativeNumber: "null");

		                    table.execute(TableOperation.insertOrReplace(casosEntity));	
		                }
		            }else {
		            	iError=1;
		            	logger.log(Level.WARNING, "LoadCases.Cases= Respuesta No identificada "+country+"="+jsonResponse);
		            }
			    }catch(Exception e) {
			    	iError=1;
			    	logger.log(Level.WARNING, "LoadCases.Cases= Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse);
			    	e.printStackTrace();
			    }
			  //Fin de proceso de Entidad
				try { 	
					if(iError==0) {
						//ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
						logger.log(Level.INFO, instant.toString()+" Se cargan registros entidad Cases con filtro ProcessFilterBu "+country+"!!!");
						
						}
					}
				catch(Exception e) {
					iError=1;
					
					logger.log(Level.WARNING, "LoadCases.Cases = Error al actualizar Fecha"+country);
					e.printStackTrace();
				}
				if(iError==0) {
					//System.out.println(instant.toString()+" Proceso Carga de Entidad con filtro Cases Finalizada "+country+"!!!!");
					logger.log(Level.INFO, instant.toString()+" Proceso Carga de Entidad con filtro Cases Finalizada "+country+"!!!!");
					}
			
	}

}
