package es.ike.api.report.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.google.gson.JsonObject;

import es.ike.api.report.dto.GetRefreshTokenDTO;
import es.ike.api.report.dto.GetTokenDTO;
import es.ike.api.report.services.AuthenticationService;

@RestController
public class TokenController {
	
	@Autowired
	private AuthenticationService authenticationService;

	@RequestMapping(value = "/getCognitoToken", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public JsonObject getCognitoToken(@RequestBody GetTokenDTO tokenDTO) {
		System.out.println("/getCognitoToken : " + tokenDTO.getUser_name());
		JsonObject response = authenticationService.getToken(tokenDTO.getUser_name(), tokenDTO.getPassword());
		return response;
	}
	
	@RequestMapping(value = "/getCognitoRefreshToken", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public JsonObject getCognitoRefreshToken(@RequestBody GetRefreshTokenDTO refreshTokenDTO) {
		System.out.println("/getCognitoRefreshToken: " + refreshTokenDTO.getUser_name());
		JsonObject response = authenticationService.getRefreshToken(refreshTokenDTO.getUser_name(), refreshTokenDTO.getRefreshToken());
		return response;
	}
}
