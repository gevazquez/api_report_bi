package es.ike.api.report.azure.Entitys;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.microsoft.azure.storage.table.TableServiceEntity;

public class ServiciosEntity extends TableServiceEntity {
	// --------------------------------------------------------------------
	public ServiciosEntity(String id, String businessUnit) {
		this.partitionKey = id;
		this.rowKey = businessUnit;

	}

	// --------------------------------------------------------------------
	public ServiciosEntity() {

	}

	// ------------------------------------------------------------------------------------------------------------{
	public Integer id;
	public UUID businessUnit;
	public String caseId;
	public Integer comunaId;
	public String comunaName;
	public String costLines;
	public Integer customerId;
	public String initialPrice;
	public Boolean isExpress;
	public Integer professionalId;
	public Integer providerId;
	public Integer serviceTypeId;
	public Boolean assignmentManual;
	public String serviceOrigin;
	public String colorSLA;
	public String incidence;
	public Boolean isBudget;
	public String serviceOriginLatitude;
	public String serviceOriginLongitude;
	public String reasonCancellation;
	public String reasonClosedService;
	public String state;
	public Integer timeSLA;
	public String desiredDate;
	public String acceptedDate;
	public String cancellationDate;
	public String realStartDate;
	public String realFinishDate;
	public String rejectedDate;
	public String rescheduleDate;
	public String scheduleDate;
	public String usernameReschedule;
	public String transferDate;
	public String createdAt;
	public String updatedAt;
	public String assignmentDate;
	public String conclusionDate;
	public String creationDate;
	public String professionalOnTheWayDate;
	public String serviceStatus;
	public String startedDate;
	public String plannedStartDateTime;
	public String plannedFinishDateTime;
	public String professionalCost;
	//Campos nuevos
	public String bookingDate;
	public String isAccepted;
	public String isRejected;
	public String usernameManualAssignment;
	public String detailsIncidence;
	public String manualAssignmentDate;
	public String detailsCancellation;
	public Boolean emergencyService;
	public Integer planId;
	//
	public Integer subscriptionPlanId;
	public Boolean servicePurchased;
	public String serviceDestinationLatitude;
	public String serviceDestinationLongitude;
	public String serviceDestination;
 
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UUID getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(UUID businessUnit) {
		this.businessUnit = businessUnit;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public Integer getComunaId() {
		return comunaId;
	}

	public void setComunaId(Integer comunaId) {
		this.comunaId = comunaId;
	}

	public String getComunaName() {
		return comunaName;
	}

	public void setComunaName(String comunaName) {
		this.comunaName = comunaName;
	}

	public String getCostLines() {
		return costLines;
	}

	public void setCostLines(String costLines) {
		this.costLines = costLines;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getInitialPrice() {
		return initialPrice;
	}

	public void setInitialPrice(String initialPrice) {
		this.initialPrice = initialPrice;
	}

	public Boolean getIsExpress() {
		return isExpress;
	}

	public void setIsExpress(Boolean isExpress) {
		this.isExpress = isExpress;
	}

	public Integer getProfessionalId() {
		return professionalId;
	}

	public void setProfessionalId(Integer professionalId) {
		this.professionalId = professionalId;
	}

	public Integer getProviderId() {
		return providerId;
	}

	public void setProviderId(Integer providerId) {
		this.providerId = providerId;
	}

	public Integer getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(Integer serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public Boolean getAssignmentManual() {
		return assignmentManual;
	}

	public void setAssignmentManual(Boolean assignmentManual) {
		this.assignmentManual = assignmentManual;
	}

	public String getServiceOrigin() {
		return serviceOrigin;
	}

	public void setServiceOrigin(String serviceOrigin) {
		this.serviceOrigin = serviceOrigin;
	}

	public String getColorSLA() {
		return colorSLA;
	}

	public void setColorSLA(String colorSLA) {
		this.colorSLA = colorSLA;
	}

	public String getIncidence() {
		return incidence;
	}

	public void setIncidence(String incidence) {
		this.incidence = incidence;
	}

	public Boolean getIsBudget() {
		return isBudget;
	}

	public void setIsBudget(Boolean isBudget) {
		this.isBudget = isBudget;
	}

	public String getServiceOriginLatitude() {
		return serviceOriginLatitude;
	}

	public void setServiceOriginLatitude(String serviceOriginLatitude) {
		this.serviceOriginLatitude = serviceOriginLatitude;
	}

	public String getServiceOriginLongitude() {
		return serviceOriginLongitude;
	}

	public void setServiceOriginLongitude(String serviceOriginLongitude) {
		this.serviceOriginLongitude = serviceOriginLongitude;
	}

	public String getReasonCancellation() {
		return reasonCancellation;
	}

	public void setReasonCancellation(String reasonCancellation) {
		this.reasonCancellation = reasonCancellation;
	}

	public String getReasonClosedService() {
		return reasonClosedService;
	}

	public void setReasonClosedService(String reasonClosedService) {
		this.reasonClosedService = reasonClosedService;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getTimeSLA() {
		return timeSLA;
	}

	public void setTimeSLA(Integer timeSLA) {
		this.timeSLA = timeSLA;
	}

	public String getDesiredDate() {
		return desiredDate;
	}

	public void setDesiredDate(String desiredDate) {
		this.desiredDate = desiredDate;
	}

	public String getAcceptedDate() {
		return acceptedDate;
	}

	public void setAcceptedDate(String acceptedDate) {
		this.acceptedDate = acceptedDate;
	}

	public String getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(String cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public String getRealStartDate() {
		return realStartDate;
	}

	public void setRealStartDate(String realStartDate) {
		this.realStartDate = realStartDate;
	}

	public String getRealFinishDate() {
		return realFinishDate;
	}

	public void setRealFinishDate(String realFinishDate) {
		this.realFinishDate = realFinishDate;
	}

	public String getRejectedDate() {
		return rejectedDate;
	}

	public void setRejectedDate(String rejectedDate) {
		this.rejectedDate = rejectedDate;
	}

	public String getRescheduleDate() {
		return rescheduleDate;
	}

	public void setRescheduleDate(String rescheduleDate) {
		this.rescheduleDate = rescheduleDate;
	}

	public String getScheduleDate() {
		return scheduleDate;
	}

	public void setScheduleDate(String scheduleDate) {
		this.scheduleDate = scheduleDate;
	}

	public String getUsernameReschedule() {
		return usernameReschedule;
	}

	public void setUsernameReschedule(String usernameReschedule) {
		this.usernameReschedule = usernameReschedule;
	}

	public String getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getAssignmentDate() {
		return assignmentDate;
	}

	public void setAssignmentDate(String assignmentDate) {
		this.assignmentDate = assignmentDate;
	}

	public String getConclusionDate() {
		return conclusionDate;
	}

	public void setConclusionDate(String conclusionDate) {
		this.conclusionDate = conclusionDate;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getProfessionalOnTheWayDate() {
		return professionalOnTheWayDate;
	}

	public void setProfessionalOnTheWayDate(String professionalOnTheWayDate) {
		this.professionalOnTheWayDate = professionalOnTheWayDate;
	}

	public String getServiceStatus() {
		return serviceStatus;
	}

	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public String getStartedDate() {
		return startedDate;
	}

	public void setStartedDate(String startedDate) {
		this.startedDate = startedDate;
	}

	public String getPlannedStartDateTime() {
		return plannedStartDateTime;
	}

	public void setPlannedStartDateTime(String plannedStartDateTime) {
		this.plannedStartDateTime = plannedStartDateTime;
	}

	public String getPlannedFinishDateTime() {
		return plannedFinishDateTime;
	}

	public void setPlannedFinishDateTime(String plannedFinishDateTime) {
		this.plannedFinishDateTime = plannedFinishDateTime;
	}

	public String getProfessionalCost() {
		return professionalCost;
	}

	public void setProfessionalCost(String professionalCost) {
		this.professionalCost = professionalCost;
	}


	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getIsAccepted() {
		return isAccepted;
	}

	public void setIsAccepted(String isAccepted) {
		this.isAccepted = isAccepted;
	}

	public String getIsRejected() {
		return isRejected;
	}

	public void setIsRejected(String isRejected) {
		this.isRejected = isRejected;
	}

	public String getUsernameManualAssignment() {
		return usernameManualAssignment;
	}

	public void setUsernameManualAssignment(String usernameManualAssignment) {
		this.usernameManualAssignment = usernameManualAssignment;
	}

	public String getDetailsIncidence() {
		return detailsIncidence;
	}

	public void setDetailsIncidence(String detailsIncidence) {
		this.detailsIncidence = detailsIncidence;
	}

	public String getManualAssignmentDate() {
		return manualAssignmentDate;
	}

	public void setManualAssignmentDate(String manualAssignmentDate) {
		this.manualAssignmentDate = manualAssignmentDate;
	}

	public String getDetailsCancellation() {
		return detailsCancellation;
	}

	public void setDetailsCancellation(String detailsCancellation) {
		this.detailsCancellation = detailsCancellation;
	}

	public Boolean getEmergencyService() {
		return emergencyService;
	}

	public void setEmergencyService(Boolean emergencyService) {
		this.emergencyService = emergencyService;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}

	public Integer getSubscriptionPlanId() {
		return subscriptionPlanId;
	}

	public void setSubscriptionPlanId(Integer subscriptionPlanId) {
		this.subscriptionPlanId = subscriptionPlanId;
	}

	public Boolean getServicePurchased() {
		return servicePurchased;
	}

	public void setServicePurchased(Boolean servicePurchased) {
		this.servicePurchased = servicePurchased;
	}

	public String getServiceDestinationLatitude() {
		return serviceDestinationLatitude;
	}

	public void setServiceDestinationLatitude(String serviceDestinationLatitude) {
		this.serviceDestinationLatitude = serviceDestinationLatitude;
	}

	public String getServiceDestinationLongitude() {
		return serviceDestinationLongitude;
	}

	public void setServiceDestinationLongitude(String serviceDestinationLongitude) {
		this.serviceDestinationLongitude = serviceDestinationLongitude;
	}

	public String getServiceDestination() {
		return serviceDestination;
	}

	public void setServiceDestination(String serviceDestination) {
		this.serviceDestination = serviceDestination;
	}

	public List<String> getColumns(){
		List<String> list = new ArrayList<>();
		list.add("acceptedDate");
		list.add("serviceOrigin");
		list.add("assignmentDate");
		list.add("assignmentManual");
		list.add("bookingDate");
		list.add("businessUnit");
		list.add("cancellationDate");
		list.add("caseId");
		//list.add("colorSLA");
		list.add("comunaId");
		//list.add("comunaName");
		list.add("conclusionDate");
		list.add("costLines");
		list.add("createdAt");
		list.add("creationDate");
		list.add("customerId");
		list.add("desiredDate");
		//list.add("detailsCancellation");
		//list.add("detailsIncidence");
		list.add("id");
		//list.add("incidence");
		list.add("initialPrice");
		//list.add("isAccepted");
		//list.add("isBudget");
		list.add("isExpress");
		//list.add("isRejected");
		//list.add("serviceOriginLatitude");
		//list.add("serviceOriginLongitude");
		list.add("manualAssignmentDate");
		list.add("plannedFinishDateTime");
		list.add("plannedStartDateTime");
		//list.add("professionalCost");
		list.add("professionalId");
		list.add("professionalOnTheWayDate");
		list.add("providerId");
		list.add("realFinishDate");
		list.add("realStartDate");
		//list.add("reasonCancellation");
		//list.add("reasonClosedService");
		list.add("rejectedDate");
		list.add("rescheduleDate");
		list.add("scheduleDate");
		//list.add("serviceStatus");
		list.add("serviceTypeId");
		list.add("startedDate");
		list.add("state");
		//list.add("timeSLA");
		list.add("transferDate");
		list.add("updatedAt");
		//list.add("usernameManualAssignment");
		//list.add("usernameReschedule");
		//list.add("emergencyService");
		list.add("planId");
		//list.add("subscriptionPlanId");
		//list.add("servicePurchased");
		//list.add("serviceDestinationLatitude");
		//list.add("serviceDestinationLongitude");
		//list.add("serviceDestination");
		return list;
	}

}
