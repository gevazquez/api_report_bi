package es.ike.api.report.entidades;

import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.azure.data.tables.models.TableServiceException;
import com.microsoft.azure.storage.table.CloudTable;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.TableOperation;

import es.ike.api.report.azure.CreateTableConfiguration;
import es.ike.api.report.azure.GetAPIConfigurationTable;
import es.ike.api.report.azure.TableClientProvider;
import es.ike.api.report.azure.Entitys.ApiConfiguration;
import es.ike.api.report.azure.Entitys.ProfesionalesEntity;
import es.ike.api.report.azure.Entitys.ProveedoresEntity;
import es.ike.api.report.tuten.ObtenerOauthTL;
import es.ike.api.report.tuten.obtenerJsonTuten;
import es.ike.api.report.utilerias.ActualizaFecha;
import es.ike.api.report.utilerias.DatosPaginacion;
import es.ike.api.report.utilerias.Fecha;
import es.ike.api.report.utilerias.JsonUtility;
import es.ike.api.report.utilerias.Paginacion;

public class Proveedores {	
	public static void ProcesoEntidadProveedores(String StrToken,String country)throws Exception,TableServiceException{
		Integer iError=0;
		//Obtiene configuracion de Entidad
		ApiConfiguration apiConfiguration =null;
		Instant instant=Instant.now();
		apiConfiguration=(ApiConfiguration) GetAPIConfigurationTable.getAPIConfigurationTable("9","9","providers",country);
		System.out.println(instant.toString()+" Obtiene Configuacion "+country+" de Entidad "+apiConfiguration.getEntidad());
		//Crea tabla Providers si no existe en Azure
		CloudTableClient tableClient1 = null;
		CreateTableConfiguration tableConfiguration = null;
		tableClient1 = TableClientProvider.getTableClientReference(country);
		tableConfiguration.createTable(tableClient1, "Providers",country);
		//Obtenemos Token de OAuth de TL
		String token="";
		token=ObtenerOauthTL.OAuthAPI_Report(country);
		System.out.println(instant.toString()+" Proveedores.ProcesoEntidadProveedores=Obtiene Token "+country+" "+token);
		//Inserta conteo total
				if(!token.equalsIgnoreCase("")) {
					String StrJSONContoTotal="{"
							+"\n"+"\"query\":\"{"+
							"providers_aggregate {"+
							"aggregate {"+
							"count"+
								"}"+
							"}"+
			                	"}\""+
							 "}";
					String jsonResponseConteoTotal="";
					String StrConteoTotal="";
					Integer numConteoTotal = 0;
					jsonResponseConteoTotal=obtenerJsonTuten.getJson(token,StrJSONContoTotal,country);
					//Inicia Parseo de conteo Total
				    try {
				    	JSONParser parser = new JSONParser();
				    	JSONObject json=(JSONObject) parser.parse(jsonResponseConteoTotal);
				    	jsonResponseConteoTotal = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
				    	parser=null;
			            json=null;
			            parser = new JSONParser();
			            json = (JSONObject) parser.parse(jsonResponseConteoTotal);
			            jsonResponseConteoTotal=(JsonUtility.validateJsonData(json,"providers_aggregate"))?json.get("providers_aggregate").toString():"";
			            parser=null;
		                json=null;
		                parser = new JSONParser();
		                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
		                jsonResponseConteoTotal=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
		                json= (JSONObject) parser.parse(jsonResponseConteoTotal);
		                StrConteoTotal=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
		                numConteoTotal = Integer.parseInt(StrConteoTotal);
		                try {
		                	apiConfiguration.setConteoTotal(numConteoTotal);
		                }catch(Exception e) {
		                	iError=1;
					    	System.out.println("Proveedores.ProcesoEntidadProveedores = Error al cargar conteo total en Azure Entidad Proveedores "+country);
		                }
				    }catch(Exception e) {
				    	iError=1;
				    	System.out.println("Proveedores.ProcesoEntidadProveedores = Error al parsear JsonResponseConteoTotal de Entidad Proveedores "+country);}
					
				}else {System.out.println("Proveedores.ProcesoEntidadProveedores= Problema en obtener el Token del conteo Total "+country);	}
		//Implementación de filtro where
		String StrWhere=Fecha.filtroFecha(apiConfiguration, "id");
		String StrWhereCount="";
		String StrWhereQuery="";
		if (!StrWhere.equalsIgnoreCase("")) {
		   	StrWhereCount="("+StrWhere+")"; }
		if (!StrWhere.equalsIgnoreCase("")) {
		   	StrWhereQuery=","+StrWhere; }
		if(!token.equalsIgnoreCase("")) {
			String StrJSONCount="{"
					+"\n"+"\"query\":\"{"+
					"providers_aggregate "+StrWhereCount+"{"+
						"aggregate {"+
							" count"+
						"}"+
					"}"+
				"}\""+
				"}";
			//Implementacion de paginación
			try {
				String jsonResponseCount="";
				Integer paginas=0;
				String StrCount="";
				Integer numCount = 0;
				String jsonResponse="";
				Integer numPaginacion=0;
				Integer offset=0;
				jsonResponseCount=obtenerJsonTuten.getJson(token,StrJSONCount,country);
				if(!jsonResponseCount.equalsIgnoreCase("")) { 
					//---------------------------------------------------------------------
					//Inicia Parseo de count
				    try {
				    	JSONParser parser = new JSONParser();
				    	JSONObject json=(JSONObject) parser.parse(jsonResponseCount);
				    	jsonResponseCount = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
				    	parser=null;
			            json=null;
			            parser = new JSONParser();
			            json = (JSONObject) parser.parse(jsonResponseCount);
			            jsonResponseCount=(JsonUtility.validateJsonData(json,"providers_aggregate"))?json.get("providers_aggregate").toString():"";
			            parser=null;
		                json=null;
		                parser = new JSONParser();
		                json= (JSONObject) parser.parse(jsonResponseCount);
		                jsonResponseCount=(JsonUtility.validateJsonData(json, "aggregate"))?json.get("aggregate").toString():"";         
		                json= (JSONObject) parser.parse(jsonResponseCount);
		                StrCount=(JsonUtility.validateJsonData(json, "count"))?json.get("count").toString():"";
		                numCount = Integer.parseInt(StrCount);
				    }catch(Exception e) {
				    	iError=1;
				    	System.out.println("Proveedores.ProcesoEntidadProveedores = Error al parsear JsonResponseCount de Entidad Proveedores "+country);}
				    //llamada al archivo properties
					Thread currentThread = Thread.currentThread();
					ClassLoader contextClassLoader = currentThread.getContextClassLoader();
					URL resource = contextClassLoader.getResource("TutenLabsconfig/TutenLabsConfig.properties"+country);
					Properties prop = new Properties();
					if (resource == null) {	
						iError=1;
						throw new Exception("Proveedores.ProcesoEntidadProveedores= No se pudo leer el archivo de configuración TutenLabsconfig/TutenLabsConfig.properties"+country);		}
					try (InputStream is = resource.openStream()) {
						prop.load(is);
					//Se obtiene el numero de paginacion del archivo properties
						numPaginacion=Integer.parseInt(prop.getProperty("intPaginacion"));
					}catch(Exception e) {
						iError=1;
						System.out.println("Proveedores.ProcesoEntidadProveedores= Error al obtener intPaginacion del archivo TutenLabsconfig/TutenLabsConfig.properties"+country);
					}
					//Se llama y ejecuta el metodo calculaPaginacion
					Paginacion paginacion = new Paginacion();
					DatosPaginacion datosPaginacion = null;
					datosPaginacion= paginacion.calculaPaginacion(numCount, numPaginacion);
					if(datosPaginacion.getResto() ==0) { 	paginas=datosPaginacion.getEnteros();;
					}else {	paginas=datosPaginacion.getEnteros()+1;			}
					for(int i =0;i<paginas;i++){
							String StrJSON="{"
									+"\n"+"\"query\":\"{"+
									apiConfiguration.getEntidad()+"(limit:"+numPaginacion+",offset:"+offset+" "+StrWhereQuery+")" +"{ "+
									" id"+
									" businessUnit"+
									" name"+
									" businessName"+ 
									" address"+
									" legalRepresentativeName"+
									" legalRepresentativeLastname"+
									" legalRepresentativePhone"+
									" createdAt"+
									" updatedAt"+
									" status"+
									" identificationNumber"+
									" identificationTypeId"+
									" punished"+
									" catalogNotUpdated"+
									" quotation"+
									" onTime"+
									" doesNotAnswer"+
									" doesNotGiveResponseTime"+
									" noCredit"+
									" serviceByAppointment"+
									" withoutSpecializedTechnician"+
									" doesNotCoverArea"+
									" onlyUnderAuthorization"+
											" }"+
					                	"}\""+
									 "}";

							offset=offset+numPaginacion;
							jsonResponse=obtenerJsonTuten.getJson(StrToken, StrJSON,country);  
							if(!jsonResponse.equalsIgnoreCase("")) {
								//Parsear para obtener Arreglo json de Entidad
							    try {
							    	JSONParser parser = new JSONParser();
							        JSONObject json=(JSONObject) parser.parse(jsonResponse);
							        jsonResponse = (JsonUtility.validateJsonData(json,"data"))?json.get("data").toString():"";
							        parser=null;
						            json=null;
						            parser = new JSONParser();
						            json = (JSONObject) parser.parse(jsonResponse);
						            jsonResponse = (JsonUtility.validateJsonData(json,apiConfiguration.getEntidad()))?json.get(apiConfiguration.getEntidad()).toString():"";
							    }catch(Exception e){
							    	iError=1;
							    	System.out.println("Proveedores.ProcesoEntidadProveedores = Error al parsear JsonResponse de Entidad Proveedores "+country);
							    	e.printStackTrace();
							    	}
								//Se carga jsonResponse en Arreglo Json
							    try {
							    	if(String.valueOf(jsonResponse.charAt(0)).equalsIgnoreCase("[")){
						            	JSONArray jsonarray= new JSONArray(jsonResponse);  
						            	CloudTableClient tableClient = null;
						                tableClient = TableClientProvider.getTableClientReference(country);
						                //Se genera la variable tabla, con el nombre de la tabla en AZURE
						                CloudTable table = tableClient.getTableReference("Providers");
						            	for (int a = 0; a<jsonarray.length(); a++) {
						            		JSONParser parser=null;
						            		JSONObject jsonProviders=null;
						            		String strId="";
						            		String strUuidBusinessUnit="";
						                    String strName="";
						                    String strBusinessName="";
						                    String strAddress="";
						                    String strLegalRepresentativeName="";
						                    String strLegalRepresentativeLastname="";
						                    String strLegalRepresentativePhone="";
						                    String strCreatedAt="";
						                    String strUpdatedAt="";
						                    String strStatus="";
						                    String strIdentificationNumber="";
						                    String strIdentificationTypeId="";
						                    String strPunished="";
						            		String strCatalogNotUpdated="";
						            		String strQuotation="";
						            		String strOnTime="";
						            		String strDoesNotAnswer="";
						            		String strDoesNotGiveResponseTime="";
						            		String strNoCredit="";
						            		String strServiceByAppointment="";
						            		String strWithoutSpecializedTechnician="";
						            		String strDoesNotCoverArea="";
						            		String strOnlyUnderAuthorization="";
						            		
						            		parser = new JSONParser();
						            		jsonProviders= (JSONObject)parser.parse(jsonarray.get(a).toString());
						            		strId = JsonUtility.JsonValidaExisteLlave(jsonProviders,"id" );
						            		strUuidBusinessUnit = JsonUtility.JsonValidaExisteLlave(jsonProviders,"businessUnit" );
						            		strName = JsonUtility.JsonValidaExisteLlave(jsonProviders,"name" );
						            		strBusinessName = JsonUtility.JsonValidaExisteLlave(jsonProviders,"businessName" );
						            		strAddress = JsonUtility.JsonValidaExisteLlave(jsonProviders,"address" );
						            		strLegalRepresentativeName = JsonUtility.JsonValidaExisteLlave(jsonProviders,"legalRepresentativeName" );
						            		strLegalRepresentativeLastname = JsonUtility.JsonValidaExisteLlave(jsonProviders,"legalRepresentativeLastname" );
						            		strLegalRepresentativePhone = JsonUtility.JsonValidaExisteLlave(jsonProviders,"legalRepresentativePhone" );
						            		strCreatedAt = JsonUtility.JsonValidaExisteLlave(jsonProviders,"createdAt" );
						            		strUpdatedAt = JsonUtility.JsonValidaExisteLlave(jsonProviders,"updatedAt" );
						            		strStatus =  JsonUtility.JsonValidaExisteLlave(jsonProviders,"status");
						            		strIdentificationNumber = JsonUtility.JsonValidaExisteLlave(jsonProviders,"identificationNumber");
						            		strIdentificationTypeId = JsonUtility.JsonValidaExisteLlave(jsonProviders,"identificationTypeId");
						            		strPunished= JsonUtility.JsonValidaExisteLlave(jsonProviders,"punished");
						            		strCatalogNotUpdated= JsonUtility.JsonValidaExisteLlave(jsonProviders,"catalogNotUpdated");
						            		strQuotation= JsonUtility.JsonValidaExisteLlave(jsonProviders,"quotation");
						            		strOnTime= JsonUtility.JsonValidaExisteLlave(jsonProviders,"onTime");
						            		strDoesNotAnswer= JsonUtility.JsonValidaExisteLlave(jsonProviders,"doesNotAnswer");
						            		strDoesNotGiveResponseTime= JsonUtility.JsonValidaExisteLlave(jsonProviders,"doesNotGiveResponseTime");
						            		strNoCredit= JsonUtility.JsonValidaExisteLlave(jsonProviders,"noCredit");
						            		strServiceByAppointment= JsonUtility.JsonValidaExisteLlave(jsonProviders,"serviceByAppointment");
						            		strWithoutSpecializedTechnician= JsonUtility.JsonValidaExisteLlave(jsonProviders,"withoutSpecializedTechnician");
						            		strDoesNotCoverArea= JsonUtility.JsonValidaExisteLlave(jsonProviders,"doesNotCoverArea");
						            		strOnlyUnderAuthorization= JsonUtility.JsonValidaExisteLlave(jsonProviders,"onlyUnderAuthorization");
						            		
						                    ProveedoresEntity proveedoresEntity = new ProveedoresEntity(strId,strUuidBusinessUnit); //PartitionKey & RowKey
						                    proveedoresEntity.setEtag(proveedoresEntity.getEtag());
						                    proveedoresEntity.setId(strId != null ? Integer.parseInt(strId) : null);
						                    proveedoresEntity.setBusinessUnit(strUuidBusinessUnit !=null ? UUID.fromString(strUuidBusinessUnit):null);
						                    proveedoresEntity.setName(strName);
						                    proveedoresEntity.setBusinessName(strBusinessName);
						                    proveedoresEntity.setAddress(strAddress);
						                    proveedoresEntity.setLegalRepresentativeName(strLegalRepresentativeName);
						                    proveedoresEntity.setLegalRepresentativeLastname(strLegalRepresentativeLastname);
						                    proveedoresEntity.setLegalRepresentativePhone(strLegalRepresentativePhone);
						                    proveedoresEntity.setCreatedAt(strCreatedAt);
						                    proveedoresEntity.setUpdatedAt(strUpdatedAt);
						                    proveedoresEntity.setStatus(strStatus !=null ? strStatus:"null");
						                    proveedoresEntity.setIdentificationNumber(strIdentificationNumber !=null ? strIdentificationNumber: "null");
						                    proveedoresEntity.setIdentificationTypeId(strIdentificationTypeId !=null ? strIdentificationTypeId: "null");
						                    proveedoresEntity.setPunished(strPunished !=null ? Boolean.valueOf(strPunished): false);
						                    proveedoresEntity.setCatalogNotUpdated(strCatalogNotUpdated !=null ? Boolean.valueOf(strCatalogNotUpdated): false);
						            		proveedoresEntity.setQuotation(strQuotation !=null ? Boolean.valueOf(strQuotation): false);
						            		proveedoresEntity.setOnTime(strOnTime !=null ? Boolean.valueOf(strOnTime): false);
						            		proveedoresEntity.setDoesNotAnswer(strDoesNotAnswer !=null ? Boolean.valueOf(strDoesNotAnswer): false);
						            		proveedoresEntity.setDoesNotGiveResponseTime(strDoesNotGiveResponseTime !=null ? Boolean.valueOf(strDoesNotGiveResponseTime): false);
						            		proveedoresEntity.setNoCredit(strNoCredit !=null ? Boolean.valueOf(strNoCredit): false);
						            		proveedoresEntity.setServiceByAppointment(strServiceByAppointment !=null ? Boolean.valueOf(strServiceByAppointment): false);
						            		proveedoresEntity.setWithoutSpecializedTechnician(strWithoutSpecializedTechnician !=null ? Boolean.valueOf(strWithoutSpecializedTechnician): false);
						            		proveedoresEntity.setDoesNotCoverArea(strDoesNotCoverArea !=null ? Boolean.valueOf(strDoesNotCoverArea): false);
						            		proveedoresEntity.setOnlyUnderAuthorization(strOnlyUnderAuthorization !=null ? Boolean.valueOf(strOnlyUnderAuthorization): false);
						                    table.execute(TableOperation.insertOrReplace(proveedoresEntity));
						            		}
						            }else {
						            	iError=1;
						            	System.out.println("Proveedores.ProcesoEntidadProveedores= Respuesta No identificada "+country+"="+jsonResponse);      }
							    }catch(Exception e) {
							    	iError=1;
							    	System.out.println("Proveedores.ProcesoProveedores= Error al procesar arreglo e insertar en AZURE "+country+"="+jsonResponse);
							    	e.printStackTrace();
							    }	
							}else {
								iError=1;
						    	System.out.println("Proveedores.ProcesoEntidadProveedores= Error en JSON QUERY GRAPHQL "+country);
							}
					}
				}else {
					iError=1;
					System.out.println("Proveedores.ProcesoEntidadProveedores = Error en JSON QUERY COUNT "+country);
				}
			}catch(Exception e){
				iError=1;
				System.out.println("Proveedores.ProcesoProveedores = Error ejecutar paginación "+country);
				e.printStackTrace();
				}
	        //Fin de proceso de Entidad
			try { 	
				if(iError==0) {
					ActualizaFecha.actualizaUltimaFecha(apiConfiguration,instant,country);
					System.out.println(instant.toString()+" Actualizacion de Fecha completada "+country+"!!!");
					}
				}
			catch(Exception e) {
				iError=1;
				System.out.println("Proveedores.ProcesoEntidadProveedores = Error al actualizar Fecha "+country);
				e.printStackTrace();
			}
			if(iError==0) {
				System.out.println(instant.toString()+" Proceso de Entidad Proveedores Finalizada "+country+"!!!!");}
		}else {System.out.println("Proveedores.ProcesoEntidadProveedores= Problema en obtener el Token "+country);	}
		
	}
	
}
