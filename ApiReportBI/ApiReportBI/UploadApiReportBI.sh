#!/usr/bin/env sh


AWS_ACCOUNT_ID=186005301796

REGION=us-east-1

VERSION=$(cat pom.xml | grep "<version>" | cut -d '>' -f 2 |cut -d '<' -f 1 | head -1)
JAR_VERSION=${VERSION}

APP=ike_api_reportbi_test

echo  $APP-$JAR_VERSION.jar
echo "REGION ${REGION}"
echo "ApiReportBI-$JAR_VERSION.jar"
# mvn clean install

cp target/ApiReportBI-$JAR_VERSION.jar Docker/app/$APP-$JAR_VERSION.jar

cd Docker/app

echo "--- BUILD DOCKER IMAGE ---"
docker build  --build-arg PROJECT_NAME=$APP  --build-arg VERSION=$JAR_VERSION   --build-arg APP_PORT=8091  -t ${APP}-latest .

echo "--- LOGIN AWS ---"

aws ecr get-login-password --region $REGION | docker login --username AWS --password-stdin 186005301796.dkr.ecr.$REGION.amazonaws.com


echo "--- CREATE TAG ---"
docker  tag $APP-latest  $AWS_ACCOUNT_ID.dkr.ecr.$REGION.amazonaws.com/${APP}
echo  tag $APP-latest  $AWS_ACCOUNT_ID.dkr.ecr.$REGION.amazonaws.com/${APP}
echo "--- PUSH IMAGEN ---"
docker push $AWS_ACCOUNT_ID.dkr.ecr.$REGION.amazonaws.com/${APP}
